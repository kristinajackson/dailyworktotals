﻿using System;
using System.Linq;  // yes, we are using it this time
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Admin
{
    public partial class UserAdmin : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        LoadAvailUsers();
                        LoadAppUsers();
                        UpdateAppUsersCount();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: DoAdmin
                        if (eventTarget == "DoAdmin")
                        {
                            DoAdmin();
                        }
                        else if (eventTarget == "DoActive")
                        {
                            DoActive();
                        }
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void MakeChanges()
        {
            // This routine adds and removes users from the users table. It builds the lists so it can then determine
            // which users need added and which users need deleted.
            List<int> AssignedToUsers = new List<int>();
            List<int> CurrentUsers = new List<int>();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            // are there any assigned users?
            if (ddlAssignedToUsers.Items.Count > 0)
            {
                // yes, add the userid's to the list
                for (int i = 0; i < ddlAssignedToUsers.Items.Count; i++)
                {
                    AssignedToUsers.Add(Convert.ToInt32(ddlAssignedToUsers.Items[i].Value));
                }
                // did we try to remove ourselves?
                if (!AssignedToUsers.Contains(MyUserID))
                {
                    // give message you cannot remove yourself
                    ShowPopUp("Removal error!", "You cannot remove yourself. Please contact the administrator.");
                    return;
                }
            }
            else
            {
                // give message you can't delete everyone
                ShowPopUp("Removal error!", "You cannot everyone!!");
                return;
            }

            int status = -1;
            DataSet ds = new DataSet();
            DWUtility dwt = new DWUtility();
            ds = dwt.GetAppUsers();

            // check if the users table is empty
            if (ds != null)
            {
                // no, its not empty
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        CurrentUsers.Add(Convert.ToInt32(ds.Tables[0].Rows[i].ItemArray[0]));
                    }
                }
            }
            else
            {
                // yes it is, so add all of these users
                for (int i = 0; i < ddlAssignedToUsers.Items.Count; i++)
                {
                    status = dwt.AddUser(Convert.ToInt32(ddlAssignedToUsers.Items[i].Value), MyUserID);
                    if (status < 1)
                    {
                        string user = ddlAssignedToUsers.Items[i].Value.ToString();
                        // display error message
                        // 0 = not in inventory DB
                        if (status == 0)
                        {
                            ShowPopUp("User not found!", "The person (" + user + ") you are trying to add could not be found. Please contact the system administrator.");
                        }
                        // -1 = insert was unsuccessful
                        else
                        {
                            ShowPopUp("Unsuccessful!", "The person (" + user + ") you are trying to add was not successful. Please contact the system administrator.");
                        }
                        return;
                    }
                }
                return;
            }
            // if we are here, then we have users from the table and we have users from the list box, so we
            // need to figure out what's what
            int[] cu = CurrentUsers.ToArray();      // copy to arrays for Linq routine
            int[] au = AssignedToUsers.ToArray();

            status = -1;
            // determine new users (nu)
            int[] nu = au.Except(cu).ToArray(); // this is a Linq function
            for (int i = 0; i < nu.Length; i++)
            {
                status = dwt.AddUser(nu[i], MyUserID);
                if (status < 1)
                {
                    // display error message
                    // 0 = not in inventory DB
                    if (status == 0)
                    {
                        ShowPopUp("User not found!", "The person (" + nu[i].ToString() + ") you are trying to add could not be found. Please contact the system administrator.");
                    }
                    // -1 = insert was unsuccessful
                    else
                    {
                        ShowPopUp("Unsuccessful!", "The person (" + nu[i].ToString() + ") you are trying to add was not successful. Please contact the system administrator.");
                    }
                    return;
                }
            }

            status = -1;
            // determine users to delete (du)
            int[] du = cu.Except(au).ToArray(); // this is a Linq function
            for (int i = 0; i < du.Length; i++)
            {
                status = dwt.DelUser(du[i]);
                if (status < 1)
                {
                    // display error message
                    // 0 = delete was unsuccessful
                    ShowPopUp("Unsuccessful!", "The person (" + nu[i].ToString() + ") could not be deleted. Please contact the system administrator.");
                    return;
                }
            }
        }
        protected void LoadAppUsers()
        {
            DWUtility dwt = new DWUtility();
            ddlAssignedToUsers.Items.Clear();       // clear in case it's already populated
            ddlAssignedToUsers.DataSource = dwt.GetAppUsers();
            ddlAssignedToUsers.DataTextField = "DDName";
            ddlAssignedToUsers.DataValueField = "UserID";

            ddlAssignedToUsers.DataBind();
            //ddlAssignedToUsers.Items.Insert(0, "-- Select User --");
        }
        protected void LoadAvailUsers()
        {
            DWUtility dwt = new DWUtility();
            ddlAvailableUsers.Items.Clear();       // clear in case it's already populated
            ddlAvailableUsers.DataSource = dwt.GetAvailUsers();
            ddlAvailableUsers.DataTextField = "Name";
            ddlAvailableUsers.DataValueField = "UserID";

            ddlAvailableUsers.DataBind();
            //ddlAvailableUsers.Items.Insert(0, "-- Select User --");
        }
        protected void UpdateAppUsersCount()
        {
            if (ddlAssignedToUsers.Items.Count > 0)
            {
                lblAssgnedQty.Text = " (" + ddlAssignedToUsers.Items.Count.ToString() + ")";
                lblAssgnedQty.Visible = true;
            }
            else
            {
                lblAssgnedQty.Visible = true;
            }
        }
        protected void ChangeAdmin_Click(object sender, EventArgs e)
        {
            int SelectIndex = ddlAssignedToUsers.SelectedIndex;

            // did we really select any?
            if (SelectIndex > -1)
            {
                string Users = "";

                // get the ones selected
                int[] x = ddlAssignedToUsers.GetSelectedIndices();
                for (int i = 0; i < x.Length; i++)
                {
                    // build the list
                    Users += ddlAssignedToUsers.Items[x[i]].Text.ToString() + "<br />";
                }
                ShowPopUpOkCancel("Change Admin?", "Are you sure you want to change the Admin status of these user(s)?<br /><br />" + Users, "DoAdmin", "");
            }

        }
        protected void ChangeActive_Click(object sender, EventArgs e)
        {
            int SelectIndex = ddlAssignedToUsers.SelectedIndex;

            // did we really select any?
            if (SelectIndex > -1)
            {
                string Users = "";

                // get the ones selected
                int[] x = ddlAssignedToUsers.GetSelectedIndices();
                for (int i = 0; i < x.Length; i++)
                {
                    // build the list
                    Users += ddlAssignedToUsers.Items[x[i]].Text.ToString() + "<br />";
                }
                ShowPopUpOkCancel("Change Active?", "Are you sure you want to change the Active status of these user(s)?<br /><br />" + Users, "DoActive", "");
            }

        }
        protected void DoAdmin()
        {
            bool TryToChangeMyStatus = false;
            int UserID = 0;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DWUtility dwt = new DWUtility();

            int[] x = ddlAssignedToUsers.GetSelectedIndices();

            // loop thru the list and change the admin status
            for (int i = 0; i < x.Length; i++)
            {
                int.TryParse(ddlAssignedToUsers.Items[x[i]].Value, out UserID);
                if (UserID != MyUserID)
                {
                    int status = dwt.ChangeAdminStatus(UserID, MyUserID);
                    if (status < 1)
                    {
                        ShowPopUp("Unsuccessful!", "The Admin status of (" + ddlAssignedToUsers.Items[x[i]].Text.ToString() + ") could not be changed. The action has been canceled. Please contact the system administrator.");
                        return;
                    }
                }
                else
                {
                    TryToChangeMyStatus = true;
                }
            }

            // reload the users
            LoadAppUsers();

            if (TryToChangeMyStatus)
            {
                ShowPopUp("Not Completed!", "You cannot change the Admin status of yourself. Please have the administrator do that for you.");
            }
            else
            {
                ShowPopUp("Successful!", "The Admin status of those selected have been changed.");
            }
        }
        protected void DoActive()
        {
            bool TryToChangeMyStatus = false;
            int UserID = 0;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DWUtility dwt = new DWUtility();

            int[] x = ddlAssignedToUsers.GetSelectedIndices();

            // loop thru the list and change the admin status
            for (int i = 0; i < x.Length; i++)
            {
                int.TryParse(ddlAssignedToUsers.Items[x[i]].Value, out UserID);
                if (UserID != MyUserID)
                {
                    int status = dwt.ChangeActiveStatus(UserID, MyUserID);
                    if (status < 1)
                    {
                        ShowPopUp("Unsuccessful!", "The Active status of (" + ddlAssignedToUsers.Items[x[i]].Text.ToString() + ") could not be changed. The action has been canceled. Please contact the system administrator.");
                        return;
                    }
                }
                else
                {
                    TryToChangeMyStatus = true;
                }
            }

            // reload the users
            LoadAppUsers();

            if (TryToChangeMyStatus)
            {
                ShowPopUp("Not Completed!", "You cannot change the Active status of yourself. Please have the administrator do that for you.");
            }
            else
            {
                ShowPopUp("Successful!", "The Active status of those selected have been changed.");
            }
        }
        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            if (Session["UsersChanged"] != null)
            {
                MakeChanges();
            }
        }
        protected void CopyUser_Click(object sender, ImageClickEventArgs e)
        {
            int j = 0;  // as items are removed from the dropdowns, the indexes are then off
            if (ddlAvailableUsers.SelectedIndex > -1)
            {
                Session["UsersChanged"] = true;

                foreach (int i in ddlAvailableUsers.GetSelectedIndices())
                {
                    ddlAssignedToUsers.Items.Add(ddlAvailableUsers.Items[i - j]);
                    ddlAvailableUsers.Items.Remove(ddlAvailableUsers.Items[i - j++]);
                }
            }
            else if (ddlAssignedToUsers.SelectedIndex > -1)
            {
                Session["UsersChanged"] = true;

                foreach (int i in ddlAssignedToUsers.GetSelectedIndices())
                {
                    ddlAvailableUsers.Items.Add(ddlAssignedToUsers.Items[i - j]);
                    ddlAssignedToUsers.Items.Remove(ddlAssignedToUsers.Items[i - j++]);
                }
            }
            ddlAvailableUsers.SelectedIndex = -1;
            ddlAssignedToUsers.SelectedIndex = -1;

            ddlAvailableUsers.DataBind();
            ddlAssignedToUsers.DataBind();

            UpdateAppUsersCount();
        }
    }
}