﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Admin
{
    public partial class FormSpecs : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!IsPostBack)
                {
                    gvLoad();
                    //                modPopUp.Show();
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void gvLoad()
        {
            DataTable dt = new DataTable();
            DWUtility dwt = new DWUtility();
            dt = dwt.GetFormSpecs();
            BindGridView(dt);
        }
        protected void gvRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteForm")
            {
                int rowindex = Convert.ToInt32( e.CommandArgument );//id of row
                int FormID = (int)gvFormSpecs.DataKeys[rowindex].Value;

                DWUtility dwt = new DWUtility();
                int status = dwt.DeleteFormSpec(FormID);
                if (status > 0)
                {
                    // Notify the user that the file was deleted successfully.
                    ShowPopUpAndRD("Successful!", "Your form specification was successfully deleted.", "/Admin/FormSpecs.aspx");
                }
                else
                {
                    ShowPopUpAndRD("Error!", "Your form specification could not be deleted. Please tell the administrator about this error message.", "/Admin/FormSpecs.aspx");
                }
            }
            else if (e.CommandName == "PrintForm")
            {
                int rowindex = Convert.ToInt32(e.CommandArgument);//id of row
                int FormID = (int)gvFormSpecs.DataKeys[rowindex].Value;

                DWUtility dwt = new DWUtility();
                DataSet ds = new DataSet();

                ds = dwt.GetFormSpec(FormID);

                if (ds != null)
                {
                    Response.Clear();
                    // in Chrome, 'inline' works for pdf's, but little else
                    Response.AddHeader("content-disposition", "inline;filename=" + ds.Tables[0].Rows[0]["FileName"].ToString());
//                    Response.AddHeader("content-disposition", "attachment;filename=" + ds.Tables[0].Rows[0]["FileName"].ToString());
                    Response.ContentType = ds.Tables[0].Rows[0]["FormType"].ToString();
                    Response.BinaryWrite((Byte[])ds.Tables[0].Rows[0]["Form"]);
                    Response.End();

                }
            }
            else if (e.CommandName == "DownloadForm")
            {
                int rowindex = Convert.ToInt32(e.CommandArgument);//id of row
                int FormID = (int)gvFormSpecs.DataKeys[rowindex].Value;

                DWUtility dwt = new DWUtility();
                DataSet ds = new DataSet();

                ds = dwt.GetFormSpec(FormID);

                if (ds != null)
                {
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = ds.Tables[0].Rows[0]["FormType"].ToString();
                    Response.AddHeader("content-disposition", "attachment;filename=" + ds.Tables[0].Rows[0]["FileName"].ToString());
                    Response.BinaryWrite((Byte[])ds.Tables[0].Rows[0]["Form"]);
                    Response.End();
                }
            }
        }
        protected void gvSorting(object sender, GridViewSortEventArgs e)
        {
            DWUtility dwt = new DWUtility();
            GridView gv = (GridView)sender;
            DataView dv = gv.DataSource as DataView;
            DataTable dt = new DataTable();

            if (dv != null)
                dt = dv.Table;
            else
                dt = dwt.GetFormSpecs();

            if (e.SortExpression != "" & e.SortExpression != null)
            {
                if (gvSortExpression == e.SortExpression)
                    gvSortDirection = GetSortDirection();
                else
                    gvSortDirection = "ASC";
                gvSortExpression = e.SortExpression;
                gv.EditIndex = -1;
            }
            BindGridView(dt);
        }
        private void BindGridView(DataTable dt)
        {
            gvFormSpecs.DataSource = GetSortedData(dt, gvSortExpression, gvSortDirection);
            gvFormSpecs.DataBind();
        }
        private DataView GetSortedData(DataTable dt, String SortExpression, String SortDirection)
        {
            if (dt != null)
            {
                DataView dataView = new DataView(dt);
                if (SortExpression != "" && SortExpression != null)
                    dataView.Sort = SortExpression + " " + SortDirection;
                // Add filter - You may add filter here    
                return dataView;
            }
            return null;
        }
        private String gvSortDirection
        {
            get { return ViewState["SortDirection"] as String ?? "ASC"; }
            set { ViewState["SortDirection"] = value; }
        }
        private String gvSortExpression
        {
            get { return ViewState["SortExpression"] as String ?? ""; }
            set { ViewState["SortExpression"] = value; }
        }
        private String GetSortDirection()
        {
            String newSortDirection = String.Empty;
            switch (gvSortDirection)
            {
                case "DESC":
                    newSortDirection = "ASC";
                    break;
                case "ASC":
                    newSortDirection = "DESC";
                    break;
            }
            return newSortDirection;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            // Before attempting to save the file, verify
            // that the FileUpload control contains a file.
            if (FileUpload1.HasFile)
            {
                // Get the size in bytes of the file to upload.
                int FileSize = FileUpload1.PostedFile.ContentLength;

                // Allow only files less than 5,120,000 bytes (a little over 5 MB) to be uploaded.
                if (FileSize < 10240000)
                {
                    string FileType = FileUpload1.PostedFile.ContentType;
                    string FileName = FileUpload1.PostedFile.FileName;
                    //string path = System.IO.Path.GetFullPath(FileName);
                    //System.IO.FileAttributes x = System.IO.File.GetAttributes(path);

                    byte[] FileArray = new byte[FileSize];
                    HttpPostedFile file = FileUpload1.PostedFile;
                    file.InputStream.Read(FileArray, 0, FileSize);

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    DWUtility dwt = new DWUtility();
                    //int UserID, string FileName, string Version, string Notes, byte[] Form, string FormType)
                    int status = dwt.InsFormSpec(MyUserID, FileName, "1.0", "Test note", FileArray, FileType);

                    // Notify the user that the file was uploaded successfully.
                    ShowPopUpAndRD("Successful!", "Your form upload was successful.", "/Admin/FormSpecs.aspx");
                }
                else
                {
                    // Notify the user why their file was not uploaded.
                    ShowPopUp("File too big!", "Please select a Form that is smaller than 10mb.");
                    //UploadStatusLabel.Text = "Your file was not uploaded because " +
                    //                         "it exceeds the 2 MB size limit.";
                }
            }
            else
            {
                ShowPopUp("No file selected!", "Please select a picture before clicking the Upload button.");
                // Notify the user that a file was not uploaded.
                //UploadStatusLabel.Text = "You did not specify a file to upload.";
            }
        }

        protected void btnPopCancel_Click(object sender, EventArgs e)
        {

        }
    }
}