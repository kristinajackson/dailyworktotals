﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Admin
{
    public partial class DataEntryConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddForm
                        if (eventTarget == "AddApp")
                        {
                            AddApp();
                        }
                        else if (eventTarget == "Save")
                        {
                            if (ddlApp.SelectedIndex > 0)  // did they select one?
                            {
                                SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void ddlApp_Selected(object sender, EventArgs e)
        {
            if (ddlApp.SelectedIndex > 0)  // did they select one?
            {
                int AppID = Convert.ToInt32(ddlApp.SelectedValue);
                if (AppID > 0)
                {
                    LoadDDApp(AppID);
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }
        protected void SaveChanges()
        {
            if (ddlApp.SelectedIndex > 0)  // is an app selected?
            {
                int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                int AppID = Convert.ToInt32(ddlApp.SelectedValue);
                if (AppID > 0)
                {
                    DWUtility dwt = new DWUtility();
                    AppConfigInfo aci = new AppConfigInfo();

                    // get the saved App configuration
                    aci = dwt.GetAppByID(AppID);

                    if (aci != null)
                    {
                        // get the checked values from the screen
                        bool Region = cbRegion.Checked;
                        bool BatchNum = cbBatchNum.Checked;
                        bool Packets = cbPackets.Checked;

                        // now save the changes
                        int status = dwt.UpsertAppConfig(AppID, aci.IsAppUsed, aci.AppName, Region, BatchNum, Packets, MyUserID);
                        if (status > 0)
                        {
                            ShowPopUp("Success!", "The application was updated successfully.");
                        }
                        else
                        {
                            ShowPopUp("Error!", "Application could not be updated. Error = " + status.ToString() + " Please show the administrator this error message.");
                        }
                    }
                    else
                    {
                        ShowPopUp("Error!", "The application could not be retrieved. Please try again.");
                    }
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }
        protected void LoadDDApp(int AppID)
        {
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = new AppConfigInfo();
            aci = dwt.GetAppByID(AppID);

            if (aci != null)
            {
                cbRegion.Checked = aci.Region;
                cbBatchNum.Checked = aci.BatchNum;
                cbPackets.Checked = aci.Packets;
            }
            else
            {
                ShowPopUp("Error!", "The application you selected could not be retrieved. Please try again.");
            }

        }
        protected void CreateForm()
        {
            LoadAppDDL();

            // default to unchecked
            cbRegion.Checked = false;
            cbBatchNum.Checked = false;
            cbPackets.Checked = false;
        }
        protected void LoadAppDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;

            ddlApp.Items.Clear();       // clear in case it's already populated
            ddlApp.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApp.DataTextField = "AppName";
            ddlApp.DataValueField = "AppID";
            ddlApp.DataBind();
            ddlApp.Items.Insert(0, " -- Select Application -- ");
        }

        protected void AddApp()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            string tbAddApp = Request.Form["tbAddApp"].ToString();
            string AppName = string.IsNullOrEmpty(tbAddApp) ? "" : Regex.Replace(tbAddApp, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();
            if (AppName.Length > 5 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this App name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    ShowPopUp("Error!", "You entered an existing application name. Please try again.");
                }
                else
                {
                    // new App, so AppID=0, AppIsUsed=true, Region=false, BatchNum=false, Packets=false
                    int status = dwt.UpsertAppConfig(0, true, AppName, false, false, false, MyUserID);
                    if (status < 1)
                    {
                        ShowPopUp("Error!", "Application could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                    }
                    else
                    {
                        // recreate the form
                        CreateForm();

                        ShowPopUp("Success!", "Application was added successfully!<br /><br />Do not forget to configure the Region, Batch#, and Packet#s options!");
                    }
                }
            }
            else
            {
                ShowPopUp("Error!", "You entered an invalid name for the new Application. Please try again.");
            }
            return;
        }
    }
}