﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace DailyWT.Classes
{
    public class Users
    {
        public int UserID { get; set; }
        public string RacfID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsAdmin { get; set; }
        public int ImageID { get; set; }
        public int CreatedByUserID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedByUserID { get; set; }
        public DateTime ModifiedDate { get; set; }

        //constructors
        public Users() { } //Empty constructor

        public Users(
            int p_UserID,
            string p_RacfID,
            string p_Name,
            bool p_IsActive,
            bool p_IsAdmin,
            int p_ImageID,
            int p_CreatedByUserID,
            DateTime p_CreatedDate,
            int p_ModifiedByUserID,
            DateTime p_ModifiedDate)
        {
            this.UserID = p_UserID;
            this.RacfID = p_RacfID;
            this.Name = p_Name;
            this.IsActive = p_IsActive;
            this.IsAdmin = p_IsAdmin;
            this.ImageID = p_ImageID;
            this.CreatedByUserID = p_CreatedByUserID;
            this.CreatedDate = p_CreatedDate;
            this.ModifiedByUserID = p_ModifiedByUserID;
            this.ModifiedDate = p_ModifiedDate;
        }
    }
    public class FormConfigInfo
    {
        public int FormID { get; set; }
        public bool IsFormUsed { get; set; }
        public string FormName { get; set; }
        public bool ShipInfo { get; set; }
        public bool Sequence { get; set; }
        public int CreatedByUserID { get; set; }
        public DateTime CreatedDate { get; set; }

        //constructors
        public FormConfigInfo() { } //Empty constructor

        public FormConfigInfo(
            int p_FormID,
            bool p_IsFormUsed,
            string p_FormName,
            bool p_ShipInfo,
            bool p_Sequence,
            int p_CreatedByUserID,
            DateTime p_CreatedDate)
        {
            this.FormID = p_FormID;
            this.IsFormUsed = p_IsFormUsed;
            this.FormName = p_FormName;
            this.ShipInfo = p_ShipInfo;
            this.Sequence = p_Sequence;
            this.CreatedByUserID = p_CreatedByUserID;
            this.CreatedDate = p_CreatedDate;
        }
    }
    public class AppConfigInfo
    {
        public int AppID { get; set; }
        public bool IsAppUsed { get; set; }
        public string AppName { get; set; }
        public bool Region { get; set; }
        public bool BatchNum { get; set; }
        public bool Packets { get; set; }
        public int CreatedByUserID { get; set; }
        public DateTime CreatedDate { get; set; }

        //constructors
        public AppConfigInfo() { } //Empty constructor

        public AppConfigInfo(
            int p_AppID,
            bool p_IsAppUsed,
            string p_AppName,
            bool p_Region,
            bool p_BatchNum,
            bool p_Packets,
            int p_CreatedByUserID,
            DateTime p_CreatedDate)
        {
            this.AppID = p_AppID;
            this.IsAppUsed = p_IsAppUsed;
            this.AppName = p_AppName;
            this.Region = p_Region;
            this.BatchNum = p_BatchNum;
            this.Packets = p_Packets;
            this.CreatedByUserID = p_CreatedByUserID;
            this.CreatedDate = p_CreatedDate;
        }
    }
    public class JobConfigInfo
    {
        public int JobID { get; set; }
        public bool IsJobUsed { get; set; }
        public string JobName { get; set; }
        public int LastFormID { get; set; }
        public int CreatedByUserID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedByUserID { get; set; }
        public DateTime ModifiedDate { get; set; }

        //constructors
        public JobConfigInfo() { } //Empty constructor

        public JobConfigInfo(
            int p_JobID,
            bool p_IsJobUsed,
            string p_JobName,
            int p_LastFormID,
            int p_CreatedByUserID,
            DateTime p_CreatedDate,
            int p_ModifiedByUserID,
            DateTime p_ModifiedDate)
        {
            this.JobID = p_JobID;
            this.IsJobUsed = p_IsJobUsed;
            this.JobName = p_JobName;
            this.LastFormID = p_LastFormID;
            this.CreatedByUserID = p_CreatedByUserID;
            this.CreatedDate = p_CreatedDate;
            this.ModifiedByUserID = p_ModifiedByUserID;
            this.ModifiedDate = p_ModifiedDate;
        }
    }
}