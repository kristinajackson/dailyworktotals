﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FormConfig.aspx.cs" Inherits="DailyWT.Admin.FormConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<script type="text/javascript">
    // this is run when the user clicks the submit button
    $(document).ready(function () {
        $('#mpForm')
            .bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    tbAddForm: {
                        validators: {
                            regexp: {
                                regexp: /^[A-Za-z0-9\ \-]{6,50}$/,
                                message: 'Must be between 6-50 alphanumeric chars'
                            }
                        }
                    }
                }
            })  // end registration form
    });
    </script>
<div class="container-fluid toggle">
            <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Configuration:&nbsp;&nbsp;&nbsp;Forms</span><span class="col-xs-1">&nbsp;</span>
            </div>
</div>
<!-- Section 1 -->
<section class="container-fluid toggle col-xs-8" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Form Configuration</span></div>
<div class="container-fluid">
    <div class="h4 form-group">
        
        <label class="v-align">&nbsp;&nbsp;&nbsp;&nbsp<asp:DropDownList ID="ddlForm" placeholder="Select Form" runat="server" OnSelectedIndexChanged="ddlForm_Selected"  AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <label class="v-align"><input id="tbAddForm" name="tbAddForm" type="text" placeholder="New Form" /></label>
            <%--<asp:TextBox ID="" placeholder="New Form" runat="server"></asp:TextBox>--%>
        <label class="v-align MousePointer" onclick="__doPostBack('AddForm', '');" title="Enter new form name, then click the plus sign!">&nbsp;&nbsp;&nbsp;
            <img id="ibAddForm" class="PlusSign" src='<%= Page.ResolveClientUrl("~/Images/PlusGoogle.png") %>' />&nbsp;&nbsp;&nbsp;Add Form&nbsp;&nbsp;&nbsp;
<%--            <asp:ImageButton ID="" runat="server" ImageAlign="Top" ImageUrl="~/Images/PlusGoogle.png" style="width: 25px;" CausesValidation="False" CommandName="ibAddForm" OnClick="ibAddForm_Click" />--%>
        </label>
    </div>
    <hr />
    <div class="h4">
        <label class="col-xs-6 control-label">
            Shipping Info</label>
        <div>
            <label class="v-align"><input id="cbShipInfo" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Used" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
    <div class="h4">
        <label class="col-xs-6 control-label">
            Sequence Numbers</label>
        <div>
            <label class="v-align"><input id="cbSequence" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Used" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
</div>
<div class="panel-footer">
<%--    <button type="submit" name="btnSave" onclick="Save_Click" class="btn btn-success btn-lg" >--%>
    <button type="submit" name="btnSave" onclick="__doPostBack('Save', '');" class="btn btn-success btn-lg" >
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>

</div>


</section>


</asp:Content>
