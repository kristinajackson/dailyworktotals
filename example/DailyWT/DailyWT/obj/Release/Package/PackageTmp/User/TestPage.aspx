﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="TestPage.aspx.cs" Inherits="DailyWT.User.TestPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<!-- Section 1 -->
<section class="container-fluid toggle" id="section1">
    <div class="col-xs-11">
    <div class="panel panel-default" id="DataEntryForm">
        <div class="panel-heading" id="DataEntryHeader">
<%--            <span class="glyphicon glyphicon-registration-mark"></span> <label class="panel-title">&nbsp;&nbsp;&nbsp;Data Entry</label></div>--%>
            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Data Entry</span></div>
        <div class="panel-body" id="PanelBody" runat="server" >
<%--            <div class="form-group">
                <label class="col-xs-2 control-label h4">
                    Project</label>
                <label class="col-xs-2 control-label h4">
                    Region</label>
                <label class="col-xs-2 control-label h4">
                    Batch #</label>
                <label class="col-xs-2 control-label h4">
                    Quantity</label>
                <label class="col-xs-2 control-label h4">
                    Proofed</label>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">
                    Citations</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbReg1" placeholder="Region" value="<%=Request.Form["tbReg1"] %>"autofocus />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbBatch1" placeholder="Batch" value="<%=Request.Form["tbBatch1"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbQty1" placeholder="Qty Entered" value="<%=Request.Form["tbQty1"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbPQty1" placeholder="Qty Proofed" value="<%=Request.Form["tbPQty1"] %>" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">
                    Dispositions</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbReg2" placeholder="Region" value="<%=Request.Form["tbReg2"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbBatch2" placeholder="Batch" value="<%=Request.Form["tbBatch2"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbQty2" placeholder="Qty Entered" value="<%=Request.Form["tbQty2"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbPQty2" placeholder="Qty Proofed" value="<%=Request.Form["tbPQty2"] %>" />
                </div>
            </div>
            <div class="form-group last">
                <label class="col-xs-2 control-label">
                    Expungements</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbReg3" placeholder="Region" value="<%=Request.Form["tbReg3"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbBatch3" placeholder="Batch" value="<%=Request.Form["tbBatch3"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbQty3" placeholder="Qty Entered" value="<%=Request.Form["tbQty3"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbPQty3" placeholder="Qty Proofed" value="<%=Request.Form["tbPQty3"] %>" />
                </div>
            </div>
            <div class="form-group last">
                <label class="col-xs-2 control-label">
                    Revocations</label>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbReg4" placeholder="Region" value="<%=Request.Form["tbReg4"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbBatch4" placeholder="Batch" value="<%=Request.Form["tbBatch4"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbQty4" placeholder="Qty Entered" value="<%=Request.Form["tbQty4"] %>" />
                </div>
                <div class="col-xs-2">
                    <input type="text" class="form-control" name="tbPQty4" placeholder="Qty Proofed" value="<%=Request.Form["tbPQty4"] %>" />
                </div>
            </div>--%>
        </div>
    </div>
    </div>
</section>
<!-- End Section 1 -->


</asp:Content>
