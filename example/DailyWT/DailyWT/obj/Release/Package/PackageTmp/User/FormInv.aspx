﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FormInv.aspx.cs" Inherits="DailyWT.User.FormInv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<style>
    /*This is a wide form, so set column 1 to 0%, and the rest of the columns to 100%*/
    .col-xs-1 {
        width:0% !important;    
    }
    .col-xs-11 {
        width: 100% !important;
    }
</style>
<script>
    // extend the Number function
    Number.prototype.pad = function (size) {
        var s = String(this);
        while (s.length < (size || 2)) { s = "0" + s; }
        return s;
    };
</script>
<script>
    $('#mpForm').on('dblclick', 'td', function (e) {
        var str = this.firstChild.name; // get the name of the input tag that was double-clicked
        var num = str.replace(/^\D+/g, ''); // get the numeric portion of the tag name

        var pos1 = str.indexOf("Partial");   // was this tbPartialBoxAmtxx?
        if (pos1 > 0) {
            var fpb = document.querySelector('[name=tbFormsPerBox' + num + ']');
            var tb = document.querySelector('[name=tbTotalBoxes' + num + ']');

            // is either one blank?
            if (fpb.value.length < 1 || tb.value.length < 1)
                return;

            var pba = document.querySelector('[name=tbPartialBoxAmt' + num + ']');
            var ba = document.querySelector('[name=tbBegAmt' + num + ']');
            var ea = document.querySelector('[name=tbEndAmt' + num + ']');

            // calculate and populate the boxes
            var total = fpb.value * tb.value;
            pba.value = 0;
            ba.value = total;
            ea.value = total;
            return;
        }

        var pos2 = str.indexOf("EndAmt");   // was this tbEndAmtxx?
        if (pos2 > 0) {
            var to = document.querySelector('[name=tbBegAmt' + num + ']');
            var from = document.querySelector('[name=' + str + ']');
            to.value = from.value;
            from.value = "";
            return;
        };

        var pos3 = str.indexOf("EndSeq");   // was this tbEndSeqxx?
        if (pos3 > 0) {
            var to = document.querySelector('[name=tbBegSeq' + num + ']');
            var from = document.querySelector('[name=' + str + ']');
            if (from.value.length > 0) {
                var newval = (Number(from.value) + 1).pad(to.value.length);
                to.value = newval;
                from.value = "";
            };
        };
        return;
    });
    // function to copy the value from one input tag to another, then clears the input tag that was double clicked
    function CopyAndClear(totag, fromtag) {
        var to = document.querySelector('[name=' + totag + ']');
        var from = document.querySelector('[name=' + fromtag + ']');
        //to.value = from.value;
        //from.value = "";
    };
</script>
<script>
    $(document).ready(function () {
        var ShipNum = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'The Shipping# must be alphabetic chars'
                }
            }
        },
        ShipDate = {
            validators: {
                regexp: {
                    regexp: /^\d{2}\/\d{2}\/\d{4}$/,
                    message: 'Shipping date must be MM/DD/YYYY'
                }
            }
        },
        FormsPerBox = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Forms Per Box must be a decimal number'
                }
            }
        },
        TotalBoxes = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Total Boxes must be a decimal number'
                }
            }
        },
        PartialBoxAmt = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Partial Box Amt must be a decimal number'
                }
            }
        },
        BegAmt = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Beginning Amt must be a decimal number'
                }
            }
        },
        EndAmt = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Ending Amt must be a decimal number'
                }
            }
        },
        BegSeq = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'Beginning Seq# must be alphabetic chars'
                }
            }
        },
        EndSeq = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'Ending Seq# must be alphabetic chars'
                }
            }
        },
        FormLocation = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'Form Location must be alphabetic chars'
                }
            }
        },

        fIndex = 0;

        //$('#mpForm')
        //    .bootstrapValidator({
        //        feedbackIcons: {
        //            valid: 'glyphicon glyphicon-ok',
        //            invalid: 'glyphicon glyphicon-remove',
        //            validating: 'glyphicon glyphicon-refresh'
        //        }
        //    })  // end data form

        // get number of lines so the correct number of validators are created
        var formCount = document.getElementById("<%=hfForms.ClientID%>").value;

        while (fIndex < formCount) {
            $('#mpForm')
            .bootstrapValidator('addField', 'tbShipNum' + fIndex, ShipNum)
            .bootstrapValidator('addField', 'tbShipDate' + fIndex, ShipDate)
            .bootstrapValidator('addField', 'tbFormsPerBox' + fIndex, FormsPerBox)
            .bootstrapValidator('addField', 'tbTotalBoxes' + fIndex, TotalBoxes)
            .bootstrapValidator('addField', 'tbPartialBoxAmt' + fIndex, PartialBoxAmt)
            .bootstrapValidator('addField', 'tbBegAmt' + fIndex, BegAmt)
            .bootstrapValidator('addField', 'tbEndAmt' + fIndex, EndAmt)
            .bootstrapValidator('addField', 'tbBegSeq' + fIndex, BegSeq)
            .bootstrapValidator('addField', 'tbEndSeq' + fIndex, EndSeq)
            .bootstrapValidator('addField', 'tbFormLocation' + fIndex++, FormLocation);
        }
    });
</script>
<div class="container-fluid toggle">
            <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Inventory Module</span><span class="col-xs-1">&nbsp;</span>
<%--                <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static"></asp:TextBox>--%>
                <!--hidden fields used to pass number of lines to javascript -->
                <input type="hidden" runat="server" id="hfForms" />
            </div>
</div>
<!-- Section 1 -->
<section class="container-fluid toggle" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Form Inventory</span></div>
<div class="container-fluid">
  <table class="table table-condensed">
    <thead>
      <tr class="h4">
        <th>Form</th>
        <th>Ship #</th>
        <th>Ship Date</th>
        <th>Forms Per Box</th>
        <th>Total Boxes</th>
        <th>Open Box Amt</th>
        <th>Beg Amt</th>
        <th>End Amt</th>
        <th>Beg Seq#</th>
        <th>End Seq#</th>
        <th>Location</th>
      </tr>
    </thead>
    <tbody id="tblBodyInv" runat="server">
    </tbody>
  </table>

</div>
<div class="panel-footer">
<%--    <button type="submit" name="btnSave" onclick="Save_Click" class="btn btn-success btn-lg" >--%>
    <button type="submit" name="btnSave" class="btn btn-success btn-lg" onclick="__doPostBack('Save', '');">
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="reset" id="resetregform" class="btn btn-default btn-lg">
        Clear Form</button>&nbsp;&nbsp;
            <button type="submit" id="btnCancel" class="btn btn-danger btn-lg" onclick="__doPostBack('Cancel', '');">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>

</div>


</section>

</asp:Content>
