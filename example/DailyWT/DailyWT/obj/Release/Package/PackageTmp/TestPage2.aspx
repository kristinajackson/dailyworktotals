﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="TestPage2.aspx.cs" Inherits="DailyWT.TestPage2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" 
            CellPadding="4" ForeColor="Black" 
            GridLines="Vertical" OnRowCreated="GridView1_RowCreated" >
            <RowStyle BackColor="#F7F7DE" />
            <Columns>
                <asp:BoundField DataField="AppID" SortExpression="App" Visible="false" />
                <asp:TemplateField HeaderText="App">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="tbApp" AutoPostBack="true" Text='<%# Bind("AppID") %>'></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
<%--  --%>
                <asp:BoundField DataField="Region" SortExpression="Region" Visible="false" />
                <asp:TemplateField HeaderText="Region">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="tbRegion"  Text='<%# Bind("Region") %>' Visible='<%# GetAppConfig((System.Data.DataSet)Eval("ds1"), "Region", (int)Eval("AppID")) %>'></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
<%--  --%>
                <asp:BoundField DataField="BatchNum" SortExpression="BatchNum" Visible="false"  />
                <asp:TemplateField HeaderText="BatchNum">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="tbBatchNum" Text='<%# Bind("BatchNum") %>'></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
<%--  --%>
                <asp:BoundField DataField="QtyVerified" SortExpression="QtyVerified" Visible="false"  />
                <asp:TemplateField HeaderText="Verified">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="tbQtyVerified"  Text='<%# Bind("QtyVerified") %>'></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
<%--  --%>
                <asp:BoundField DataField="Quantity" SortExpression="Quantity" />
                <asp:TemplateField HeaderText="Quantity">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="tbQuantity" OnTextChanged="TextBox_Changed" AutoPostBack="true"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
<%--  --%>
                <asp:BoundField DataField="Packets" SortExpression="Packets" />
                <asp:TemplateField HeaderText="Packets">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="tbPackets" OnTextChanged="TextBox_Changed" AutoPostBack="true"></asp:TextBox>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCC99" />
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>

</asp:Content>
