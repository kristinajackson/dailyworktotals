﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class FormInv : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: tbDEDate
                        if (eventTarget == "Save")
                        {
                            //        string eventArgument = this.Request["__EVENTARGUMENT"]; // event arg: date in MM/DD/YYYY format

                            //        DateTime WorkDate = Convert.ToDateTime(eventArgument);
                            //        if (WorkDate < DateTime.Today.AddDays(1))
                            //        {
                            //            CreateForm(WorkDate);
                            //            tbDEDate.Text = eventArgument;
                            //        }
                            //        else
                            //        {
                            //            DateTime dtNow = DateTime.Today;
                            //            tbDEDate.Text = dtNow.ToString("d");

                            //            // recreate the screen
                            //            CreateForm(dtNow);

                            //            ShowPopUp("Error!", "Future dates do not make any sense!!");
                            //            return;
                            //        }
                            //    }
                            ReadData();
                            CreateForm();
                        }
                        else if (eventTarget == "Cancel")
                        {
                            Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"), false);
                        }
                        else
                        {
                            ShowPopUpAndRD("Error!", "Unexpected postback. Event target = " + eventTarget + " Please tell the administrator this error message.", "/Error.aspx");
                        }
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void CreateForm()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;

            // copy number of rows to hidden field for javascript
            hfForms.Value = ds1Count.ToString();

            string iStr = "";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();

                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");
                
                // assign 'info' styling to every other line
                sb.Append("<tr " + ((i % 2 == 0) ? "class='info'" : "") + ">");
                sb.Append("<td target='tdName'>" + ds1.Tables[0].Rows[i][2].ToString() + "</td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][3])) ?
                        "<input type='text' class='form-control' name='tbShipNum" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[3].ToString()) +
                        "' placeholder='Shipping#' />" : "") + "</td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][3])) ?
                        "<input type='text' class='form-control' name='tbShipDate" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : ((DateTime)DRow[0].ItemArray[4]).ToString("MM/dd/yyyy")) +
                        "' placeholder='MM/DD/YYYY' />" : "") + "</td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbFormsPerBox" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[5].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbTotalBoxes" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[6].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbPartialBoxAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[7].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbBegAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[8].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbEndAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[9].ToString()) +
                        "' placeholder='Qty' /></td>");
//                        "' placeholder='Qty' ondblclick=\"CopyAndClear('tbBegAmt" + iStr + "','tbEndAmt" + iStr + "')\" /></td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][4])) ?
                        "<input type='text' class='form-control' name='tbBegSeq" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[10].ToString()) +
                        "' placeholder='Seq#' />" : "") + "</td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][4])) ?
                        "<input type='text' class='form-control' name='tbEndSeq" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[11].ToString()) +
                        "' placeholder='Seq#' />" : "") + "</td>");
//                        "' placeholder='Seq#' ondblclick=\"CopyAndClear('tbBegSeq" + iStr + "','tbEndSeq" + iStr + "')\" />" : "") + "</td>");
                sb.Append("<td target='tdName' class='form-group'><input type='text' class='form-control' name='tbFormLocation" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[12].ToString()) +
                        "' placeholder='Loc' /></td>");
                sb.Append("<td target='tdComm' class='form-group'><textarea class='form-control' name='tbComment" + iStr +
                        "' placeholder='Comment' ></textarea></td>");
                sb.Append("</tr>");
            }

            // display it on the screen
            tblBodyInv.InnerHtml = sb.ToString();

        }
        protected void ReadData()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;

            DateTime ShipDate;
            string ShipNum, BegSeq, EndSeq, FormLocation;
            int status, FormsPerBox, TotalBoxes, PartialBoxAmt, BegAmt, EndAmt, Total;
            bool FormInvUpdated;

            string iStr = "";

            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();
                FormInvUpdated = false;

                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

                // myNewValue = myValue ?? new MyValue();
                ShipNum = Request.Form["tbShipNum" + iStr] ?? "";
                ShipNum = ShipNum.ToUpper();
                if (DRow.Length > 0)
                {
                    if (ShipNum != DRow[0].ItemArray[3].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (ShipNum.Length > 0)
                        FormInvUpdated = true;
                }

                DateTime.TryParse(Request.Form["tbShipDate" + iStr], out ShipDate);
                if (DRow.Length > 0)
                {
                    if (ShipDate != (DateTime)DRow[0].ItemArray[4])
                        FormInvUpdated = true;
                }
                else
                {
                    if (ShipDate != new DateTime())
                    {
                        FormInvUpdated = true;
                    }
                    else
                    {
                        ShipDate = Convert.ToDateTime("01/01/1800");    // load a dummy, but legit date
                    }
                }

                int.TryParse(Request.Form["tbFormsPerBox" + iStr], out FormsPerBox);
                if (DRow.Length > 0)
                {
                    if (FormsPerBox != (int)DRow[0].ItemArray[5])
                        FormInvUpdated = true;
                }
                else
                {
                    if (FormsPerBox > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbTotalBoxes" + iStr], out TotalBoxes);
                if (DRow.Length > 0)
                {
                    if (TotalBoxes != (int)DRow[0].ItemArray[6])
                        FormInvUpdated = true;
                }
                else
                {
                    if (TotalBoxes > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbPartialBoxAmt" + iStr], out PartialBoxAmt);
                if (DRow.Length > 0)
                {
                    if (PartialBoxAmt != (int)DRow[0].ItemArray[7])
                        FormInvUpdated = true;
                }
                else
                {
                    if (PartialBoxAmt > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbBegAmt" + iStr], out BegAmt);
                if (DRow.Length > 0)
                {
                    if (BegAmt != (int)DRow[0].ItemArray[8])
                        FormInvUpdated = true;
                }
                else
                {
                    if (BegAmt > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbEndAmt" + iStr], out EndAmt);
                if (DRow.Length > 0)
                {
                    if (EndAmt != (int)DRow[0].ItemArray[9])
                        FormInvUpdated = true;
                }
                else
                {
                    if (EndAmt > 0)
                        FormInvUpdated = true;
                }

                BegSeq = Request.Form["tbBegSeq" + iStr] ?? "";
                if (DRow.Length > 0)
                {
                    if (BegSeq != DRow[0].ItemArray[10].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (BegSeq.Length > 0)
                        FormInvUpdated = true;
                }

                EndSeq = Request.Form["tbEndSeq" + iStr] ?? "";
                if (DRow.Length > 0)
                {
                    if (EndSeq != DRow[0].ItemArray[11].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (EndSeq.Length > 0)
                        FormInvUpdated = true;
                }

                FormLocation = Request.Form["tbFormLocation" + iStr] ?? "";
                FormLocation = FormLocation.ToUpper();
                if (DRow.Length > 0)
                {
                    if (FormLocation != DRow[0].ItemArray[12].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (FormLocation.Length > 0)
                        FormInvUpdated = true;
                }

                string Comment = Regex.Replace(Request.Form["tbComment" + iStr], @"[^A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]", "").ToUpper();
                Comment = Regex.Replace(Comment, @"\r\n", " "); // ok, now change the cr/lf to spaces

                // did something change?
                if (FormInvUpdated)
                {
                    // total all up so we don't insert a bunch of zeros data
                    Total = FormsPerBox + TotalBoxes + PartialBoxAmt + BegAmt + EndAmt;

                    // set IsUsed to true for now
                    status = dwt.UpsertFormData((int)ds1.Tables[0].Rows[i][0], true, ShipNum, ShipDate, FormsPerBox, TotalBoxes, PartialBoxAmt, BegAmt, EndAmt, BegSeq, EndSeq, FormLocation, Comment, MyUserID, Total);
                    if (status < 0)
                    {
                        // Notify the user that an error occurred.
                        ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                        return;
                    }
                }
            }
        }
        protected string CreateFormatSpec(string Example)
        {
            // this is presently not used

            char[] StrArray = Example.ToCharArray();
            string NewRegex = "";
            int ExLen = Example.Length;
            for (int i = 0; i < ExLen; i++)
            {

                if (StrArray[i] == ' ')
                    NewRegex += @"[\ ]";
                else
                    if (StrArray[i] == '-')
                        NewRegex += @"[\-]";
                    else
                        if (StrArray[i] == '.')
                            NewRegex += @"[\.]";
                        else
                            if (Regex.IsMatch(StrArray[i].ToString(), @"[0-9]"))
                                NewRegex += @"[0-9]";
                            else
                                if (Regex.IsMatch(StrArray[i].ToString(), @"[A-Za-z]"))
                                    NewRegex += @"[A-Za-z]";
                                else
                                    return "";
            }
            return NewRegex;
        }
    }
}