﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class Test : BSDialogs
    {
        // had to do it this way to get the bootstrapvalidator to work
        // bootstrapvalidator doesn't work with runat="server"
        protected string tbRegion { get; set; }
        protected string tbPackets { get; set; }
        protected string tbFormCount { get; set; }
        protected string tbQuantity { get; set; }
        protected string tbQtyVerified { get; set; }
        protected string tbBatchNum { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDApp();
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void LoadDDApp()
        {
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");

            tblForm2.Visible = false;
        }
        protected void ddlApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                int AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                if (AppID > 0)
                {
                    tblForm2.Visible = true;
                    btnSave.CssClass = btnSave.CssClass.ToString().Replace("disabled", ""); // remove disabled class

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);
                    DWUtility dwt = new DWUtility();

                    AppConfigInfo aci = dwt.GetAppByID(AppID);
                    DataRow dr = null;

                    int Region = 0;
                    int QtyVerified = 0;
                    int Quantity = 0;
                    int Packets = 0;

                    if (aci.BatchNum)
                    {
                        // if batch data, set date to today
                        tbDEDate.Text = String.Format("{0:MM/dd/yyyy}", DateTime.Now);
                        dr = dwt.GetAppBatchData(AppID);

                        int TQuantity = 0;
                        int BatchNum = 1;
                        if (dr != null)
                        {

                            int ModifiedByUserID = Convert.ToInt32(dr["ModifiedByUserID"]);
                            BatchNum = Convert.ToInt32(dr["BatchNum"]);

                            // if this batch was completed, start a new one
                            if (Convert.ToBoolean(dr["BComplete"]))
                            {
                                BatchNum = BatchNum + 1;
                            }
                            else
                            {
                                // let them see the Total Qty entered
                                TQuantity = Convert.ToInt32(dr["TQuantity"]);

                                // only show their own data
                                if (ModifiedByUserID == MyUserID)
                                {
                                    Region = Convert.ToInt32(dr["Region"]);
                                    QtyVerified = Convert.ToInt32(dr["QtyVerified"]);
                                    Quantity = Convert.ToInt32(dr["Quantity"]);
                                    Packets = Convert.ToInt32(dr["Packets"]);
                                }
                            }
                        }
                        tbBatchNum = BatchNum.ToString();

                        tbQuantity = Quantity < 1 ? "" : Quantity.ToString();
                        tbFormCount = TQuantity < 1 ? "" : TQuantity.ToString();
                        tbRegion = Region < 1 ? "" : Region.ToString();
                        tbQtyVerified = QtyVerified < 1 ? "" : QtyVerified.ToString();
                        tbPackets = Packets < 1 ? "" : Packets.ToString();

                        spBatch.Visible = true;
                        tblForm1.Visible = true;
                    }
                    else
                    {
                        dr = dwt.GetAppData(AppID, MyUserID, Convert.ToDateTime(tbDEDate.Text));
                        if (dr != null)
                        {
                            Region = Convert.ToInt32(dr["Region"]);
                            Quantity = Convert.ToInt32(dr["Quantity"]);
                            QtyVerified = Convert.ToInt32(dr["QtyVerified"]);
                            Packets = Convert.ToInt32(dr["Packets"]);

                            tbRegion = Region < 1 ? "" : Region.ToString();
                            
                            tbQuantity = Quantity < 1 ? "" : Quantity.ToString();
                            tbQtyVerified = QtyVerified < 1 ? "" : QtyVerified.ToString();
                            tbPackets = Packets < 1 ? "" : Packets.ToString();
                        }
                        else
                        {
                            // didn't find any, so blank the textboxes
                            tbQuantity = "";
                            tbRegion = "";

                            tbQtyVerified = "";
                            tbPackets = "";
                        }
                        spBatch.Visible = false;
                        tblForm1.Visible = false;
                    }
                    // hide or show these fields
                    if (aci.Region)
                        trRegion.Visible = true;
                    else
                        trRegion.Visible = false;

                    if (aci.Packets)
                        trPackets.Visible = true;
                    else
                        trPackets.Visible = false;

                }
                else
                {
                    // if the Save button doesn't contain a 'disabled' class, add one
                    if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    spBatch.Visible = false;
                    tblForm1.Visible = false;    // invalid form selected, so hide data
                    tblForm2.Visible = false;    // invalid form selected, so hide data
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            int AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            if (AppID > 0)
            {
                int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                DWUtility dwt = new DWUtility();

                AppConfigInfo aci = dwt.GetAppByID(AppID);

                int Region, Quantity, QtyVerified, Packets, TQuantity = 0, BatchNum = 0;

                int.TryParse(Request.Form["tbRegion"], out Region);

                int.TryParse(Request.Form["tbPackets"], out Packets);

                int.TryParse(Request.Form["tbQuantity"], out Quantity);
                int.TryParse(Request.Form["tbQtyVerified"], out QtyVerified);

                int.TryParse(Request.Form["tbBatchNum"], out BatchNum);
                int.TryParse(Request.Form["tbFormCount"], out TQuantity);

                if ((Region + Quantity + Packets + QtyVerified + TQuantity) < 1)
                {
                    ShowPopUp("Error!", "You must enter data to save this record!");
                    return;
                }

                if (aci.Region && (Region < 1 || Region > 5))
                {
                    ShowPopUp("Error!", "The Region number must be between 1 and 5.");
                    return;
                }

                if (aci.Packets && Packets < 1)
                {
                    ShowPopUp("Error!", "The number of packets cannot be less than 1.");
                    return;
                }

                bool BComplete = cbBatch.Checked;
                DateTime WorkDate = Convert.ToDateTime(tbDEDate.Text);
                int status = dwt.UpsertAppData(AppID, Region, BatchNum, TQuantity, QtyVerified, Quantity, Packets, MyUserID, WorkDate, BComplete);

                DisableAndHideStuff();

                if (status < 0)
                {
                    // Notify the user that an error occurred.
                    ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                    return;
                }
                ShowPopUp("Success!", "The data was successfully saved!");
            }
            else
            {
                DisableAndHideStuff();
                ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
            }
        }
        protected void DisableAndHideStuff()
        {
            ddlApplication.ClearSelection();
            // if the Save button doesn't contain a 'disabled' class, add one
            if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

            cbBatch.Checked = false;
            spBatch.Visible = false;
            tblForm1.Visible = false;    // invalid form selected, so hide data
            tblForm2.Visible = false;    // invalid form selected, so hide data
        }
    }
}