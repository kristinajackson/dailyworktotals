﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class Images : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadImages();
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void LoadImages()
        {
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            DWUtility dwt = new DWUtility();

            gvImages.DataSource = dwt.GetImages(MyUserID);
            gvImages.DataBind();

            // 5 images maximum allowed
            if (gvImages.Rows.Count < 5)
            {
                pnlUpload.Visible = true;
            }
            else
            {
                pnlUpload.Visible = false;
            }

        }
        protected void gv_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            DWUtility dwt = new DWUtility();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            int MyImageID = Convert.ToInt32(Session["MyImageID"]);

            int RowNum = Convert.ToInt32(e.CommandArgument);    // get this row number
            int ImageID = (int)gvImages.DataKeys[RowNum].Value; // find imageid from this row

            if (e.CommandName == "SetBkg")
            {
                // set the new background default image
                Session["MyImageID"] = ImageID;

                // and update the user account
                int status = dwt.SetDefImageID(MyUserID, ImageID);
            }
            else if (e.CommandName == "Delete")
            {
                int RowCount = gvImages.DataKeys.Count;
                if (RowCount > 1)
                {
                    int NewDefImageID = 0;

                    // are we deleting the default image?
                    if (ImageID == MyImageID)
                    {
                        // yes, find a new one
                        int NewRowNum = RowNum + 1;
                        if (NewRowNum >= RowCount)
                            NewRowNum = 0;

                        NewDefImageID = (int)gvImages.DataKeys[NewRowNum].Value;

                        // now set the new background default image
                        Session["MyImageID"] = NewDefImageID;
                    }
                    int status = dwt.DeleteImage(MyUserID, ImageID, NewDefImageID);
                }

            }
        }

        protected void gvImages_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            LoadImages();
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            //CheckFile();

            // Before attempting to save the file, verify
            // that the FileUpload control contains a file.
            if (FileUpload1.HasFile)
            {
                // Get the size in bytes of the file to upload.
                int ImageSize = FileUpload1.PostedFile.ContentLength;

                // Allow only files less than 5,120,000 bytes (a little over 5 MB) to be uploaded.
                if (ImageSize < 5120000)
                {
                    string ImageType = FileUpload1.PostedFile.ContentType;
                    string ImageName = FileUpload1.PostedFile.FileName;

                    byte[] ImageArray = new byte[ImageSize];
                    HttpPostedFile image = FileUpload1.PostedFile;
                    image.InputStream.Read(ImageArray, 0, ImageSize);

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    DWUtility dwt = new DWUtility();
                    int status = dwt.InsImage(MyUserID, ImageName, ImageArray, ImageType);

                    // Notify the user that the file was uploaded successfully.
                    ShowPopUpAndRD("Successful!", "Your picture upload was successful.", "/User/Images.aspx");
                }
                else
                {
                    // Notify the user why their file was not uploaded.
                    ShowPopUp("File too big!", "Please select a picture that is smaller than 5mb.");
                    //UploadStatusLabel.Text = "Your file was not uploaded because " +
                    //                         "it exceeds the 2 MB size limit.";
                }
            }
            else
            {
                ShowPopUp("No file selected!", "Please select a picture before clicking the Upload button.");
                // Notify the user that a file was not uploaded.
                //UploadStatusLabel.Text = "You did not specify a file to upload.";
            }
        }


        //protected void CheckFile()
        //{
        //    if (Request.Files["UploadedFile"] != null)
        //    {
        //        HttpPostedFile MyFile = Request.Files["UploadedFile"];
        //        //Setting location to upload files
        //        //string TargetLocation = Server.MapPath("~/Files/");
        //        try
        //        {
        //            if (MyFile.ContentLength > 0)
        //            {
        //                //Determining file name. You can format it as you wish.
        //                string FileName = MyFile.FileName;
        //                //Determining file size.
        //                int FileSize = MyFile.ContentLength;
        //                //Creating a byte array corresponding to file size.
        //                byte[] FileByteArray = new byte[FileSize];
        //                //Posted file is being pushed into byte array.
        //                MyFile.InputStream.Read(FileByteArray, 0, FileSize);
        //                //Uploading properly formatted file to server.
        //                //MyFile.SaveAs(TargetLocation + FileName);
        //            }
        //        }
        //        catch (Exception BlueScreen)
        //        {
        //            //Handle errors
        //        }
        //    }
        //}
        //protected void FileSizeValidator(object source, ServerValidateEventArgs args)
        //{
        //    if (FileUpload1.FileBytes.Length > 5125)
        //    {
        //        args.IsValid = false;
        //    }
        //    else
        //    {
        //        args.IsValid = true;
        //    }
        //}
        //public void Page_Error(object sender, EventArgs e)
        //{
        //    // get the error code
        //    int ErrorCode = Server.GetLastError().HResult;

        //    // maxRequestLength="5125000" in web.config
        //    int maxRequestLengthExceededErrorCode = unchecked((int)0x80004005);

        //    // clear it so we can move on
        //    Server.ClearError();
                
        //    // is this the error we think it is??
        //    if (ErrorCode == maxRequestLengthExceededErrorCode)
        //    {
        //        //yes, let them know
        //        //ShowPopUpAndRD("Image too big!", "The maximum image size is approximately 5mb. please select a smaller image.", "/User/Images.aspx");
        //        ShowPopUp("Image too big!", "The maximum image size is approximately 5mb. please select a smaller image.");
        //    }
        //    else
        //    {
        //        ShowPopUpAndRD("Page error!", "An unknown error occurred on this page. The error code is: " + ErrorCode.ToString("X"), "/Error.aspx");
        //    }
        //}
        //[WebMethod]
        //public static void TooBig(object sender, EventArgs e)
        //{
        //    Images i = new Images();
        //    i.ShowPopUp("Image too big!", "The maximum image size is approximately 5mb. please select a smaller image.");
        //}
    }
}