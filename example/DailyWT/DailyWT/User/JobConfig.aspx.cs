﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class JobConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                //if (Session["Admin"] != null)
                //{
                if (!Page.IsPostBack)
                {
                    CreateForm();
                }
                else
                {
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddJob
                    string eventArgument = this.Request["__EVENTARGUMENT"]; // event arg: Job name

                    if (eventTarget == "AddJob")
                    {
                        if (Session["Admin"] != null)
                        {
                            AddJob(eventArgument);
                        }
                        else
                        {
                            ShowPopUp("Error!", "You must have Admin rights to add a new job.");
                        }
                    }
                }
            }
        }
        protected void CreateForm()
        {
            LoadJobDDL();

            //LoadJFormDDL();

            //LoadAFormDDL();

            // now disable it until job is selected
            //            ddlForm.Enabled = false;
        }
        protected void LoadJobDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";
            ddlJob.DataBind();
            ddlJob.Items.Insert(0, " -- Select Job -- ");
        }
        protected void LoadJFormDDL()
        {
            DWUtility dwt = new DWUtility();

            ddlJForms.Items.Clear();       // clear in case it's already populated

            if (ddlJob.SelectedIndex > 0)
            {
                int JobID = Convert.ToInt32(ddlJob.SelectedValue);
                ddlJForms.DataSource = dwt.GetFormsByJobID(JobID);
                ddlJForms.DataTextField = "FormName";
                ddlJForms.DataValueField = "FormID";
                ddlJForms.DataBind();

            }
            else
            {
            }
            ddlJForms.Items.Insert(0, " -- Select Form -- ");
        }
        protected void LoadAFormDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlAForms.Items.Clear();       // clear in case it's already populated
            ddlAForms.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlAForms.DataTextField = "FormName";
            ddlAForms.DataValueField = "FormID";
            ddlAForms.DataBind();
            ddlAForms.Items.Insert(0, " -- Select Form -- ");

            if (ddlJob.SelectedIndex > 0)
            {
            }
            else
            {
            }
        }
        protected void ddlJob_Selected(object sender, EventArgs e)
        {
            trJForms.Visible = true;
            trAForms.Visible = true;
            LoadJFormDDL();

            LoadAFormDDL();
        }
        protected void ibAddJob_Click(object sender, EventArgs e)
        {
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            string name = Request.Form["tbAddJob"].ToString();
            string JobName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (JobName.Length > 5 && JobName.Length < 51)
            {
                DWUtility dwt = new DWUtility();

                DataSet ds1 = new DataSet();
                // check if this form name already exists
                ds1 = dwt.GetJobByName(JobName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    ShowPopUp("Error!", "You entered an existing job name. Please try again.");
                }
                else
                {
                    ShowPopUpOkCancel("Action: Add job", "Are you sure you want to create this job?", "AddJob", JobName);
                }
            }
            else
            {
                ShowPopUp("Error!", "Please enter a valid name from 6-50 chars.");
            }
        }
        protected void AddJob(string JobName)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            // new form, so JobID=0, IsJobUsed=true, and FormID=0
            int status = dwt.UpsertJobConfig(0, true, JobName, 0, MyUserID);
            if (status < 1)
            {
                ShowPopUp("Error!", "Job could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
            }
            else
            {
                // recreate the form
                CreateForm();

                ShowPopUp("Success!", "Job was added successfully!");
            }
        }
        protected void ibAddJForm_Click(object sender, EventArgs e)
        {
        }
        protected void ibRemoveJForm_Click(object sender, EventArgs e)
        {
        }
    }
}