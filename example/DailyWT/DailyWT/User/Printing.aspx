﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Printing.aspx.cs" Inherits="DailyWT.User.Printing" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<script>
    $(document).ready(function () {
        var Qty = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Qty must be a decimal number'
                }
            }
        },
        Requester = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z\ \-\.]+$/,
                    message: 'The Requester must be alphabetic chars'
                }
            }
        },
        Comment = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]+$/,
                    message: 'The Comments must be alphabetic chars'
                }
            }
        };

        $('#mpForm')
        .bootstrapValidator('addField', 'tbFormsUsed', Qty)
        .bootstrapValidator('addField', 'tbFormsReq', Qty)
        .bootstrapValidator('addField', 'tbRequester', Requester)
        .bootstrapValidator('addField', 'tbComment', Comment);
    });
</script>


<!-- Section 1 -->
<section class="container-fluid toggle col-xs-8" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Printing Module</span></div>
<div class="container-fluid">
    <div class="h4 form-group">        
        <label class="v-align-tc">&nbsp;&nbsp;Job Name:&nbsp;&nbsp<asp:DropDownList ID="ddlJob" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="h4 form-group">        
        <label class="v-align-tc">&nbsp;&nbsp;Form Used:&nbsp;&nbsp<asp:DropDownList ID="ddlForm" Enabled="false" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <br />
    </div>
    <table class="table table-condensed" id="tblForm" runat="server" visible="false">
        <tr class="info">
            <td><b>Forms Per Box</b></td>
            <td><label id="lblFormsPerBox" runat="server"></label></td>
        </tr>
        <tr>
            <td><b>Total Boxes</b></td>
            <td><label id="lblTotalBoxes" runat="server"></label></td>
        </tr>
        <tr class="info">
            <td><b>Open Box Amount</b></td>
            <td><label id="lblOpenBoxAmount" runat="server"></label></td>
        </tr>
        <tr>
            <td><b>Beginning Amount</b></td>
            <td><label id="lblEndAmount" runat="server"></label></td>
        </tr>
        <tr class="info" id="trSeq1" runat="server" visible="false">
            <td><b>Beginning Sequence#</b></td>
            <td><label id="lblBegSeq" runat="server"></label></td>
        </tr>
        <tr id="trSeq2" runat="server" visible="false">
            <td><b>Ending Sequence#</b></td>
            <td><label id="lblEndSeq" runat="server"></label></td>
        </tr>
        <tr class="info">
            <td><b>Quantity Used</b></td>
            <td class="form-group"><input type='text' class='form-control' name='tbFormsUsed' placeholder='Qty' value="<%=Request.Form["tbFormsUsed"] %>" /></td>
        </tr>
        <tr>
            <td><b>Reason, if applicable</b></td>
            <td class="form-group"><asp:DropDownList ID="ddlReason" runat="server" AppendDataBoundItems="true"></asp:DropDownList></td>
        </tr>
        <tr class="info">
            <td><b>Quantity Requested</b></td>
            <td class="form-group"><input type='text' class='form-control' name='tbFormsReq' placeholder='Qty' value="<%=Request.Form["tbFormsReq"] %>" /></td>
        </tr>
        <tr>
            <td><b>Requester</b></td>
            <td class="form-group"><input type='text' class='form-control' name='tbRequester' placeholder='Requester' value="<%=Request.Form["tbRequester"] %>" /></td>
        </tr>
        <tr class="info">
            <td><b>Comment</b></td>
            <td class="form-group"><textarea class='form-control' name='tbComment' rows="6" placeholder='Comment'><%=Request.Form["tbComment"] %></textarea></td>
        </tr>
     </table>
    <div id="divNoData" runat="server" visible="false">
        <br />
        <div class="h3 text-warning">&nbsp;&nbsp;Initial form inventory not entered.
        </div>
    </div>
</div>
<div class="panel-footer">
        <asp:Button ID="btnSave" CssClass="btn btn-success btn-lg submit-button disabled" runat="server" Text="Save Changes" OnClick="SaveChanges_Click" />
 <%--    <button type="submit" name="btnSave" class="btn btn-success btn-lg" >
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>--%>
    &nbsp;&nbsp;
            <button type="reset" id="resetregform" class="btn btn-default btn-lg">
        Clear Form</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;
</div>

</div>


</section>


</asp:Content>
