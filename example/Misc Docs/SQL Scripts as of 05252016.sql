USE [DWTest]
GO
/****** Object:  Schema [DWT]    Script Date: 5/25/2016 3:21:14 PM ******/
CREATE SCHEMA [DWT]
GO
/****** Object:  Schema [NET\BH01567]    Script Date: 5/25/2016 3:21:14 PM ******/
CREATE SCHEMA [NET\BH01567]
GO
/****** Object:  StoredProcedure [DWT].[AddUser]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 03/24/2016
-- Description:	Procedure used to copy a user from the inventory DB to the Daily Work DB
-- =============================================
CREATE PROCEDURE [DWT].[AddUser]
	@UserID int,
	@CreatedByUserID int
AS

DECLARE @ROWS int

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY
		-- Insert statements for procedure here
		INSERT INTO [DWT].Users (UserID, RacfID, Name, IsActive, IsAdmin, ImageID, CreatedByUserID)
		SELECT up.UserID, up.RacfID,
			REPLACE((COALESCE(up.LastName, '') + ' ' + COALESCE(up.Suffix, '') + ', ' + COALESCE(up.FirstName,'') + ' ' + COALESCE(up.MI,'') ), ' ,', ',') AS Name,
			1, 0, 1, @CreatedByUserID

		FROM [Inventory_Test].[dbo].[UserProfile] up

		WHERE up.UserID = @UserID

		SET @ROWS = @@ROWCOUNT

		SELECT @ROWS
		RETURN @ROWS
	END TRY
	
	BEGIN CATCH
		SELECT -1
		RETURN -1
	END CATCH

END

GO
/****** Object:  StoredProcedure [DWT].[DeleteImage]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/06/2016
-- Description:	Procedure used to delete an image from the image table
-- =============================================
CREATE PROCEDURE [DWT].[DeleteImage]
	@ImageID int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE
	
	FROM [DWT].Images

	WHERE ID = @ImageID

	SELECT @@ROWCOUNT

END

GO
/****** Object:  StoredProcedure [DWT].[DelUser]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 03/30/2016
-- Description:	Procedure used to delete a user from the Daily Work DB
-- =============================================
CREATE PROCEDURE [DWT].[DelUser]
	@UserID int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	DELETE
	
	FROM [DWT].Users

	WHERE UserID = @UserID


	SELECT @@ROWCOUNT

END

GO
/****** Object:  StoredProcedure [DWT].[GetAppByID]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/23/2016
-- Description:	Procedure used to get application configuration by ID
-- =============================================
CREATE PROCEDURE [DWT].[GetAppByID] 
	@AppID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[AppConfig] WHERE [AppID] = @AppID
END

GO
/****** Object:  StoredProcedure [DWT].[GetAppByName]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/25/2016
-- Description:	Procedure used to get application config by app name
-- =============================================
CREATE PROCEDURE [DWT].[GetAppByName] 
	@AppName nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

	BEGIN
		SELECT *
		FROM [DWT].[AppConfig] WHERE
			[AppName] = @AppName
	END
END

GO
/****** Object:  StoredProcedure [DWT].[GetAppConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/11/2016
-- Description:	Procedure used to get application configuration
-- =============================================
CREATE PROCEDURE [DWT].[GetAppConfig] 
	@IsAppUsed bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[AppConfig] WHERE [IsAppUsed] = @IsAppUsed

	ORDER BY [AppName]
END

GO
/****** Object:  StoredProcedure [DWT].[GetAppData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/12/2016
-- Description:	Procedure used to get application data
-- =============================================
CREATE PROCEDURE [DWT].[GetAppData] 
	@UserID int,
	@WorkDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[AppData] WHERE [CreatedByUserID] = @UserID AND
		DATEDIFF(day,@WorkDate,[CreatedDate]) = 0

	ORDER BY [AppID]
END

GO
/****** Object:  StoredProcedure [DWT].[GetAppUsers]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 03/24/2016
-- Description:	Procedure used to get application users
-- =============================================
CREATE PROCEDURE [DWT].[GetAppUsers] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT UserID, RacfID, [Name]
	FROM [DWT].[Users]

	ORDER BY [Name]
END

GO
/****** Object:  StoredProcedure [DWT].[GetAvailUsers]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 03/24/2016
-- Description:	Procedure used to get available users from the inventory DB
-- =============================================
CREATE PROCEDURE [DWT].[GetAvailUsers] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT up.UserID, up.RacfID,
	REPLACE((COALESCE(up.LastName, '') + ' ' + COALESCE(up.Suffix, '') + ', ' + COALESCE(up.FirstName,'') + ' ' + COALESCE(up.MI,'') ), ' ,', ',') AS Name

	FROM [Inventory_Test].[dbo].[UserProfile] up
		INNER JOIN [Inventory_Test].[dbo].[Category] c
			ON c.CategoryName = 'User Status'
		INNER JOIN [Inventory_Test].[dbo].[DropDownNames] d
			ON c.CategoryID = d.CategoryID and d.DDName = 'Active'

	WHERE (up.StatusNameID = d.DDNameID AND up.RacfID != '') AND
		-- we are selecting users for the available dropdown, so we don't want existing users
		up.RacfID NOT IN (SELECT RacfID FROM [DWT].[Users])

	ORDER BY up.LastName, up.FirstName, up.MI, up.Suffix
END

GO
/****** Object:  StoredProcedure [DWT].[GetFormByID]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/17/2016
-- Description:	Procedure used to get form configuration by ID
-- =============================================
CREATE PROCEDURE [DWT].[GetFormByID] 
	@FormID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[FormConfig] WHERE [FormID] = @FormID
END

GO
/****** Object:  StoredProcedure [DWT].[GetFormByName]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/17/2016
-- Description:	Procedure used to get form config by form name
-- =============================================
CREATE PROCEDURE [DWT].[GetFormByName] 
	@FormName nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

	BEGIN
		SELECT *
		FROM [DWT].[FormConfig] WHERE
			[FormName] = @FormName
	END
END

GO
/****** Object:  StoredProcedure [DWT].[GetFormConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/02/2016
-- Description:	Procedure used to get form configuration
-- =============================================
CREATE PROCEDURE [DWT].[GetFormConfig] 
	@IsFormUsed bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[FormConfig] WHERE [IsFormUsed] = @IsFormUsed

	ORDER BY [FormName]
END

GO
/****** Object:  StoredProcedure [DWT].[GetFormData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/02/2016
-- Description:	Procedure used to get form data
-- =============================================
CREATE PROCEDURE [DWT].[GetFormData] 
	@IsUsed bit,
	@FormID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

-- If FormID < 1, then we want all form data
IF (@FormID < 1)
	BEGIN
		SELECT *
		FROM [DWT].[FormData] WHERE
			[IsUsed] = @IsUsed
	END
ELSE
	BEGIN
		SELECT *
		FROM [DWT].[FormData] WHERE
			[IsUsed] = @IsUsed AND
			[FormID] = @FormID
	END
END

GO
/****** Object:  StoredProcedure [DWT].[GetFormDestroy]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/03/2016
-- Description:	Procedure used to get form destroy data (forms destroyed)
-- =============================================
CREATE PROCEDURE [DWT].[GetFormDestroy] 
	@IsUsed bit,
	@FormID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[GetFormDestroy] WHERE
		[IsUsed] = @IsUsed AND
		[FormID] = @FormID

END

GO
/****** Object:  StoredProcedure [DWT].[GetImage]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 03/19/2016
-- Description:	Get image from DB
-- =============================================
CREATE PROCEDURE [DWT].[GetImage]
(
        @ImageID int
)
AS
BEGIN
	SELECT *
	FROM [DWT].[Images]
	WHERE @ImageID = [ID]
END
GO
/****** Object:  StoredProcedure [DWT].[GetImages]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/01/2016
-- Description:	Get images from DB by userid
-- =============================================
CREATE PROCEDURE [DWT].[GetImages]
(
        @UserID int
)
AS
BEGIN
	SELECT *
	FROM [DWT].[Images]
	WHERE @UserID = UserID
END
GO
/****** Object:  StoredProcedure [DWT].[GetImagesIDsForGV]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/15/2016
-- Description:	Get image ids from DB by userid
-- =============================================
CREATE PROCEDURE [DWT].[GetImagesIDsForGV]
(
        @UserID int
)
AS
BEGIN
	SELECT ID, ImageName
	FROM [DWT].[Images]
	WHERE @UserID = UserID
END
GO
/****** Object:  StoredProcedure [DWT].[GetJobByID]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/23/2016
-- Description:	Procedure used to get job configuration by ID
-- =============================================
CREATE PROCEDURE [DWT].[GetJobByID] 
	@JobID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[JobConfig] WHERE [JobID] = @JobID
END

GO
/****** Object:  StoredProcedure [DWT].[GetJobByName]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/17/2016
-- Description:	Procedure used to get form config by form name
-- =============================================
CREATE PROCEDURE [DWT].[GetJobByName] 
	@JobName nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

	BEGIN
		SELECT *
		FROM [DWT].[JobConfig] WHERE
			[JobName] = @JobName
	END
END

GO
/****** Object:  StoredProcedure [DWT].[GetJobConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/17/2016
-- Description:	Procedure used to get job configuration
-- =============================================
CREATE PROCEDURE [DWT].[GetJobConfig] 
	@IsJobUsed bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[JobConfig] WHERE [IsJobUsed] = @IsJobUsed

	ORDER BY [JobName]
END

GO
/****** Object:  StoredProcedure [DWT].[GetTWorkByName]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/23/2016
-- Description:	Procedure used to get TWork config by TWorkName
-- =============================================
CREATE PROCEDURE [DWT].[GetTWorkByName] 
	@TWorkName nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

	BEGIN
		SELECT *
		FROM [DWT].[TWorkConfig] WHERE
			[TWorkName] = @TWorkName
	END
END

GO
/****** Object:  StoredProcedure [DWT].[GetTWorkConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/12/2016
-- Description:	Procedure used to get timed work configuration
-- =============================================
CREATE PROCEDURE [DWT].[GetTWorkConfig] 
	@IsTWorkUsed bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[TWorkConfig] WHERE [IsTWorkUsed] = @IsTWorkUsed

	ORDER BY [TWorkName]
END

GO
/****** Object:  StoredProcedure [DWT].[GetTWorkData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/12/2016
-- Description:	Procedure used to get hours worked data
-- =============================================
CREATE PROCEDURE [DWT].[GetTWorkData] 
	@UserID int,
	@WorkDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [DWT].[TWorkData] WHERE [CreatedByUserID] = @UserID AND 
		DATEDIFF(day,@WorkDate,[CreatedDate]) = 0

	ORDER BY [TWorkID]
END

GO
/****** Object:  StoredProcedure [DWT].[GetUserIDByRacfID]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 03/31/2016
-- Description:	Procedure used to get a userid record from the Users table via the RacfID
-- =============================================
CREATE PROCEDURE [DWT].[GetUserIDByRacfID] 
           @RacfID nvarchar(20)
AS

DECLARE @UserID INT

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT @UserID = UserID FROM [DWT].Users WHERE (RacfID = @RacfID AND IsActive = 1)
	SELECT ISNULL(@UserID, -1)
END

GO
/****** Object:  StoredProcedure [DWT].[GetUserInfoByRacfID]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/01/2016
-- Description:	Procedure used to get user record based upon the RacfID
-- =============================================
CREATE PROCEDURE [DWT].[GetUserInfoByRacfID] 
           @RacfID nvarchar(20)
AS

DECLARE @UserID INT

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM [DWT].Users WHERE RacfID = @RacfID
END

GO
/****** Object:  StoredProcedure [DWT].[InsImage]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/07/2014
-- Description:	Procedure used to insert a new image in the Images table
-- =============================================
CREATE PROCEDURE [DWT].[InsImage] 
           @UserID int,
           @ImageName nvarchar(100),
           @Image image,
           @ImageType nvarchar(20)
AS

DECLARE @RETURNVAL int

BEGIN
	BEGIN TRY
		INSERT INTO [DWT].[Images]
          ([UserID],
           [ImageName],
           [ImageType],
           [Image])
		VALUES
          (@UserID,
           @ImageName,
           @ImageType,
           @Image)
           
		SET @RETURNVAL = @@IDENTITY
		SELECT @RETURNVAL
		RETURN @RETURNVAL
	END TRY
	BEGIN CATCH
		SELECT -1
		RETURN -1
	END CATCH
END

GO
/****** Object:  StoredProcedure [DWT].[SetDefImageID]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/06/2016
-- Description:	Procedure used to update a user's default image
-- =============================================
CREATE PROCEDURE [DWT].[SetDefImageID]
	@UserID int,
	@ImageID int
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRY
		UPDATE [DWT].[Users] 
		SET
           ImageID = @ImageID WHERE UserID = @UserID

		SELECT @@ROWCOUNT
		RETURN @@ROWCOUNT
	END TRY
	
	BEGIN CATCH
		SELECT -1
		RETURN -1
	END CATCH

END

GO
/****** Object:  StoredProcedure [DWT].[UpsertAppConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/25/2016
-- Description:	Procedure used to insert or update application configuration info
-- =============================================
CREATE PROCEDURE [DWT].[UpsertAppConfig] 
	@AppID int,
	@IsAppUsed bit,
	@AppName nvarchar(50),
	@Region bit,
	@BatchNum bit,
	@Packets bit,
	@UserID int
AS

DECLARE @ERRVAL int
SET @ERRVAL = -1

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[AppConfig]
	SET
		IsAppUsed = @IsAppUsed,
		AppName = @AppName,
		Region = @Region,
		BatchNum = @BatchNum,
		Packets = @Packets,
		CreatedByUserID = @UserID

	WHERE AppID = @AppID

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1)
		BEGIN
			INSERT INTO [DWT].[AppConfig] (
				IsAppUsed,
				AppName,
				Region,
				BatchNum,
				Packets,
				CreatedByUserID
			)
			VALUES (
				@IsAppUsed,
				@AppName,
				@Region,
				@BatchNum,
				@Packets,
				@UserID
			)

			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertAppData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/13/2016
-- Description:	Procedure used to insert or update quantity worked data
-- Note: When record is first created, the CreatedByUserID and ModifiedByUserID will be the same UserID
-- =============================================
CREATE PROCEDURE [DWT].[UpsertAppData] 
	@AppID int,
	@Region int,
	@BatchNum int,
	@QtyVerified int,
	@Quantity int,
	@Packets int,
	@UserID int,
	@WorkDate datetime,
	@Total int
AS

DECLARE @ERRVAL int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[AppData]
	SET
		Region = @Region,
		BatchNum = @BatchNum,
		QtyVerified = @QtyVerified,
		Quantity = @Quantity,
		Packets = @Packets,
		ModifiedByUserID = @UserID,
		ModifiedDate = GETDATE()

	WHERE (AppID = @AppID AND ModifiedByUserID = @UserID AND DATEDIFF(day,CreatedDate,@WorkDate) = 0)

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1 AND @TOTAL > 0)
		BEGIN
			INSERT INTO [DWT].[AppData] (
				AppID,
				Region,
				BatchNum,
				QtyVerified,
				Quantity,
				Packets,
				CreatedByUserID,
				CreatedDate,
				ModifiedByUserID
			)
			VALUES (
				@AppID,
				@Region,
				@BatchNum,
				@QtyVerified,
				@Quantity,
				@Packets,
				@UserID,
				@WorkDate,
				@UserID
			)

			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertFormConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/17/2016
-- Description:	Procedure used to insert or update form configuration info
-- =============================================
CREATE PROCEDURE [DWT].[UpsertFormConfig] 
	@FormID int,
	@IsFormUsed bit,
	@FormName nvarchar(50),
	@ShipInfo bit,
	@Sequence bit,
	@UserID int
AS

DECLARE @ERRVAL int
SET @ERRVAL = -1

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[FormConfig]
	SET
		IsFormUsed = @IsFormUsed,
		FormName = @FormName,
		ShipInfo = @ShipInfo,
		Sequence = @Sequence,
		CreatedByUserID = @UserID

	WHERE FormID = @FormID

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1)
		BEGIN
			INSERT INTO [DWT].[FormConfig] (
				IsFormUsed,
				FormName,
				ShipInfo,
				Sequence,
				CreatedByUserID
			)
			VALUES (
				@IsFormUsed,
				@FormName,
				@ShipInfo,
				@Sequence,
				@UserID
			)

			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertFormData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/02/2016
-- Description:	Procedure used to insert or update form inventory data
-- Note: When record is first created, the CreatedByUserID and ModifiedByUserID will be the same UserID
--       FormsReq and Requester are not logged with this procedure
-- =============================================
CREATE PROCEDURE [DWT].[UpsertFormData] 
	@FormID int,
	@IsUsed bit,
	@ShipNum nvarchar(30),
	@ShipDate datetime,
	@FormsPerBox int,
	@TotalBoxes int,
	@PartialBoxAmt int,
	@BegAmt int,
	@EndAmt int,
	@BegSeq nvarchar(30),
	@EndSeq nvarchar(30),
	@FormLocation nvarchar(50),
	@UserID int,
	@Total int
AS

DECLARE @ERRVAL int
DECLARE @Action nvarchar(30)
SET @Action = 'UPDATE-INV'
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[FormData]
	SET
		IsUsed = @IsUsed,
		ShipNum = @ShipNum,
		ShipDate = @ShipDate,
		FormsPerBox = @FormsPerBox,
		TotalBoxes = @TotalBoxes,
		PartialBoxAmt = @PartialBoxAmt,
		BegAmt = @BegAmt,
		EndAmt = @EndAmt,
		BegSeq = @BegSeq,
		EndSeq = @EndSeq,
		FormLocation = @FormLocation,
		ModifiedByUserID = @UserID,
		ModifiedDate = GETDATE()

	WHERE (FormID = @FormID)

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1 AND @TOTAL > 0)
		BEGIN
			SET @Action = 'INSERT-INV'
			INSERT INTO [DWT].[FormData] (
				FormID,
				IsUsed,
				ShipNum,
				ShipDate,
				FormsPerBox,
				TotalBoxes,
				PartialBoxAmt,
				BegAmt,
				EndAmt,
				BegSeq,
				EndSeq,
				FormLocation,
				CreatedByUserID,
				ModifiedByUserID
			)
			VALUES (
				@FormID,
				@IsUsed,
				@ShipNum,
				@ShipDate,
				@FormsPerBox,
				@TotalBoxes,
				@PartialBoxAmt,
				@BegAmt,
				@EndAmt,
				@BegSeq,
				@EndSeq,
				@FormLocation,
				@UserID,
				@UserID
			)

			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END


	-- now log it
		IF (@TOTAL > 0)
			BEGIN
				INSERT INTO [DWT].[FormDataLog] (
					[Action],
					FormID,
					IsUsed,
					ShipNum,
					ShipDate,
					FormsPerBox,
					TotalBoxes,
					PartialBoxAmt,
					BegAmt,
					EndAmt,
					BegSeq,
					EndSeq,
					FormsReq,
					Requester,
					FormLocation,
					CreatedByUserID
				)
				VALUES (
					@Action,
					@FormID,
					@IsUsed,
					@ShipNum,
					@ShipDate,
					@FormsPerBox,
					@TotalBoxes,
					@PartialBoxAmt,
					@BegAmt,
					@EndAmt,
					@BegSeq,
					@EndSeq,
					0,
					'',
					@FormLocation,
					@UserID
					)
			END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertJobConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/17/2016
-- Description:	Procedure used to insert or update Job configuration info
-- =============================================
CREATE PROCEDURE [DWT].[UpsertJobConfig] 
	@JobID int,
	@IsJobUsed bit,
	@JobName nvarchar(50),
	@FormID int,
	@UserID int
AS

DECLARE @ERRVAL int
SET @ERRVAL = -1

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[JobConfig]
	SET
		IsJobUsed = @IsJobUsed,
		JobName = @JobName,
		FormID = @FormID,
		ModifiedByUserID = @UserID,
		ModifiedDate = GETDATE()

	WHERE JobID = @JobID

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1)
		BEGIN
			INSERT INTO [DWT].[JobConfig] (
				IsJobUsed,
				JobName,
				FormID,
				CreatedByUserID,
				ModifiedByUserID
			)
			VALUES (
				@IsJobUsed,
				@JobName,
				@FormID,
				@UserID,
				@UserID
			)

			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertPrintData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/02/2016
-- Description:	Procedure used to insert or update form inventory data
-- Note: When record is first created, the CreatedByUserID and ModifiedByUserID will be the same UserID
-- =============================================
CREATE PROCEDURE [DWT].[UpsertPrintData] 
	@FormID int,
	@IsUsed bit,
	@TotalBoxes int,
	@PartialBoxAmt int,
	@BegAmt int,
	@EndAmt int,
	@BegSeq nvarchar(30),
	@EndSeq nvarchar(30),
	@FormsReq int,
	@Requester nvarchar(50),
	@UserID int
AS

DECLARE @ERRVAL int
DECLARE @Action nvarchar(30)
SET @Action = 'UPDATE-PRINT'
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[FormData]
	SET
		IsUsed = @IsUsed,
		TotalBoxes = @TotalBoxes,
		PartialBoxAmt = @PartialBoxAmt,
		BegAmt = @BegAmt,
		EndAmt = @EndAmt,
		BegSeq = @BegSeq,
		EndSeq = @EndSeq,
		ModifiedByUserID = @UserID,
		ModifiedDate = GETDATE()

	WHERE (FormID = @FormID)

	SET @ERRVAL = @@ROWCOUNT


	-- now log it
	BEGIN TRY
		INSERT INTO [DWT].[FormDataLog] (
			[Action],
			FormID,
			IsUsed,
			ShipNum,
			ShipDate,
			FormsPerBox,
			TotalBoxes,
			PartialBoxAmt,
			BegAmt,
			EndAmt,
			BegSeq,
			EndSeq,
			FormsReq,
			Requester,
			FormLocation,
			CreatedByUserID
		)
		SELECT
			@Action,
			@FormID,
			@IsUsed,
			ShipNum,
			ShipDate,
			FormsPerBox,
			@TotalBoxes,
			@PartialBoxAmt,
			@BegAmt,
			@EndAmt,
			@BegSeq,
			@EndSeq,
			@FormsReq,
			@Requester,
			FormLocation,
			@UserID
		FROM [DWT].[FormData]
		WHERE (FormID = @FormID)
	END TRY
	BEGIN CATCH
		SET @ERRVAL = ERROR_NUMBER() * -1
	END CATCH
	END TRY

BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertTWorkConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 05/23/2016
-- Description:	Procedure used to insert or update TWork configuration info
-- =============================================
CREATE PROCEDURE [DWT].[UpsertTWorkConfig] 
	@TWorkID int,
	@IsTWorkUsed bit,
	@TWorkName nvarchar(50),
	@UserID int
AS

DECLARE @ERRVAL int
SET @ERRVAL = -1

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRY
	UPDATE [DWT].[TWorkConfig]
	SET
		IsTWorkUsed = @IsTWorkUsed,
		TWorkName = @TWorkName

	WHERE TWorkID = @TWorkID

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1)
		BEGIN
			INSERT INTO [DWT].[TWorkConfig] (
				IsTWorkUsed,
				TWorkName,
				CreatedByUserID
			)
			VALUES (
				@IsTWorkUsed,
				@TWorkName,
				@UserID
			)

			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  StoredProcedure [DWT].[UpsertWorkData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mark Clark
-- Create date: 04/13/2016
-- Description:	Procedure used to insert or update hours worked data
-- Note: When record is first created, the CreatedByUserID and ModifiedByUserID will be the same UserID
-- =============================================
CREATE PROCEDURE [DWT].[UpsertWorkData] 
	@TWorkID int,
	@Hours float,
	@UserID int,
	@WorkDate datetime
AS

DECLARE @ERRVAL int

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
BEGIN TRY
	UPDATE  [DWT].[TWorkData]
	SET 
		[Hours] = @Hours,
		ModifiedDate = GETDATE()

	WHERE (TWorkID = @TWorkID AND ModifiedByUserID = @UserID AND DATEDIFF(day,CreatedDate,@WorkDate) = 0)

	SET @ERRVAL = @@ROWCOUNT

	-- no rows updated and we have data to save?
	IF (@ERRVAL < 1 AND @Hours > 0)
		BEGIN
			INSERT INTO [DWT].[TWorkData](
				TWorkID,
				[Hours],
				CreatedByUserID,
				CreatedDate,
				ModifiedByUserID
			)
			VALUES (
				@TWorkID,
				@Hours,
				@UserID,
				@WorkDate,
				@UserID
			);
			SET @ERRVAL = ISNULL(SCOPE_IDENTITY(),0)
		END
END TRY
BEGIN CATCH
	SET @ERRVAL = ERROR_NUMBER() * -1
END CATCH

SELECT @ERRVAL
RETURN

GO
/****** Object:  Table [DWT].[AppConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[AppConfig](
	[AppID] [int] IDENTITY(1,1) NOT NULL,
	[IsAppUsed] [bit] NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[Region] [bit] NOT NULL,
	[BatchNum] [bit] NOT NULL,
	[Packets] [bit] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppConfig] PRIMARY KEY CLUSTERED 
(
	[AppID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[AppData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[AppData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AppID] [int] NOT NULL,
	[Region] [int] NOT NULL,
	[BatchNum] [int] NOT NULL,
	[QtyVerified] [int] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Packets] [int] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[FormConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[FormConfig](
	[FormID] [int] IDENTITY(1,1) NOT NULL,
	[IsFormUsed] [bit] NOT NULL,
	[FormName] [nvarchar](50) NOT NULL,
	[ShipInfo] [bit] NOT NULL,
	[Sequence] [bit] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FormConfig] PRIMARY KEY CLUSTERED 
(
	[FormID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[FormData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[FormData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[IsUsed] [bit] NOT NULL,
	[ShipNum] [nvarchar](30) NOT NULL,
	[ShipDate] [datetime] NOT NULL,
	[FormsPerBox] [int] NOT NULL,
	[TotalBoxes] [int] NOT NULL,
	[PartialBoxAmt] [int] NOT NULL,
	[BegAmt] [int] NOT NULL,
	[EndAmt] [int] NOT NULL,
	[BegSeq] [nvarchar](30) NOT NULL,
	[EndSeq] [nvarchar](30) NOT NULL,
	[FormLocation] [nvarchar](50) NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FormData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[FormDataLog]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[FormDataLog](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Action] [nvarchar](30) NOT NULL,
	[FormID] [int] NOT NULL,
	[IsUsed] [bit] NOT NULL,
	[ShipNum] [nvarchar](30) NOT NULL,
	[ShipDate] [datetime] NOT NULL,
	[FormsPerBox] [int] NOT NULL,
	[TotalBoxes] [int] NOT NULL,
	[PartialBoxAmt] [int] NOT NULL,
	[BegAmt] [int] NOT NULL,
	[EndAmt] [int] NOT NULL,
	[BegSeq] [nvarchar](30) NOT NULL,
	[EndSeq] [nvarchar](30) NOT NULL,
	[FormsReq] [int] NOT NULL,
	[Requester] [nvarchar](50) NOT NULL,
	[FormLocation] [nvarchar](50) NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FormDataLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[FormDestroy]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[FormDestroy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[IsUsed] [bit] NOT NULL,
	[ShipNum] [nvarchar](30) NOT NULL,
	[ShipDate] [datetime] NOT NULL,
	[FormsPerBox] [int] NOT NULL,
	[TotalBoxes] [int] NOT NULL,
	[PartialBoxAmt] [int] NOT NULL,
	[BegAmt] [int] NOT NULL,
	[EndAmt] [int] NOT NULL,
	[BegSeq] [nvarchar](30) NOT NULL,
	[EndSeq] [nvarchar](30) NOT NULL,
	[FormLocation] [nvarchar](50) NOT NULL,
	[FormsShredded] [int] NOT NULL,
	[BoxesShredded] [int] NOT NULL,
	[Note] [nvarchar](512) NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FormDestroy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[Images]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[Images](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ImageName] [nvarchar](100) NOT NULL,
	[Image] [image] NOT NULL,
	[UserID] [int] NULL,
	[ImageType] [nvarchar](30) NULL,
 CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [DWT].[JobConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[JobConfig](
	[JobID] [int] IDENTITY(1,1) NOT NULL,
	[IsJobUsed] [bit] NOT NULL,
	[JobName] [nvarchar](50) NOT NULL,
	[FormID] [int] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_JobConfig] PRIMARY KEY CLUSTERED 
(
	[JobID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[PrintData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[PrintData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FormID] [int] NOT NULL,
	[QtyPrinted] [int] NOT NULL,
	[QtyFormsUsed] [int] NOT NULL,
	[RequestorID] [int] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PrintData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[TWorkConfig]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[TWorkConfig](
	[TWorkID] [int] IDENTITY(1,1) NOT NULL,
	[IsTWorkUsed] [bit] NOT NULL,
	[TWorkName] [nvarchar](50) NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TWorkConfig] PRIMARY KEY CLUSTERED 
(
	[TWorkID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[TWorkData]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[TWorkData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TWorkID] [int] NOT NULL,
	[Hours] [float] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedByUserID] [int] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_TWorkData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [DWT].[Users]    Script Date: 5/25/2016 3:21:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [DWT].[Users](
	[UserID] [int] NOT NULL,
	[RacfID] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](70) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsAdmin] [bit] NOT NULL,
	[ImageID] [int] NOT NULL,
	[CreatedByUserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
 CONSTRAINT [UQ_User] UNIQUE NONCLUSTERED 
(
	[UserID] ASC,
	[RacfID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [DWT].[AppConfig] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[AppData] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[AppData] ADD  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [DWT].[FormConfig] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[FormData] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[FormData] ADD  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [DWT].[FormDataLog] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[FormDestroy] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[FormDestroy] ADD  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [DWT].[JobConfig] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[PrintData] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[TWorkConfig] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[TWorkData] ADD  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [DWT].[TWorkData] ADD  DEFAULT (getdate()) FOR [ModifiedDate]
GO
ALTER TABLE [DWT].[Users] ADD  CONSTRAINT [DF_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
