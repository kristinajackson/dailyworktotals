﻿<%@ Page Title="" Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Shipping.aspx.cs" Inherits="DailyWT.Admin.Shipping" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true">
    </asp:ScriptManager>
   <style>
           .shipHeader {
            vertical-align: central;
            /*background: #FFFFFF;
            background: #FFFFFF;
            background: #FFFFFF;*/
            text-align: center;
            font-size:small;

        }
   </style>
    <script type="text/javascript">
        function myFunction() {
            
            var r = confirm("Are You Certain You Want to Delete?");
            if (r == true) {
                return true;
                
            } else {
                return false;
            }
        }
</script>
    <%--<br />
    <br />
    <br />--%>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="test" runat="server" ></asp:Label>
                                <div class="col-xs-12" style="margin-left:-20%; padding:2%;" >
 <div class="container"  id="section1" style="background-color:#FFFFFF; margin-left:7%;  border-radius:5%; width:125%; padding:2%;">
        
        
        <div class=" panel panel-default" >
            <br />
            <div class="panel-heading" style="border-radius:5%;">
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Shipping Module</span></div>
                                               <div class="container">
                <div class="panel-body">
                    <div id="alertsection" style="margin-left:-5%;">
                <div class="alert alert-danger col-md-12" runat="server" id="danger" visible="false" role="alert">
                    <strong>ERROR!</strong> Record was not saved. Please check your work
                </div>

                <div class="alert alert-success col-md-12" runat="server" id="alert" visible="false" role="alert">
                    <strong>Success!</strong> Record entered successfully.
                </div>
                      <div class="alert alert-warning col-md-12" runat="server" id="delete" visible="false" role="alert">
                    <strong>Success!</strong> Record deleted successfully.
                </div>
                        </div>
                                <asp:ListView ID="ListView1" runat="server" DataKeyNames="ID" DataSourceID="SqlDataSource1"  InsertItemPosition="LastItem" OnDataBound="ListView1_DataBound" OnItemDeleting="ListView1_ItemDeleting" OnItemInserted="ListView1_ItemInserted" OnPagePropertiesChanged="ListView1_PagePropertiesChanged" OnItemDeleted="ListView1_ItemDeleted" OnItemInserting="ListView1_ItemInserting">
            <LayoutTemplate>
                <table id="Table1" runat="server" style="width:142%; margin-left:-25%;">
                    <tr id="Tr1" runat="server" >
                        <td id="Td1" runat="server">
                            <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width:thick; font-family: Verdana, Arial, Helvetica, sans-serif;  margin-left:0%; padding-left:0%;" class="table table-bordered">
                                <tr id="Tr2" runat="server" style="background-color: #DCDCDC; color: #000000;">
                                    <%--<th id="Th1" runat="server" class="shipHeader" style="width:4%;">Action</th>--%>
                                    <th id="Th2" runat="server" class="shipHeader" style="width:4%;">Shipping Date</th>
                                    <th id="Th3" runat="server" class="shipHeader" style="width:4%;">Date Received</th>
                                    <th id="Th4" runat="server" class="shipHeader" style="width:4%;">Shipping Number</th>
                                    <th id="Th5" runat="server" class="shipHeader" style="width:4%;">Job Name</th>
                                    <th id="Th6" runat="server" class="shipHeader" style="width:4%;">Purchase Order</th>
                                    <th id="Th7" runat="server" class="shipHeader" style="width:4%;">Total Boxes</th>
                                    <th id="Th8" runat="server" class="shipHeader" style="width:4%;">Total Forms</th>
                                    <th id="Th9" runat="server" class="shipHeader" style="width:4%;">Beg Sequence Number</th>
                                    <th id="Th10" runat="server" class="shipHeader" style="width:4%;">End Sequence Number</th>
                                      <th id="Th12" runat="server" class="shipHeader" style="width:4%;">Location</th>
                                    <th id="Th11" runat="server"  class="shipHeader" style="width:4%;" >Comments</th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server" class="">
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered" style="width:100%; top:0; margin-left:-5%;">
                    <tr id="Tr3" runat="server" style="background-color: #DCDCDC; color: #000000;">
                        <td id="Td2" runat="server" style=" text-align: center; background-color: #DCDCDC; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; padding:2%; width:100%;" class="form-group">
                            <asp:DataPager ID="DataPager1" runat="server" PageSize="3">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" ButtonCssClass="btn btn-default" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <EmptyDataTemplate>
                <table runat="server" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width:thin">
                    <tr>
                        <td>No data was returned.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <ItemTemplate>
                <table class="table table-bordered ">
                    <tr class=" <%# Convert.ToBoolean(Container.DataItemIndex % 2) ? "alt-theme" : " w3-theme" %> ">
                   <%--     <td class="form-group" style="width: 4%;">
                            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete Record" Font-Size="Small" CssClass="btn btn-rounded btn-xs " OnClientClick="return myFunction();" Width="100%"  />
                        </td>--%>
                        <td class="form-group" style=" text-align: center; width: 4%;">
                            <asp:Label ID="ShippingdateLabel" runat="server" Text='<%# Eval("Shippingdate","{0:MM/dd/yyyy}") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style="text-align: center; width: 4%;">
                            <asp:Label ID="DateReceivedLabel" runat="server" Text='<%# Eval("DateReceived","{0:MM/dd/yyyy}") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style=" text-align: center; width: 4%;">
                            <asp:Label ID="ShippingNumLabel" runat="server" Text='<%# Eval("ShippingNum") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style=" text-align: center; width: 4%;">
                            <asp:Label ID="FormNameLabel" runat="server" Text='<%# Eval("FormName") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style=" text-align: center; width: 4%;">
                            <asp:Label ID="PurchaseOrderLabel" runat="server" Text='<%# Eval("PurchaseOrder") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style=" text-align: center; width: 4%;">
                            <asp:Label ID="TotalBoxesLabel" runat="server" Text='<%# Eval("TotalBoxes") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style="text-align: center; width: 4%;">
                            <asp:Label ID="TotalFormsLabel" runat="server" Text='<%# Eval("TotalForms") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style="text-align: center; width: 4%;">
                            <asp:Label ID="BegSeqLabel" runat="server" Text='<%# Eval("BegSeq") %>' Font-Size="Small" Width="100%" />
                        </td>
                        <td class="form-group" style=" text-align: center; width: 4%;">
                            <asp:Label ID="EndSeqLabel" runat="server" Text='<%# Eval("EndSeq") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style="width: 4%; text-align: center;">
                            <asp:Label ID="FormLocationLabel" runat="server" Text='<%# Eval("FormLocation") %>' Font-Size="Small" Width="100%"/>
                        </td>
                        <td class="form-group" style="width: 4%; text-align: center;" runat="server" >
                            <asp:Label ID="CommentLabel" runat="server" Text='<%# Eval("Comment") %>' Font-Size="Small" Width="100%"/>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
            <InsertItemTemplate>
                <asp:Panel ID="pnlInsert" runat="server" DefaultButton="InsertButton">
                    <table class="table table-bordered">
                        <tr >
                            <td class="form-group" style=" text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5; border-top-width:thick; width: 4%;">
                                <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Add New Shipment" Font-Size="X-Small" ValidationGroup="ship" CssClass="btn btn-rounded btn-xs btn-success" OnClick="InsertButton_Click" ToolTip="Add Shipment" OnClientClick="return confirm('Are You Certain You Want to Save?');" />
                                <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear Form" Font-Size="X-Small" CssClass="btn btn-default btn-rounded btn-xs" ToolTip="Clear Textboxes" />
                              <button type="button" id="btnCancel" class="btn btn-rounded btn-xs btn-danger" style="font-size:x-small;" onclick="NewPage('/User/LandingPage.aspx')">Cancel</button>
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5;border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="ShippingdateTextBox" runat="server" Text='<%# Bind("Shippingdate") %>' CssClass="form-control" placeholder="ShipDate" Font-Size="X-Small" Width="100%"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="ShippingdateTextBox" PopupButtonID="ImageButton1"></asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorShipDate" runat="server" ErrorMessage="Required Field" ControlToValidate="ShippingdateTextBox" Display="Dynamic" Font-Size="X-Small" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                                  <asp:MaskedEditExtender ID="MaskedEditExtenderShippingdateTextBox" runat="server" TargetControlID="ShippingdateTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="99/99/9999" MaskType="Date" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorShippingdateTextBox" runat="server" ErrorMessage="Must be date format (MM/DD/YYYY)" Font-Size="X-Small" ValidationExpression="^([0-9]|0[1-9]|1[012])\/([0-9]|0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$" ValidationGroup="ship" Font-Bold="True"  ControlToValidate="ShippingdateTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5;border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="DateReceivedTextBox" runat="server" Text='<%# Bind("DateReceived") %>' CssClass="form-control" placeholder="DateRec"  Font-Size="X-Small" Width="100%"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtenderDateReceivedTextBox" runat="server" TargetControlID="DateReceivedTextBox" PopupButtonID="ImageButton2"></asp:CalendarExtender>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDateRecieve" runat="server" ErrorMessage="Required Field" ControlToValidate="DateReceivedTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                                  <asp:MaskedEditExtender ID="MaskedEditExtenderDateReceivedTextBox" runat="server" TargetControlID="DateReceivedTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="99/99/9999" MaskType="Date" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorDateReceivedTextBox" Font-Size="X-Small"  runat="server" ErrorMessage="Must be date format (MM/DD/YYYY)" ValidationExpression="^([0-9]|0[1-9]|1[012])\/([0-9]|0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$" ValidationGroup="ship" Font-Bold="True" ControlToValidate="DateReceivedTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color:#F5F5F5;border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="ShippingNumTextBox" runat="server" Text='<%# Bind("ShippingNum") %>' CssClass="form-control" placeholder="ShipNum"  Font-Size="X-Small" Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorShipNumber" Font-Size="X-Small" runat="server" ErrorMessage="Required Field" ControlToValidate="ShippingNumTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True" ></asp:RequiredFieldValidator>
<%--                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorShippingNumTextBox" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="ship" Font-Bold="True" ControlToValidate="ShippingNumTextBox" SetFocusOnError="True" Font-Size="X-Small" Display="Dynamic"></asp:RegularExpressionValidator> --%>
                            </td>
                            <td class="form-group" runat="server" visible="false">
                                <asp:TextBox ID="ActionTextBox" runat="server" Text='<%# Bind("Action") %>' CssClass="form-control" Width="5%" />
                            </td>
                            <td class="form-group" runat="server" visible="false">
                                <asp:TextBox ID="FormIDTextBox" runat="server" Text='<%# Bind("FormID") %>' CssClass="form-control" Width="4%" />
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5;border-top-width:thick; width: 7%;">
                                <asp:TextBox ID="FormNameTextBox" runat="server" Text='<%# Bind("FormName") %>' CssClass="form-control" placeholder="Name" Visible="false"  Width="100%"></asp:TextBox>
                                <asp:DropDownList ID="ddlForm" placeholder="InventoryName" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="dropdown form-control"  Width="100%" Font-Size="X-Small"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorFormName" Font-Size="X-Small" runat="server" ErrorMessage="Required Field" ControlToValidate="ddlForm" Display="Dynamic" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5;border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="PurchaseOrderTextBox" runat="server" Text='<%# Bind("PurchaseOrder") %>' CssClass="form-control" placeholder="PurchaseOr"  Font-Size="X-Small" Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorPurchaseOrderTextBox" Font-Size="X-Small" runat="server" ErrorMessage="Required Field" ControlToValidate="PurchaseOrderTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5;border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="TotalBoxesTextBox" runat="server" Text='<%# Bind("TotalBoxes") %>' CssClass="form-control" placeholder="TotalBoxes" Font-Size="X-Small" Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTotalBoxesTextBox" Font-Size="X-Small" runat="server" ErrorMessage="Required Field" ControlToValidate="TotalBoxesTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorTotalBoxesTextBox" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="ship" Font-Bold="True" ControlToValidate="TotalBoxesTextBox" SetFocusOnError="True" Font-Size="X-Small" Display="Dynamic"></asp:RegularExpressionValidator> 
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5;border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="TotalFormsTextBox" runat="server" Text='<%# Bind("TotalForms") %>' CssClass="form-control" placeholder="TotalForms"  Font-Size="X-Small" Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorTotalFormsTextBox" Font-Size="X-Small" runat="server" ErrorMessage="Required Field" ControlToValidate="TotalFormsTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="ship" SetFocusOnError="True" ></asp:RequiredFieldValidator>
                           <asp:RegularExpressionValidator ID="RegularExpressionValidatorTotalFormsTextBox" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="ship" Font-Bold="True" ControlToValidate="TotalFormsTextBox" SetFocusOnError="True" Font-Size="X-Small" Display="Dynamic"></asp:RegularExpressionValidator> 
                                 </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5; border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="BegSeqTextBox" runat="server" Text='<%# Bind("BegSeq") %>' CssClass="form-control" placeholder="BegSeq" Font-Size="X-Small" />
                            </td>
                            <td class="form-group" style="text-align: center; background-color: #FAFAD2; border-top-color: #F5F5F5; border-top-width:thick; width: 4%;">
                                <asp:TextBox ID="EndSeqTextBox" runat="server" Text='<%# Bind("EndSeq") %>' CssClass="form-control" placeholder="EndSeq"  Font-Size="X-Small" />
                            </td>
                            <td class="form-group" style="text-align: center; word-wrap: break-word; background-color: #FAFAD2; border-top-color: #F5F5F5; border-top-width:thick; width: 5%;">
                                <asp:DropDownList ID="FormLocationTextBox" placeholder="FormLoc"  AppendDataBoundItems="true" runat="server" SelectedValue='<%# Bind("FormLocation") %>'  CssClass="dropdown form-control" Font-Size="X-Small" Width="100%" >
                                    <asp:ListItem>- Select Location -</asp:ListItem>
                                    <asp:ListItem>IT PRINT OP</asp:ListItem>
                                    <asp:ListItem>PRINT SHOP</asp:ListItem>
                                </asp:DropDownList>
                            </td> 
                            <td class="form-group" style="text-align: center; word-wrap: break-word; background-color: #FAFAD2; border-top-color: #F5F5F5; border-top-width:thick; width: 4%;" runat="server" >
                                <asp:TextBox ID="CommentTextBox" runat="server" Text='<%# Bind("Comment") %>' CssClass="form-control" placeholder="Comment" Font-Size="X-Small" Width="100%" ></asp:TextBox>
                            </td>
                            <td class="form-group" runat="server" visible="false">
                                <asp:TextBox ID="CreatedByUserIDTextBox" runat="server" Text='<%# Bind("CreatedByUserID") %>' />
                            </td>

                        </tr>
                        <tr >
                            <td style="border:none; border-color:#FFFFFF;"></td>
                            <td style="text-align: center; top:0%; ">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.png" /></td>
                            <td style="text-align: center;  top:0%;">
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.png" /></td>
                        </tr>
                    </table>
                </asp:Panel>
            </InsertItemTemplate>
        </asp:ListView>
            </div>
</div>
            </div>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" DeleteCommand="DELETE FROM DWT.Shipping WHERE (ID = @ID)" InsertCommand="INSERT INTO DWT.Shipping(Shippingdate, DateReceived, ShippingNum, Action, FormID, FormName, PurchaseOrder, TotalBoxes, TotalForms, BegSeq, EndSeq, Comment, CreatedByUserID, FormLocation) VALUES (@Shippingdate, @DateReceived, @ShippingNum, @Action, @FormID, @FormName, @PurchaseOrder, @TotalBoxes, @TotalForms, @BegSeq, @EndSeq, @Comment, @CreatedByUserID, @FormLocation)" SelectCommand="SELECT ID, Shippingdate, DateReceived, ShippingNum, Action, FormID, FormName, PurchaseOrder, TotalBoxes, TotalForms, BegSeq, EndSeq, Comment, CreatedByUserID, CreatedDate, FormLocation, IsUsed FROM DWT.Shipping WHERE IsUsed = 1 ORDER BY CreatedDate DESC">
            <DeleteParameters>
                <asp:Parameter Name="ID" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Shippingdate" />
                <asp:Parameter Name="DateReceived" />
                <asp:Parameter Name="ShippingNum" />
                <asp:Parameter Name="Action" />
                <asp:Parameter Name="FormID" />
                <asp:Parameter Name="FormName" />
                <asp:Parameter Name="PurchaseOrder" />
                <asp:Parameter Name="TotalBoxes" />
                <asp:Parameter Name="TotalForms" />
                <asp:Parameter Name="BegSeq" />
                <asp:Parameter Name="EndSeq" />
                <asp:Parameter Name="Comment" />
                <asp:Parameter Name="CreatedByUserID" />                
                <asp:Parameter Name="FormLocation" />
            </InsertParameters>
        </asp:SqlDataSource>
            
<div class="col-xs-14">
                        <div class="panel-footer">
         

    </div>
    </div>
        </div></div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
</asp:Content>

