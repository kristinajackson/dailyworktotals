﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FormConfig.aspx.cs" Inherits="DailyWT.Admin.FormConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ScriptManager>
             <style>
        /*#section1 {
        margin-left:-30%;
        padding-left:0px;
        }*/

    </style>
<script type="text/javascript">
    // this is run when the user clicks the submit button
    $(document).ready(function () {
        $('#mpForm')
            .bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                }
                //,
                //fields: {
                //    tbAddForm: {
                //        validators: {
                //            regexp: {
                //                regexp: /^[A-Za-z0-9\ \-]{1,50}$/,
                //                message: 'Must be between 1-50 alphanumeric chars'
                //            }
                //        }
                //    }
                //}
            }
            )  // end registration form
    });


    </script>
    <script>
        function searchKeyPress(e)
        {
             e = e || window.event;
        if (e.keyCode == 13)
            {
            document.getElementById('ibAddForm').click();
                return false;
            }
            return true;
        };
    </script>

<!-- Section 1 -->
<section class="container" id="section1">
    <div class="col-xs-9">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Job Configuration Module</span></div>
<br />

<div class="container-fluid">

    <hr />
      <div class="h4 form-group">
        <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Add New Job</span></div>
        <br />
        <label class="v-align-tc">&nbsp;&nbsp;&nbsp;&nbsp<input id="tbAddForm" name="tbAddForm" type="text" placeholder="New Job" onkeypress="return searchKeyPress(event);"/></label>
        <label class="v-align-tc MousePointer" onclick="__doPostBack('AddForm', '');" title="Enter new Job name, then click the plus sign!">&nbsp;&nbsp;&nbsp;
            <img id="ibAddForm" class="MyIcons" src='<%= Page.ResolveClientUrl("~/Images/PlusGoogle.png") %>' />&nbsp;&nbsp;&nbsp;Add Job&nbsp;&nbsp;&nbsp;
        </label>
    </div>
    <hr />
    <div class="h4" runat="server" visible="false">
        <label class="col-xs-6 control-label">
            Shipping Info</label>
        <div>
            <label class="v-align-tc"><input id="cbShipInfo" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Used" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
        <div class="h4 form-group">
        <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Add Job Specifications</span></div>
        <br />
        <label class="v-align-tc">&nbsp;&nbsp;&nbsp;&nbsp<asp:DropDownList ID="ddlForm" placeholder="Select Job" runat="server" OnSelectedIndexChanged="ddlForm_Selected"  AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
     <label class="v-align-tc MousePointer"><button id="Button1" type="submit" name="btnSave" onclick="__doPostBack('Save', '');" style="background: none; border: none; text-align:center; font-weight:bold;" title="Save Job specification" runat="server"  >
        <img class="MyIcons" src="../Images/PlusGoogle.png" />&nbsp;&nbsp;Save Changes&nbsp;&nbsp;</button>&nbsp;&nbsp;           
         </label>     
        </div>
       <%-- </div>--%>
    <div class="h4" runat="server" >
        <label class="v-align-tc col-xs-4 control-label">Sequence Numbers</label>
        <div>
            <label class="v-align-tc"><input id="cbSequence" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Use SeqNum" data-off="Not Used" data-size="small"/></label>
        </div>
        </div>
  
    
    <div class="h4 form-group" runat="server">
         <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Delete Job</span></div>
        <br />
        <label class="v-align-tc">&nbsp;&nbsp;&nbsp;&nbsp<asp:DropDownList ID="ddlFormsRemove" placeholder="Select Job" runat="server"  AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <label class="v-align-tc MousePointer" onclick="__doPostBack('SaveRemove_Click', '');" title="Select Job name, then click the delete sign!">&nbsp;&nbsp;&nbsp;
            <img id="Img1" class="MyIcons" src='<%= Page.ResolveClientUrl("../Images/deletered.png") %>' />&nbsp;&nbsp;&nbsp;Remove Job&nbsp;&nbsp;&nbsp;
        </label>
    </div>

</div>
    <br />
    <br />
    
<div class="col-xs-14">
<div class="panel-footer">
   
            <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>
    </div>

</div>

</div>
   
</section>


</asp:Content>
