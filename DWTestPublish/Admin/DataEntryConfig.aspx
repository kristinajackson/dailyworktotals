﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntryConfig.aspx.cs" Inherits="DailyWT.Admin.DataEntryConfig" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <style type="text/css">
    .submit1 {
        /*border: 1px solid #563d7c;
        border-radius: 5px;
        color: white;
        padding: 5px 10px 5px 25px;*/
        background: url(/Images/deletered.png) 
            left 3px top 5px no-repeat #563d7c;
    }
        </style>
<script type="text/javascript">
    // this is run when the user clicks the submit button
    $(document).ready(function () {
        $('#mpForm')
            .bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    tbAddApp: {
                        validators: {
                            regexp: {
                                regexp: /^[A-Za-z0-9\ \-]{1,50}$/,
                                message: 'Must be between 1-50 alphanumeric chars'
                            }
                        }
                    }
                }
            })  // end registration form
    });

    function deleteApp()
    {
        page
    }
    </script>

<!-- Section 1 -->
<section class="container-fluid toggle col-xs-8" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Data Entry Configuration</span></div>
<div class="container-fluid">
    <div class="h4 form-group">
        <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Add New Application</span></div>
        <br />
        <table class="table table-condensed">

                    <tr class="h4 v-align" id="tr1" runat="server">
                        <td>
        <label class="v-align-tc">&nbsp;&nbsp;&nbsp;&nbsp<asp:Textbox ID="tbAddApp" runat="server"  placeholder="New Application" /></label>
                             </td>
                        <td>
        <label class="v-align-tc MousePointer" onclick="__doPostBack('SaveNew_Click', '');" title="Enter new Application name, then click the plus sign!" style="font-size: x-large">&nbsp;&nbsp;&nbsp;
            <img id="ibAddApp" class="MyIcons" src='<%= Page.ResolveClientUrl("~/Images/PlusGoogle.png") %>' />&nbsp;&nbsp;&nbsp;Add Application&nbsp;&nbsp;&nbsp;
        </label>
       </td>
                        <td>&nbsp;</td>
                    </tr>                                 
                </table>
   </div>

     <hr />
    <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Delete Application</span></div>
    <table class="table table-condensed">

                    <tr class="h4 v-align" id="trJForms" runat="server">
                        <td>
                            <asp:DropDownList ID="ddlapprem" runat="server" AppendDataBoundItems="true" Font-Size="Large"></asp:DropDownList>
                            </td>
                        <td>
                        <label class="v-align-tc MousePointer btn-group" style="position:relative" title="Select Application name, then click the remove sign!" role="group">
                            <img class="MyIcons" src="../Images/deletered.png" />
                            <asp:Button ID="Button3" runat="server" Text="Remove Application" style="background: none; border: none;" OnClick="SaveRemove_Click"  title="Select Application, then click the delete sign" Font-Size="Large" /></label>

                        </td>
                        <td>&nbsp;</td>
                    </tr>                                 
                </table>

         <hr />
    <div class="panel-heading" style="background-color:#f7f8f9;">
        
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Add Application Configuration</span></div>
    <br />
    <label class="v-align-tc">&nbsp;&nbsp;&nbsp;&nbsp<asp:DropDownList ID="ddlApp" placeholder="Select Application" runat="server" OnSelectedIndexChanged="ddlApp_Selected"  AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
   <br />
     <div class="h4">
        <label class="col-xs-6 control-label">
            Region</label>
        <div>
            <label class="v-align-tc"><input id="cbRegion" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show Region" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
    <div class="h4">
        <label class="col-xs-6 control-label">
            Batch Numbers</label>
        <div>
            <label class="v-align-tc"><input id="cbBatchNum" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show Batch Number" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
    <div class="h4">
        <label class="col-xs-6 control-label">
            Packets</label>
        <div>
            <label class="v-align-tc"><input id="cbPackets" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show Packets" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
      <div class="h4">
        <label class="col-xs-6 control-label">
            Angular</label>
        <div>
            <label class="v-align-tc"><input id="cbQuantity" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show Angular" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
        <div class="h4">
        <label class="col-xs-6 control-label">
            Verified Quantity</label>
        <div>
            <label class="v-align-tc"><input id="cbVerQuantity" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show VerQuan" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
     <div class="h4" runat="server" visible="false">
        <label class="col-xs-6 control-label">
            Total Forms</label>
        <div>
            <label class="v-align-tc"><input id="cbTotalForms" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show TF" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
    <div class="h4">
        <label class="col-xs-6 control-label">
            Interviews</label>
        <div>
            <label class="v-align-tc"><input id="cbNumberInterview" runat="server" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Show Interv" data-off="Not Used" data-size="small"/></label>
        </div>                
        </div>
       

</div>
    <div class="panel-footer">
    <button type="submit" name="btnSave" onclick="__doPostBack('Save', '');" class="btn btn-success btn-lg" >
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="button" id="Button1" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>



</div>


</section>



</asp:Content>
