﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="JobConfig.aspx.cs" Inherits="DailyWT.User.JobConfig" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #section1 {
        /*margin-left:-30%;
        padding-left:0px;*/
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ScriptManager>
    <script type="text/javascript">
        // this is run when the user clicks the submit button
        $(document).ready(function () {
            $('#mpForm')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    }
                })  // end job form
        });
    </script>
        <script>
            function searchKeyPress(e) {
                //e = e || window.event;
                if (e.keyCode == 13) {
                    document.getElementById('ibAddJob').click();
                    return false;
                }
                document.location.reload();
                return true;
            };
    </script>

    <!-- Section 1 -->
    <section class="container " id="section1">
        <div class="col-lg-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;FormType Configuration Module</span>
            </div>
            <div class="container-fluid">
                <div class="h4 form-group"></div>
                <asp:Label ID="lblmsg" runat="server"  ForeColor="#FF3300" Font-Size="Large" Font-Bold="True" ></asp:Label>
                      <div class="h4 form-group">
                    <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Add New FormType</span></div>
                    <br />
                    <label class="v-align-tc">&nbsp;&nbsp;&nbsp;&nbsp<input name="tbAddJob" id="tbAddJob"  type="text" placeholder="New FormType"  /></label>
                    <label class="v-align-tc MousePointer" title="Enter new FormType name, then click the plus sign!" onclick="__doPostBack('AddJob', '');"  onkeypress="return searchKeyPress(event);">
                        &nbsp;&nbsp;&nbsp;
                    <img id="ibAddJob" class="MyIcons" src='<%= Page.ResolveClientUrl("~/Images/PlusGoogle.png") %>' onkeypress="return searchKeyPress(event);" />&nbsp;&nbsp;&nbsp;Add FormType&nbsp;&nbsp;&nbsp;</label>
                </div>
                <hr />
                <div class="panel-heading" style="background-color:#f7f8f9;">
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Add Job To FormType</span></div>
                <br />
                   <p class="form-text text-muted">&nbsp;&nbsp;Instructions: Select a FormType first then job name. Click Add Job to FormType</p>
                <br />
                    <table class="table table-condensed">
                     <tr class="h4 v-align" id="trAForms" runat="server">
                         <td><label class="v-align-tc">&nbsp;<asp:DropDownList ID="ddlJob" runat="server" OnSelectedIndexChanged="ddlJob_Selected" AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                         </td>
                         </tr>
                         <tr id="Tr2" class="h4 v-align " runat="server">
                        <td>
                            <asp:DropDownList ID="ddlAForms" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem>-- Select Job --</asp:ListItem>
                            </asp:DropDownList></td>
                      </tr>
                    <tr class="h4 v-align " runat="server">
                           <td class="v-align-tc" >
                             <button id="Button3" type="submit" runat="server" style="background: none; border: none; text-align:center;" onserverclick="SaveChanges_Click" title="Select FormName, then click the plus sign!">
                             <label class="v-align-tc MousePointer">   <img class="MyIcons" src="../Images/PlusGoogle.png" />&nbsp;&nbsp;&nbsp;Add Job to Formtype</button>
                     </label>  </td>
                    </tr>
                </table>

                <div class="panel-heading" style="background-color:#f7f8f9;" runat="server" >
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;Delete FormType</span></div>
                <br />
                <table class="table table-condensed" runat="server" >
                    <tr class="h4 v-align" id="tr1" runat="server">
                        <td>
                            <asp:DropDownList ID="ddlJjob" runat="server" AppendDataBoundItems="true" AutoPostBack="true"></asp:DropDownList></td>
                        <td>
                            <button id="ibRemoveJob" type="submit" runat="server" style="background: none; border: none;" onserverclick="SaveRemove_Click" title="Select FormType, then click the delete sign">
                                <img class="MyIcons" src="../Images/deletered.png" />&nbsp;&nbsp;&nbsp;Remove FormType</button></td>
                        <td>&nbsp;</td>
                    </tr>
          
                </table>

                <div class="panel-heading" style="background-color:#f7f8f9;" runat="server" >
 <span class="panel-title h3" style="font-weight:bold;">&nbsp;&nbsp;&nbsp;View Data</span></div>
                <br />
            <label class="v-align-tc">    <rsweb:ReportViewer ID="ReportViewerjobformtype" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="100%" ShowBackButton="False" ShowExportControls="False" ShowFindControls="False" ShowPrintButton="False" SizeToReportContent="True" PromptAreaCollapsed="True">
                    <LocalReport ReportEmbeddedResource="DailyWT.Reports.FormtypeJob.rdlc" ReportPath="Reports/FormtypeJob.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DailyWT.App_Code.ViewjobformtypeTableAdapters.DataTable1TableAdapter"></asp:ObjectDataSource>
            </label>    <br />

            </div>
            <div class="panel-footer">
                <button id="Button1" type="submit" name="btnSave" onclick="__doPostBack('Save', '');" class="btn btn-success btn-lg" title="Save form specification" runat="server" visible="false" >
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;
                <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
                    &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
            </div>

        </div>

        </div>
    </section>

</asp:Content>
