﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Reprint.aspx.cs" Inherits="DailyWT.User.Reprint" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager> 
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        <section class="container " id="section1" >
    <div class="col-sm-13" style="margin-left:-5%;">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Reprints Module</span></div>
<div class="container toggle ">
        <%--<div id="Div1" style ="background-color: #f4f4f4; width:100%;" class="page-header" >
         <asp:Image ID="Image1" ImageUrl="~/Images/logoheader.jpg" runat="server" Width="35%"  />   
  </div>--%>
           
        
    <div class="col-sm-12" style="margin-left:-1%;">
      <div id="alertsection" style="margin-left:1%;">
          <br />
                <div class="alert alert-danger col-md-7" runat="server" id="danger" visible="false" role="alert">
                    <strong>ERROR!</strong> Record was not saved. Please check your work
                </div>

                <div class="alert alert-success col-md-7" runat="server" id="alert" visible="false" role="alert">
                    <strong>Success!</strong> Record entered successfully.
                </div>
                      <div class="alert alert-warning col-md-7" runat="server" id="delete" visible="false" role="alert">
                    <strong>Success!</strong> Record deleted successfully.
                </div>
                        </div>

        <%--<div class="col-md-12">        
        <div class="col-md-10">Select Job:&nbsp;&nbsp<asp:DropDownList ID="ddlJob" CssClass="form-control"  OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
        <br />
    </div>--%>
        <table class="table table-striped" style="border:5px;">
           <tr >
                <td >Select Job:</td>
                <td class="col-md-5" >
                    <%--<asp:DropDownList CssClass="dropdown-toggle form-control" ID="ddl_forms" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_forms_SelectedIndexChanged" >
                    </asp:DropDownList>--%>
                          <asp:DropDownList ID="ddlJob" AutoPostBack="true" runat="server" AppendDataBoundItems="true" CssClass="form-control" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged"></asp:DropDownList>

                </td>
            </tr>
            </table>
            <table class="table table-striped" style="border:5px;" runat="server" id="maintable" visible="false">
            <tr >
                <td >Date: </td>
                <td >
                    <asp:TextBox ID="txt_date" runat="server" CssClass="form-control"></asp:TextBox></td>
            </tr>
            <tr >
                <td >Type form: </td>
                <td>
                    <asp:TextBox ID="txt_job_type" runat="server" CssClass="form-control"></asp:TextBox></td>
            </tr>
        <%--    <tr>
                <td>Identify Document: </td>
                <td>
                    <asp:DropDownList ID="ddl_identify_document" runat="server" CssClass="dropdown-toggle form-control">
                    </asp:DropDownList></td>
            </tr>--%>
            <tr>
                <td >Number of pages/cards: </td>
                <td >
                    <asp:TextBox ID="txt_num_of_docs" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txt_num_of_docs" FilterType="Numbers"></asp:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td>Start Time: </td>
                <td>
                    <asp:TextBox ID="txt_request_time" runat="server" CssClass="form-control"></asp:TextBox>
                     <asp:MaskedEditExtender ID="MaskedEditExtender1" TargetControlID="txt_request_time"
                                Mask="99:99"
                                MaskType="Time"
                                CultureName="en-us"
                                MessageValidatorTip="true"
                                runat="server" AcceptAMPM="True">
        </asp:MaskedEditExtender>
                </td>
            </tr>
            <tr>
                <td>Finish Time: </td>
                <td>
                    <asp:TextBox ID="txtfinishtime" runat="server" CssClass="form-control"></asp:TextBox>
                     <asp:MaskedEditExtender ID="MaskedEditExtender2" TargetControlID="txtfinishtime"
                                Mask="99:99"
                                MaskType="Time"
                                CultureName="en-us"
                                MessageValidatorTip="true"
                                runat="server" AcceptAMPM="True">
        </asp:MaskedEditExtender>
                </td>
            </tr>
            <tr>
                <td >Requestor: </td>
                <td >
                    <asp:TextBox ID="txt_requestor" runat="server" CssClass="form-control"></asp:TextBox></td>
            </tr>
            <tr>
                <td >Printer: </td>
                <td >
                    <asp:TextBox ID="txt_printer_used" runat="server" CssClass="form-control"></asp:TextBox></td>
            </tr>
            <tr>
                <td>Comments: </td>
                <td><textarea class='form-control' runat="server" id="txtcomment" name='tbComment' rows="3" placeholder='Comment'></textarea></td>
            </tr>
              <%--  </table>
                <table class="table table-striped" style="border:5px;">--%>
            <tr>
                <td class="col-sm-4">
                    <%--<asp:Button ID="btnNew" runat="server" Text="New Form" OnClientClick="return confirm('Are you sure you want to start a new form?');" OnClick="btnNew_Click" CssClass="btn btn-primary" ToolTip="New Form" />--%>

                </td>
                <td class="col-sm-3">
                    <asp:Button ID="btnSave" Text="Save" runat="server" Width="40%"  CssClass="btn btn-success" ToolTip="Save Form" OnClientClick="return confirm('Are you sure you want to save reprint?');" OnClick="btnSave_Click" />
                  <%--<button type="button" runat="server" id="btnsaveform" class="btn btn-success" style="width:100%;" data-toggle="modal" data-target="#myModal">Save</button>--%></td>
            </tr>
        </table>
        <br />
        <div class="panel-footer">
            <br />
            <br />
        </div>
        <br />
  </div>
</div>
</div>
</div>
</section>     
</ContentTemplate>
         </asp:UpdatePanel>
</asp:Content>
