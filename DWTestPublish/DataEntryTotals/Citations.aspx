﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeBehind="Citations.aspx.cs" Inherits="DailyWT.DataEntryTotals.Citations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">



    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- Section 1 -->
            <section class="container" id="section1">
                <div class="col-sm-13">

                    <div class=" panel panel-default">
                        <br />
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Citations Module</span>
                        </div>
                        <div>
                            <div class="container">
                                <div class="panel-body">
                                    <div class="alert alert-danger col-sm-10" runat="server" id="danger" visible="false" role="alert">
                                        <strong>ERROR!</strong> Record was not saved. Please check your work
                                    </div>

                                    <div class="alert alert-success col-sm-10" runat="server" id="alert" visible="false" role="alert">
                                        <strong>Success!</strong> Record entered successfully.
                                    </div>
                                    <div class="alert alert-warning col-sm-10" runat="server" id="delete" visible="false" role="alert">
                                        <strong>Success!</strong> Record deleted successfully.
                                    </div>
                                    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource2" InsertItemPosition="LastItem" DataKeyNames="ID" OnDataBound="ListView1_DataBound" OnItemInserted="ListView1_ItemInserted" OnPagePropertiesChanged="ListView1_PagePropertiesChanged" OnItemDeleted="ListView1_ItemDeleted" OnItemDeleting="ListView1_ItemDeleting" OnItemInserting="ListView1_ItemInserting">
                                        <LayoutTemplate>
                                            <table id="Table4" runat="server">
                                                <tr id="Tr4" runat="server">
                                                    <td id="Td3" runat="server">
                                                        <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-left: 0px; padding-left: 0px;" class="table table-bordered">
                                                            <tr id="Tr5" runat="server" style="background-color: #DCDCDC; color: #000000;">
                                                                <th id="Th6" runat="server" style="width: 200px; text-align: center;">Action</th>
                                                                <th id="Th7" runat="server" style="width: 200px; text-align: center;">Date</th>
                                                                <th id="Th8" runat="server" style="width: 200px; text-align: center;">Region</th>
                                                                <th id="Th9" runat="server" style="width: 200px; text-align: center;">Citations</th>
                                                                <th id="Th10" runat="server" style="width: 200px; text-align: center;">Disposition</th>
                                                                <th id="Th11" runat="server" visible="false">ID</th>
                                                            </tr>
                                                            <tr id="itemPlaceholder" runat="server">
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table runat="server" class="table table-bordered" style="width: 1000px;">
                                                <tr id="Tr6" runat="server" style="background-color: #DCDCDC; color: #000000;">
                                                    <td id="Td4" runat="server" style="text-align: center; font-size: 12px; background-color: #DCDCDC; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; width: 1000px;" class="form-group">
                                                        <asp:DataPager ID="DataPager1" runat="server" PageSize="4">
                                                            <Fields>
                                                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" ButtonCssClass="btn" />
                                                            </Fields>
                                                        </asp:DataPager>
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td class="form-group" style="width: 200px; text-align: center;">
                                                        <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" CssClass="btn btn-danger btn-md btn-rounded" OnClientClick="return confirm('Are You Certain You Want to Delete?');" Width="120px" />
                                                    </td>
                                                    <td class="form-group" style="width: 200px; text-align: center;">
                                                        <asp:Label ID="DateLabel" runat="server" Text='<%# Eval("Date") %>' Width="200px" />
                                                    </td>
                                                    <td class="form-group" style="width: 200px; text-align: center;">
                                                        <asp:Label ID="RegionLabel" runat="server" Text='<%# Eval("Region") %>' Width="200px" />
                                                    </td>
                                                    <td class="form-group" style="width: 200px; text-align: center;">
                                                        <asp:Label ID="CitationsLabel" runat="server" Text='<%# Eval("Citations") %>' Width="200px" />
                                                    </td>
                                                    <td class="form-group" style="width: 200px; text-align: center;">
                                                        <asp:Label ID="DispositionLabel" runat="server" Text='<%# Eval("Disposition") %>' Width="200px" />
                                                    </td>
                                                    <td class="form-group" style="width: 200px; text-align: center;" runat="server" visible="false">
                                                        <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                        <EmptyDataTemplate>
                                            <table id="Table1" runat="server" style="background-color: #000000; border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px;">
                                                <tr>
                                                    <td>No data was returned.</td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                        <InsertItemTemplate>
                                            <asp:Panel ID="pnlInsert" runat="server" DefaultButton="InsertButton">
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                            <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text=" Enter " CssClass="btn btn-success btn-md" Width="80px"  ToolTip="Save Record" />
<%--                                                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text=" Clear " CssClass="btn btn-default btn-sm" Width="55px" ToolTip="Clear Textboxes"  />--%>
                                                            <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-md btn-warning" Text=" Cancel " OnClick="btnCancel_Click" ToolTip="Exit out this module" Width="80px" />
                                                        </td>
                                                        <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                            <asp:TextBox ID="DateTextBox" runat="server" Text='<%# Bind("Date") %>' CssClass="form-control" Width="200px"></asp:TextBox>
                                                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="DateTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="99/99/9999" MaskType="Date" />
                                                            <asp:CalendarExtender ID="RTCE" runat="server" TargetControlID="DateTextBox" Format="M/dd/yyyy" PopupButtonID="ImageButton1" />
                                                        </td>
                                                        <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                            <asp:DropDownList ID="ddlRegion" AutoPostBack="true" runat="server" CssClass="form-control" Width="200px" Text='<%# Bind("Region") %>'>
                                                                <asp:ListItem>REGION 1</asp:ListItem>
                                                                <asp:ListItem>REGION 2</asp:ListItem>
                                                                <asp:ListItem>REGION 3</asp:ListItem>
                                                                <asp:ListItem>REGION 4</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                            <asp:TextBox ID="CitationsTextBox" runat="server" Text='<%# Bind("Citations") %>' CssClass="form-control" Width="200px" />
                                                        </td>
                                                        <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                            <asp:TextBox ID="DispositionTextBox" runat="server" Text='<%# Bind("Disposition") %>' CssClass="form-control" Width="200px" />
                                                            <asp:TextBox ID="Labeluserid" runat="server" Text='<%# Bind("CreatedByUserID") %>' Visible="false"></asp:TextBox>
                                                            <asp:TextBox ID="lblappid" runat="server" Visible="false" Text='<%# Bind("AppID") %>'></asp:TextBox>
                                                            <asp:TextBox ID="lblappname" runat="server" Visible="false" Text='<%# Bind("AppName") %>'></asp:TextBox>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td style="text-align: center;">
                                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.png" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </InsertItemTemplate>

                                    </asp:ListView>
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" DeleteCommand="DELETE FROM DWT.Citations WHERE (ID = @ID)" InsertCommand="INSERT INTO DWT.Citations(Date, Region, Citations, Disposition, CreatedByUserID, CreatedDate, AppID, AppName) VALUES (@Date, @Region, @Citations, @Disposition, @CreatedByUserID, GETDATE(), @AppID, @AppName)" SelectCommand="SELECT DWT.Citations.* FROM DWT.Citations ORDER BY CreatedDate DESC">
                                        <DeleteParameters>
                                            <asp:Parameter Name="ID" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="Date" />
                                            <asp:Parameter Name="Region" />
                                            <asp:Parameter Name="Citations" />
                                            <asp:Parameter Name="Disposition" />
                                            <asp:Parameter Name="CreatedByUserID" />
                                            <asp:Parameter Name="AppID" />
                                            <asp:Parameter Name="AppName" />
                                        </InsertParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
