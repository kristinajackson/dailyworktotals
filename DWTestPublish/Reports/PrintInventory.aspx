﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PrintInventory.aspx.cs" Inherits="DailyWT.Reports.PrintInventory" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <section class="container" id="section1" style="color: #000000; margin-left: -5%; height: 500px;">
                <div class="col-xs-12 col-xs-offset-1">
                    <div class="panel panel-default" style="height: 5000px;">
                        <br />
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Print Inventory Report Module
                            </span>
                        </div>
                        <div class="panel-body">

                            <div style="background-color: #f5f5f0;" runat="server" id="bodby">
                                <BR />
                                <!---------  BUTTONS ---------->
                                <div class="container" style="margin-left: 4%">
                                    <div class="row">
                                        <div class="form-group">

                                            <button id="Backbutton" runat="server" class="btn-primary" onserverclick="Backbutton_Click"><span class="glyphicon glyphicon-chevron-left"></span>Back to Reports Menu</button>

                                        </div></div></div>
                            </div>
                            <br />
                                        </div>
                            <div class="row" style="margin-left: 5%">
                                <div style="margin-bottom: 10px; margin-top: 10px;" class="col-xs-10  form-group">
                                    <br />
                                    <asp:Label ID="lblPrintInstr" runat="server" Text="*Please Note: To Print the report, click on the Export drop down menu (the blue disk icon, third button from right ) --> Select Excel --> Open the file or save it to your computer --> From MS Excel go to File --> Print --> Select Landscape Orientation --> And change the No Scaling option (the last option) to Fit All Columns on One Page  --> Click on the Print button." ForeColor="Black" BackColor="#FFFF66" Font-Bold="True" Font-Names="Times New Roman" />
                                </div>
                            </div>



                        <div class="row" style="margin-left: 5%">



                            <rsweb:ReportViewer ID="ReportViewerPrintInv" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" SizeToReportContent="True" Width="100%" Height="100%">
                                <LocalReport ReportEmbeddedResource="DailyWT.Reports.PrintInventory.rdlc" ReportPath="Reports/PrintInventory.rdlc">
                                    <DataSources>
                                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                                    </DataSources>
                                </LocalReport>
                            </rsweb:ReportViewer>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DailyWT.App_Code.PrintInventoryTableAdapters.GetPrintInvTableAdapter" OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>



                            </div>
                          </div>
                                </div>

            </section>

            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
