﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="JobConfig.aspx.cs" Inherits="DailyWT.User.JobConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 41px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<script type="text/javascript">
    // this is run when the user clicks the submit button
    $(document).ready(function () {
        $('#mpForm')
            .bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    tbAddJob: {
                        validators: {
                            regexp: {
                                regexp: /^[A-Za-z0-9\ \-]{6,50}$/,
                                message: 'Must be between 6-50 alphanumeric chars'
                            },
                            notEmpty: {
                                message: 'Must be between 6-50 alphanumeric chars'
                            }
                        }
                    }
                }
            })  // end job form
    });
    </script>
<div class="container-fluid toggle">
            <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Configuration:&nbsp;&nbsp;&nbsp;Jobs</span><span class="col-xs-1">&nbsp;</span>
                <!--hidden fields used to pass number of lines to javascript -->
                <input type="hidden" runat="server" id="hfJobs" />
            </div>
</div>
<!-- Section 1 -->
<section class="container-fluid toggle col-xs-8" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Job Configuration</span></div>
<div class="container-fluid">
    <table class="table table-condensed">
        <tr class="h4 v-align form-group">
            <td class="auto-style1"><asp:DropDownList ID="ddlJob" runat="server" OnSelectedIndexChanged="ddlJob_Selected"  AutoPostBack="true" AppendDataBoundItems="true"></asp:DropDownList></td>
            <td class="auto-style1"><label><input name="tbAddJob" id="tbAddJob" class="form-control" type="text" placeholder="New Job" /></label></td>
            <td class="auto-style1"><button id="Button1" type="submit" runat="server" style="background:none; border:none;" onserverclick="ibAddJob_Click" ><img class="MyIcons"src="../Images/PlusGoogle.png" />&nbsp;&nbsp;&nbsp;Add Job</button></td>
        </tr>
        <tr class="h4 v-align" id="trJForms" runat="server">
            <td><asp:DropDownList ID="ddlJForms" runat="server" AppendDataBoundItems="true" AutoPostBack="true"></asp:DropDownList></td>
            <td><button id="Button2" type="submit" runat="server" style="background:none; border:none;" onserverclick="ibRemoveJForm_Click" ><img class="MyIcons"src="../Images/deletered.png" />&nbsp;&nbsp;&nbsp;Remove Form</button></td>
            <td>&nbsp;</td>
        </tr>
        <tr class="h4 v-align" id="trAForms" runat="server">
            <td><asp:DropDownList ID="ddlAForms" runat="server" AppendDataBoundItems="true"></asp:DropDownList></td>
            <td> 
           <button id="Button3" type="submit" runat="server" style="background:none; border:none;" onserverclick="ibAddJForm_Click" ><img class="MyIcons"src="../Images/PlusGoogle.png" />&nbsp;&nbsp;&nbsp;Add Form</button>
              </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</div>
<div class="panel-footer">
<%--    <button type="submit" name="btnSave" onclick="Save_Click" class="btn btn-success btn-lg" >--%>
<%--    <button type="submit" name="btnSave" onclick="__doPostBack('Save', '');" class="btn btn-success btn-lg" >
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;--%>
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>

</div>


</section>

</asp:Content>
