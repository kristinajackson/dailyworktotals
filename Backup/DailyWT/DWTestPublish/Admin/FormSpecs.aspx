﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FormSpecs.aspx.cs" Inherits="DailyWT.Admin.FormSpecs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<div id="pnlUpload" runat="server" visible="true">
    <asp:FileUpload ID="FileUpload1" CssClass="bg-primary fUpload" runat="server"  /><br />
    <asp:Button ID="btnUpload" runat="server" Text="Upload Form Specification" CssClass="btn btn-info btn-sm" OnClick="btnUpload_Click" />
</div>
<br />

<asp:GridView ID="gvFormSpecs" runat="server" DataKeyNames="FormID" Width="1200px"
        AutoGenerateColumns="false" CssClass="ListViewTheme" AllowSorting="true"
        OnRowCommand="gvRowCommand" OnSorting="gvSorting">
<Columns>
    <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/Print.png"
        ControlStyle-CssClass="mybutton2"
        ControlStyle-Width="70%"
        ControlStyle-BorderColor="#005c9c"
        ItemStyle-BorderColor="#005c9c"
        ItemStyle-VerticalAlign="Top"
        ItemStyle-HorizontalAlign="Center"
        ItemStyle-CssClass="mybutton2" 
        HeaderStyle-BorderColor="#005c9c"
        HeaderStyle-ForeColor="White"
        HeaderStyle-BackColor="#005c9c" HeaderStyle-Width="2%"
        ShowHeader="true" 
        CommandName="PrintForm" />
    <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/Download.png"
        ControlStyle-CssClass="mybutton2"
        ControlStyle-Width="70%"
        ControlStyle-BorderColor="#005c9c"
        ItemStyle-BorderColor="#005c9c"
        ItemStyle-VerticalAlign="Top"
        ItemStyle-HorizontalAlign="Center"
        ItemStyle-CssClass="mybutton2" 
        HeaderStyle-BorderColor="#005c9c"
        HeaderStyle-ForeColor="White"
        HeaderStyle-BackColor="#005c9c" HeaderStyle-Width="2%"
        ShowHeader="true" 
        CommandName="DownloadForm" />
    <asp:BoundField HeaderText="Form" 
        SortExpression="FileName"
        DataField="FileName"
        ItemStyle-ForeColor="Black" 
        ItemStyle-HorizontalAlign="Left"
        ItemStyle-VerticalAlign="Top"
        HeaderStyle-BorderColor="#005c9c"
        HeaderStyle-BackColor="#005c9c" HeaderStyle-Width="16%"
        HeaderStyle-ForeColor="White"></asp:BoundField>
    <asp:BoundField HeaderText="Vers" 
        SortExpression="Version"
		DataField="Version" 
		ItemStyle-ForeColor="Black" 
		ItemStyle-HorizontalAlign="Left" 
        ItemStyle-VerticalAlign="Top"
		HeaderStyle-BorderColor="#005c9c" HeaderStyle-Width="10%"
		HeaderStyle-BackColor="#005c9c"
		HeaderStyle-ForeColor="White"></asp:BoundField>
    <asp:BoundField HeaderText="Notes"
        DataField="Notes"
        ItemStyle-ForeColor="Black" 
        ItemStyle-HorizontalAlign="Left"
        ItemStyle-VerticalAlign="Top"
        HeaderStyle-BorderColor="#005c9c" HeaderStyle-Width="40%"
        HeaderStyle-BackColor="#005c9c" 
        HeaderStyle-ForeColor="White"></asp:BoundField>
    <asp:BoundField HeaderText="UploadedBy"
        SortExpression="UploadedBy"
        DataField="UploadedBy" 
        ItemStyle-ForeColor="Black"
        ItemStyle-HorizontalAlign="Left"
        ItemStyle-VerticalAlign="Top"
        HeaderStyle-BorderColor="#005c9c" HeaderStyle-Width="15%"
        HeaderStyle-BackColor="#005c9c" 
        HeaderStyle-ForeColor="White"></asp:BoundField>
    <asp:BoundField HeaderText="Date" 
        SortExpression="CreatedDate"
        DataField="CreatedDate" 
        DataFormatString="{0:g}"
        ItemStyle-ForeColor="Black" 
        ItemStyle-HorizontalAlign="Left"
        ItemStyle-VerticalAlign="Top"
        HeaderStyle-BorderColor="#005c9c" HeaderStyle-Width="13%"
        HeaderStyle-BackColor="#005c9c" 
        HeaderStyle-ForeColor="White"></asp:BoundField>
    <asp:ButtonField ButtonType="Image" ImageUrl="~/Images/DeleteRed.png"
        ControlStyle-CssClass="mybutton2"
        ControlStyle-Width="70%"
        ControlStyle-BorderColor="#005c9c"
        ItemStyle-BorderColor="#005c9c"
        ItemStyle-VerticalAlign="Top"
        ItemStyle-HorizontalAlign="Center"
        ItemStyle-CssClass="mybutton2" 
        HeaderStyle-BorderColor="#005c9c"
        HeaderStyle-ForeColor="White"
        HeaderStyle-BackColor="#005c9c" HeaderStyle-Width="2%"
        ShowHeader="true" 
        CommandName="DeleteForm" />
</Columns>
</asp:GridView>

<asp:Button ID="Button1" CssClass="hide" runat="server" Text="" />
<!-- ModalPopupExtender -->
<asp:ModalPopupExtender ID="modPopUp" runat="server"  Drag="true" TargetControlID="Button1" PopupControlID="PModal" PopupDragHandleControlID="Button1"  BackgroundCssClass="bModal">
</asp:ModalPopupExtender>
<asp:Panel CssClass="bootstrap-dialog type-success modStyle" ID="PModal" runat="server">

   <asp:Panel ID="Panel1" runat="server" CssClass="modal-header bootstrap-dialog-title">TWRA Inventory</asp:Panel>
   <div class="bootstrap-dialog-message"><br />
       <asp:Label ID="lblModalPopup" runat="server" Text="Label"></asp:Label><br /><br />
<%--       <asp:Button ID="btnPopOk" runat="server" CausesValidation="false" Text="OK" Width="70px"  CssClass="mybutton2" OnClick="btnPopOk_Click" />--%>
       <asp:Button ID="btnPopCancel" runat="server" CausesValidation="false" Text="Cancel" Width="70px"  CssClass="mybutton2" OnClick="btnPopCancel_Click" />
<%--       <asp:Button ID="btnOk" runat="server" Visible="false" CausesValidation="false" Text="OK" Width="70px"  CssClass="mybutton2" OnClick="btnOk_Click" />--%>
   </div>
</asp:Panel>
<!-- ModalPopupExtender -->


</asp:Content>
