﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="DailyWT.Error" %>

    <link href="/CSS/flatly/bootstrap.min.css" rel="stylesheet" title="main" />

    <div class="container-fluid">
        <div class="jumbotron">
            <div style="text-align: center;" >
                <br />
                <br />
<%--                <label ID="lblerror" runat="server" Text="An Error Has Occured. Please Contact System Administrator!" ForeColor="#990000" Font-Size="Large" Font-Bold="True"></label>--%>
                <label class="h1 alert alert-danger">An Error has occured. Please contact your friendly System Administrator!</label>
                <br />
                <br />
            </div>
        </div>
    </div>
