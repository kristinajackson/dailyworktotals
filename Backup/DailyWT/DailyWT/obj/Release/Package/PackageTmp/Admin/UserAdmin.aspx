﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="UserAdmin.aspx.cs" Inherits="DailyWT.Admin.UserAdmin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>
<script>
    function EnableButtons() {
        document.getElementById('<%=btnChangeAdmin.ClientID%>').classList.remove("disabled");
        document.getElementById('<%=btnChangeActive.ClientID%>').classList.remove("disabled");
    };
</script>
<!-- Section 2 -->
<section class="container-fluid" id="section2">
    <div class="col-xs-8 col-xs-offset-1">
        <div class="panel panel-default" id="registrationForm">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Add/Remove User</span></div>
            <div class="panel-body">
                <div class="form-group h5">
                    <div class="col-xs-1">&nbsp;</div>
                    <div class="col-xs-4 list-group-item-heading h5">TWRA Employees</div>
                    <div class="col-xs-2">&nbsp;</div>
                    <div class="col-xs-4 list-group-item-heading h5">App Users<asp:Label ID="lblAssgnedQty" runat="server" Visible="false"></asp:Label></div>
                    <div class="col-xs-1">&nbsp;</div>
                </div>
                <div class="form-group">
                    <div class="col-xs-1">&nbsp;</div>

                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <asp:ListBox CssClass="col-xs-4 list-group-item" ID="ddlAvailableUsers" SelectionMode="Multiple" runat="server" Rows="5" ></asp:ListBox>                                                            
                            <asp:ImageButton CssClass="col-xs-2" ID="iCopyUser" ToolTip="Add or remove a user" Enabled="true" runat="server" ImageUrl="~/Images/DoubleArrow.jpg" onclick="CopyUser_Click" />
                            <asp:ListBox CssClass="col-xs-4 list-group-item" ID="ddlAssignedToUsers" onchange="javascript:EnableButtons();" SelectionMode="Multiple" runat="server" Rows="5" ></asp:ListBox>                                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="col-xs-1">&nbsp;</div>
                </div>
                <div class="form-group last">
                    <div class="col-xs-offset-1 col-xs-10">
                        <asp:Button ID="btnSave" CssClass="btn btn-success btn-sm submit-button" runat="server" Text="Save Changes" OnClick="SaveChanges_Click" />
                        <button type="button" id="btnCancel" class="btn btn-danger btn-sm" onclick="NewPage('/User/LandingPage.aspx')">
                            &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
                        <div class="pull-right">
                            <b>(A)=Admin, (I)=Inactive&nbsp;&nbsp;&nbsp;</b>
                            <asp:Button ID="btnChangeAdmin" CssClass="btn btn-warning btn-sm disabled" runat="server" Text="Change Admin" OnClick="ChangeAdmin_Click" />
                            <asp:Button ID="btnChangeActive" CssClass="btn btn-warning btn-sm disabled" runat="server" Text="Change Active" OnClick="ChangeActive_Click" />
                        </div>
                    </div>
                    <div class="col-xs-1">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>
</section>

</asp:Content>
