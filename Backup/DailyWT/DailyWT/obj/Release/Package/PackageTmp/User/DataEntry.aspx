﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntry.aspx.cs" Inherits="DailyWT.User.DataEntry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <script>
        $(document).ready(function () {
            $("#tbDEDate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            },
            function (start, end) { // when calendar closes, this function is called, so do a postback to read new data
                //__doPostBack('tbDEDate', start.format('MM/DD/YYYY'));
            });
        });
    </script>

<div class="container-fluid">

    <!--Row with one column -->
    <div class="row">
        <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Data Entry Module</span><span class="col-xs-1">&nbsp;</span>
<%--                <asp:Calendar CssClass="col-xs-2"  ID="tbDEDate" OnSelectionChanged="tbDEDate_SelectionChanged" runat="server"></asp:Calendar>--%>
<%--                <input class="col-xs-2" type="text" name="tbDEDate" value="<%=Request.Form["tbDEDate"] %>" required />--%>
            <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static"></asp:TextBox>
            <!--hidden fields used to pass number of lines to javascript -->
            <input type="hidden" runat="server" id="hfApps" />
            <input type="hidden" runat="server" id="hfWorked" />
        </div>
    </div>
</div>

<!-- Section 1 -->
<section class="container-fluid toggle col-xs-8" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Printing Module</span></div>
<div class="container-fluid">
    <div class="h4 form-group">        
        <label class="v-align">&nbsp;&nbsp;&nbsp;&nbsp;Application:&nbsp;&nbsp<asp:DropDownList ID="ddlApplication" OnSelectedIndexChanged="ddlApplication_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <table class="table table-condensed" id="tblForm1" runat="server" visible="false">
        <tr id="trBatchNum" runat="server">
            <td><b>Batch Number</b></td>
            <td class="form-group"><input type="text" class="form-control" runat="server" id="tbBatchNum" placeholder="Batch#"  /></td>
        </tr>
        <tr id="trFormCount" class="info">
            <td><b>Total Forms</b></td>
            <td class="form-group"><input type="text" class="form-control" runat="server" id="tbFormCount" placeholder="Quantity" /></td>
        </tr>
<%--        <tr>
            <td colspan="2"><button type="button" class="btn btn-xs btn-success" name="btnNewBatch">&nbsp;&nbsp;New Batch&nbsp;&nbsp;</button>&nbsp;&nbsp;<button type="button" class="btn btn-xs btn-danger" name="btnComplete">&nbsp;&nbsp;Complete&nbsp;&nbsp;</button>&nbsp;&nbsp;</td>
        </tr>--%>
    </table>
    <table class="table table-condensed" id="tblForm2" runat="server" visible="false">
        <tr id="trRegion" runat="server" visible="false">
            <td><b>Region</b></td>
            <td class="form-group"><input type="text" class="form-control" runat="server" id="tbRegion" placeholder="Region" /></td>
        </tr>
        <tr class="info">
            <td><b>Quantity</b></td>
            <td class="form-group"><input type="text" class="form-control" runat="server" id="tbQuantity" placeholder="Quantity" /></td>
        </tr>
        <tr>
            <td><b>Verify Quantity</b></td>
            <td class="form-group"><input type="text" class="form-control" runat="server" id="tbQtyVerified" placeholder="Verify Quantity" /></td>
        </tr>
        <tr class="info" id="trPackets" runat="server" visible="false">
            <td><b>Packets</b></td>
            <td class="form-group"><input type="text" class="form-control" runat="server" id="tbPackets" placeholder="Packets" /></td>
        </tr>
	</table>

</div>
</div>

</section>

</asp:Content>
