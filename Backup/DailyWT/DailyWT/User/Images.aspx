﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Images.aspx.cs" Inherits="DailyWT.User.Images" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

<div id="pnlUpload" runat="server" visible="false">
    <asp:FileUpload ID="FileUpload1" CssClass="bg-primary fUpload" runat="server"  /><br />
    <asp:Button ID="btnUpload" runat="server" Text="Upload Pic" CssClass="btn btn-info btn-sm" OnClick="btnUpload_Click" />
</div>
<br />
<!-- Gridview Form -->
<asp:GridView ID="gvImages" DataKeyNames="ID" OnRowCommand="gv_RowCommand" OnRowDeleting="gvImages_RowDeleting" runat="server" AutoGenerateColumns = "false" Font-Names = "Arial" CssClass="gvStyle">
<Columns>
    <asp:ButtonField Text="Set Bkg" CommandName="SetBkg" ControlStyle-CssClass="btn btn-info btn-xs gvPadding" />
    <asp:ButtonField Text="Delete" CommandName="Delete" ControlStyle-CssClass="btn btn-danger btn-xs gvPadding" />
    <asp:TemplateField>  
        <ItemTemplate>  
            <asp:Image CssClass="gvImageStyle gvPadding" ID="Image1" runat="server" ImageUrl='<%# "../ImgHandler.ashx?ImageID="+ Eval("ID") %>' />  
        </ItemTemplate>  
    </asp:TemplateField>
    <asp:TemplateField>
        <ItemTemplate>
            <div class="gvImageNameStyle gvPadding bg-info">
                <asp:Label runat="server" Text='<%# Eval("ImageName") %>' ></asp:Label>
            </div>
        </ItemTemplate>
    </asp:TemplateField> 
</Columns>
</asp:GridView>


</asp:Content>
