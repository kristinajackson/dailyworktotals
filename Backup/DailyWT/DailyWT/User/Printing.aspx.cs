﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class Printing : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDJob();
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void LoadDDJob()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";

            ddlJob.DataBind();
            ddlJob.Items.Insert(0, "-- Select Job --");

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.Items.Insert(0, "-- Select Form --");
            ddlForm.Enabled = false;
        }
        protected void LoadDDForm(int JobID)
        {
            DWUtility dwt = new DWUtility();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.DataSource = dwt.GetFormsByJobID(JobID);
            ddlForm.DataTextField = "FormName";
            ddlForm.DataValueField = "FormID";

            ddlForm.DataBind();
            ddlForm.Items.Insert(0, "-- Select Form --");
        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            tblForm.Visible = false;    // something selected, so hide data

            if (ddlJob.SelectedIndex > 0)  // did they select one?
            {
                int JobID = Convert.ToInt32(ddlJob.SelectedValue);
                if (JobID > 0)
                {
                    ddlForm.Enabled = true;
                    LoadDDForm(JobID);
                }
                else
                {
                    ddlForm.Enabled = false;
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            else
            {
                ddlForm.Items.Clear();       // clear in case it's already populated
                ddlForm.Items.Insert(0, "-- Select Form --");
                ddlForm.Enabled = false;
            }

        }
        protected void ddlForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlForm.SelectedIndex > 0)  // did they select one?
            {
                int FormID = Convert.ToInt32(ddlForm.SelectedValue);
                if (FormID > 0)
                {
                    tblForm.Visible = true;
                    LoadForm(FormID);
                    btnSave.CssClass = btnSave.CssClass.ToString().Replace("disabled", ""); // remove disabled class
                }
                else
                {
                    // if the Save button doesn't contain a 'disabled' class, add one
                    if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    tblForm.Visible = false;    // invalid form selected, so hide data
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            else
            {
                // if the Save button doesn't contain a 'disabled' class, add one
                if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                tblForm.Visible = false;    // no form selected, so hide data
            }

        }
        protected void LoadForm(int FormID)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds2 = new DataSet();

            bool IsUsed = true;

            string FormName = ddlForm.SelectedItem.ToString();

            FormConfigInfo fci = new FormConfigInfo();
            fci = dwt.GetFormByID(FormID);

            ds2 = dwt.GetFormData(IsUsed, FormID);  // get all form data

            // is there data in table?
            if (ds2.Tables[0].Rows.Count > 0)
            {
                bool IsReasonUsed = true;

                // hide no data message and show printing form
                divNoData.Visible = false;
                tblForm.Visible = true;

                ddlReason.Items.Clear();       // clear in case it's already populated
                ddlReason.DataSource = dwt.GetReasons(IsReasonUsed);
                ddlReason.DataTextField = "Reason";
                ddlReason.DataValueField = "ReasonID";

                ddlReason.DataBind();
                ddlReason.Items.Insert(0, "-- Select Reason --");


                lblFormsPerBox.InnerText = ds2.Tables[0].Rows[0].ItemArray[5].ToString();
                lblTotalBoxes.InnerText = ds2.Tables[0].Rows[0].ItemArray[6].ToString();
                lblOpenBoxAmount.InnerText = ds2.Tables[0].Rows[0].ItemArray[7].ToString();
                lblEndAmount.InnerText = ds2.Tables[0].Rows[0].ItemArray[9].ToString();

                // do we have sequernce numbers?
                if (fci.Sequence)
                {
                    trSeq1.Visible = true;
                    trSeq2.Visible = true;
                    lblBegSeq.InnerText = ds2.Tables[0].Rows[0].ItemArray[10].ToString();
                    lblEndSeq.InnerText = ds2.Tables[0].Rows[0].ItemArray[11].ToString();
                }
                else
                {
                    trSeq1.Visible = false;
                    trSeq2.Visible = false;
                }
            }
            else
            {
                // hide printing form and show no data message 
                tblForm.Visible = false;
                divNoData.Visible = true;
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            int FormsUsed, FormsReq;

            string Requester = Regex.Replace(Request.Form["tbRequester"], @"[^a-zA-Z\ \-]", "").ToUpper();
            string Approver = Regex.Replace(Request.Form["tbApprover"], @"[^a-zA-Z\ \-]", "").ToUpper();
            string Comment = Regex.Replace(Request.Form["tbComment"], @"[^A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]", "").ToUpper();  // get rid of any junk characters
            Comment = Regex.Replace(Comment, @"\r\n", " "); // ok, now change the cr/lf to spaces

            int.TryParse(Request.Form["tbFormsUsed"], out FormsUsed);
            int.TryParse(Request.Form["tbFormsReq"], out FormsReq);

            if (FormsUsed < 1)
            {
                ShowPopUp("Error!", "Quantity Used cannot be blank or zero.");
            }
            else if (Requester.Length < 5)
            {
                ShowPopUp("Error!", "Requester cannot be blank.");
            }
            else
            {
                if (FormsReq != FormsUsed && ddlReason.SelectedIndex < 1)
                {
                    ShowPopUp("Error!", "A Reason must be supplied when the Quantity Used and the Quantity Requested are not equal.");
                }
                else
                {
                    DWUtility dwt = new DWUtility();

                    DataSet ds2 = new DataSet();

                    bool IsUsed = true;
                    int ReasonID = 0;
                    if (ddlReason.SelectedIndex > 0)
                    {
                        ReasonID = Convert.ToInt32(ddlReason.SelectedValue);
                    }
                    int FormID = Convert.ToInt32(ddlForm.SelectedValue);

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    FormConfigInfo fci = new FormConfigInfo();
                    fci = dwt.GetFormByID(FormID);

                    ds2 = dwt.GetFormData(IsUsed, FormID);  // get all form data

                    int FormsPerBox = (int)ds2.Tables[0].Rows[0].ItemArray[5];
                    int TotalBoxes = (int)ds2.Tables[0].Rows[0].ItemArray[6];
                    int PartialBoxAmt = (int)ds2.Tables[0].Rows[0].ItemArray[7];

                    int TotalForms = (FormsPerBox * TotalBoxes) + PartialBoxAmt;

                    int BegAmt = (int)ds2.Tables[0].Rows[0].ItemArray[9]; // use EndAmt field data

                    // calculate new totals
                    TotalForms = TotalForms - FormsUsed;
                    if (TotalForms < 0)
                    {
                        // Notify the user that an error occurred.
                        ShowPopUp("Error!", "The number of forms used is more than the quantity on hand. Please inform the administrator.");
                        return;
                    }
                    TotalBoxes = TotalForms / FormsPerBox;
                    PartialBoxAmt = TotalForms - (FormsPerBox * TotalBoxes);

                    string BegSeq = "";
                    string EndSeq = "";

                    // do we have sequernce numbers?
                    if (fci.Sequence)
                    {
                        // get the length to format it in case there are leading 0's
                        int SeqLen = ((string)ds2.Tables[0].Rows[0].ItemArray[10]).Length;
                        BegSeq = (Convert.ToInt32(ds2.Tables[0].Rows[0].ItemArray[10]) + FormsUsed).ToString("D" + SeqLen.ToString());
                        EndSeq = (Convert.ToInt32(ds2.Tables[0].Rows[0].ItemArray[11])).ToString("D" + SeqLen.ToString());
                    }

                    int status = dwt.UpsertPrintData(FormID, IsUsed, TotalBoxes, PartialBoxAmt, BegAmt,  TotalForms, BegSeq, EndSeq, FormsReq, Requester, Approver, ReasonID, Comment, MyUserID);
                    if (status < 0)
                    {
                        // Notify the user that an error occurred.
                        ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                        return;
                    }
                    //else
                    {
                        LoadDDJob();

                        // if the Save button doesn't contain a 'disabled' class, add one
                        if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                        tblForm.Visible = false;    // no form selected, so hide data
                        ShowPopUp("Success!", "The data was successfully saved!");
                    }
                }
            }
        }
    }
}