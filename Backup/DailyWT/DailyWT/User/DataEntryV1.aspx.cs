﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class DataEntry : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    CreateForm(DateTime.Today);
                }
                else
                {
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: tbDEDate
                    if (eventTarget == "tbDEDate")
                    {
                        string eventArgument = this.Request["__EVENTARGUMENT"]; // event arg: date in MM/DD/YYYY format

                        DateTime WorkDate = Convert.ToDateTime(eventArgument);
                        if (WorkDate < DateTime.Today.AddDays(1))
                        {
                            CreateForm(WorkDate);
                            tbDEDate.Text = eventArgument;
                        }
                        else
                        {
                            DateTime dtNow = DateTime.Today;
                            tbDEDate.Text = dtNow.ToString("d");

                            // recreate the screen
                            CreateForm(dtNow);

                            ShowPopUp("Error!", "Future dates do not make any sense!!");
                            return;
                        }
                    }
                    else if (eventTarget == "AddBatch")
                    {
                        string row = this.Request["__EVENTARGUMENT"]; // event arg: line number of button clicked

                        ReCreateHtmlTable(row);
                    }
                    else
                    {
                        DateTime WorkDate = Convert.ToDateTime(tbDEDate.Text);
                        ReadData(WorkDate);
                        CreateForm(WorkDate);
                    }
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void ReCreateHtmlTable(string row)
        {
            // need to do a mass replacement for now, otherwise, IndexOf will return an inaccurate position
            string htmltable = tblBodyDE.InnerHtml.Replace(@"""", "^");

            string newlastrow = Regex.Matches(htmltable, "id='tr").Count.ToString() + "'";  // count the number of lines in the table

            int j = htmltable.IndexOf("id='tr" + row) - 4;   // find the id=trX of this row

            int k = htmltable.IndexOf("/tr>", j) + 4;    // now find the end of this tr

            string trline = htmltable.Substring(j, k - j);    // copy the tr line so we can create a new one out of it

            int i = 0, l = 0;
            string x = "";

            // this routine clears the values in case the duplicated line has values in it
            while (true)
            {
                // find the the value
                l = trline.IndexOf("value='", i);
                if (l > 0)
                {
                    l += 7;
                    x = x + trline.Substring(i, l - i);
                    i = trline.IndexOf("' ", l);
                }
                else
                {
                    x = x + trline.Substring(i, trline.Length - i);
                    break;
                }
            }
            trline = x.Replace(row + "'", newlastrow);  // replace row number with the new last row number

            x = htmltable.Substring(0, k) + trline;    // create the first part of the table with lines thru the end of this line plus the new line

            x = x + htmltable.Substring(k, htmltable.Length - k);   // tack on the rest of the table

            // ok, restore the imbedded double quotes
            string finalhtmltable = x.Replace("^", @"""");

            // display it on the screen
            tblBodyDE.InnerHtml = finalhtmltable;
        }
        protected void ReadData(DateTime WorkDate)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds3 = new DataSet();

            int status = 0;
            bool IsAppUsed = true;
            bool IsTWorkUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            // get config info
            ds1 = dwt.GetAppConfig(IsAppUsed);
            ds3 = dwt.GetTWorkConfig(IsTWorkUsed);

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds3Count = ds3.Tables[0].Rows.Count;

            string htmltable = tblBodyDE.InnerHtml.Replace(@"""", "^");

            // there must be a better way to do this, but for now, this is how we read the data entry section data

            // this section is used to retrieve the appids and the rowids so we can properly retrieve the data
            List<int> AppIDs = new List<int>();
            List<int> RowIDs = new List<int>();

            int i = 0, appid = 0, trnum = 0;
            string id = "";
            while ((i = htmltable.IndexOf("id='tr", i)) != -1)
            {
                id = htmltable.Substring(i + 5, 5);    // retrieve the tr number
                trnum = Convert.ToInt32(Regex.Replace(id, "[^0-9]", ""));   // strip out the non-numeric chars
                RowIDs.Add(trnum);

                i = htmltable.IndexOf("data-appid=", i);
                id = htmltable.Substring(i + 11, 5);    // retrieve the appid
                appid = Convert.ToInt32(Regex.Replace(id, "[^0-9]", ""));   // strip out the non-numeric chars
                AppIDs.Add(appid);
                // Increment the index skipping a bunch of characters
                i += 100;
            }
            int numrows = AppIDs.Count;     // could have used RowIDs.Count as well

            int Region, BatchNum, Quantity, QtyVerified, Packets, Total;

            string istr = "";

            // now that we have the info we need, lets read all of the values
            for (i = 0; i < numrows; i++)
            {
                istr = RowIDs[i].ToString();    // have to track rowids in case they have added rows for additional batches

                int.TryParse(Request.Form["tbRegion" + istr], out Region);
                int.TryParse(Request.Form["tbBatchNum" + istr], out BatchNum);
                int.TryParse(Request.Form["tbQuantity" + istr], out Quantity);
                int.TryParse(Request.Form["tbQtyVerified" + istr], out QtyVerified);
                int.TryParse(Request.Form["tbPackets" + istr], out Packets);

                // total all up so we don't insert a bunch of zeros data
                Total = Region + BatchNum + QtyVerified + Quantity + Packets;

                status = dwt.UpsertAppData(AppIDs[i], Region, BatchNum, QtyVerified, Quantity, Packets, MyUserID, WorkDate, Total);
                if (status < 0)
                {
                    // Notify the user that an error occurred.
                    ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                    return;
                }
            }

            // end read data entry data

            // read the work hours
            float Hours = 0;
            for (i = 0; i < ds3Count; i++)
            {
                float.TryParse(Request.Form["tbHours" + i.ToString()], out Hours);

                status = dwt.UpsertWorkData((int)ds3.Tables[0].Rows[i][0], Hours, MyUserID, WorkDate);
                if (status < 0)
                {
                    // Notify the user that an error occurred.
                    ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                    return;
                }
            }
        }
        protected void CreateForm(DateTime SearchDate)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            DataSet ds3 = new DataSet();
            DataSet ds4 = new DataSet();

            bool IsAppUsed = true;
            bool IsTWorkUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetAppConfig(IsAppUsed);
            ds2 = dwt.GetAppData(MyUserID, SearchDate);
            ds3 = dwt.GetTWorkConfig(IsTWorkUsed);
            ds4 = dwt.GetTWorkData(MyUserID, SearchDate);

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds3Count = ds3.Tables[0].Rows.Count;

            string iStr = "";
            int linenum = 0, drowlen = 0;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ds1Count; i++)
            {
                // search the dataset for a row (by AppID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("AppID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");
                drowlen = DRow.Length;

                // this makes sure that we do each configured row, whether it has data or not, AND makes sure we don't do too many data rows
                if (drowlen > 0)
                    drowlen -= 1;

                for (int j = 0; j <= drowlen; j++)  // do this at least once
                {
                    iStr = linenum++.ToString();    // convert it to a string and increment the line number

                    // assign 'info' styling to every other line
                    sb.Append("<tr id='tr" + iStr + "' " + ((i % 2 == 0) ? "class='info'" : "") + ">");
                    // note: the 'x' following the appid value is there to prevent an invalid string substitution when a row is added later
                    sb.Append("<td target='tdApp' data-appid='" + ds1.Tables[0].Rows[i][0].ToString() + "x'>" + ds1.Tables[0].Rows[i][2].ToString() + "</td>");
                    sb.Append("<td class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][3])) ?
                            "<input type='text' class='form-control' name='tbRegion" + iStr +
                            "' value='" + ((DRow.Length < 1) ? "" : DRow[j].ItemArray[2].ToString()) +
                            "' placeholder='Region#' />" : "") + "</td>");
                    sb.Append("<td class='form-group'><input type='text' class='form-control' name='tbQuantity" + iStr +
                            "' value='" + ((DRow.Length < 1) ? "" : DRow[j].ItemArray[5].ToString()) +
                            "' placeholder='Quantity' /></td>");
                    sb.Append("<td class='form-group'><input type='text' class='form-control' name='tbQtyVerified" + iStr +
                            "' value='" + ((DRow.Length < 1) ? "" : DRow[j].ItemArray[4].ToString()) +
                            "' placeholder='Verify Qty' /></td>");
                    sb.Append("<td class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][5])) ?
                            "<input type='text' class='form-control' name='tbPackets" + iStr +
                            "' value='" + ((DRow.Length < 1) ? "" : DRow[j].ItemArray[6].ToString()) +
                            "' placeholder='Packets' />" : "") + "</td>");
                    sb.Append("<td class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][4])) ?
                            "<input type='text' class='form-control' name='tbBatchNum" + iStr +
                            "' value='" + ((DRow.Length < 1) ? "" : DRow[j].ItemArray[3].ToString()) +
                            "' placeholder='Batch#' />" : "") + "</td>");
                    sb.Append("<td class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][4])) ?
                            "<input type='button' class='btn btn-xs myPlusSign' name='addBatch" + iStr +
                            "' value='' onclick=\"__doPostBack('AddBatch', '" + iStr + "');\" />" : "") + "</td>");
//                            "' value='' onclick=\"CloneBatchRow(" + iStr + ");\" />" : "") + "</td>");
                    sb.Append("</tr>");
                }
            }

            // copy number of rows to hidden field for javascript
            hfApps.Value = linenum.ToString();
            hfWorked.Value = ds3Count.ToString();

            // display it on the screen
            tblBodyDE.InnerHtml = sb.ToString();

            sb.Length = 0;  // clear it.
            for (int i = 0; i < ds3Count; i++)
            {
                // search the dataset for a row (by AppID) since we can't sort it
                DataRow[] DRow = ds4.Tables[0].Select("TWorkID = '" + (int)ds3.Tables[0].Rows[i][0] + "'");

                // assign 'info' styling to every other line
                sb.Append("<tr " + ((i % 2 == 0) ? "class='info'" : "") + "'>");
                sb.Append("<td target='tdWork'>" + ds3.Tables[0].Rows[i][2].ToString() + "</td>");
                sb.Append("<td class='form-group'><input type='text' class='form-control inputFloat' name='tbHours" + i.ToString() +
                          "' value='" + ((DRow.Length < 1) ? "" : Convert.ToSingle(DRow[0].ItemArray[2]).ToString("F")) +
                          "' placeholder='Hours' /></td>");
                sb.Append("<td>&nbsp;</td>");
                sb.Append("</tr>");
            }

            // display it on the screen
            tblBodyTW.InnerHtml = sb.ToString();
        }

    }
}