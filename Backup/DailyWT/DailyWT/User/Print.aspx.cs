﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class Print : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    CreateForm();
                }
                else
                {
                    //string eventTarget = this.Request["__EVENTTARGET"];     // event name: tbDEDate
                    //if (eventTarget == "Save")
                    //{
                        ReadData();
                        CreateForm();
                    //}
                    //else if (eventTarget == "Cancel")
                    //{
                    //    Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"), false);
                    //}
                    //else
                    //{
                    //    ShowPopUp("Error!", "Unexpected postback. Event target = " + eventTarget + " Please tell the administrator this error message.");
                    //}

                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }

        }
        protected void CreateForm()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            DataSet ds3 = new DataSet();

            bool IsFormUsed = true;
            bool IsJobUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data
            ds3 = dwt.GetJobConfig(IsJobUsed);

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds3Count = ds3.Tables[0].Rows.Count;

            // copy number of rows to hidden field for javascript
            hfForms.Value = ds1Count.ToString();

            string iStr = "";
            int FormID = 0;

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ds3Count; i++)
            {
                iStr = i.ToString();

                // get formid for this row
                FormID = (int)ds3.Tables[0].Rows[i][3];

                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRowF = ds2.Tables[0].Select("FormID = '" + FormID + "'");    // data for this form
                DataRow[] DRowFC = ds1.Tables[0].Select("FormID = '" + FormID + "'");   // config info for this form

                // assign 'info' styling to every other line
                sb.Append("<tr " + ((i % 2 == 0) ? "class='info'" : "") + ">");
                sb.Append("<td data-toggle='tooltip' data-container='body' data-placement='right' title='Form Used:&nbsp;&nbsp;&nbsp;" + ((DRowFC.Length < 1) ? "" : DRowFC[0].ItemArray[2].ToString()) + "' target='tdWName'>" + ds3.Tables[0].Rows[i][2].ToString() + "</td>");
                sb.Append("<td target='tdWNumb' class='form-group'><input type='text' disabled class='form-control' name='tbFormsPerBox" + iStr +
                        "' value='" + ((DRowF.Length < 1) ? "" : DRowF[0].ItemArray[5].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdWNumb' class='form-group'><input type='text' disabled class='form-control' name='tbTotalBoxes" + iStr +
                        "' value='" + ((DRowF.Length < 1) ? "" : DRowF[0].ItemArray[6].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdWNumb' class='form-group'><input type='text' disabled class='form-control' name='tbPartialBoxAmt" + iStr +
                        "' value='" + ((DRowF.Length < 1) ? "" : DRowF[0].ItemArray[7].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdWNumb' class='form-group'><input type='text' disabled class='form-control' name='tbEndAmt" + iStr +
                        "' value='" + ((DRowF.Length < 1) ? "" : DRowF[0].ItemArray[9].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdWText' class='form-group'>" + ((Convert.ToBoolean(DRowFC[0].ItemArray[4])) ?
                        "<input type='text' disabled class='form-control' name='tbBegSeq" + iStr +
                        "' value='" + ((DRowF.Length < 1) ? "" : DRowF[0].ItemArray[10].ToString()) +
                        "' placeholder='Seq#' />" : "") + "</td>");
                sb.Append("<td target='tdWText' class='form-group'>" + ((Convert.ToBoolean(DRowFC[0].ItemArray[4])) ?
                        "<input type='text' disabled class='form-control' name='tbEndSeq" + iStr +
                        "' value='" + ((DRowF.Length < 1) ? "" : DRowF[0].ItemArray[11].ToString()) +
                        "' placeholder='Seq#' />" : "") + "</td>");
                sb.Append("<td target='tdWNumb' class='form-group'><input type='text' class='form-control' name='tbFormsUsed" + iStr +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdWNumb' class='form-group'><input type='text' class='form-control' name='tbFormsReq" + iStr +
                        "' placeholder='#Req' /></td>");
                sb.Append("<td target='tdWName' class='form-group'><input type='text' class='form-control' name='tbRequester" + iStr +
                        "' placeholder='Requester' /></td>");
                sb.Append("</tr>");
            }

            // display it on the screen
            tblBodyPri.InnerHtml = sb.ToString();
        }
        protected void ReadData()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            DataSet ds3 = new DataSet();

            bool IsFormUsed = true;
            bool IsJobUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds3 = dwt.GetJobConfig(IsJobUsed);

            string Requester, BegSeq, EndSeq;
            int status, FormID, TotalForms, FormsPerBox, TotalBoxes, PartialBoxAmt, BegAmt, FormsUsed, FormsReq, SeqLen;

            for (int i = 0; i < ds3.Tables[0].Rows.Count; i++)  // loop thru the number of jobs configured
            {
                // get formid for this row
                FormID = (int)ds3.Tables[0].Rows[i][3];

                // note: we have to get the data for this form for each job, because the same form may be used for different jobs
                ds2 = dwt.GetFormData(IsUsed, FormID);  // get the data for this form

                // search the dataset for the config info for form used by this job
                DataRow[] DRowFC = ds1.Tables[0].Select("FormID = '" + FormID + "'");   // config info for this form

                if (ds2.Tables[0].Rows.Count > 0)   // is there existing data for this form?
                {
                    int.TryParse(Request.Form["tbFormsUsed" + i.ToString()], out FormsUsed);
                    int.TryParse(Request.Form["tbFormsReq" + i.ToString()], out FormsReq);
                    Requester = Request.Form["tbRequester" + i.ToString()] ?? "";
                    Requester = Requester.ToUpper();

                    // check if anything changed
                    if (FormsUsed != 0 || FormsReq != 0)
                    {
                        FormsPerBox = (int)ds2.Tables[0].Rows[0].ItemArray[5];
                        TotalBoxes = (int)ds2.Tables[0].Rows[0].ItemArray[6];
                        PartialBoxAmt = (int)ds2.Tables[0].Rows[0].ItemArray[7];

                        TotalForms = (FormsPerBox * TotalBoxes) + PartialBoxAmt;

                        BegAmt = (int)ds2.Tables[0].Rows[0].ItemArray[9]; // use EndAmt field data

                        // calculate new totals
                        TotalForms = TotalForms - FormsUsed;
                        TotalBoxes = TotalForms / FormsPerBox;
                        PartialBoxAmt = TotalForms - (FormsPerBox * TotalBoxes);

                        BegSeq = "";
                        EndSeq = "";

                        // do we have a config row?
                        if (DRowFC.Length > 0)
                        {
                            // using sequence numbers??
                            if (Convert.ToBoolean(DRowFC[0].ItemArray[4]))
                            {
                                // get the length to format it in case there are leading 0's
                                SeqLen = ds2.Tables[0].Rows[0].ItemArray[10].ToString().Length;
                                BegSeq = (Convert.ToInt32(ds2.Tables[0].Rows[0].ItemArray[10]) + FormsUsed).ToString("D" + SeqLen.ToString());
                                EndSeq = (Convert.ToInt32(ds2.Tables[0].Rows[0].ItemArray[11]) + FormsUsed).ToString("D" + SeqLen.ToString());
                            }
                        }

                        // set IsUsed to true for now, ReasonID=0, Comment=""
                        status = dwt.UpsertPrintData(FormID, true, TotalBoxes, PartialBoxAmt, BegAmt, TotalForms, BegSeq, EndSeq, FormsReq, Requester, 0, "", MyUserID);
                        if (status < 0)
                        {
                            // Notify the user that an error occurred.
                            ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                            return;
                        }
                    }
                }
            }
        }
        protected string CreateFormatSpec(string Example)
        {
            // this is presently not used

            char[] StrArray = Example.ToCharArray();
            string NewRegex = "";
            int ExLen = Example.Length;
            for (int i = 0; i < ExLen; i++)
            {

                if (StrArray[i] == ' ')
                    NewRegex += @"[\ ]";
                else
                    if (StrArray[i] == '-')
                        NewRegex += @"[\-]";
                    else
                        if (StrArray[i] == '.')
                            NewRegex += @"[\.]";
                        else
                            if (Regex.IsMatch(StrArray[i].ToString(), @"[0-9]"))
                                NewRegex += @"[0-9]";
                            else
                                if (Regex.IsMatch(StrArray[i].ToString(), @"[A-Za-z]"))
                                    NewRegex += @"[A-Za-z]";
                                else
                                    return "";
            }
            return NewRegex;
        }
    }
}