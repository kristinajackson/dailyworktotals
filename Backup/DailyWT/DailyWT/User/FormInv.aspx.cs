﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class FormInv : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
                if (!Page.IsPostBack)
                    if (Session["Admin"] != null)
                        CreateForm();
                    else
                        CheckUser();

                else
                {
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: tbDEDate
                    if (eventTarget == "Save")
                    {
                        ReadData("S");
                        CreateForm();
                    }
                    else if (eventTarget == "Cancel")
                    {
                        Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"), false);
                    }
                    else if (eventTarget == "Approve")
                    {

                        ReadData("A");
                        CreateForm();
                    }
                    else
                    {
                        ShowPopUpAndRD("Error!", "Unexpected postback. Event target = " + eventTarget + " Please tell the administrator this error message.", "/Error.aspx");
                    }
                }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }

        }
        protected void CreateForm()
        {

            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;

            // copy number of rows to hidden field for javascript
            hfForms.Value = ds1Count.ToString();

            string iStr = "";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();

                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

                // assign 'info' styling to every other line

                sb.Append("<tr " + ((i % 2 == 0) ? "class='info'" : "") + ">");
                sb.Append("<td target='tdName'>" + ds1.Tables[0].Rows[i][2].ToString() + "</td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][3])) ?
                        "<input type='text' class='form-control' name='tbShipNum" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[3].ToString()) +
                        "' placeholder='Shipping#' />" : "") + "</td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][3])) ?
                        "<input type='text' class='form-control' name='tbShipDate" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : ((DateTime)DRow[0].ItemArray[4]).ToString("MM/dd/yyyy")) +
                        "' placeholder='MM/DD/YYYY' />" : "") + "</td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbFormsPerBox" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[5].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' name='tbUserBegAmt" + iStr +
                       "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[17].ToString()) +
                       "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdName' class='form-group'><input type='text' class='form-control' name='tbUserEndAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[18].ToString()) +
                        "' placeholder='FormNumber' /></td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][4])) ?
                         "<input type='text' class='form-control' name='tbEndSeq" + iStr +
                         "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[11].ToString()) +
                         "' placeholder='Seq#' />" : "") + "</td>");
                sb.Append("<td target='tdName' class='form-group'><input type='text' class='form-control' name='tbFormLocation" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[12].ToString()) +
                        "' placeholder='Loc' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' readonly name='tbTotalBoxes" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[6].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' readonly name='tbPartialBoxAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[7].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' readonly name='tbBegAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[8].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text' class='form-control' readonly  name='tbEndAmt" + iStr +
                        "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[9].ToString()) +
                        "' placeholder='Qty' /></td>");
                sb.Append("<td target='tdText' class='form-group'>" + ((Convert.ToBoolean(ds1.Tables[0].Rows[i][4])) ?
                      "<input type='text' class='form-control' readonly name='tbBegSeq" + iStr +
                      "' value='" + ((DRow.Length < 1) ? "" : DRow[0].ItemArray[10].ToString()) +
                      "' placeholder='Seq#' />" : "") + "</td>");
                
                sb.Append("<td target='tdComm' class='form-group'><textarea class='form-control' name='tbComment" + iStr +
                        "' placeholder='Comment' ></textarea></td>");
                sb.Append("</tr>");

            }


            // display it on the screen
            tblBodyInv.InnerHtml = sb.ToString();

            //Enable the button for Admin
            if (Session["Admin"] != null)
            {
                Button2.Visible = true;
            }


        }

        protected void ReadData(string action)
        {
            bool AdminApprove = false;
            bool approve = false;
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;

            DateTime ShipDate;
            string ShipNum, BegSeq, EndSeq, FormLocation, UserEndAmt;
            int status, FormsPerBox, TotalBoxes, PartialBoxAmt, BegAmt, EndAmt, UserBegAmt, Total;
            bool FormInvUpdated = false;

            string iStr = "";

            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();
                FormInvUpdated = false;

                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

                // myNewValue = myValue ?? new MyValue();
                ShipNum = Request.Form["tbShipNum" + iStr] ?? "";
                ShipNum = ShipNum.ToUpper();
                if (DRow.Length > 0)
                {
                    if (ShipNum != DRow[0].ItemArray[3].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (ShipNum.Length > 0)
                        FormInvUpdated = true;
                }

                DateTime.TryParse(Request.Form["tbShipDate" + iStr], out ShipDate);
                if (DRow.Length > 0)
                {
                    if (ShipDate != (DateTime)DRow[0].ItemArray[4])
                        FormInvUpdated = true;
                }
                else
                {
                    if (ShipDate != new DateTime())
                    {
                        FormInvUpdated = true;
                    }
                    else
                    {
                        ShipDate = Convert.ToDateTime("01/01/1800");    // load a dummy, but legit date
                    }
                }

                int.TryParse(Request.Form["tbFormsPerBox" + iStr], out FormsPerBox);
                if (DRow.Length > 0)
                {
                    if (FormsPerBox != (int)DRow[0].ItemArray[5])
                        FormInvUpdated = true;
                }
                else
                {
                    if (FormsPerBox > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbTotalBoxes" + iStr], out TotalBoxes);
                if (DRow.Length > 0)
                {
                    if (action == "A")
                    {
                        TotalBoxes = (int)DRow[0].ItemArray[17];
                        AdminApprove = true;
                    }

                    if (TotalBoxes != (int)DRow[0].ItemArray[6])
                        FormInvUpdated = true;
                }
                else
                {
                    if (TotalBoxes > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbPartialBoxAmt" + iStr], out PartialBoxAmt);
                if (DRow.Length > 0)
                {
                    if (PartialBoxAmt != (int)DRow[0].ItemArray[7])
                        FormInvUpdated = true;
                }
                else
                {
                    if (PartialBoxAmt > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbBegAmt" + iStr], out BegAmt);
                if (DRow.Length > 0)
                {
                    if (BegAmt != (int)DRow[0].ItemArray[8])
                        FormInvUpdated = true;
                }
                else
                {
                    if (BegAmt > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbEndAmt" + iStr], out EndAmt);
                if (DRow.Length > 0)
                {
                    if (EndAmt != (int)DRow[0].ItemArray[9])
                        FormInvUpdated = true;
                }
                else
                {
                    if (EndAmt > 0)
                        FormInvUpdated = true;
                }


                int.TryParse(Request.Form["tbUserBegAmt" + iStr], out UserBegAmt);
                if (DRow.Length > 0)
                {
                    if (UserBegAmt != (int)DRow[0].ItemArray[17])
                        FormInvUpdated = true;
                }
                else
                {
                    if (UserBegAmt > 0)
                        FormInvUpdated = true;
                }

                UserEndAmt = Request.Form["tbUserEndAmt" + iStr] ?? "";
                UserEndAmt = UserEndAmt.ToUpper();
                if (DRow.Length > 0)
                {
                    if (UserEndAmt != DRow[0].ItemArray[18].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (UserEndAmt.Length > 0)
                        FormInvUpdated = true;
                }

                BegSeq = Request.Form["tbBegSeq" + iStr] ?? "";
                if (DRow.Length > 0)
                {
                    if (BegSeq != DRow[0].ItemArray[10].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (BegSeq.Length > 0)
                        FormInvUpdated = true;
                }

                EndSeq = Request.Form["tbEndSeq" + iStr] ?? "";
                if (DRow.Length > 0)
                {
                    if (EndSeq != DRow[0].ItemArray[11].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (EndSeq.Length > 0)
                        FormInvUpdated = true;
                }

                FormLocation = Request.Form["tbFormLocation" + iStr] ?? "";
                FormLocation = FormLocation.ToUpper();
                if (DRow.Length > 0)
                {
                    if (FormLocation != DRow[0].ItemArray[12].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (FormLocation.Length > 0)
                        FormInvUpdated = true;
                }

                string Comment = "";
                if (Request.Form["tbComment"] != null)
                {
                    Regex.Replace(Request.Form["tbComment" + iStr], @"[^A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]", "").ToUpper();
                    Comment = Regex.Replace(Comment, @"\r\n", " "); // ok, now change the cr/lf to spaces
                }
                if (UserBegAmt > 0 && UserBegAmt != TotalBoxes)
                {
                    approve = true;
                }



                if (FormInvUpdated)
                {
                    // total all up so we don't insert a bunch of zeros data
                    Total = FormsPerBox + TotalBoxes + PartialBoxAmt + BegAmt + EndAmt;

                    // set IsUsed to true for now
                    status = dwt.UpsertFormData((int)ds1.Tables[0].Rows[i][0], true, ShipNum, ShipDate, FormsPerBox, TotalBoxes, PartialBoxAmt, BegAmt, EndAmt, UserBegAmt, UserEndAmt, BegSeq, EndSeq, FormLocation, Comment, MyUserID, Total, AdminApprove);

                    if (status < 0)
                    {
                        // Notify the user that an error occurred.
                        ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                        return;
                    }

                }
            }

            if (approve == true && Session["Admin"] == null)
            {
                ShowPopUp("Error!", "Your User Total Boxes does not match with the system Total Boxes. Please contact Administration for approval");
                tblBodyInv.Visible = false;
            }
            else
                ShowPopUp("Thanks", "Data has been Saved! ");

        }

        protected string CreateFormatSpec(string Example)
        {
            // this is presently not used

            char[] StrArray = Example.ToCharArray();
            string NewRegex = "";
            int ExLen = Example.Length;
            for (int i = 0; i < ExLen; i++)
            {

                if (StrArray[i] == ' ')
                    NewRegex += @"[\ ]";
                else
                    if (StrArray[i] == '-')
                        NewRegex += @"[\-]";
                    else
                        if (StrArray[i] == '.')
                            NewRegex += @"[\.]";
                        else
                            if (Regex.IsMatch(StrArray[i].ToString(), @"[0-9]"))
                                NewRegex += @"[0-9]";
                            else
                                if (Regex.IsMatch(StrArray[i].ToString(), @"[A-Za-z]"))
                                    NewRegex += @"[A-Za-z]";
                                else
                                    return "";
            }
            return NewRegex;
        }



        protected void CheckUser()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;
            bool allMatch = true;

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;


            string iStr = "";

            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();
                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

                // if (ds2.Tables[0].Rows[i] != null)
                if (DRow.Length > 0)
                {
                    int systemTotalBoxes = (int)DRow[0].ItemArray[6];
                    int userTotalBoxes = (int)DRow[0].ItemArray[17];

                    // if (tb != (int)DRow[0].ItemArray[17])
                    if (systemTotalBoxes != userTotalBoxes)
                    {
                        allMatch = false;
                        break;
                    }
                }
            }
            if (!allMatch)
            {
                ShowPopUp("Error!", "User Total Boxes doesnt match system Total Boxes. Please contact Administration for Approval!");
                tblBodyInv.Visible = false;
            }
            else
                CreateForm();

        }


    }
}