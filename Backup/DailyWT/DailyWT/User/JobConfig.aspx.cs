﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class JobConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddJob
                        string eventArgument = this.Request["__EVENTARGUMENT"]; // event arg: Job name

                        if (eventTarget == "AddJob")
                        {
                            if (Session["Admin"] != null)
                            {
                                AddJob(eventArgument);
                            }
                            else if (eventTarget == "Cancel")
                            {
                                Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"), false);
                            }
                        }
                    }
                }
                        else
                        {
                            ShowPopUp("Error!", "You must have Admin rights to add a new job.");
                        }
                }
            }

        protected void CreateForm()
        {
            LoadJobDDL();

            //LoadJFormDDL();

            //LoadAFormDDL();

            // now disable it until job is selected
            //            ddlForm.Enabled = false;
           
        }
        protected void LoadJobDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";
            ddlJob.DataBind();
            ddlJob.Items.Insert(0, " -- Select Job -- ");
        }
        protected void LoadJFormDDL()
        {
            DWUtility dwt = new DWUtility();

            ddlJForms.Items.Clear();       // clear in case it's already populated

            if (ddlJob.SelectedIndex > 0)
            {
                int JobID = Convert.ToInt32(ddlJob.SelectedValue);
                ddlJForms.DataSource = dwt.GetFormsByJobID(JobID);
                ddlJForms.DataTextField = "FormName";
                ddlJForms.DataValueField = "FormID";
                ddlJForms.DataBind();

            }
            else
            {
            }
            ddlJForms.Items.Insert(0, " -- Select Form -- ");
        }
        protected void LoadAFormDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlAForms.Items.Clear();       // clear in case it's already populated
            ddlAForms.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlAForms.DataTextField = "FormName";
            ddlAForms.DataValueField = "FormID";
            ddlAForms.DataBind();
            ddlAForms.Items.Insert(0, " -- Select Form -- ");

            if (ddlJob.SelectedIndex > 0)
            {
            }
            else
            {
            }
        }
        protected void ddlJob_Selected(object sender, EventArgs e)
        {
            trJForms.Visible = true;
            trAForms.Visible = true;
            LoadJFormDDL();

            LoadAFormDDL();
        }
        protected void ibAddJob_Click(object sender, EventArgs e)
        {
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            string name = Request.Form["tbAddJob"].ToString();
            string JobName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (JobName.Length > 5 && JobName.Length < 51)
            {
                DWUtility dwt = new DWUtility();

                DataSet ds1 = new DataSet();
                // check if this form name already exists
                ds1 = dwt.GetJobByName(JobName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    ShowPopUp("Error!", "You entered an existing job name. Please try again.");
                }
                else
                {
                    ShowPopUpOkCancel("Action: Add job", "Are you sure you want to create this job?", "AddJob", JobName);
                }
            }
            else
            {
                ShowPopUp("Error!", "Please enter a valid name from 6-50 chars.");
            }
        }
        protected void AddJob(string JobName)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            if (JobName.Length > 5 && JobName.Length < 51)
            {
                JobName = JobName.ToUpper();
                // check if this form name already exists
                ds1 = dwt.GetJobByName(JobName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    ShowPopUp("Error!", "You entered an existing form name. Please try again.");
                }
                else
                {

                    // new form, so JobID=0, IsJobUsed=true
                    int status = dwt.UpsertJobConfig(0, true, JobName, MyUserID);
                    if (status < 1)
                    {
                        ShowPopUp("Error!", "Job could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                    }
                    else
                    {
                        // recreate the form
                        CreateForm();

                        ShowPopUp("Success!", "Job was added successfully!");
                    }
                }
            }
            else
            {
                ShowPopUp("Error!", "You entered an invalid name for the new job. Please try again.");
            }
            return;
        }

        protected void ibAddJForm_Click(object sender, EventArgs e)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
       
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
  

            int JobID = Convert.ToInt32(ddlJob.SelectedValue);
            int FormID = Convert.ToInt32(ddlAForms.SelectedValue);

            //Check if Job and Formname exist
            ds2 = dwt.GetJobData(FormID);

            if (ds2.Tables[0].Rows.Count > 0)
            {
                ShowPopUp("Error!", "You entered an existing job and form name. Please try again.");
            }
            else
            {
                //new form for job 
                int status = dwt.UpsertJobData(JobID, FormID, MyUserID);
                if (status > 0)
                {
                    ShowPopUp("Success!", "The form was updated successfully.");
                    //clear dropdownlist
                    LoadAFormDDL();
                }
                else
                {
                    ShowPopUp("Error!", "Form could not be updated. Error = " + status.ToString() + " Please show the administrator this error message.");
                }
            }
}
        protected void ibRemoveJForm_Click(object sender, EventArgs e)
        {
            //declare variables
            int FormID = Convert.ToInt32(ddlJForms.SelectedValue);

           //Run stored prodcedure
            DWUtility dwt = new DWUtility();
            int status = dwt.DeleteJobConfigForm(FormID);
            if (status > 0)
            {
                // Notify the user that the file was deleted successfully.
                ShowPopUpAndRD("Successful!", "This form  was successfully Removed.", "/User/JobConfig.aspx");
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your form specification could not be deleted. Please tell the administrator about this error message.", "/User/JobConfig.aspx");
            }
        }
    }
}