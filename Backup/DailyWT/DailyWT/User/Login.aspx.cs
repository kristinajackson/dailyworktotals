﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class Login : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          //  if (User.Identity.IsAuthenticated)    // Windows authenticated user
          //  {
                string s = HttpContext.Current.User.Identity.Name;
                string[] sarray = s.Split('\\');
                string RacfID = sarray[1];

                DWUtility dwt = new DWUtility();
                Users u = new Users();
                u = dwt.GetUserInfoByRacfID(RacfID);

                if (u != null)
                {
                    if (u.UserID > 0)
                    {
                        if (u.IsActive)
                        {
                            Session["MyName"] = u.Name;
                            Session["MyUserID"] = u.UserID;
                            Session["MyImageID"] = u.ImageID;
                            if (u.IsAdmin)
                            {
                                Session["Admin"] = u.UserID;
                            }
                            Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"), false);

                            return;
                        }
                        else
                        {
                            ShowPopUpAndRD("Inactive account!", "Your account is inactive. Please ask the administrator to activate your account.", "/Error.aspx");
                        }
                    }
                    else
                    {
                        ShowPopUpAndRD("Error!", "Error retrieving your account information. Please tell the administrator about this error message.", "/Error.aspx");
                    }
                }
                else
                {
                    ShowPopUpAndRD("Unknown User!", "I am sorry, you do not have an account. Please ask the administrator for an account.", "/Error.aspx");
                }
            }
            //else
            //{
            //    ShowPopUpAndRD("Invalid User!", "I am sorry, your Windows account is not valid. Please tell the administrator about this error message.", "/Error.aspx");
            //}
      //  }
    }
} 