﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class DataEntry : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDApp();
                }
                else
                {
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: tbDEDate
                    if (eventTarget == "tbDEDate")
                    {
                        string eventArgument = this.Request["__EVENTARGUMENT"]; // event arg: date in MM/DD/YYYY format

                        DateTime WorkDate = Convert.ToDateTime(eventArgument);
                        if (WorkDate < DateTime.Today.AddDays(1))
                        {
//                            CreateForm(WorkDate);
                            tbDEDate.Text = eventArgument;
                        }
                        else
                        {
                            DateTime dtNow = DateTime.Today;
                            tbDEDate.Text = dtNow.ToString("d");

                            // recreate the screen
//                            CreateForm(dtNow);

                            ShowPopUp("Error!", "Future dates do not make any sense!!");
                            return;
                        }
                    }
                }
            }
        }
        protected void LoadDDApp()
        {
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");

            tblForm2.Visible = false;
        }
        protected void ddlApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                int AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                if (AppID > 0)
                {
                    tblForm2.Visible = true;

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);
                    DWUtility dwt = new DWUtility();
                    
                    AppConfigInfo aci = dwt.GetAppByID(AppID);
                    DataRow dr = null;

                    int Region = 0;
                    int QtyVerified = 0;
                    int Quantity = 0;
                    int Packets = 0;

                    if (aci.BatchNum)
                    {
                        dr = dwt.GetAppBatchData(AppID);

                        int TQuantity = 0;
                        int BatchNum = 1;
                        if (dr != null)
                        {

                            int ModifiedByUserID = Convert.ToInt32(dr["ModifiedByUserID"]);
                            BatchNum = Convert.ToInt32(dr["BatchNum"]);
                            if (Convert.ToBoolean(dr["BComplete"]))
                            {
                                BatchNum = BatchNum + 1;
                            }
                            else
                            {
                                TQuantity = Convert.ToInt32(dr["TQuantity"]);

                                // only show their own data
                                if (ModifiedByUserID == MyUserID)
                                {
                                    Region = Convert.ToInt32(dr["Region"]);
                                    QtyVerified = Convert.ToInt32(dr["QtyVerified"]);
                                    Quantity = Convert.ToInt32(dr["Quantity"]);
                                    Packets = Convert.ToInt32(dr["Packets"]);
                                }
                            }

                        }
                        tbBatchNum.Value = BatchNum.ToString();
                        tbQuantity.Value = Quantity.ToString();
                        tbFormCount.Value = TQuantity.ToString();

                        tbRegion.Value = Region.ToString();
                        tbQtyVerified.Value = QtyVerified.ToString();
                        tbPackets.Value = Packets.ToString();
                        tblForm1.Visible = true;
                    }
                    else
                    {
                        dr = dwt.GetAppData(AppID, MyUserID, Convert.ToDateTime(tbDEDate.Text));
                        if (dr != null)
                        {
                            tbRegion.Value = Convert.ToString(dr["Region"]);
                            tbQuantity.Value = Convert.ToString(dr["Quantity"]);
                            tbRegion.Value = Convert.ToString(dr["Region"]);
                            tbQtyVerified.Value = Convert.ToString(dr["QtyVerified"]);
                            tbPackets.Value = Convert.ToString(dr["Packets"]);
                        }
                        else
                        {
                            tbRegion.Value = "";
                            tbQuantity.Value = "";
                            tbRegion.Value = "";
                            tbQtyVerified.Value = "";
                            tbPackets.Value = "";
                        }
                        tblForm1.Visible = false;
                    }
                    if (aci.Region)
                        trRegion.Visible = true;
                    else
                        trRegion.Visible = false;
                    if (aci.Packets)
                        trPackets.Visible = true;
                    else
                        trPackets.Visible = false;
                }
            }
        }
    }
}