﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Test.aspx.cs" Inherits="DailyWT.User.Test" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <script>
        $(document).ready(function () {
            $("#tbDEDate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            },
            function (start, end) { // when calendar closes, this function is called, so do a postback to read new data
                //__doPostBack('tbDEDate', start.format('MM/DD/YYYY'));
            });
        });
    </script>
<script>
    $(document).ready(function () {
        var Region = {
            validators: {
                regexp: {
                    regexp: /^[1-5]+$/,
                    message: 'The Region# must be a decimal number'
                }
            }
        },
        Qty = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The number entered must be a decimal number'
                }
            }
        };

        $('#mpForm')
        .bootstrapValidator('addField', 'tbRegion', Region)
        .bootstrapValidator('addField', 'tbBatchNum', Qty)
        .bootstrapValidator('addField', 'tbFormCount', Qty)
        .bootstrapValidator('addField', 'tbQuantity', Qty)
        .bootstrapValidator('addField', 'tbQtyVerified', Qty)
        .bootstrapValidator('addField', 'tbPackets', Qty);
    });
</script>
<div class="container-fluid">

    <!--Row with one column -->
    <div class="row">
        <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Data Entry Module</span><span class="col-xs-1">&nbsp;</span>
<%--                <asp:Calendar CssClass="col-xs-2"  ID="tbDEDate" OnSelectionChanged="tbDEDate_SelectionChanged" runat="server"></asp:Calendar>--%>
<%--                <input class="col-xs-2" type="text" name="tbDEDate" value="<%=Request.Form["tbDEDate"] %>" required />--%>
            <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static"></asp:TextBox>
            <!--hidden fields used to pass number of lines to javascript -->
            <input type="hidden" runat="server" id="hfApps" />
            <input type="hidden" runat="server" id="hfWorked" />
        </div>
    </div>
</div>

<!-- Section 1 -->
<section class="container-fluid" id="section1">
<div class="col-xs-7 col-xs-offset-1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Data Entry Module</span></div>
<div class="panel-body">
    <div class="form-group h4 MyInfo">        
        <label class="control-label col-xs-6">Application:</label>
        <div class="col-xs-6">
            <asp:DropDownList ID="ddlApplication" OnSelectedIndexChanged="ddlApplication_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true" CssClass="form-control"></asp:DropDownList>
        </div>
    </div>
    <div id="tblForm1" runat="server" visible="false">
        <div class="form-group h4 MyInfoNC">        
            <label class="control-label col-xs-6">Batch Number</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbBatchNum" readonly="true" value="<%= this.tbBatchNum %>" placeholder="Batch#"  />
            </div>
        </div>
        <div class="form-group h4 MyInfo">        
            <label class="control-label col-xs-6">Total Forms</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbFormCount" value="<%= this.tbFormCount %>" placeholder="Quantity" />
            </div>
        </div>
    </div>
    <div id="tblForm2" runat="server">
        <div class="form-group h4 MyInfoNC">        
            <label class="control-label col-xs-6">Quantity</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbQuantity" value="<%= this.tbQuantity %>" placeholder="Quantity" />
            </div>
        </div>
        <div class="form-group h4 MyInfo">        
            <label class="control-label col-xs-6">Verify Quantity</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbQtyVerified" value="<%= this.tbQtyVerified %>" placeholder="Verify Quantity" />
            </div>
        </div>
        <div class="form-group h4 MyInfoNC" id="trPackets" runat="server" visible="false">        
            <label class="control-label col-xs-6">Packets</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbPackets" value="<%= this.tbPackets %>" placeholder="Packets" />
            </div>
        </div>
        <div class="form-group h4 MyInfo" id="trRegion" runat="server" visible="false">        
            <label class="control-label col-xs-6">Region</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbRegion" value="<%= this.tbRegion %>" placeholder="Region" />
            </div>
        </div>
    </div>

</div>
<div class="panel-footer">
<%--        <span id="cbDiv" runat="server">--%>
            <span class="MyAboveBelow h6" id="spBatch" runat="server" visible="false">
            <label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="cbBatch" type="checkbox" runat="server" style="zoom:1.5;" />
                <br />&nbsp;&nbsp;&nbsp;Batch
                <br />Complete
            </label>
            </span>
<%--            <label class="Vertical-Text h5 small">Batch<br />Complete</label>--%>
<%--        </span>--%>
        <asp:Button ID="btnSave" CssClass="btn btn-success btn-lg submit-button disabled" runat="server" Text="Save Changes" OnClick="SaveChanges_Click" />
<%--        <asp:Button ID="btnBatch" CssClass="btn btn-success btn-lg submit-button disabled" runat="server" Text="Batch Complete" OnClick="BatchComplete_Click" />--%>
            <button type="reset" id="resetregform" class="btn btn-default btn-lg">
        Clear Forms</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>

</div>
</div>
</section>

</asp:Content>

