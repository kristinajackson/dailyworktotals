﻿using System;
using System.Data;
using System.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.UI.WebControls;

namespace DailyWT.Classes
{
    public class DWUtility
    {
        // data procedures *************************************************************************************************
        public int UpsertFormDestroy(int FormID, bool IsUsed, int FormsShredded, int BoxesShredded, string Note, int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertFormDataII", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@IsUsed", IsUsed);
                    cmd.Parameters.AddWithValue("@FormsShredded", FormsShredded);
                    cmd.Parameters.AddWithValue("@BoxesShredded", BoxesShredded);
                    cmd.Parameters.AddWithValue("@Note", Note);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int UpsertPrintData(int FormID, bool IsUsed, int TotalBoxes, int PartialBoxAmt, int BegAmt, int EndAmt, string BegSeq, string EndSeq, int FormsReq, string Requester, string Approver, int ReasonID, string Comment, int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.UpsertPrintData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@IsUsed", IsUsed);
                    cmd.Parameters.AddWithValue("@TotalBoxes", TotalBoxes);
                    cmd.Parameters.AddWithValue("@PartialBoxAmt", PartialBoxAmt);
                    cmd.Parameters.AddWithValue("@BegAmt", BegAmt);
                    cmd.Parameters.AddWithValue("@EndAmt", EndAmt);
                    cmd.Parameters.AddWithValue("@BegSeq", BegSeq);
                    cmd.Parameters.AddWithValue("@EndSeq", EndSeq);
                    cmd.Parameters.AddWithValue("@FormsReq", FormsReq);
                    cmd.Parameters.AddWithValue("@Requester", Requester);
                    cmd.Parameters.AddWithValue("@Approver", Approver);
                    cmd.Parameters.AddWithValue("@ReasonID", ReasonID);
                    cmd.Parameters.AddWithValue("@Comment", Comment);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int UpsertJobData(int JobID, int FormID,  int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertJobData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@JobID", JobID);
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int UpsertFormData(int FormID, bool IsUsed, string ShipNum, DateTime ShipDate, int FormsPerBox, int TotalBoxes, int PartialBoxAmt, int BegAmt, int EndAmt, int UserBegAmt, string UserEndAmt, string BegSeq, string EndSeq, string FormLocation, string Comment, int UserID, int Total, bool AdminApprove)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertFormData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@IsUsed", IsUsed);
                    cmd.Parameters.AddWithValue("@ShipNum", ShipNum);
                    cmd.Parameters.AddWithValue("@ShipDate", ShipDate);
                    cmd.Parameters.AddWithValue("@FormsPerBox", FormsPerBox);
                    cmd.Parameters.AddWithValue("@TotalBoxes", TotalBoxes);
                    cmd.Parameters.AddWithValue("@PartialBoxAmt", PartialBoxAmt);
                    cmd.Parameters.AddWithValue("@BegAmt", BegAmt);
                    cmd.Parameters.AddWithValue("@EndAmt", EndAmt);
                    cmd.Parameters.AddWithValue("@UserBegAmt", UserBegAmt);
                    cmd.Parameters.AddWithValue("@UserEndAmt", UserEndAmt);
                    cmd.Parameters.AddWithValue("@BegSeq", BegSeq);
                    cmd.Parameters.AddWithValue("@EndSeq", EndSeq);
                    cmd.Parameters.AddWithValue("@FormLocation", FormLocation);
                    cmd.Parameters.AddWithValue("@Comment", Comment);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@Total", Total);
                    cmd.Parameters.AddWithValue("@AdminApprove", AdminApprove);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }

        public int UpdateFormData(int FormID, bool IsUsed, string ShipNum, DateTime ShipDate, int FormsPerBox, int TotalBoxes, int PartialBoxAmt, int BegAmt, int EndAmt, int UserBegAmt, string UserEndAmt, string BegSeq, string EndSeq, string FormLocation, int UserID, int Total)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpdateFormData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@IsUsed", IsUsed);
                    cmd.Parameters.AddWithValue("@ShipNum", ShipNum);
                    cmd.Parameters.AddWithValue("@ShipDate", ShipDate);
                    cmd.Parameters.AddWithValue("@FormsPerBox", FormsPerBox);
                    cmd.Parameters.AddWithValue("@TotalBoxes", TotalBoxes);
                    cmd.Parameters.AddWithValue("@PartialBoxAmt", PartialBoxAmt);
                    cmd.Parameters.AddWithValue("@BegAmt", BegAmt);
                    cmd.Parameters.AddWithValue("@EndAmt", EndAmt);
                    cmd.Parameters.AddWithValue("@UserBegAmt", UserBegAmt);
                    cmd.Parameters.AddWithValue("@UserEndAmt", UserEndAmt);
                    cmd.Parameters.AddWithValue("@BegSeq", BegSeq);
                    cmd.Parameters.AddWithValue("@EndSeq", EndSeq);
                    cmd.Parameters.AddWithValue("@FormLocation", FormLocation);
                  //  cmd.Parameters.AddWithValue("@Comment", Comment);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@Total", Total);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int UpsertWorkData(int TWorkID, float Hours, int UserID, DateTime WorkDate)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertWorkData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TWorkID", TWorkID);
                    cmd.Parameters.AddWithValue("@Hours", Hours);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@WorkDate", WorkDate);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int UpsertAppData(int AppID, int Region, int BatchNum, int TQuantity, int QtyVerified, int Quantity, int Packets, int UserID, DateTime WorkDate, bool BComplete)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertAppData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AppID", AppID);
                    cmd.Parameters.AddWithValue("@Region", Region);
                    cmd.Parameters.AddWithValue("@BatchNum", BatchNum);
                    cmd.Parameters.AddWithValue("@TQuantity", TQuantity);
                    cmd.Parameters.AddWithValue("@QtyVerified", QtyVerified);
                    cmd.Parameters.AddWithValue("@Quantity", Quantity);
                    cmd.Parameters.AddWithValue("@Packets", Packets);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@WorkDate", WorkDate);
                    cmd.Parameters.AddWithValue("@BComplete", BComplete);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public DataSet GetFormDestroy(bool IsUsed, int FormID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetFormDestroy", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsUsed", IsUsed);
                    cmd.Parameters.AddWithValue("@FormID", FormID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetFormData(bool IsUsed, int FormID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetFormData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsUsed", IsUsed);
                    cmd.Parameters.AddWithValue("@FormID", FormID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetTWorkData(int UserID, DateTime WorkDate)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetTWorkData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@WorkDate", WorkDate);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataRow GetAppData(int AppID, int UserID, DateTime WorkDate)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetAppData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AppID", AppID);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@WorkDate", WorkDate);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0];
                }
                return (DataRow)null;
            }
        }
        public DataRow GetAppBatchData(int AppID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetAppBatchData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AppID", AppID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].Rows[0];
                }
                return (DataRow)null;
            }
        }
        public DataSet GetFormSpec(int FormID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetFormSpec", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataTable GetFormSpecs()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.GetFormSpecs", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds.Tables[0];
            }
        }
        public int InsFormSpec(int UserID, string FileName, string Version, string Notes, byte[] Form, string FormType)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.InsFormSpec", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@FileName", FileName);
                    cmd.Parameters.AddWithValue("@Version", Version);
                    cmd.Parameters.AddWithValue("@Notes", Notes);
                    cmd.Parameters.AddWithValue("@Form", Form);
                    cmd.Parameters.AddWithValue("@FormType", FormType);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int DeleteFormSpec(int FormID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.DeleteFormSpec", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
            }
            return status;
        }
        public int DeleteJobConfigForm(int FormID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.DeleteJobConfigForm", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    // cmd.Parameters.AddWithValue("@FormName", FormName);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
            }
            return status;
        }
        // config procedures *************************************************************************************************
        public int UpsertFormConfig(int FormID, bool IsFormUsed, string FormName, bool ShipInfo, bool Sequence, int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertFormConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@IsFormUsed", IsFormUsed);
                    cmd.Parameters.AddWithValue("@FormName", FormName);
                    cmd.Parameters.AddWithValue("@ShipInfo", ShipInfo);
                    cmd.Parameters.AddWithValue("@Sequence", Sequence);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }

        public int UpsertJobConfig(int JobID, bool IsJobUsed, string JobName, int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.UpsertJobConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@JobID", JobID);
                    cmd.Parameters.AddWithValue("@IsJobUsed", IsJobUsed);
                    cmd.Parameters.AddWithValue("@JobName", JobName);
                    //  cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }

        public int UpsertAppConfig(int AppID, bool IsAppUsed, string AppName, bool Region, bool BatchNum, bool Packets, int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertAppConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AppID", AppID);
                    cmd.Parameters.AddWithValue("@IsAppUsed", IsAppUsed);
                    cmd.Parameters.AddWithValue("@AppName", AppName);
                    cmd.Parameters.AddWithValue("@Region", Region);
                    cmd.Parameters.AddWithValue("@BatchNum", BatchNum);
                    cmd.Parameters.AddWithValue("@Packets", Packets);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int UpsertTWorkConfig(int TWorkID, bool IsTWorkUsed, string TWorkName, int UserID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpsertTWorkConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TWorkID", TWorkID);
                    cmd.Parameters.AddWithValue("@IsTWorkUsed", IsTWorkUsed);
                    cmd.Parameters.AddWithValue("@TWorkName", TWorkName);
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }

        public int DeleteJobConfigForm(int FormID, int JobID, string FormName)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.DeleteJobConfigForm", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);
                    cmd.Parameters.AddWithValue("@JobID", JobID);
                    cmd.Parameters.AddWithValue("@FormName", FormName);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }

        public FormConfigInfo GetFormByID(int FormID)
        {
            DataSet ds = new DataSet();
            FormConfigInfo fci = new FormConfigInfo();

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DWT.GetFormByID", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@FormID", FormID);

                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                        }
                    }

                    DataRow dr = ds.Tables[0].Rows[0];

                    fci.FormID = Convert.ToInt32(dr["FormID"]);
                    fci.IsFormUsed = Convert.ToBoolean(dr["IsFormUsed"]);
                    fci.FormName = (dr["FormName"]).ToString();
                    fci.ShipInfo = Convert.ToBoolean(dr["ShipInfo"]);
                    fci.Sequence = Convert.ToBoolean(dr["Sequence"]);
                    fci.CreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"]);
                    fci.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                }
            }
            catch
            {

            }
            return fci;  // return a populated fci or a null fci
        }
        public AppConfigInfo GetAppByID(int AppID)
        {
            DataSet ds = new DataSet();
            AppConfigInfo aci = new AppConfigInfo();

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DWT.GetAppByID", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@AppID", AppID);

                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                        }
                    }

                    DataRow dr = ds.Tables[0].Rows[0];

                    aci.AppID = Convert.ToInt32(dr["AppID"]);
                    aci.IsAppUsed = Convert.ToBoolean(dr["IsAppUsed"]);
                    aci.AppName = (dr["AppName"]).ToString();
                    aci.Region = Convert.ToBoolean(dr["Region"]);
                    aci.BatchNum = Convert.ToBoolean(dr["BatchNum"]);
                    aci.Packets = Convert.ToBoolean(dr["Packets"]);
                    aci.CreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"]);
                    aci.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                }
            }
            catch
            {

            }
            return aci;  // return a populated fci or a null fci
        }
        public JobConfigInfo GetJobByID(int JobID)
        {
            DataSet ds = new DataSet();
            JobConfigInfo jci = new JobConfigInfo();

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DWT.GetJobByID", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobID", JobID);

                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            da.SelectCommand = cmd;
                            da.Fill(ds);
                        }
                    }

                    DataRow dr = ds.Tables[0].Rows[0];

                    jci.JobID = Convert.ToInt32(dr["JobID"]);
                    jci.IsJobUsed = Convert.ToBoolean(dr["IsJobUsed"]);
                    jci.JobName = (dr["JobName"]).ToString();
                    jci.LastFormID = Convert.ToInt32(dr["LastFormID"]);
                    jci.CreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"]);
                    jci.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    //jci.ModifiedByUserID = Convert.ToInt32(dr["ModifiedByUserID"]);
                    //jci.ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]);
                }
            }
            catch
            {

            }
            return jci;  // return a populated jci or a null jci
        }
        public DataSet GetReasons(bool IsReasonUsed)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetReasons", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsReasonUsed", IsReasonUsed);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetFormByName(string FormName)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetFormByName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormName", FormName);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetAppByName(string AppName)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetAppByName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AppName", AppName);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetTWorkByName(string TWorkName)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetTWorkByName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TWorkName", TWorkName);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetJobByName(string JobName)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetJobByName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@JobName", JobName);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetFormConfig(bool IsFormUsed)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetFormConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsFormUsed", IsFormUsed);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetFormsByJobID(int JobID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetFormsByJobID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@JobID", JobID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetJobConfig(bool IsJobUsed)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetJobConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsJobUsed", IsJobUsed);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetJobData(int FormID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetJobData", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormID", FormID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetTWorkConfig(bool IsTWorkUsed)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetTWorkConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsTWorkUsed", IsTWorkUsed);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public DataSet GetAppConfig(bool IsAppUsed)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetAppConfig", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IsAppUsed", IsAppUsed);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        // User procedures *************************************************************************************************
        public int ChangeAdminStatus(int UserID, int ModifiedByUserID)
        {
            int Count = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.ChangeAdminStatus", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@ModifiedByUserID", ModifiedByUserID);
                    cmd.Connection = con;
                    con.Open();
                    Count = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return Count;
            }
        }
        internal int ChangeActiveStatus(int UserID, int ModifiedByUserID)
        {
            int Count = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.ChangeActiveStatus", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@ModifiedByUserID", ModifiedByUserID);
                    cmd.Connection = con;
                    con.Open();
                    Count = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return Count;
            }
        }
        public int AddUser(int UserID, int CreatedByUserID)
        {
            int Count = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.AddUser", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@CreatedByUserID", CreatedByUserID);
                    cmd.Connection = con;
                    con.Open();
                    Count = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return Count;
            }
        }
        public int DelUser(int UserID)
        {
            int Count = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.DelUser", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Connection = con;
                    con.Open();
                    Count = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return Count;
            }
        }

        public int GetUserIDByRacfID(string RacfID)
        {
            int UserID = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetUserIDByRacfID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RacfID", RacfID);
                    cmd.Connection = con;
                    con.Open();
                    UserID = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return UserID;
            }
        }
        public Users GetUserInfoByRacfID(string RacfID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetUserInfoByRacfID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@RacfID", RacfID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
            }

            Users u = new Users();
            if (ds != null)
            {
                u.UserID = Convert.ToInt32(ds.Tables[0].Rows[0]["UserID"]);
                u.RacfID = ds.Tables[0].Rows[0]["RacfID"].ToString();
                u.Name = ds.Tables[0].Rows[0]["Name"].ToString();
                u.IsActive = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsActive"]);
                u.IsAdmin = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsAdmin"]);
                u.ImageID = Convert.ToInt32(ds.Tables[0].Rows[0]["ImageID"]);
                u.CreatedByUserID = Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedByUserID"]);
                u.CreatedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["CreatedDate"]);
                u.ModifiedByUserID = Convert.ToInt32(ds.Tables[0].Rows[0]["ModifiedByUserID"]);
                u.ModifiedDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["ModifiedDate"]);
            }

            return u;
        }
        public DataSet GetAppUsers()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetAppUsers", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }

                return ds;
            }
        }
        public DataSet GetAvailUsers()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetAvailUsers", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }

                return ds;
            }
        }
        public DataSet GetAllUsers()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetAllUsers", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }

                return ds;
            }
        }
        public DataSet GetAdmin()
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetAdmin", con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }

                return ds;
            }
        }
        // Image procedures *************************************************************************************************
        public int DeleteImage(int UserID, int ImageID, int NewDefImageID)
        {
            int status = 1;

            if (NewDefImageID != 0)
                status = SetDefImageID(UserID, NewDefImageID);

            if (status > 0)
            {

                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DWT.DeleteImage", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ImageID", ImageID);
                        cmd.Connection = con;
                        con.Open();
                        status = Convert.ToInt32(cmd.ExecuteScalar());
                        con.Close();
                    }
                }
            }
            return status;
        }
        public int SetDefImageID(int UserID, int ImageID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.SetDefImageID", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@ImageID", ImageID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public int InsImage(int UserID, string ImageName, byte[] Image, string ImageType)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.InsImage", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);
                    cmd.Parameters.AddWithValue("@ImageName", ImageName);
                    cmd.Parameters.AddWithValue("@Image", Image);
                    cmd.Parameters.AddWithValue("@ImageType", ImageType);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                return status;
            }
        }
        public DataSet GetImage(int ImageID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetImage", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ImageID", ImageID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public Byte[] GetImageBytes(int ImageID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DWT.GetImage", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ImageID", ImageID);
                        cmd.Connection = con;
                        con.Open();
                        byte[] img = (byte[])cmd.ExecuteScalar();
                        con.Close();
                        return img;
                    }

                }
            }
            catch
            {
                return (Byte[])null;
            }
        }
        public DataSet GetImages(int UserID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DWT.GetImages", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@UserID", UserID);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }
        public int GetImagesIDsForGV(int UserID, GridView gv)
        {
            SqlDataReader dr;

            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("DWT.GetImages", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UserID", UserID);
                        con.Open();
                        dr = cmd.ExecuteReader();
                        gv.DataSource = dr;
                        gv.DataBind();
                        con.Close();
                    }
                }
            }
            catch
            {
                return -1;
            }
            return 1;
        }
        public static void RemoveCssClass(WebControl control, String css)
        {
            control.CssClass = String.Join(" ", control.CssClass.Split(' ').Where(x => x != css).ToArray());
        }

        public static void AddCssClass(WebControl control, String css)
        {
            css += " " + control.CssClass;
            control.CssClass = css;
        }


        internal int ChangeAdminstatus(int UserID, int MyUserID)
        {
            throw new NotImplementedException();
        }

        internal int ChangeActivestatus(int UserID, int MyUserID)
        {
            throw new NotImplementedException();
        }
    }
}