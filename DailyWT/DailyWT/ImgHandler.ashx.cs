﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;

namespace DailyWT
{
    /// <summary>
    /// ImgHandler is used to retrieve an image for the background image of the page
    /// </summary>
    public class ImgHandler : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
    //public class ImgHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int ImageID = 0;

            // did we pass the imageid via querystring?
            if (context.Request.QueryString["ImageID"] != null)
            {
                // yes
                ImageID = Convert.ToInt32(context.Request.QueryString["ImageID"]);
            }
            else
            {
                if (context.Session["MyImageID"] != null)
                {
                    // no, so get it from the session variable
                    ImageID = Convert.ToInt32(context.Session["MyImageID"]);
                }
                else
                {
                    // for some reason, no image identified, so just return without one
                    return;
                }
            }
            Classes.DWUtility dwu = new Classes.DWUtility();
            DataSet ds = new DataSet();
            ds = dwu.GetImage(ImageID);

            if (ds != null)
            {
                context.Response.Clear();
                context.Response.ContentType = ds.Tables[0].Rows[0]["ImageType"].ToString();
                context.Response.BinaryWrite((Byte[])ds.Tables[0].Rows[0]["Image"]);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}