﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CitationsDaily.aspx.cs" Inherits="DailyWT.DataEntryDaily.CitationsDaily" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ScriptManager>

<div id="dialog" title="Congrats!!">
    <div id="dialog-text"></div>
</div>
    <div id="dialog1" title="Thank you!!">
    <div id="dialog2"></div>
</div>

    <style>
        .txtboxde {
        text-align: right;
        color: #000000;
        }

        .line {
        border-top:solid;
        border-left:none;
        border-right: none;
        border-bottom:double;
        background-color:#f7f8f9;
        }
        .outstanding {
       border-top:solid;
        border-left:none;
        border-right: none;
        border-bottom:none;
        background-color:#f7f8f9;
        }
        .labeldwt {
        /*width:300px;*/
        text-align: right;
        border-left:none;
        border-right: none;
        border-bottom:none;
        border-top:none;
        }
    </style>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server"><ContentTemplate>
    <div class="container-fluid">
   
    <!--Row with one column -->
    <div class="row">
        <br />
        <br />
        <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Personal Daily Module</span><span class="col-xs-1">&nbsp;</span>
        </div>
    </div>
</div>




<!-- Section 1 -->
<section class="container-fluid" id="section1">
<div class="col-xs-7 col-xs-offset-1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Personal Daily Module</span></div>
<div class="panel-body">
         <label class="control-label col-xs-6">Application:</label>
        <div class="col-xs-6">
            <asp:DropDownList ID="ddlApplication" OnSelectedIndexChanged="ddlApplication_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true" CssClass="form-control"></asp:DropDownList>
        </div>
    <asp:Label ID="lblmsg" runat="server"  ForeColor="#FF3300" Font-Size="Large" Font-Bold="True" ></asp:Label>

      <!------- Batch Number ------>
       <div id="BatchnumbertblForm" runat="server" visible="false"  >
  <table id="tblfrm3" class="table table-striped" runat="server" >
      <tbody>
          <tr style="background-color:#cccc00;" >
            <td class="form-group" ><div id="trBatchNum" runat="server" style="background-size:20px;">
                <div class="col-xs-7">*
                <label id="Label1" for="BatchNumlbl">Batch Number </label></div>
                <asp:TextBox ID="txtBatchNum" runat="server"  CssClass="txtboxde form-control" ToolTip="What is the batch number?" Width="300px" TabIndex="1"></asp:TextBox>                               
            </div> 
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="txtBatchNum" SetFocusOnError="True"></asp:RegularExpressionValidator> 
            </td>
            </tr> 
          </tbody>
          </table></div>
    
                       <!------ Region ------>
    <div id="RegiontblForm" runat="server" visible="false">
           <table id="tblfrm4" class="table table-striped" runat="server" >
               <tbody> 
           <tr style="background-color:#cccc00;">
            <td class="form-group" ><div id="trRegion" runat="server" style=" background-size:10px;">
                <div class="col-xs-7">*
                <label id="Label3" for="txtRegion">Region </label></div>
                 <asp:DropDownList ID="txtRegion" runat="server" Width="300px" CssClass="btn dropdown-toggle txtboxde" style="color:#000000; border:1px; border-color:#f7f8f9;" TabIndex="2">
                                                            <asp:ListItem Value="1">Region 1</asp:ListItem>
                                                            <asp:ListItem Value="2">Region 2</asp:ListItem>
                                                            <asp:ListItem Value="3">Region 3</asp:ListItem>
                                                            <asp:ListItem Value="4">Region 4</asp:ListItem>
                                                        </asp:DropDownList>
              </div>  </td>
            </tr> 
      </tbody>
  </table>
        </div>
    <!------- Packets ------>
      <div id="PacketstblForm" runat="server" visible="false"  >
  <table id="Table2" class="table table-striped" runat="server" >
      <tbody> 
    <tr style="background-color:#cccc00;">
            <td class="form-group" ><div id="trPackets" runat="server"  style="background-size:10px;">
                <div class="col-xs-7">*
                <label id="PacketsLabel" for="txtPackets"> Packets Recieved</label></div>
                <asp:TextBox ID="txtPackets" runat="server" CssClass="txtboxde form-control" ToolTip="How many Packets have you recieved?" Width="300px" TabIndex="3"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="txtPackets" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>

              </div>  </td>
            </tr> 
          </tbody>
       </table>
          </div>
          <!------ Angular ------>
          <div id="AngulartblForm" runat="server" visible="false"  >
  <table id="Table3" class="table table-striped" runat="server" >
      <tbody>
        <tr style="background-color:#cccc00;">
            <td class="form-group" ><div id="trAngular" runat="server" >
                <div class="col-xs-7">*
                <label id="Angularlbl" for="txtAngular"> Angular Recieved</label></div>
                <asp:TextBox ID="txtAngular" runat="server" CssClass="txtboxde form-control"  ToolTip="How many Angular did you recieved?" OnTextChanged="txtAngular_TextChanged" AutoPostBack="true" Width="300px" TabIndex="4"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="txtAngular" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
              </div>  </td>
            </tr> 
          </tbody>
      </table>
              </div>
              <!------- Interviews ------->
                <div id="InterviewtblForm" runat="server" visible="false"  >
  <table id="Table4" class="table table-striped" runat="server" >
      <tbody>
        <tr style="background-color:#cccc00;">
            <td class="form-group" ><div id="trInterviews" runat="server" >
                <div class="col-xs-7">*
                <label id="Interviewslbl" for="txtInterviews">Interviews Recieved</label></div>
                <asp:TextBox ID="txtInterviews" runat="server" CssClass="txtboxde form-control"  ToolTip="How many Interviews did you recieved?" OnTextChanged="txtInterviews_TextChanged" AutoPostBack="true" Width="300px" TabIndex="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="txtInterviews" SetFocusOnError="True"></asp:RegularExpressionValidator>
              </div>  </td>
            </tr> 
          </tbody>
      </table>
                    </div>
                  
 
</div>
    <!------ Table Form ------>
    <div id="tblForm1" runat="server" visible="false" >
        <table id="Table1" class="table table-striped" runat="server" >
            <tbody>
        <tr>
            <td class="form-group" style="background-color:#cccc00;">
                <div class="col-xs-7" >
                <label id="frmsreclbl" for="frmsrectxt">Total Forms Recieved</label><asp:TextBox ID="frmsrecdate" runat="server" BorderStyle="Dotted" BorderWidth="1px" Width="100px" Font-Bold="True"></asp:TextBox></div>
                <asp:TextBox ID="frmsrectxt" runat="server" CssClass="txtboxde form-control"  Width="300px" TabIndex="6"></asp:TextBox>
                <label style="font-size: small" id="lblsumr" runat="server">Sum of angular and Interviews</label>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="frmsrectxt" SetFocusOnError="True"></asp:RegularExpressionValidator>
            </td>
            </tr>
                <tr>
            <td class="form-group">
                <div class="col-xs-7">
                <label id="Label4" for="frmsrectxt"></label></div>
               <label>=</label>
                
                  </td>
            </tr>
        <tr runat="server" visible="false">
            <td class="form-group" runat="server" >
                 <div class="col-xs-7">
                <label id="ttlincompLabel" for="ttlincomptxt" style="color:	#A0A0A0;">Total Incomplete</label></div>
                <asp:Textbox ID="ttlincomptxt" runat="server" CssClass="labeldwt "  style="background-color:#f7f8f9;" Width="300px" Enabled="False" ReadOnly="true"></asp:Textbox>
            </td>
        </tr> 
        <tr>
            <td style="background-color: #00cc66;" class="form-group">
                <div class="col-xs-7">
                <label id="ttlkeyLabel" for="ttlkeytxt">Total to be Keyed</label>
                </div>
                <asp:Textbox ID="ttlkeytxt" runat="server" style="background-color:#f7f8f9;" CssClass="labeldwt form-control"   Width="300px" Enabled="False" ReadOnly="true" TabIndex="7"></asp:Textbox>
                </td>
                </tr>
                <%--<tr><td></td></tr>--%>
                    <!------ Angular Keyed------>
        <tr  runat="server" visible="true" id="tblangukey">
            <td class="form-group" style="background-color:#cccc00;"><div id="Div2" runat="server" >
                <div class="col-xs-7">*
                <label id="Label7" for="txtAngular">Angular Keyed</label></div>
                <asp:TextBox ID="txtangkey" runat="server" CssClass="txtboxde form-control"  ToolTip="How many Angular did you complete?" OnTextChanged="txtangkey_TextChanged" AutoPostBack="true" Width="300px" TabIndex="4"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="txtangkey" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
              </div> <br /><br /> </td>
            </tr> 
              <!------- Interviews Keyed ------->
        <tr  runat="server" visible="true" id="tblinterkey">
            <td class="form-group" style="background-color:#cccc00;"><div id="Div4" runat="server" >
                <div class="col-xs-7">*
                <label id="Label8" for="txtInterviews">Interviews Keyed</label></div>
                <asp:TextBox ID="txtInterkey" runat="server" CssClass="txtboxde form-control"  ToolTip="How many Interviews did you complete?" OnTextChanged="txtInterkey_TextChanged" AutoPostBack="true" Width="300px" TabIndex="5"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="txtInterkey" SetFocusOnError="True"></asp:RegularExpressionValidator>
              </div> <br /> <br /></td>           
            </tr> 
        <tr >
            <td class="form-group" style="background-color:#cccc00;">
                <div class="col-xs-7" >
                <label id="keyedLabel" for="keyeddate">Total Keyed </label><asp:Textbox ID="keyeddate" runat="server" BorderStyle="Dotted" BorderWidth="1px" Width="100px" Font-Bold="True"></asp:Textbox></div>
                <asp:TextBox ID="keytxt" runat="server" CssClass="txtboxde form-control" Width="300px" TabIndex="8"></asp:TextBox>
                <label style="font-size: small" runat="server" visible="false" id="lblsum">Sum of angular and Interviews</label>
               <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="keytxt" SetFocusOnError="True"></asp:RegularExpressionValidator>    
                 </td>
            </tr>
              
                 <tr>
            <td class="form-group">
                <div class="col-xs-7">
                <label id="Label5" for="outtxt"></label></div>
               <label>=</label>
                
                  </td>
            </tr>
        <tr>
            <td class="form-group">
                 <div class="col-xs-7">
                <label id="outLabel" for="outtxt" style="color:	#A0A0A0;">Need To Be Keyed</label></div>
                <asp:Textbox ID="outtxt" runat="server" CssClass="line txtboxde form-control" ReadOnly="true" TextMode="Number" ToolTip="Can't type here" Enabled="false" Width="300px"></asp:Textbox>
            </td>
            </tr>
             
    </tbody>
  </table>
          
    </div>

<!------ Verify Quantity ------>
    <div id="VerifyQuantitytblForm" runat="server" visible="false"  >
  <table id="tblfrm" class="table table-striped" runat="server" >
    <tbody>
        <tr style="background-color:#cccc00;">
            <td class="form-group"><div id="trVerifyQuantity" runat="server"  >
                <div class="col-xs-7">*
                <label id="Label2" for="tbQtyVerified">Verify Forms </label></div>
         <div class="col-xs-offset-7">       <asp:CheckBox ID="tbQtyVerified" runat="server" CssClass="checkbox"  Font-Size="XX-Large" OnCheckedChanged="tbQtyVerified_CheckedChanged" ToolTip="Check If All forms are Verified" TabIndex="9" /></div>

              </div>  </td>
            </tr> 
           
       
        </tbody>
        </table>
        </div>
    <!------ Batch Complete ------>
    <div id="BatchnumtblForm" runat="server" visible="false"  >
  <table id="batchtblfrm" class="table table-striped" runat="server" >
    <tbody>
        <tr style="background-color:#cccc00;">
            <td class="form-group"><div id="trBatchNumCheck" runat="server" >
                <div class="col-xs-7">*
                <label id="Label6" for="tbBathNumCheck">Batch Completed </label></div>
           <div class="col-xs-offset-7">     <asp:CheckBox ID="cbBatch" runat="server"  Font-Size="XX-Large" ToolTip="Batch number completed checkbox" CssClass="checkbox" TabIndex="11" /></div>

              </div>  </td>
            </tr> 
           
       
        </tbody>
        </table>
        </div>
    <br />

    <label style="font-size: small" id="reqlbl" runat="server" visible="false">* = Reqiured Field</label><br />
    <label style="font-size: small" id="sclbl" runat="server" visible="false">Please make sure you Save All changes**</label>
    <asp:Button ID="Button1" runat="server"  BackColor="White" BorderColor="White" Width="1px" Height="1px" BorderStyle="None" />
<div class="panel-footer" runat="server" >
    
        <asp:Button ID="btnSave"  CssClass="btn btn-success btn-lg submit-button disabled"  runat="server" Text="  ENTER " OnClick="btnSave_Click"  ValidationGroup="dewt" TabIndex="12" />
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;CANCEL&nbsp;&nbsp;</button>
        <br />
      
</div>

</div>
</div>
    </div>
</section>
        </ContentTemplate></asp:UpdatePanel>
</asp:Content>


