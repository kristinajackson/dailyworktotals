﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Timers;



namespace DailyWT.DataEntryDaily
{
    public partial class CitationsDaily : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Session["MyUserID"] != null)
            //{
                if (!Page.IsPostBack)
                {
                    LoadDDApp();

                }
        }

        protected void LoadDDApp()
        {
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;

            int MyUserID = 0;

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");
            frmsrecdate.Text = String.Format("{0:M/dd/yyyy h:mm:ss tt}", DateTime.Now);
            keyeddate.Text = String.Format("{0:M/dd/yyyy h:mm:ss tt}", DateTime.Now);

        }



        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                frmsrectxtTextChanged();
                //   keytxtTextChanged();

                bool val = false;
                val = TableValidator();
                //Save data
                DWUtility dwt = new DWUtility();
                int AppID = 0, MyUserID = 0;
                string AppName = "";
                AppName = ddlApplication.SelectedItem.Text;
                AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                MyUserID = Convert.ToInt32(Session["MyUserID"]);
                AppConfigInfo aci = dwt.GetAppByID(AppID);


                int Region = 0;
                if (aci.Region) { int.TryParse(txtRegion.Text, out Region); }
                int BatchNum = 0;
                int.TryParse(txtBatchNum.Text, out BatchNum);
                int Interviews = 0;
                int.TryParse(txtInterviews.Text, out Interviews);
                int NAddForms = 0;
                int.TryParse(frmsrectxt.Text, out NAddForms); //frms rec'd
                int TQuantity = 0;
                int.TryParse(ttlkeytxt.Text, out TQuantity); //total to be keyed
                bool QtyVerified = false;
                QtyVerified = tbQtyVerified.Checked;
                int Quantity = 0;
                int.TryParse(txtAngular.Text, out Quantity); // Angulars
                int Packets = 0;
                int.TryParse(txtPackets.Text, out Packets);
                DateTime WorkDate = DateTime.Now;
                bool BComplete = false;
                BComplete = cbBatch.Checked;
                int TIncomplete = 0;
                int.TryParse(ttlincomptxt.Text, out TIncomplete); //total incomplete
                int TComplete = 0;
                int.TryParse(keytxt.Text, out TComplete); //total keyed
                int RBalance = 0;
                int.TryParse(outtxt.Text, out RBalance); //total outstanding
                int OldTQuantity = 0;
                int OldTIncomplete = 0;
                int OldTComplete = 0;
                int OldRBalance = 0;
                int OldNAddForms = 0;
                int InterviewsCompleted = 0;
                int.TryParse(txtInterkey.Text, out InterviewsCompleted);
                int AngularCompleted = 0;
                int.TryParse(txtangkey.Text, out AngularCompleted);
                DateTime FrmsRecDate = Convert.ToDateTime("01/01/1800");
                FrmsRecDate = Convert.ToDateTime(frmsrecdate.Text);
                DateTime TotalKeyedDate = Convert.ToDateTime("01/01/1800");
                TotalKeyedDate = Convert.ToDateTime(keyeddate.Text);
                bool chechcom = CheckBComplete();
                bool alertverify = AlertVerify();


                DataRow dr = null;
                dr = dwt.GetRecentAppData(AppID, MyUserID);

                if (dr != null)
                {//save All previous numbers 
                    OldTQuantity = Convert.ToInt32(dr["TQuantity"]);//total to be keyed;
                    OldTIncomplete = Convert.ToInt32(dr["TIncomplete"]);
                    OldTComplete = Convert.ToInt32(dr["TComplete"]);
                    OldRBalance = Convert.ToInt32(dr["RBalance"]);
                    OldNAddForms = Convert.ToInt32(dr["NAddForms"]);
                }



                if (val == true)
                {
                    lblmsg.Text = "Please enter All Reqiured Fields!";
                    return;
                }

                if (RBalance < 0)
                {
                    lblmsg.Text = "Error!, More forms entered than keyed!";
                    return;
                }
                if (aci.VerifiedQuantity == true && RBalance == 0 && tbQtyVerified.Checked == false)
                {
                    lblmsg.Text = "Error! Data Not saved! Please check Verified Forms checkbox!";
                    DisableAndHideStuff();
                    return;


                }

                if (aci.BatchNum && RBalance == 0 && cbBatch.Checked == false)
                {
                    lblmsg.Text = "Error! Data Not saved! Please check Batchnumber checkbox!";
                    DisableAndHideStuff();
                    return;
                }

                if (TQuantity == 0 && TIncomplete == 0 && TComplete == 0 && RBalance == 0 && NAddForms == 0)
                {//If they forget to check verified quantity                    
                    lblmsg.Text = "Error! Form is empty. No data to save!. Please enter number of forms recieved!";
                    return;
                }

                if (aci.VerifiedQuantity == true && RBalance == 0 && tbQtyVerified.Checked == false && OldRBalance == TComplete)
                {

                    lblmsg.Text = "Error! All Forms completed. Please check Verified Forms checkbox!";
                    DisableAndHideStuff();
                    return;
                }
                if (aci.BatchNum == true && RBalance == 0 && cbBatch.Checked == false && OldRBalance == TComplete)
                {

                    lblmsg.Text = "Error! All Forms completed. Please check BatchNumber checkbox!";
                    DisableAndHideStuff();
                    return;
                }

                if (chechcom == true && RBalance == 0 && aci.BatchNum && txtBatchNum.Text.Length > 0)
                {
                    lblmsg.Text = "Error! Duplicate batch number!";
                    return;
                }

                if (!aci.VerifiedQuantity == true && !aci.BatchNum == true && NAddForms < TComplete && OldRBalance == 0)
                {
                    lblmsg.Text = "Error! More forms keyed than entered. Please check your work!";
                    return;
                }

                if (frmsrectxt.Text.Length <= 0 && keytxt.Text.Length <= 0 && tbQtyVerified.Checked == false && cbBatch.Checked == false)
                {
                    lblmsg.Text = "Error! No data to save!";
                    return;
                }

                if ((tbQtyVerified.Checked || cbBatch.Checked) && RBalance > 0)
                {
                    lblmsg.Text = "Error! Quantity Verified and Batchnumber CheckBox needs to be checked when all forms are completed!";
                    DisableAndHideStuff();
                    return;

                }

                int status = 0;
                status = dwt.UpsertAppData(AppID, Region, BatchNum, Interviews, TQuantity, QtyVerified, Quantity, Packets, MyUserID, WorkDate, BComplete, TIncomplete, TComplete, RBalance, NAddForms, OldTQuantity, OldTIncomplete, OldTComplete, OldRBalance, OldNAddForms, FrmsRecDate, TotalKeyedDate, AppName, InterviewsCompleted, AngularCompleted);
                if (status < 1)
                {
                    lblmsg.Text = "Error! Application could not be added. Error = " + status.ToString() + " Please show the administrator this error message.";
                }
                else
                {
                    // recreate the form
                    CreateForm();

                    if (RBalance > 0)
                    {
                        lblmsg.Text = "Thank you! Data has been saved!";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    }
                    else
                    {

                        lblmsg.Text = "Congrats! All forms completed!";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    }
                }
            }
            else
            {
                DisableAndHideStuff();
                lblmsg.Text = "Error! The dropdown was not valid. Please try reloading this application and try again.";
            }
        }






        protected void ddlApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateForm();
        }

        protected void CreateForm()
        {

            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                ttlincomptxt.Enabled = false;
                ttlkeytxt.Enabled = false;
                outtxt.Enabled = false;
                ttlincomptxt.ReadOnly = true;
                ttlkeytxt.ReadOnly = true;
                outtxt.ReadOnly = true;
                lblmsg.Text = "";
                tbQtyVerified.Checked = false;
                cbBatch.Checked = false;
                txtRegion.ClearSelection();
                txtBatchNum.Text = "";
                txtAngular.Text = "";
                txtPackets.Text = "";
                txtInterviews.Text = "";

                int AppID = 0, MyUserID = 0;
                MyUserID = Convert.ToInt32(Session["MyUserID"]);

                AppID = Convert.ToInt32(ddlApplication.SelectedValue);

                DWUtility dwt = new DWUtility();
                AppConfigInfo aci = dwt.GetAppByID(AppID);

                DataRow dr = null;


                TableFlicker();
                btnSave.CssClass = btnSave.CssClass.ToString().Replace("disabled", ""); // remove disabled class

                int Region = 0;
                int BatchNum = 0;
                int NAddForms = 0;
                int TQuantity = 0;
                int Packets = 0;
                int TIncomplete = 0;
                int TComplete = 0;
                int RBalance = 0;
                int Quantity = 0;
                int Interviews = 0;
                DateTime ModifiedDate = System.DateTime.Now;
                dr = dwt.GetRecentAppData(AppID, MyUserID);
                if (dr != null)
                {

                    TQuantity = Convert.ToInt32(dr["TQuantity"]);//total to be keyed
                    Region = Convert.ToInt32(dr["Region"]);
                    Quantity = Convert.ToInt32(dr["Quantity"]);
                    Packets = Convert.ToInt32(dr["Packets"]);
                    BatchNum = Convert.ToInt32(dr["BatchNum"]);
                    TIncomplete = Convert.ToInt32(dr["TIncomplete"]);//total incomplete
                    TComplete = Convert.ToInt32(dr["TComplete"]);//total keyed
                    RBalance = Convert.ToInt32(dr["RBalance"]);//total outstanding
                    NAddForms = Convert.ToInt32(dr["NAddForms"]);//frms rec'd
                    ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]);
                    Interviews = Convert.ToInt32(dr["Interviews"]);



                    if (RBalance == 0)
                    {//Clean the form if nothing remaining
                        TQuantity = 0;//total to be keyed
                        TIncomplete = 0;//total incomplete
                        TComplete = 0;//total keyed
                        NAddForms = 0;//frms rec'd
                        Interviews = 0;
                        Packets = 0;
                        Quantity = 0;
                        BatchNum = 0;
                    }
                    DateTime date = Convert.ToDateTime(frmsrecdate.Text).Date;
                    DateTime createdate = Convert.ToDateTime(ModifiedDate).Date;
                    if (createdate == date && TIncomplete > 0)
                    {//If today and both frms rec && keyed are in textbx
                        ttlincomptxt.Text = RBalance < 1 ? "" : RBalance.ToString();
                        frmsrectxt.Text = "";
                        keytxt.Text = "";
                        ttlkeytxt.Text = TQuantity < 1 ? "" : TQuantity.ToString();
                        outtxt.Text = RBalance < 1 ? "" : RBalance.ToString();

                        switch (Region)
                        {
                            case 1:
                                txtRegion.Items.FindByValue("1").Selected = true;
                                break;
                            case 2:
                                txtRegion.Items.FindByValue("2").Selected = true;
                                break;
                            case 3:
                                txtRegion.Items.FindByValue("3").Selected = true;
                                break;
                            case 4:
                                txtRegion.Items.FindByValue("4").Selected = true;
                                break;
                        }

                        if (trBatchNum.Visible)
                        {
                            txtBatchNum.Text = BatchNum < 1 ? "" : BatchNum.ToString();
                        }

                        if (trPackets.Visible)
                        {

                            txtPackets.Text = Packets < 1 ? "" : Packets.ToString();
                        }
                    }
                    else
                    {//clean page

                        ttlincomptxt.Text = TIncomplete < 1 ? "" : TIncomplete.ToString();
                        frmsrectxt.Text = "";
                        keytxt.Text = "";
                        ttlkeytxt.Text = TQuantity < 1 ? "" : TQuantity.ToString();
                        outtxt.Text = RBalance < 1 ? "" : RBalance.ToString();

                    }
                    //no matter what day it is always have a value in outstanding and Total to be keyed                   
                    outtxt.Text = RBalance < 1 ? "" : RBalance.ToString();

                    if (frmsrectxt.Text.Length > 0 && keytxt.Text.Length < 0)
                    {///when user entering only what they recieved today to save that number in textbx

                        if (trAngular.Visible) //Angulars
                        { txtAngular.Text = Quantity < 1 ? "" : Quantity.ToString(); }
                        if (trInterviews.Visible)
                        { txtInterviews.Text = Interviews < 1 ? "" : Interviews.ToString(); }
                        keytxt.Text = "";
                    }

                    if (aci.VerifiedQuantity && tbQtyVerified.Checked == false && TQuantity == 0)
                    {//When nothing is chexked and no balance to key for Verify quantity

                        keytxt.Text = TComplete < 1 ? "" : TComplete.ToString();
                        if (trAngular.Visible) //Angulars
                        { txtAngular.Text = Quantity < 1 ? "" : Quantity.ToString(); }
                        if (trInterviews.Visible)
                        { txtInterviews.Text = Interviews < 1 ? "" : Interviews.ToString(); }
                    }

                    if (aci.BatchNum && cbBatch.Checked == false && TQuantity == 0)
                    {//When nothing is chexked and no balance to key for Verify quantity

                        keytxt.Text = TComplete < 1 ? "" : TComplete.ToString();
                        if (trAngular.Visible) //Angulars
                        { txtAngular.Text = Quantity < 1 ? "" : Quantity.ToString(); }
                        if (trInterviews.Visible)
                        { txtInterviews.Text = Interviews < 1 ? "" : Interviews.ToString(); }
                    }


                }
                else
                {
                    //   txtRegion.DataBind();
                    keytxt.Text = "";
                    ttlkeytxt.Text = "";
                    frmsrectxt.Text = "";
                    outtxt.Text = "";
                    lblmsg.Text = "";
                    txtBatchNum.Text = "";
                    txtAngular.Text = "";
                    txtPackets.Text = "";
                    txtInterviews.Text = "";
                    ttlincomptxt.Text = "";
                }

            }
            else
            {
                // if the Save button doesn't contain a 'disabled' class, add one
                if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";
                tblForm1.Visible = false;    // invalid form selected, so hide data
                PacketstblForm.Visible = false;
                BatchnumbertblForm.Visible = false;
                RegiontblForm.Visible = false;
                VerifyQuantitytblForm.Visible = false;
                BatchnumtblForm.Visible = false;
                InterviewtblForm.Visible = false;
                AngulartblForm.Visible = false;
                PacketstblForm.Visible = false;
                //footer.Visible = false;
                //  ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                lblmsg.Text = "Error! More forms keyed than entered. Please check your work!";
            }
        }
        protected void frmsrectxtTextChanged()
        {
            //math calculations for forms rec
            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = dwt.GetAppByID(AppID);

            lblmsg.Text = "";
            ttlincomptxt.Enabled = false;
            ttlkeytxt.Enabled = false;
            outtxt.Enabled = false;
            ttlincomptxt.ReadOnly = true;
            ttlkeytxt.ReadOnly = true;
            outtxt.ReadOnly = true;
            int NAddForms = 0;
            int.TryParse(frmsrectxt.Text, out NAddForms); //frms rec'd
            int TIncomplete = 0;
            int.TryParse(ttlincomptxt.Text, out TIncomplete);
            int TQuantity = 0;//total to be keyed
            int TComplete = 0;
            int.TryParse(keytxt.Text, out TComplete); //total keyed
            int RBalance = 0;
            TQuantity = NAddForms + TIncomplete;
            RBalance = TQuantity - TComplete;

            if (frmsrectxt.Text.Length > 0)
            {
                ttlkeytxt.Text = RBalance < 1 ? "" : RBalance.ToString();
                outtxt.Text = RBalance.ToString();
                ttlincomptxt.Text = RBalance < 1 ? "" : RBalance.ToString();
            }
            else if (keytxt.Text.Length > 0)
            {


                if (RBalance >= 0)
                {
                    outtxt.Text = RBalance.ToString();
                    ttlincomptxt.Text = RBalance < 1 ? "" : RBalance.ToString();
                    ttlkeytxt.Text = RBalance < 1 ? "" : RBalance.ToString();
                }
                else
                {
                    outtxt.Text = RBalance.ToString();
                }
            }



        }


        protected bool AlertVerify()
        {
            bool alertverify = false;
            if (tbQtyVerified.Checked == false)
            {
                alertverify = true;
            }
            return alertverify;

        }


        protected void DisableAndHideStuff()
        {
            ddlApplication.ClearSelection();
            // if the Save button doesn't contain a 'disabled' class, add one
            if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

            tblForm1.Visible = false;    // invalid form selected, so hide data
            PacketstblForm.Visible = false;
            BatchnumbertblForm.Visible = false;
            BatchnumtblForm.Visible = false;
            RegiontblForm.Visible = false;
            VerifyQuantitytblForm.Visible = false;
            InterviewtblForm.Visible = false;
            AngulartblForm.Visible = false;
            PacketstblForm.Visible = false;
            reqlbl.Visible = false;
            sclbl.Visible = false;

        }
        protected bool TableValidator()
        {
            ttlincomptxt.Enabled = false;
            ttlkeytxt.Enabled = false;
            outtxt.Enabled = false;
            ttlincomptxt.ReadOnly = true;
            ttlkeytxt.ReadOnly = true;
            outtxt.ReadOnly = true;
            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();
            bool chbatch = false;

            AppConfigInfo aci = dwt.GetAppByID(AppID);
            if (aci.BatchNum && txtBatchNum.Text.Length <= 0 && trBatchNum.Visible || aci.Packets && txtPackets.Text.Length <= 0 && trPackets.Visible)
            {
                chbatch = true;

            }

            return chbatch;
        }

        protected void TableFlicker()
        {//Turn on off tables visibility

            tblForm1.Visible = false;
            PacketstblForm.Visible = false;
            BatchnumbertblForm.Visible = false;
            RegiontblForm.Visible = false;
            VerifyQuantitytblForm.Visible = false;
            BatchnumtblForm.Visible = false;
            InterviewtblForm.Visible = false;
            tblinterkey.Visible = false;
            AngulartblForm.Visible = false;
            tblangukey.Visible = false;
            PacketstblForm.Visible = false;
            reqlbl.Visible = false;
            lblsum.Visible = false;
            lblsumr.Visible = false;
            sclbl.Visible = true;


            tblForm1.Visible = true;
            sclbl.Visible = true;
            ttlincomptxt.Enabled = false;
            ttlkeytxt.Enabled = false;
            outtxt.Enabled = false;
            ttlincomptxt.ReadOnly = true;
            ttlkeytxt.ReadOnly = true;
            outtxt.ReadOnly = true;
            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = dwt.GetAppByID(AppID);

            if (aci.Packets)
            {

                reqlbl.Visible = true;
                PacketstblForm.Visible = true;
            }
            if (aci.BatchNum)
            {
                BatchnumbertblForm.Visible = true;
                BatchnumtblForm.Visible = true;
                reqlbl.Visible = true;
            }
            if (aci.Region)
            {
                RegiontblForm.Visible = true;
                reqlbl.Visible = true;
            }

            if (aci.VerifiedQuantity)
            {
                reqlbl.Visible = true;
                VerifyQuantitytblForm.Visible = true;
            }
            if (aci.Quantity)
            {
                AngulartblForm.Visible = true;
                tblangukey.Visible = true;
                lblsum.Visible = true;
                lblsumr.Visible = true;
                reqlbl.Visible = true;
            }
            if (aci.Interviews)
            {
                InterviewtblForm.Visible = true;
                tblinterkey.Visible = true;
                lblsum.Visible = true;
                lblsumr.Visible = true;
                reqlbl.Visible = true;
            }


        }
        protected bool ChkMatchBox()
        {//If check box has beedn checked but still have forms to finish
            int frmsrec = 0;
            int.TryParse(frmsrectxt.Text, out frmsrec);
            int key = 0;
            int.TryParse(keytxt.Text, out key); //total keyed
            bool check = false;
            if (ttlkeytxt.Text.Length > 0)
            {
                check = true;
            } return check;

        }


        protected void tbQtyVerified_CheckedChanged(object sender, EventArgs e)
        {
            lblmsg.Text = "";
            bool check = ChkMatchBox();

            if (check == true)
            {

                lblmsg.Text = "ERROR! Forms Recieved and Total Keyed should equal before checking the checkbox!";
            }

        }


        protected bool CheckBComplete()
        {//Check if batch number exsist for the fiscal year. Prevents from updating a record that is already complete


            //Declare Variables
            int BatchNum = 0, TQuantity = 0, AppID = 0;
            bool checkbox2 = false;
            bool BComplete = false;
            BComplete = cbBatch.Checked;
            DateTime today, startDate, endDate;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            int CreatedByUserID = 0;
            CreatedByUserID = Convert.ToInt32(Session["MyUserID"]);

            DWUtility dwt = new DWUtility();
            AppConfigInfo aci = dwt.GetAppByID(AppID);
            DataRow dr = null;


            //Set variables

            if (aci.BatchNum && txtBatchNum.Text.Length > 0)
            {
                int.TryParse(txtBatchNum.Text, out BatchNum);
            }
            if (aci.TQuantity && ttlkeytxt.Text != "")
            {
                TQuantity = Convert.ToInt32(ttlkeytxt.Text);
            }
            int RBalance = 0;
            int.TryParse(outtxt.Text, out RBalance); //total outstanding

            //Update 2/7/2017: I added where BComplete < 1 so that it is checking for completed batches
            //Count the number of duplicates for the fiscal year to flag to make batchnum unique
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ToString());
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from DWT.AppDataLog  where BatchNum = @BatchNum AND AppID = @AppID AND BComplete = 1 AND CreatedByUserID = @CreatedByUserID", sqlConnection))
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@BatchNum", Convert.ToInt32(BatchNum).ToString());
                sqlCommand.Parameters.AddWithValue("@AppID", Convert.ToInt32(AppID).ToString());
                sqlCommand.Parameters.AddWithValue("@BComplete", Convert.ToInt32(BComplete).ToString());
                sqlCommand.Parameters.AddWithValue("@CreatedByUserID", Convert.ToInt32(CreatedByUserID).ToString());

                Int32 userCount = Convert.ToInt32(sqlCommand.ExecuteScalar());
                SqlDataReader reader = sqlCommand.ExecuteReader();


                dr = dwt.GetAppBatchData(AppID, BatchNum); //get all app data

                today = DateTime.Today;

                startDate = new DateTime(DateTime.Today.Year, 7, 1);//startdate of fiscal year
                endDate = new DateTime(DateTime.Today.Year + 1, 7, 1).AddDays(-1);//enddate of fiscal year

                if (today < startDate && today < endDate)//check if your in the current fiscal year
                {
                    startDate = startDate.AddYears(-1);
                    endDate = endDate.AddYears(-1);
                }

                if (dr != null)
                {
                    DateTime ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]); //today date
                    if (ModifiedDate > startDate && ModifiedDate < endDate)//if it is in the fiscal year?
                    {
                        if (reader.HasRows)//Does BatchNum, AppID already exsists ?
                        {
                            checkbox2 = true;
                        }
                    }
                }
                reader.Close();
                sqlConnection.Close();


            }

            return checkbox2;
        }

        protected void txtAngular_TextChanged(object sender, EventArgs e)
        {
            if (frmsrectxt.Text.Length < 0)
            {
                int Quantity = 0;

                if (int.TryParse(txtAngular.Text, out Quantity))
                {

                    frmsrectxt.Text = txtAngular.Text;

                }
                else
                { RegularExpressionValidator8.IsValid = false; }

            }

            else
            {
                int Quantity = 0;
                int.TryParse(txtAngular.Text, out Quantity); // Angulars
                int Interviews = 0;
                int.TryParse(txtInterviews.Text, out Interviews);

                int NAddForms = 0;
                NAddForms = Quantity + Interviews;
                if (int.TryParse(txtAngular.Text, out Quantity))
                {
                    frmsrectxt.Text = NAddForms.ToString();
                }
                else
                { RegularExpressionValidator8.IsValid = false; }
            }
        }

        protected void txtInterviews_TextChanged(object sender, EventArgs e)
        {
            if (frmsrectxt.Text.Length < 0)
            {
                int Interviews = 0;
                if (int.TryParse(txtInterviews.Text, out Interviews))
                {
                    frmsrectxt.Text = txtInterviews.Text;
                }
                else
                {
                    RegularExpressionValidator2.IsValid = false;
                }
            }

            else
            {
                int Quantity = 0;
                int.TryParse(txtAngular.Text, out Quantity); // Angulars
                int Interviews = 0;
                int.TryParse(txtInterviews.Text, out Interviews);

                if (int.TryParse(txtInterviews.Text, out Interviews))
                {
                    int NAddForms = 0;
                    NAddForms = Quantity + Interviews;
                    frmsrectxt.Text = NAddForms.ToString();
                }
                else
                {
                    RegularExpressionValidator2.IsValid = false;
                }
            }
        }

        protected void txtangkey_TextChanged(object sender, EventArgs e)
        {
            if (keytxt.Text.Length < 0)
            {
                int Quantity = 0;

                if (int.TryParse(txtangkey.Text, out Quantity))
                {

                    keytxt.Text = txtangkey.Text;

                }
                else
                { RegularExpressionValidator3.IsValid = false; }

            }

            else
            {
                int Quantity = 0;
                int.TryParse(txtangkey.Text, out Quantity); // Angulars
                int Interviews = 0;
                int.TryParse(txtInterkey.Text, out Interviews);

                int NAddForms = 0;
                NAddForms = Quantity + Interviews;
                if (int.TryParse(txtAngular.Text, out Quantity))
                {
                    keytxt.Text = NAddForms.ToString();
                }
                else
                { RegularExpressionValidator3.IsValid = false; }
            }
        }

        protected void txtInterkey_TextChanged(object sender, EventArgs e)
        {
            if (keytxt.Text.Length < 0)
            {
                int Interviews = 0;
                if (int.TryParse(txtInterkey.Text, out Interviews))
                {
                    keytxt.Text = txtInterkey.Text;
                }
                else
                {
                    RegularExpressionValidator5.IsValid = false;
                }
            }

            else
            {
                int Quantity = 0;
                int.TryParse(txtangkey.Text, out Quantity); // Angulars
                int Interviews = 0;
                int.TryParse(txtInterkey.Text, out Interviews);

                if (int.TryParse(txtInterkey.Text, out Interviews))
                {
                    int NAddForms = 0;
                    NAddForms = Quantity + Interviews;
                    keytxt.Text = NAddForms.ToString();
                }
                else
                {
                    RegularExpressionValidator5.IsValid = false;
                }
            }
        }
    }
}