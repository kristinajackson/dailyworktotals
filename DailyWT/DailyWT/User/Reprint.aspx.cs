﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class Reprint : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["MyUserID"] != null)
            {
            if (!IsPostBack)
            {
                LoadDDJob();
                
            }
            }

             else
             {
                 ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
             }
        }

        protected void LoadDDJob()
        {
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlJob.DataTextField = "FormName";
            ddlJob.DataValueField = "FormID";
            ddlJob.DataBind();
            ddlJob.Items.Insert(0, " -- Select -- ");

            //LoadText();
            txt_job_type.Text = "REPRINT";
        }

        protected void init_views()
        {
            txt_date.Text = DateTime.Now.ToShortDateString();
            txt_request_time.Text = DateTime.Now.ToShortTimeString();
            txtfinishtime.Text = DateTime.Now.ToShortTimeString();
            //loadFormNames();
            //loadSavedForms();
        }



     

        protected void btnSave_Click(object sender, EventArgs e)
        {
            alert.Visible = false;
            delete.Visible = false;
                if (!utils.checkEmptyTextBoxes(new TextBox[] { txt_num_of_docs }) && ddlJob.SelectedIndex > 0)
                {
                   // if (Session["id"] == null)
                    //{
                        int id = DataAccess.CreateRequest(ddlJob.SelectedValue, txt_requestor.Text, Convert.ToDateTime(txt_date.Text), Convert.ToDateTime(txt_request_time.Text),
                            txtfinishtime.Text.ToString().ToUpper(),  Convert.ToInt32(txt_num_of_docs.Text), DateTime.Now, txt_printer_used.Text.ToString(), txt_job_type.Text, txtcomment.InnerText.ToString());
                        Session["id"] = id;
                  //      loadSavedForms();
                        alert.Visible = true;
                        delete.Visible = false;
                        btnNew_Click();
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);
                   // }
                    //else
                    //{
                    //    //update
                    //    DataAccess.UpdateRequest(ddlJob.SelectedValue, txt_requestor.Text, Convert.ToDateTime(txt_date.Text), Convert.ToDateTime(txt_request_time.Text),
                    //     txtfinishtime.Text.ToString().ToUpper(), Convert.ToInt32(txt_num_of_docs.Text), DateTime.Now, txt_printer_used.Text, txt_job_type.Text, txtcomment.InnerText.ToString(), Convert.ToInt32(Session["id"]));
                    //    alert.Visible = true;
                    //    delete.Visible = false;

                    //    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);

                    //}

                        
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.UpdatePanel1, typeof(string), "Alert", "alert('ERROR! Save Failed. Please fill in all fields.');", true);


                }
          
            
        }

        protected void btnNew_Click()
        {
                Session["id"] = null;
                txt_date.Text = DateTime.Now.ToShortDateString();
                txt_request_time.Text = DateTime.Now.ToShortTimeString();
                txtfinishtime.Text = DateTime.Now.ToShortTimeString();
                txt_job_type.Text = String.Empty;
                txt_num_of_docs.Text = String.Empty;
                txt_printer_used.Text = String.Empty;
                txt_requestor.Text = String.Empty;
                txtcomment.InnerText = String.Empty;
                //alert.Visible = false;
                //delete.Visible = false;
                LoadDDJob();
           
        }


        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            maintable.Visible = true;
            init_views();
        }

    }
}