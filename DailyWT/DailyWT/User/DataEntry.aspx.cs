﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace DailyWT.User
{
    public partial class DataEntry : BSDialogs
    {
        // had to do it this way to get the bootstrapvalidator to work
        // bootstrapvalidator doesn't work with runat="server"
        protected string tbRegion { get; set; }
        protected string tbPackets { get; set; }
       // protected string tbFormCount { get; set; }
        protected string tbQuantity { get; set; }
        protected string tbInterviews { get; set; }
        protected string tbBatchNum { get; set; }
        protected string tbCreatedUser { get; set; } //To be able to use the user on the front page
        protected string tbModifiedUser { get; set; }//To be able to use the user on the front page
        protected string tbUser { get; set; }//To be able to use the user on the front page
        protected string tbaddedforms { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDApp();
                    
                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
            if (Session["MyUserID"] != null)

                tbUser = Session["MyUserID"].ToString();
        }
        protected void LoadDDApp()
        {
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;

            int MyUserID = 0;

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");

            tblForm2.Visible = false;
            tblForm1.Visible = false;
            tblForm.Visible = false;
        }

        protected void ddlApplication_SelectedIndexChanged(object sender, EventArgs e)
        {

            CreateForm();
        }

        protected void CreateForm()
        {
            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                int AppID = 0, MyUserID=0;
                AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                if (AppID > 0)
                {
                    tblForm2.Visible = true;
                    tblForm1.Visible = true;//
                    tblForm.Visible = true;
                    cbBatch.Checked = false;
                    spBatch.Visible = false;
                    tblForm1.Visible = false;
                    btnaddnf.Visible = false;
                    tbQtyVerified.Checked = false;

                    btnSave.CssClass = btnSave.CssClass.ToString().Replace("disabled", ""); // remove disabled class

                    MyUserID = Convert.ToInt32(Session["MyUserID"]);
                    DWUtility dwt = new DWUtility();

                    AppConfigInfo aci = dwt.GetAppByID(AppID);
                    DataRow dr = null;

                    int Region = 0;
                  //  bool QtyVerified = false;
                    int Quantity = 0;
                    int Packets = 0;
                    int CreatedByUserID = 0;
                    int ModifiedByUserID1 = 0;//Using this modified user for front page
                    int TQuantity = 0;
                    int TIncomplete = 0;
                    int TComplete = 0;
                    int RBalance = 0;
                    int ModifiedByUserID = 0;
                    int Interviews = 0;
                    tbFormCount.Disabled = false;         
                    
                    if (aci.BatchNum)
                    {
                        CheckTFDisble();
                        // if batch data, set date to today
                        tbDEDate.Text = String.Format("{0:MM/dd/yyyy}", DateTime.Now);
                        int BatchNum = 0;
                        dr = dwt.GetAppBatchData(AppID, BatchNum);
                        

                        

                        if (dr != null)
                        {
                             ModifiedByUserID = Convert.ToInt32(dr["ModifiedByUserID"]);

                            BatchNum = Convert.ToInt32(dr["BatchNum"]);
                            if (Convert.ToBoolean(dr["BComplete"]))
                            {
                                BatchNum = 0;
                            }
                            else
                            {
                                // let them see the Total Qty entered
                                TQuantity = Convert.ToInt32(dr["TQuantity"]);
                                Region = Convert.ToInt32(dr["Region"]);
                                Packets = Convert.ToInt32(dr["Packets"]);
                                CreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"]);
                                ModifiedByUserID1 = Convert.ToInt32(dr["ModifiedByUserID"]);
                                BatchNum = Convert.ToInt32(dr["BatchNum"]); 
                                TIncomplete = Convert.ToInt32(dr["TIncomplete"]);
                                TComplete = Convert.ToInt32(dr["TComplete"]);
                                RBalance = Convert.ToInt32(dr["RBalance"]);
                           //     Interviews = Convert.ToInt32(dr["Interviews"]);
                            }
                        }


                        tbQuantity = Quantity < 1 ? "" : Quantity.ToString();
                        tbFormCount.Value = TQuantity < 1 ? "" : TQuantity.ToString();

                        tbRegion = Region < 1 ? "" : Region.ToString();
                        tbInterviews = Interviews < 1 ? "" : Interviews.ToString();
                       // tbQtyVerified = QtyVerified < 1 ? "" : QtyVerified.ToString();
                        tbPackets = Packets < 1 ? "" : Packets.ToString();
                        tbCreatedUser = CreatedByUserID < 1 ? "" : CreatedByUserID.ToString();
                        tbModifiedUser = CreatedByUserID < 1 ? "" : ModifiedByUserID1.ToString();
                        tbCreatedUser = CreatedByUserID.ToString();//
                        tbBatchNum = BatchNum < 1 ? "" : BatchNum.ToString();
                        lblincomplete.InnerHtml = TIncomplete.ToString();
                        lblTotalCompleted.InnerHtml = TComplete.ToString();
                        lblremaining.InnerHtml = RBalance.ToString();

                        spBatch.Visible = true;
                        tblForm1.Visible = true;
                        btnaddnf.Visible = false;
                        
                    }
                    else
                    {
                        dr = dwt.GetAppData(AppID, MyUserID, Convert.ToDateTime(tbDEDate.Text));

                        if (dr != null && Convert.ToBoolean(dr["BComplete"]) == false)
                        {

                            
                            
                            //to view all data with incomplete batches
                                TQuantity = Convert.ToInt32(dr["TQuantity"]);
                                Region = Convert.ToInt32(dr["Region"]);
                                Packets = Convert.ToInt32(dr["Packets"]);
                                CreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"]);
                                ModifiedByUserID1 = Convert.ToInt32(dr["ModifiedByUserID"]);
                                TIncomplete = Convert.ToInt32(dr["TIncomplete"]);
                                TComplete = Convert.ToInt32(dr["TComplete"]);
                                RBalance = Convert.ToInt32(dr["RBalance"]);
                            //    Interviews = Convert.ToInt32(dr["Interviews"]);

                                tbRegion = Region < 1 ? "" : Region.ToString();

                                tbQuantity = Quantity < 1 ? "" : Quantity.ToString();
                                tbInterviews = Interviews < 1 ? "" : Interviews.ToString();
                             //   tbQtyVerified = QtyVerified < 1 ? "" : QtyVerified.ToString();
                                tbPackets = Packets < 1 ? "" : Packets.ToString();
                                tbCreatedUser = CreatedByUserID < 1 ? "" : CreatedByUserID.ToString();//to view all users
                                tbModifiedUser = ModifiedByUserID1 < 1 ? "" : ModifiedByUserID1.ToString();
                                tbFormCount.Value = TQuantity < 1 ? "" : TQuantity.ToString();
                                lblincomplete.InnerHtml = TIncomplete.ToString();
                                lblTotalCompleted.InnerHtml =TComplete.ToString();
                                lblremaining.InnerHtml =  RBalance.ToString();

                                tbFormCount.Disabled = true;
                        }

                        else
                        {
                            // didn't find any, so blank the textboxes
                            tbQuantity = "";
                            tbRegion = "";
                            tbInterviews = "";
                        //    tbQtyVerified = "";
                            tbPackets = "";
                            tbCreatedUser = ""; //
                            tbBatchNum = ""; //
                            tbFormCount.Value = "";
                            tbFormCount.Disabled = true; 
                       //     DataRow dr = null;

                            dr = dwt.GetRecentAppData(AppID, MyUserID);

                            if (dr != null)
                            {
                                TIncomplete = Convert.ToInt32(dr["TIncomplete"]);
                                TComplete = Convert.ToInt32(dr["TComplete"]);
                                RBalance = Convert.ToInt32(dr["RBalance"]);
                                lblincomplete.InnerHtml =  TIncomplete.ToString();
                                lblTotalCompleted.InnerHtml =  TComplete.ToString();
                                lblremaining.InnerHtml =  RBalance.ToString();
                            }
                            else
                            {
                                lblincomplete.InnerHtml = "0";
                                lblTotalCompleted.InnerHtml = "0";
                                lblremaining.InnerHtml = "0";
                            }

                        }
                            spBatch.Visible = false;
                            tblForm1.Visible = false;
                            btnaddnf.Visible = true;
                        
                    }
                        // hide or show these fields
                        if (aci.Region)
                            trRegion.Visible = true;
                        else
                            trRegion.Visible = false;

                        if (aci.Packets)
                            trPackets.Visible = true;
                        else
                            trPackets.Visible = false;

                        if (aci.VerifiedQuantity)
                            trVerifyQuantity.Visible = true;
                        else
                            trVerifyQuantity.Visible = false;
                        if (aci.TQuantity)
                            trTotalForms.Visible = true;
                        else
                            trTotalForms.Visible = false;
                        if (aci.Interviews)
                            trInterviews.Visible = true;
                        else
                            trInterviews.Visible = false;
                        if (aci.Quantity)
                            trQuantity.Visible = true;
                        else
                            trQuantity.Visible = false; 
                }
            }
                else
                {
                    // if the Save button doesn't contain a 'disabled' class, add one
                    if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    spBatch.Visible = false;
                    tblForm1.Visible = false;    // invalid form selected, so hide data
                    tblForm2.Visible = false;    // invalid form selected, so hide data
                    tblForm.Visible = false;
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }          
        }


        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            if (ddlApplication.SelectedIndex > 0)
            {
                int AppID = 0, MyUserID = 0;
                 AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                if (AppID > 0)
                {
                    Validation();
                    MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    DWUtility dwt = new DWUtility();

                    AppConfigInfo aci = dwt.GetAppByID(AppID);

                    DataRow dr = null;
                    

                    int Region = 0, Quantity = 0, Packets = 0, TQuantity = 0, BatchNum = 0, NAddForms = 0, totalCom = 0, tincom = 0, Interv=0;
                    int addedforms = 0;
                    int RBalance = 0;
                    int TComplete = 0;
                    int TIncomplete = 0;
                    int status = 0;
                    int Interviews = 0;
                    //int vq = 0;
                    //int q = 0;
                    //int vqt = 0;
                    //int qt = 0;
                    bool checkbox1 = false; 
                    bool checkbox2 = false;
                    bool check = false;
                    bool checkBatch = false;
                    bool BComplete = false;
                    bool QtyVerified = false;

                    int.TryParse(Request.Form["tbaddedforms"], out addedforms);

                    int.TryParse(Request.Form["tbInterviews"], out Interviews);

                    int.TryParse(Request.Form["tbRegion"], out Region);

                    int.TryParse(Request.Form["tbPackets"], out Packets);

                    int.TryParse(Request.Form["tbQuantity"], out Quantity);
                 //   int.TryParse(Request.Form["tbQtyVerified"], out QtyVerified);
                    QtyVerified = tbQtyVerified.Checked;
                    int.TryParse(Request.Form["tbBatchNum"], out BatchNum);


                    dr = dwt.GetAppBatchData(AppID, BatchNum);
                     checkbox1 = CheckBox();
                     checkbox2 = CheckBComplete();
                     check = CheckTotalForms();
                     checkBatch = CheckBatchTCom();

                    if (tbFormCount.Value != "")
                    {
                        TQuantity = Convert.ToInt32(tbFormCount.Value);
                    }


                    if (aci.BatchNum)
                    {//need different dr if have batchnum
                        dr = dwt.GetAppBatchData(AppID, BatchNum);
                        if (dr != null)
                        {
                            totalCom = Convert.ToInt32(dr["TComplete"]);
                            tincom = Convert.ToInt32(dr["TIncomplete"]);
                            //vq = Convert.ToInt32(dr["QtyVerified"]);
                            //q = Convert.ToInt32(dr["Quantity"]);
                        }
                    }
                    else
                    {
                        dr = dwt.GetAppData(AppID, MyUserID, Convert.ToDateTime(tbDEDate.Text));
                        if (dr != null)
                        {

                            totalCom = Convert.ToInt32(dr["TComplete"]);
                            Interv = Convert.ToInt32(dr["Interviews"]);
                            tincom = Convert.ToInt32(dr["TIncomplete"]);
                        }
                    }
                    if (checkBatch == true)
                    {
                        TComplete =  Quantity;
                        if (aci.Interviews)
                        {
                            TComplete = Interviews;
                        }
                    }
                    else
                    {
                        TComplete = totalCom + Quantity;

                        if (aci.Interviews)
                        {
                            TComplete = totalCom + Interviews;
                        }
                    }

                    NAddForms = addedforms;

                    if (aci.BatchNum)
                    {

                        TIncomplete = TQuantity;
                    }
                    else
                    {
                        TIncomplete = tincom;
                    }

                    RBalance = TIncomplete - TComplete;

                    BComplete = cbBatch.Checked;

                    if (RBalance < 0 )
                    {
                        CreateForm();
                        ShowPopUp("Error!", "You have completed more forms than has been entered, please check your form count!");
                        return;
                    }

                    if ((Region + Quantity + Packets + TQuantity) < 1)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "You must enter data to save this record!");
                        return;
                    }

                    if (aci.Region && (Region < 1 || Region > 5))
                    {
                        CreateForm();
                        ShowPopUp("Error!", "The Region number must be between 1 and 5.");
                        return;

                    }


                    if (aci.Packets && Packets < 1)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "The number of packets cannot be less than 1.");
                        return;

                    }

                    if (aci.BatchNum && BatchNum < 1)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "Batchnumber must be entered.");
                        return;
                    }
                    if (aci.VerifiedQuantity && QtyVerified == false)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "Please verify this batch by checking the Verify checkbox!.");
                        return;
                    }

                    if (checkbox1 == true)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "Your BatchNumber is not unique for this application.");
                        return;
                    }

                    if (check == true)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "Total forms does not match number of forms entered into the system for this Batch number.");
                        return;
                    }

                    if (checkbox2 == true)
                    {
                        CreateForm();
                        ShowPopUp("Error!", "Your completed BatchNumber for this application already exist.");
                        return;
                    }


                    DateTime WorkDate = Convert.ToDateTime(tbDEDate.Text);
            //        status = dwt.UpsertAppData(AppID, Region, BatchNum, Interviews, TQuantity, QtyVerified, Quantity, Packets, MyUserID, WorkDate, BComplete, TIncomplete, TComplete, RBalance, NAddForms);

                    if (status < 0)
                    {
                        // Notify the user that an error occurred.
                        ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                        return;
                    }
                    CreateForm();
                    ShowPopUp("Success!", "The data was successfully saved!");
                    
                }
                else
                {
                    DisableAndHideStuff();
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }
        protected void DisableAndHideStuff()
        {
            ddlApplication.ClearSelection();
            // if the Save button doesn't contain a 'disabled' class, add one
            if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

            cbBatch.Checked = false;
            spBatch.Visible = false;
            tblForm1.Visible = false;    // invalid form selected, so hide data
            tblForm2.Visible = false;    // invalid form selected, so hide data
            tblForm.Visible = false;
        }




        protected bool CheckBox()
        {//Check if batch number exsist for the fiscal year
            
            int AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            
            int BatchNum = 0, TQuantity = 0;
            bool BComplete = false;
            bool checkbox1 = false;
            DateTime today, startDate, endDate;
            //if (tbBatchNum != null && tbFormCount.Value!= null) //check if anthimg is entered before checking
            //{
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = dwt.GetAppByID(AppID);

            DataRow dr = null;
            dr = dwt.GetAppBatchData(AppID, BatchNum);

            if (aci.BatchNum && Request.Form["tbBatchNum"] != null)
            {
               // int.TryParse(Request.Form["tbBatchNum"], out BatchNum);  
                BatchNum = Convert.ToInt32(Request.Form["tbBatchNum"]);
            }
            if (aci.TQuantity && tbFormCount.Value != "")
            {
                TQuantity = Convert.ToInt32(tbFormCount.Value);
            }
            BComplete = cbBatch.Checked;
            
            //Count the number of duplicates for the fiscal year to flag to make batchnum unique
            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ToString());
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from DWT.AppData  where BatchNum = @BatchNum AND AppID = @AppID  AND BComplete = @BComplete ", sqlConnection))
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@BatchNum", Convert.ToInt32(BatchNum).ToString());
                sqlCommand.Parameters.AddWithValue("@AppID", Convert.ToInt32(AppID).ToString());
                sqlCommand.Parameters.AddWithValue("@BComplete", Convert.ToBoolean(BComplete).ToString());
                int userCount = (int)sqlCommand.ExecuteScalar();


                dr = dwt.GetAppBatchData(AppID, BatchNum); //get all app data

                today = DateTime.Today;
                startDate = new DateTime(DateTime.Today.Year, 7, 1);//startdate of fiscal year
                endDate = new DateTime(DateTime.Today.Year + 1, 7, 1).AddDays(-1);//enddate of fiscal year     

                if (today < startDate && today < endDate)//check if your in the current fiscal year
                {
                    startDate = startDate.AddYears(-1);
                    endDate = endDate.AddYears(-1);
                }

                if (dr != null)
                {
                    DateTime CreatedDate = Convert.ToDateTime(dr["CreatedDate"]); //today date

                    if (BComplete == true)
                    {

                        if (CreatedDate > startDate && CreatedDate < endDate)//if it is in the fiscal year?
                        {
                            if (userCount > 0)//Does BatchNum, AppID already exsists ?
                            {
                                checkbox1 = true;
                            }
                        }
                    }
                }
            }
                 return checkbox1;
        }

        protected bool CheckBComplete()
        {//Check if batch number exsist for the fiscal year. Prevents from updating a record that is already complete

            
            //Declare Variables
            int BatchNum=0, TQuantity = 0, AppID=0;
            bool BComplete = false;
            bool checkbox2 = false;
            DateTime today, startDate, endDate;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);

                DWUtility dwt = new DWUtility();
                AppConfigInfo aci = dwt.GetAppByID(AppID);
                DataRow dr = null;


                //Set variables

                if (aci.BatchNum && Request.Form["tbBatchNum"] != null)
                {
                    BatchNum = Convert.ToInt32(Request.Form["tbBatchNum"]);
                }
                if (aci.TQuantity && tbFormCount.Value != "")
                {
                    TQuantity = Convert.ToInt32(tbFormCount.Value);
                }

                BComplete = cbBatch.Checked;


                //Update 2/7/2017: I added where BComplete < 1 so that it is checking for completed batches
                //Count the number of duplicates for the fiscal year to flag to make batchnum unique
                SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ToString());
                using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from DWT.AppData  where BatchNum = @BatchNum AND AppID = @AppID AND BComplete = 1 ", sqlConnection))
                {
                    sqlConnection.Open();
                    sqlCommand.Parameters.AddWithValue("@BatchNum", Convert.ToInt32(BatchNum).ToString());
                    sqlCommand.Parameters.AddWithValue("@AppID", Convert.ToInt32(AppID).ToString());
                    sqlCommand.Parameters.AddWithValue("@BComplete", Convert.ToBoolean(BComplete).ToString());
                    int userCount = (int)sqlCommand.ExecuteScalar();

                    dr = dwt.GetAppBatchData(AppID, BatchNum); //get all app data

                    today = DateTime.Today;

                    startDate = new DateTime(DateTime.Today.Year, 7, 1);//startdate of fiscal year
                    endDate = new DateTime(DateTime.Today.Year + 1, 7, 1).AddDays(-1);//enddate of fiscal year

                    if (today < startDate && today < endDate)//check if your in the current fiscal year
                    {
                        startDate = startDate.AddYears(-1);
                        endDate = endDate.AddYears(-1);
                    }

                    if (dr != null)
                    {
                        DateTime CreatedDate = Convert.ToDateTime(dr["CreatedDate"]); //today date

                        if (Convert.ToBoolean(dr["BComplete"]))
                        {
                            if (CreatedDate > startDate && CreatedDate < endDate)//if it is in the fiscal year?
                            {
                                if (userCount > 0)//Does BatchNum, AppID already exsists ?
                                {
                                    checkbox2 = true;
                                }
                            }
                        }
                    }
                    


                }

            return checkbox2;
        }

        protected bool CheckTotalForms()
        {//This is used to check if the batch number has completed all forms with a remaing balance of 0 for that batch

            DWUtility dwt = new DWUtility();

            int BatchNum = 0, RBalance = 0, TFormCount = 0,  AppID = 0, Quantity = 0, TComplete = 0;
            bool BComplete = false, check = false;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            AppConfigInfo aci = dwt.GetAppByID(AppID);


            if (aci.BatchNum)
            {
                int.TryParse(Request.Form["tbBatchNum"], out BatchNum);
                //   BatchNum = Convert.ToInt32(Request.Form["tbBatchNum"]);
            }
            int.TryParse(Request.Form["tbQuantity"], out Quantity);
            if (aci.TQuantity && tbFormCount.Value != "")
            {
                TFormCount = Convert.ToInt32(tbFormCount.Value);
            }
            BComplete = cbBatch.Checked;

            if (BComplete == true) //
            {

                DataRow dr = null;
                dr = dwt.GetAppCheckForms(AppID, BatchNum); //get all app data
                if (dr != null)
                {
                    RBalance = Convert.ToInt32(dr["RBalance"]);
                    TComplete = Convert.ToInt32(dr["TComplete"]);
                  
                        int Completed = Quantity + TComplete;

                        if (TFormCount != Completed)
                        {
                            check = true;
                        }
                }
                    }

                return check;
            
        }

        protected bool CheckBatchTCom()
        {//this is used for TComplete. If starting a new batch, the first record in the system with that batch num, 
            //i do not want system to add TComplete to TComplete with a different batch

            int AppID = 0, BatchNum = 0, TQuantity = 0, userCount = 0;
            bool checkBatch = false;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);

            DWUtility dwt = new DWUtility();
            AppConfigInfo aci = dwt.GetAppByID(AppID);

            if (aci.BatchNum && Request.Form["tbBatchNum"] != null)
            {
                BatchNum = Convert.ToInt32(Request.Form["tbBatchNum"]);
            }
            if (aci.TQuantity && tbFormCount.Value != "")
            {
                TQuantity = Convert.ToInt32(tbFormCount.Value);
            }

            
            DataRow dr = null;
            dr = dwt.GetAppBatchData(AppID, BatchNum); //get all app data


            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ToString());
            using (SqlCommand sqlCommand = new SqlCommand("SELECT COUNT(*) from DWT.AppDatalog  where BatchNum = @BatchNum AND AppID = @AppID  ", sqlConnection))
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@BatchNum", Convert.ToInt32(BatchNum).ToString());
                sqlCommand.Parameters.AddWithValue("@AppID", Convert.ToInt32(AppID).ToString());
               // sqlCommand.Parameters.AddWithValue("@BComplete", Convert.ToBoolean(BComplete).ToString());
                userCount = (int)sqlCommand.ExecuteScalar();

                

                if (aci.BatchNum)
                {
                    if (dr != null)
                    {
                        if (userCount == 0)
                        {//this is used for TComplete. If starting a new batch i do not want system to add TComplete to TComplete with a different batch
                            checkBatch = true;
                        }

                    }
                }
                return checkBatch;
            }
        }

        protected void btnaddnf_ServerClick(object sender, EventArgs e)
        {//this is for the pop up box when we need to quick add new forms
            //   Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "myFunction()", true);         
            Validation();
            int addedforms, TQuantity = 0, NAddForms = 0,
                nftotal = 0, RBalance = 0,
                TComplete = 0, TIncomplete = 0, tincom = 0, AppID =0, MyUserID=0, Region=0;

            DateTime WorkDate = Convert.ToDateTime(tbDEDate.Text);
            MyUserID = Convert.ToInt32(Session["MyUserID"]);
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            int.TryParse(Request.Form["tbaddedforms"], out addedforms);

            DWUtility dwt = new DWUtility();
            AppConfigInfo aci = dwt.GetAppByID(AppID);
            DataRow dr = null;
            int BatchNum = 0;
            dr = dwt.GetAppBatchData(AppID, BatchNum);


            if (aci.BatchNum)
            {//if batch number then use this dr             

                if (dr != null)
                {
                    tincom = Convert.ToInt32(dr["TIncomplete"]);
                    TComplete = Convert.ToInt32(dr["TComplete"]);
                    RBalance = Convert.ToInt32(dr["RBalance"]);
                    nftotal = Convert.ToInt32(dr["TQuantity"]);

                    TQuantity = nftotal + addedforms;
                    NAddForms = addedforms;
                    TIncomplete = (tincom + NAddForms);
                    TComplete = Convert.ToInt32(dr["TComplete"]);
                    RBalance = TIncomplete - TComplete;
                }
            }
            else
            {
                dr = dwt.GetRecentAppData(AppID, MyUserID);

                if (dr != null)
                {//If data already entered into the system today
                    tincom = Convert.ToInt32(dr["TIncomplete"]);
                    TComplete = Convert.ToInt32(dr["TComplete"]);
                    RBalance = Convert.ToInt32(dr["RBalance"]);

                    NAddForms = addedforms;
                    TIncomplete = (tincom + NAddForms);
                  //  TComplete = Convert.ToInt32(dr["TComplete"]);
                    RBalance = TIncomplete - TComplete;

                    dr = dwt.GetRecentAppDataDaily(AppID, MyUserID);
                    if (dr != null)
                    {
                        nftotal = Convert.ToInt32(dr["TQuantity"]);
                        TQuantity = nftotal + addedforms;
                    }
                    else
                    {
                        TQuantity = addedforms;
                    }
                }
                   

                else
                {
                    TQuantity = addedforms;
                    NAddForms = addedforms;
                    TIncomplete = addedforms;
                    RBalance = TIncomplete - TComplete;
                        

                }
                }

                // add , so   Region=0, BatchNum =0, TQuantity=0, QtyVerified=0,  Quantity=0, Packets=0, BComplete = false TIncomplete = 0,TComplete=0 ,RBalance=0
            int status = dwt.UpsertAppDataAddForms(AppID, 0, 0, TQuantity, 0, 0, 0, 0, MyUserID, WorkDate, false, TIncomplete, TComplete, RBalance, NAddForms);
                if (status < 0)
                {
                    ShowPopUp("Error!", "New Forms could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                }
                else
                {
                  //  DisableAndHideStuff();
                    // recreate the form
                    CreateForm();
                    ShowPopUp("Success!", "Added new forms successfully! ");
                    
                }

            }
 

        protected void CheckTFDisble()
        {//To allow users who created total forms to modify for that specific batchnum
            DWUtility dwt = new DWUtility();
            DataRow dr = null;
            tbFormCount.Disabled = false;
            int AppID = 0, MyUserID = 0, CreatedByUserID = 0, BatchNum=0;
            AppID  = Convert.ToInt32(ddlApplication.SelectedValue);
            MyUserID = Convert.ToInt32(Session["MyUserID"]);
            int.TryParse(Request.Form["tbBatchNum"], out BatchNum); 
            dr = dwt.GetAppBatchBatch(AppID, BatchNum, MyUserID);

            if (dr != null)
            {
                CreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"]);

                if (CreatedByUserID == MyUserID || CreatedByUserID < 0 || CreatedByUserID == 0)
                {
                    tbFormCount.Disabled = false;
                }
                else
                {
                    tbFormCount.Disabled = true;
                }
            }
            
        }
        protected void Validation()
        {
            DWUtility dwt = new DWUtility();
            int AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            AppConfigInfo aci = dwt.GetAppByID(AppID);

            if (aci.Interviews)
            {
                //int AppID = 0;
                //AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                //DWUtility dwt = new DWUtility();
                //AppConfigInfo aci = dwt.GetAppByID(AppID);

                //if(aci.Interviews && Request.Form["tbInterviews"] == null)
                //{
                HtmlGenericControl interview = (HtmlGenericControl)Master.FindControl("tbInterviews");
                //      interview.Attributes.Add("required");

                //}
                interview.Attributes.Remove("required");
            }
           
        }

    }
}