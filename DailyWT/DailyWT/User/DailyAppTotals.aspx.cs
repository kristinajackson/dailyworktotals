﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;


namespace DailyWT.User
{
    public partial class DailyAppTotals : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDApp();
                }

                else
                {
                    lblmsg.Text = "";
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: DoAdmin
                    if (eventTarget == "SaveInsert")
                    {
                        SaveInsert();
                    }
                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }


        protected void LoadDDApp()
        {

            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;

            int MyUserID = 0;

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");

        }

        protected void LoadDDBatch()
        {

            DropDownList BatchNumTextBox = (DropDownList)FormView1.FindControl("batch");
            TextBox txtbtch = (TextBox)FormView1.FindControl("BatchNumTextBox");

            BatchNumTextBox.Visible = true;
            txtbtch.Visible = false;

            DWUtility dwt = new DWUtility();

            int AppID = 0;

            int MyUserID = 0;

            AppID = Convert.ToInt32(ddlApplication.SelectedValue.ToString());
            if (AppID == Convert.ToInt32(1041))//FUR DEALER VERIFY
            {
                AppID = 1022;//FUR DEALER
            }
            if (AppID == Convert.ToInt32(1021))//CAPTIVE WILDLIFE VERIFY
            {
                AppID = Convert.ToInt32(4);//CAPTIVE WILDLIFE
            }
            if (AppID == Convert.ToInt32(1020))//Mussels
            {
                AppID = Convert.ToInt32(1006);
            }

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            BatchNumTextBox.Items.Clear();       // clear in case it's already populated
            BatchNumTextBox.DataSource = dwt.GetBatchNum(AppID, MyUserID);
            BatchNumTextBox.DataTextField = "BatchNum";
            BatchNumTextBox.DataValueField = "BatchNum";

            BatchNumTextBox.DataBind();
            BatchNumTextBox.Items.Insert(0, "  ");

            BatchNumTextBox.Visible = true;
            txtbtch.Visible = false;

            Style myStyle = new Style();
            myStyle.ForeColor = System.Drawing.Color.Black;
            BatchNumTextBox.ApplyStyle(myStyle);

        }



        protected void ddlApplication_SelectedIndexChanged(object sender, EventArgs e)
        {
            CreateForm();
            TextBox AppIDTextBox = (TextBox)FormView1.FindControl("AppIDTextBox");
            TextBox AppNameTextBox = (TextBox)FormView1.FindControl("AppNameTextBox");
            TextBox CreatedByUserIDTextBox = (TextBox)FormView1.FindControl("CreatedByUserIDTextBox");
            TextBox CreatedDateTextBox = (TextBox)FormView1.FindControl("CreatedDateTextBox");
            TextBox ModifiedByUserIDTextBox = (TextBox)FormView1.FindControl("ModifiedByUserIDTextBox");
            TextBox ModifiedDateTextBox = (TextBox)FormView1.FindControl("ModifiedDateTextBox");


            AppIDTextBox.Text = "";
            AppNameTextBox.Text = "";
            lblmsg.Text = "";
            AppIDTextBox.Text = ddlApplication.SelectedValue.ToString();
            AppNameTextBox.Text = ddlApplication.SelectedItem.ToString();
            CreatedByUserIDTextBox.Text = Convert.ToInt32(Session["MyUserID"]).ToString();
            CreatedDateTextBox.Text = DateTime.Now.ToString();
            ModifiedByUserIDTextBox.Text = Convert.ToInt32(Session["MyUserID"]).ToString();
            ModifiedDateTextBox.Text = DateTime.Now.ToString();
        }

        protected void CreateForm()
        {
            Control RegiontblForm = (Control)FormView1.FindControl("RegiontblForm");
            Control BatchnumbertblForm = (Control)FormView1.FindControl("BatchnumbertblForm");
            Control AngulartblForm = (Control)FormView1.FindControl("AngulartblForm");
            Control InterviewtblForm = (Control)FormView1.FindControl("InterviewtblForm");
            Control PacketstblForm = (Control)FormView1.FindControl("PacketstblForm");
            Control BatchnumtblForm = (Control)FormView1.FindControl("BatchnumtblForm");
            Control VerifyQuantitytblForm = (Control)FormView1.FindControl("VerifyQuantitytblForm");
            Control tblinterkey = (Control)FormView1.FindControl("tblinterkey");
            Control tblangukey = (Control)FormView1.FindControl("tblangukey");
            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            CheckBox tbQtyVerified = (CheckBox)FormView1.FindControl("QtyVerifiedCheckBox");
            CheckBox cbBatch = (CheckBox)FormView1.FindControl("BCompleteCheckBox");
            DropDownList txtRegion = (DropDownList)FormView1.FindControl("txtRegion");
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox txtPackets = (TextBox)FormView1.FindControl("PacketsTextBox");
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            TextBox InterviewsTextBox = (TextBox)FormView1.FindControl("InterviewsTextBox");
            TextBox AngularCompletedTextBox = (TextBox)FormView1.FindControl("AngularCompletedTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");
            Control trBatchNum = (Control)FormView1.FindControl("trBatchNum");
            Control trPackets = (Control)FormView1.FindControl("trPackets");
            Control trAngular = (Control)FormView1.FindControl("trAngular");
            Control trInterviews = (Control)FormView1.FindControl("trInterviews");
            Control tblForm1 = (Control)FormView1.FindControl("tblForm1");
            DropDownList batch = (DropDownList)FormView1.FindControl("batch");
            Control datetable = (Control)FormView1.FindControl("datetable");
            TextBox txtDateRecieved = (TextBox)FormView1.FindControl("txtDateRecieved");



            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                lblmsg.Text = "";
                txtRegion.Enabled = true;
                ttlincomptxt.Enabled = false;
                ttlkeytxt.Enabled = false;
                outtxt.Enabled = false;
                txtBatchNum.Enabled = true;
                txtPackets.Enabled = true;
                ttlincomptxt.ReadOnly = true;
                ttlkeytxt.ReadOnly = true;
                outtxt.ReadOnly = true;
                tbQtyVerified.Checked = false;
                cbBatch.Checked = false;
                txtRegion.ClearSelection();
                keytxt.Text = "";
                ttlkeytxt.Text = "";
                frmsrectxt.Text = "";
                outtxt.Text = "";
                txtBatchNum.Text = "";
                txtAngular.Text = "";
                txtPackets.Text = "";
                txtInterviews.Text = "";
                ttlincomptxt.Text = "";
                InterviewsTextBox.Text = "";
                AngularCompletedTextBox.Text = "";
                txtDateRecieved.Text = DateTime.Today.ToString("d");
                int AppID = 0, MyUserID = 0;
                MyUserID = Convert.ToInt32(Session["MyUserID"]);
                string MyName = "";
                MyName = Session["MyName"].ToString();
                Label1.Text = "Enter application for user:  " + MyName.ToString() + "";

                AppID = Convert.ToInt32(ddlApplication.SelectedValue);

                DWUtility dwt = new DWUtility();
                AppConfigInfo aci = dwt.GetAppByID(AppID);

                DataRow dr = null;


                TableFlicker();

                int Region = 0;
                int BatchNum = 0;
                // int NAddForms = 0;
                int TQuantity = 0;
                int Packets = 0;
                // int TIncomplete = 0;
                int TComplete = 0;
                int RBalance = 0;
                int Quantity = 0;
                int Interviews = 0;
                string DateRecieved;
                DateTime ModifiedDate = System.DateTime.Now;


                dr = dwt.GetRecentAppData(AppID, MyUserID);
                if (dr != null)
                {

                    TQuantity = Convert.ToInt32(dr["TQuantity"]);//total to be keyed
                    Region = Convert.ToInt32(dr["Region"]);
                    Quantity = Convert.ToInt32(dr["Quantity"]);
                    Packets = Convert.ToInt32(dr["Packets"]);
                    BatchNum = Convert.ToInt32(dr["BatchNum"]);
                    TComplete = Convert.ToInt32(dr["TComplete"]);//total keyed
                    RBalance = Convert.ToInt32(dr["RBalance"]);//total outstanding
                    ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]);
                    Interviews = Convert.ToInt32(dr["Interviews"]);
                    DateRecieved = txtDateRecieved.Text;


                    if (RBalance > 0)
                    {//Clean the form if nothing remaining
                        outtxt.Text = RBalance < 1 ? "" : RBalance.ToString();


                        switch (Region)
                        {
                            case 1:
                                txtRegion.Items.FindByValue("1").Selected = true;
                                break;
                            case 2:
                                txtRegion.Items.FindByValue("2").Selected = true;
                                break;
                            case 3:
                                txtRegion.Items.FindByValue("3").Selected = true;
                                break;
                            case 4:
                                txtRegion.Items.FindByValue("4").Selected = true;
                                break;
                        }
                        txtRegion.Enabled = false;

                        if (trBatchNum.Visible)
                        {
                            txtBatchNum.Text = BatchNum < 1 ? "" : BatchNum.ToString();
                            txtBatchNum.Enabled = false;
                        }

                        if (trPackets.Visible)
                        {

                            txtPackets.Text = Packets < 1 ? "" : Packets.ToString();
                            txtPackets.Enabled = false;
                        }
                    }
                    else
                    {
                        outtxt.Text = "";
                    }
                }
                else
                {
                    keytxt.Text = "";
                    ttlkeytxt.Text = "";
                    frmsrectxt.Text = "";
                    outtxt.Text = "";
                    txtBatchNum.Text = "";
                    txtAngular.Text = "";
                    txtPackets.Text = "";
                    txtInterviews.Text = "";
                    ttlincomptxt.Text = "";
                    InterviewsTextBox.Text = "";
                    AngularCompletedTextBox.Text = "";

                }

            }
            else
            {
                // if the Save button doesn't contain a 'disabled' class, add one
                // if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";
                tblForm1.Visible = false;    // invalid form selected, so hide data
                PacketstblForm.Visible = false;
                BatchnumbertblForm.Visible = false;
                RegiontblForm.Visible = false;
                VerifyQuantitytblForm.Visible = false;
                BatchnumtblForm.Visible = false;
                InterviewtblForm.Visible = false;
                AngulartblForm.Visible = false;
                PacketstblForm.Visible = false;
                datetable.Visible = false;
            }
        }

        protected void frmsrectxtTextChanged()
        {

            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox txtPackets = (TextBox)FormView1.FindControl("PacketsTextBox");
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            Label frmsrecdate = (Label)FormView1.FindControl("FrmsRecDateTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");
            TextBox InterviewsTextBox = (TextBox)FormView1.FindControl("InterviewsTextBox");
            TextBox AngularCompletedTextBox = (TextBox)FormView1.FindControl("AngularCompletedTextBox");

            //math calculations for forms rec
            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();
            DataRow dr = null;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            dr = dwt.GetRecentAppData(AppID, MyUserID);
            AppConfigInfo aci = dwt.GetAppByID(AppID);

            //lblmsg.Text = "";
            ttlincomptxt.Enabled = false;
            ttlkeytxt.Enabled = false;
            outtxt.Enabled = false;
            ttlincomptxt.ReadOnly = true;
            ttlkeytxt.ReadOnly = true;
            outtxt.ReadOnly = true;

            int NAddForms = 0;
            int.TryParse(frmsrectxt.Text, out NAddForms); //frms rec'd
            int TIncomplete = 0;
            int TQuantity = 0;//total to be keyed
            int TComplete = 0;
            int.TryParse(keytxt.Text, out TIncomplete);
            int RBalance = 0;
            int.TryParse(outtxt.Text, out RBalance);
            int interviews1 = 0;
            int.TryParse(InterviewsTextBox.Text, out interviews1);
            int interviewscom1 = 0;
            int.TryParse(txtInterviews.Text, out interviewscom1);
            int angularrec1 = 0;
            int.TryParse(txtAngular.Text, out angularrec1);
            int angularcom1 = 0;
            int.TryParse(AngularCompletedTextBox.Text, out angularcom1);
            int interviews = 0, interviewscom = 0, angularrec = 0, angularcom = 0;


            if (dr != null)
            {//does form exsist?
                if (RBalance > 0)
                {
                    if (aci.Interviews)
                    {
                        interviews = ((Convert.ToInt32(dr["Interviews"])) + (Convert.ToInt32(interviews1)));
                        interviewscom = ((Convert.ToInt32(dr["InterviewsCompleted"])) + (Convert.ToInt32(interviewscom1)));
                        RBalance = interviews - interviewscom;
                        InterviewsTextBox.Text = interviews.ToString();
                        txtInterviews.Text = interviewscom.ToString();
                    }
                    else if (aci.Quantity)
                    {
                        angularrec = (Convert.ToInt32(dr["Quantity"]) + Convert.ToInt32(angularrec1));
                        angularcom = (Convert.ToInt32(dr["AngularCompleted"]) + Convert.ToInt32(angularcom1));
                        txtAngular.Text = angularrec1.ToString();
                        AngularCompletedTextBox.Text = angularcom1.ToString();
                        RBalance = angularrec - angularcom;
                    }
                    else
                    {
                        TQuantity = (Convert.ToInt32(dr["TQuantity"]) + NAddForms);
                        int TComplete2 = 0;
                        TComplete2 = Convert.ToInt32(dr["TComplete"]);//total keyed
                        TComplete = Convert.ToInt32(TIncomplete) + Convert.ToInt32(TComplete2);
                        RBalance = TQuantity - TComplete;
                    }
                }
                else
                {
                    if (aci.Interviews)
                    {
                        RBalance = (Convert.ToInt32(interviews1) - Convert.ToInt32(interviewscom1));
                        InterviewsTextBox.Text = interviews1.ToString();
                        txtInterviews.Text = interviewscom1.ToString();
                    }
                    else if (aci.Quantity)
                    {
                        RBalance = (Convert.ToInt32(angularrec1) - Convert.ToInt32(angularcom1));
                        txtAngular.Text = angularrec1.ToString();
                        AngularCompletedTextBox.Text = angularcom1.ToString();
                    }
                    else
                    {
                        TQuantity = NAddForms;
                        TComplete = TIncomplete;
                        RBalance = TQuantity - TComplete;
                    }
                }
            }
            else
            {
                if (aci.Interviews)
                {
                    RBalance = (Convert.ToInt32(interviews1) - Convert.ToInt32(interviewscom1));
                    InterviewsTextBox.Text = interviews1.ToString();
                    txtInterviews.Text = interviewscom1.ToString();
                }
                else if (aci.Quantity)
                {
                    RBalance = (Convert.ToInt32(angularrec1) - Convert.ToInt32(angularcom1));
                    txtAngular.Text = angularrec1.ToString();
                    AngularCompletedTextBox.Text = angularcom1.ToString();
                }
                else
                {
                    TQuantity = NAddForms;
                    TComplete = TIncomplete;
                    RBalance = TQuantity - TComplete;
                }
            }
            ttlkeytxt.Text = TQuantity.ToString();
            outtxt.Text = RBalance.ToString();
            ttlincomptxt.Text = TIncomplete.ToString();
            keytxt.Text = TComplete.ToString();
        }

        protected void TableFlicker()
        {//Turn on off tables visibility

            Control RegiontblForm = (Control)FormView1.FindControl("RegiontblForm");
            Control BatchnumbertblForm = (Control)FormView1.FindControl("BatchnumbertblForm");
            Control AngulartblForm = (Control)FormView1.FindControl("AngulartblForm");
            Control InterviewtblForm = (Control)FormView1.FindControl("InterviewtblForm");
            Control PacketstblForm = (Control)FormView1.FindControl("PacketstblForm");
            Control BatchnumtblForm = (Control)FormView1.FindControl("BatchnumtblForm");
            Control VerifyQuantitytblForm = (Control)FormView1.FindControl("VerifyQuantitytblForm");
            Control tblinterkey = (Control)FormView1.FindControl("tblinterkey");
            Control tblangukey = (Control)FormView1.FindControl("tblangukey");
            Control tblForm1 = (Control)FormView1.FindControl("tblForm1");
            Control totalkeyed = (Control)FormView1.FindControl("Table8");
            Control totalrecieved = (Control)FormView1.FindControl("Table1");
            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            DropDownList batch = (DropDownList)FormView1.FindControl("batch");
            TextBox BatchNumTextBox = (TextBox)FormView1.FindControl("BatchNumTextBox");
            Control datetable = (Control)FormView1.FindControl("datetable");
            TextBox txtDateRecieved = (TextBox)FormView1.FindControl("txtDateRecieved");

            //lblmsg.Text = "";
            tblForm1.Visible = false;
            PacketstblForm.Visible = false;
            BatchnumbertblForm.Visible = false;
            RegiontblForm.Visible = false;
            VerifyQuantitytblForm.Visible = false;
            BatchnumtblForm.Visible = false;
            InterviewtblForm.Visible = false;
            tblinterkey.Visible = false;
            AngulartblForm.Visible = false;
            tblangukey.Visible = false;
            PacketstblForm.Visible = false;
            totalkeyed.Visible = true;
            totalrecieved.Visible = true;
            batch.Visible = false;
            datetable.Visible = false;


            tblForm1.Visible = true;
            ttlincomptxt.Enabled = false;
            ttlkeytxt.Enabled = false;
            outtxt.Enabled = false;
            ttlincomptxt.ReadOnly = true;
            ttlkeytxt.ReadOnly = true;
            outtxt.ReadOnly = true;
            datetable.Visible = true;
            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = dwt.GetAppByID(AppID);

            if (aci.Packets)
            {

                PacketstblForm.Visible = true;
            }
            if (aci.BatchNum)
            {
                BatchnumbertblForm.Visible = true;
                BatchnumtblForm.Visible = true;

                batch.Visible = false;
                BatchNumTextBox.Visible = true;
            }
            if (aci.Region)
            {
                RegiontblForm.Visible = true;
            }

            if (aci.VerifiedQuantity)
            {
                VerifyQuantitytblForm.Visible = true;
                if (aci.BatchNum)
                {
                    batch.Visible = true;
                    BatchNumTextBox.Visible = false;
                    LoadDDBatch();
                }
            }
            if (aci.Quantity)
            {
                AngulartblForm.Visible = true;
                tblangukey.Visible = true;
                totalkeyed.Visible = false;
                totalrecieved.Visible = false;
            }
            if (aci.Interviews)
            {
                InterviewtblForm.Visible = true;
                tblinterkey.Visible = true;
                totalkeyed.Visible = false;
                totalrecieved.Visible = false;
            }


        }

        protected void tbQtyVerified_CheckedChanged(object sender, EventArgs e)
        {
            //lblmsg.Text = "";
            bool check = ChkMatchBox();

            if (check == true)
            {

                lblmsg.Text = "ERROR! Forms Recieved and Total Keyed should equal before checking the checkbox!";
            }

        }

        protected bool ChkMatchBox()
        {//If check box has beedn checked but still have forms to finish

            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            //lblmsg.Text = "";
            int frmsrec = 0;
            int.TryParse(frmsrectxt.Text, out frmsrec);
            int key = 0;
            int.TryParse(keytxt.Text, out key); //total keyed
            bool check = false;
            if (ttlkeytxt.Text.Length > 0)
            {
                check = true;
            } return check;

        }

        protected void SaveData(object sender, EventArgs e)
        {
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsTextBox");
            TextBox txtInterkey = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            TextBox txtangkey = (TextBox)FormView1.FindControl("AngularCompletedTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");

            lblmsg.Text = "";
            string FormName = ddlApplication.SelectedItem.ToString().Length < 1 ? "0" : ddlApplication.SelectedItem.ToString();
            string NAddFormsTextBox = frmsrectxt.Text.ToString().Length < 1 ? "0" : frmsrectxt.Text.ToString();
            string TCompleteTextBox = keytxt.Text.ToString().Length < 1 ? "0" : keytxt.Text.ToString();

            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = dwt.GetAppByID(AppID);
            if (aci.Quantity || aci.Interviews)
            {
                string angrec = txtAngular.Text.ToString().Length < 1 ? "0" : txtAngular.Text.ToString();
                string angcom = txtangkey.Text.ToString().Length < 1 ? "0" : txtangkey.Text.ToString();
                string intrec = txtInterviews.Text.ToString().Length < 1 ? "0" : txtInterviews.Text.ToString();
                string intcom = txtInterkey.Text.ToString().Length < 1 ? "0" : txtInterkey.Text.ToString();
                int NAdd = Convert.ToInt32(angrec) + Convert.ToInt32(intrec);
                int TCom = Convert.ToInt32(angcom) + Convert.ToInt32(intcom);
                NAddFormsTextBox = NAdd.ToString();
                TCompleteTextBox = TCom.ToString();
            }
            ShowPopUpOkCancel("Save Data?", "Are you sure want to save? <br/><br/>Application: " + FormName + "<br/><br/>" + " Forms Recieved: " + NAddFormsTextBox + "<br/><br/>" + " Forms Keyed: " + TCompleteTextBox + "<br /><br />", "SaveInsert", "");

        }

        protected void SaveInsert()
        {
            lblmsg.Text = "";
            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            CheckBox tbQtyVerified = (CheckBox)FormView1.FindControl("QtyVerifiedCheckBox");
            CheckBox cbBatch = (CheckBox)FormView1.FindControl("BCompleteCheckBox");
            DropDownList txtRegion = (DropDownList)FormView1.FindControl("txtRegion");
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox txtangkey = (TextBox)FormView1.FindControl("AngularCompletedTextBox");
            TextBox txtPackets = (TextBox)FormView1.FindControl("PacketsTextBox");
            TextBox txtInterkey = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");
            TextBox AppIDTextBox = (TextBox)FormView1.FindControl("AppIDTextBox");
            TextBox OldTQuantityTextBox = (TextBox)FormView1.FindControl("OldTQuantityTextBox");
            TextBox OldTIncompleteTextBox = (TextBox)FormView1.FindControl("OldTIncompleteTextBox");
            TextBox OldTCompleteTextBox = (TextBox)FormView1.FindControl("OldTCompleteTextBox");
            TextBox OldRBalanceTextBox = (TextBox)FormView1.FindControl("OldRBalanceTextBox");
            TextBox OldNAddFormsTextBox = (TextBox)FormView1.FindControl("OldNAddFormsTextBox");
            TextBox txtDateRecieved = (TextBox)FormView1.FindControl("txtDateRecieved");

            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                frmsrectxtTextChanged();


                bool val = false;
                val = TableValidator();
                //Save data
                DWUtility dwt = new DWUtility();
                int AppID = 0, MyUserID = 0;
                string AppName = "";
                AppName = ddlApplication.SelectedItem.Text;
                AppID = Convert.ToInt32(AppIDTextBox.Text);
                MyUserID = Convert.ToInt32(Session["MyUserID"]);
                AppConfigInfo aci = dwt.GetAppByID(AppID);


                int Region = 0;
                if (aci.Region) { int.TryParse(txtRegion.Text, out Region); }
                int BatchNum = 0;
                int.TryParse(txtBatchNum.Text, out BatchNum);
                int Interviews = 0;
                int.TryParse(txtInterviews.Text, out Interviews);
                int NAddForms = 0;
                int.TryParse(frmsrectxt.Text, out NAddForms); //frms rec'd
                int TQuantity = 0;
                int.TryParse(ttlkeytxt.Text, out TQuantity); //total to be keyed
                bool QtyVerified = false;
                QtyVerified = tbQtyVerified.Checked;
                int Quantity = 0;
                int.TryParse(txtAngular.Text, out Quantity); // Angulars
                int Packets = 0;
                int.TryParse(txtPackets.Text, out Packets);
                DateTime WorkDate = DateTime.Now;
                bool BComplete = false;
                BComplete = cbBatch.Checked;
                int TIncomplete = 0;
                int.TryParse(ttlincomptxt.Text, out TIncomplete); //total incomplete
                int TComplete = 0;
                int.TryParse(keytxt.Text, out TComplete); //total keyed
                int RBalance = 0;
                int.TryParse(outtxt.Text, out RBalance); //total outstanding
                int OldTQuantity = 0;
                //   int OldTIncomplete = 0;
                int OldTComplete = 0;
                int OldRBalance = 0;
                //   int OldNAddForms = 0;
                int InterviewsCompleted = 0;
                int.TryParse(txtInterkey.Text, out InterviewsCompleted);
                int AngularCompleted = 0;
                int.TryParse(txtangkey.Text, out AngularCompleted);
                string DateRecieved = null;
                DateRecieved = txtDateRecieved.Text;
                bool chechcom = CheckBComplete();
                bool alertverify = AlertVerify();


                DataRow dr = null;

                dr = dwt.GetRecentAppData(AppID, MyUserID);

                if (dr != null)
                {//save All previous numbers 
                    OldTQuantity = Convert.ToInt32(dr["TQuantity"]);//total to be keyed;
                    //OldTIncomplete = Convert.ToInt32(dr["TIncomplete"]);
                    OldTComplete = Convert.ToInt32(dr["TComplete"]);
                    OldRBalance = Convert.ToInt32(dr["RBalance"]);
                    //   OldNAddForms = Convert.ToInt32(dr["NAddForms"]);
                }


                if (val == true)
                {
                    lblmsg.Text = "Please enter All Reqiured Fields!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    return;
                }

                if (RBalance < 0)
                {
                    lblmsg.Text = "Error!, More forms entered than keyed!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;
                }
                if (aci.VerifiedQuantity == true && RBalance == 0 && tbQtyVerified.Checked == false)
                {
                    lblmsg.Text = "Error! Data Not saved! Please check Verified Forms checkbox!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;


                }

                if (aci.BatchNum && RBalance == 0 && cbBatch.Checked == false)
                {
                    lblmsg.Text = "Error! Data Not saved! Please check Batch Completed checkbox!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;
                }

                if (TQuantity == 0 && TIncomplete == 0 && TComplete == 0 && RBalance == 0 && NAddForms == 0 && Quantity == 0 && Interviews == 0 && InterviewsCompleted == 0 && AngularCompleted == 0)
                {//If they forget to check verified quantity                    
                    lblmsg.Text = "Error! Form is empty. No data to save!. Please enter number of forms recieved!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    return;
                }

                if (aci.VerifiedQuantity == true && RBalance == 0 && tbQtyVerified.Checked == false && OldRBalance == TComplete)
                {

                    lblmsg.Text = "Error! All Forms completed. Please check Verified Forms checkbox!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;
                }
                if (aci.BatchNum == true && RBalance == 0 && cbBatch.Checked == false && OldRBalance == TComplete)
                {

                    lblmsg.Text = "Error! All Forms completed. Please check Batch Completed checkbox!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;
                }

                if (chechcom == true)
                {
                    lblmsg.Text = "Error! Duplicate batch number!";
                    DisableAndHideStuff();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    return;
                }

                if (!aci.VerifiedQuantity == true && !aci.BatchNum == true && NAddForms < TComplete && OldRBalance == 0)
                {
                    lblmsg.Text = "Error! More forms keyed than entered. Please check your work!";
                    DisableAndHideStuff();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    return;
                }

                if (frmsrectxt.Text.Length <= 0 && keytxt.Text.Length <= 0 && tbQtyVerified.Checked == false && cbBatch.Checked == false)
                {
                    lblmsg.Text = "Error! No data to save!";
                    return;
                }

                if ((tbQtyVerified.Checked) && RBalance > 0)
                {
                    lblmsg.Text = "Error! Quantity Verified CheckBox needs to be checked when all forms are completed!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;

                }

                if ((cbBatch.Checked) && RBalance > 0)
                {
                    lblmsg.Text = "Error! Batchnumber CheckBox needs to be checked when all forms are completed!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                    DisableAndHideStuff();
                    return;

                }

                int status = 0;
                status = dwt.UpsertAppData(AppID, Region, BatchNum, Interviews, TQuantity, QtyVerified, Quantity, Packets, MyUserID, WorkDate, BComplete, TIncomplete, TComplete, RBalance, NAddForms, AngularCompleted, InterviewsCompleted, DateRecieved);
                if (status > 1)
                {
                    if (RBalance > 0)
                    {
                        CreateForm();
                        lblmsg.Text = "Thank you! Data has been saved!";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                        // if the Save button doesn't contain a 'disabled' class, add one
                      //  if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    }
                    else
                    {
                        CreateForm();
                        lblmsg.Text = "Congrats! All forms completed!";
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + lblmsg.ClientID + "').style.display = 'none' },5000);", true);
                        // if the Save button doesn't contain a 'disabled' class, add one
                    //    if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    }

                }
                else
                {
                    DisableAndHideStuff();
                    lblmsg.Text = "Error! Application could not be added. Error = " + status.ToString() + " Please show the administrator this error message.";
                }
            }
            else
            {
                DisableAndHideStuff();
                lblmsg.Text = "Error! The dropdown was not valid. Please try reloading this application and try again.";
            }

        }

        protected bool AlertVerify()
        {
            lblmsg.Text = "";
            CheckBox tbQtyVerified = (CheckBox)FormView1.FindControl("QtyVerifiedCheckBox");
            bool alertverify = false;
            if (tbQtyVerified.Checked == false)
            {
                alertverify = true;
            }
            return alertverify;

        }

        protected void DisableAndHideStuff()
        {
            Control RegiontblForm = (Control)FormView1.FindControl("RegiontblForm");
            Control BatchnumbertblForm = (Control)FormView1.FindControl("BatchnumbertblForm");
            Control AngulartblForm = (Control)FormView1.FindControl("AngulartblForm");
            Control InterviewtblForm = (Control)FormView1.FindControl("InterviewtblForm");
            Control PacketstblForm = (Control)FormView1.FindControl("PacketstblForm");
            Control BatchnumtblForm = (Control)FormView1.FindControl("BatchnumtblForm");
            Control VerifyQuantitytblForm = (Control)FormView1.FindControl("VerifyQuantitytblForm");
            Control tblinterkey = (Control)FormView1.FindControl("tblinterkey");
            Control tblangukey = (Control)FormView1.FindControl("tblangukey");
            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            CheckBox tbQtyVerified = (CheckBox)FormView1.FindControl("QtyVerifiedCheckBox");
            CheckBox cbBatch = (CheckBox)FormView1.FindControl("BCompleteCheckBox");
            DropDownList txtRegion = (DropDownList)FormView1.FindControl("txtRegion");
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox txtPackets = (TextBox)FormView1.FindControl("PacketsTextBox");
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            Label frmsrecdate = (Label)FormView1.FindControl("FrmsRecDateTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");
            Control trBatchNum = (Control)FormView1.FindControl("trBatchNum");
            Control trPackets = (Control)FormView1.FindControl("trPackets");
            Control trAngular = (Control)FormView1.FindControl("trAngular");
            Control trInterviews = (Control)FormView1.FindControl("trInterviews");
            Control tblForm1 = (Control)FormView1.FindControl("tblForm1");
            Control datetable = (Control)FormView1.FindControl("datetable");

            ddlApplication.ClearSelection();
            //lblmsg.Text = "";
            tblForm1.Visible = false;    // invalid form selected, so hide data
            PacketstblForm.Visible = false;
            BatchnumbertblForm.Visible = false;
            BatchnumtblForm.Visible = false;
            RegiontblForm.Visible = false;
            VerifyQuantitytblForm.Visible = false;
            InterviewtblForm.Visible = false;
            AngulartblForm.Visible = false;
            PacketstblForm.Visible = false;
            datetable.Visible = false;


        }
        protected bool TableValidator()
        {
            Control RegiontblForm = (Control)FormView1.FindControl("RegiontblForm");
            Control BatchnumbertblForm = (Control)FormView1.FindControl("BatchnumbertblForm");
            Control AngulartblForm = (Control)FormView1.FindControl("AngulartblForm");
            Control InterviewtblForm = (Control)FormView1.FindControl("InterviewtblForm");
            Control PacketstblForm = (Control)FormView1.FindControl("PacketstblForm");
            Control BatchnumtblForm = (Control)FormView1.FindControl("BatchnumtblForm");
            Control VerifyQuantitytblForm = (Control)FormView1.FindControl("VerifyQuantitytblForm");
            Control tblinterkey = (Control)FormView1.FindControl("tblinterkey");
            Control tblangukey = (Control)FormView1.FindControl("tblangukey");
            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            CheckBox tbQtyVerified = (CheckBox)FormView1.FindControl("QtyVerifiedCheckBox");
            CheckBox cbBatch = (CheckBox)FormView1.FindControl("BCompleteCheckBox");
            DropDownList txtRegion = (DropDownList)FormView1.FindControl("txtRegion");
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox txtPackets = (TextBox)FormView1.FindControl("PacketsTextBox");
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            Label frmsrecdate = (Label)FormView1.FindControl("FrmsRecDateTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");
            Control trBatchNum = (Control)FormView1.FindControl("trBatchNum");
            Control trPackets = (Control)FormView1.FindControl("trPackets");
            Control trAngular = (Control)FormView1.FindControl("trAngular");
            Control trInterviews = (Control)FormView1.FindControl("trInterviews");
            Control tblForm1 = (Control)FormView1.FindControl("tblForm1");
            //lblmsg.Text = "";
            ttlincomptxt.Enabled = false;
            ttlkeytxt.Enabled = false;
            outtxt.Enabled = false;
            ttlincomptxt.ReadOnly = true;
            ttlkeytxt.ReadOnly = true;
            outtxt.ReadOnly = true;
            int AppID = 0;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            DWUtility dwt = new DWUtility();
            bool chbatch = false;

            AppConfigInfo aci = dwt.GetAppByID(AppID);
            if (aci.BatchNum && txtBatchNum.Text.Length <= 0 && trBatchNum.Visible || aci.Packets && txtPackets.Text.Length <= 0 && trPackets.Visible)
            {
                chbatch = true;

            }

            return chbatch;
        }

        protected bool CheckBComplete()
        {//Check if batch number exsist for the fiscal year. Prevents from updating a record that is already complete

            TextBox ttlincomptxt = (TextBox)FormView1.FindControl("TIncompleteTextBox");
            TextBox ttlkeytxt = (TextBox)FormView1.FindControl("TQuantityTextBox");
            TextBox outtxt = (TextBox)FormView1.FindControl("RBalanceTextBox");
            CheckBox tbQtyVerified = (CheckBox)FormView1.FindControl("QtyVerifiedCheckBox");
            CheckBox cbBatch = (CheckBox)FormView1.FindControl("BCompleteCheckBox");
            DropDownList txtRegion = (DropDownList)FormView1.FindControl("txtRegion");
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            TextBox txtAngular = (TextBox)FormView1.FindControl("QuantityTextBox");
            TextBox txtangkey = (TextBox)FormView1.FindControl("AngularCompletedTextBox");
            TextBox txtPackets = (TextBox)FormView1.FindControl("PacketsTextBox");
            TextBox txtInterkey = (TextBox)FormView1.FindControl("InterviewsCompletedTextBox");
            TextBox txtInterviews = (TextBox)FormView1.FindControl("InterviewsTextBox");
            Label frmsrecdate = (Label)FormView1.FindControl("FrmsRecDateTextBox");
            TextBox keyeddate = (TextBox)FormView1.FindControl("TotalKeyedDateTextBox");
            TextBox frmsrectxt = (TextBox)FormView1.FindControl("NAddFormsTextBox");
            TextBox keytxt = (TextBox)FormView1.FindControl("TCompleteTextBox");

            //Declare Variables
            int BatchNum = 0, TQuantity = 0, AppID = 0;
            bool checkbox2 = false;
            bool BComplete = false;
            BComplete = cbBatch.Checked;
            DateTime today, startDate, endDate;
            AppID = Convert.ToInt32(ddlApplication.SelectedValue);
            int CreatedByUserID = 0;
            CreatedByUserID = Convert.ToInt32(Session["MyUserID"]);
            //lblmsg.Text = "";
            DWUtility dwt = new DWUtility();
            AppConfigInfo aci = dwt.GetAppByID(AppID);
            DataRow dr = null;


            //Set variables

            if (aci.BatchNum && txtBatchNum.Text.Length > 0)
            {
                int.TryParse(txtBatchNum.Text, out BatchNum);
            }
            if (aci.TQuantity && ttlkeytxt.Text != "")
            {
                TQuantity = Convert.ToInt32(ttlkeytxt.Text);
            }
            int RBalance = 0;
            int.TryParse(outtxt.Text, out RBalance); //total outstanding

            SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ToString());
            using (SqlCommand sqlCommand = new SqlCommand("SELECT * from DWT.AppDataLog  where BatchNum = @BatchNum AND AppID = @AppID AND BComplete = 1 AND CreatedByUserID = @CreatedByUserID", sqlConnection))
            {
                sqlConnection.Open();
                sqlCommand.Parameters.AddWithValue("@BatchNum", Convert.ToInt32(BatchNum).ToString());
                sqlCommand.Parameters.AddWithValue("@AppID", Convert.ToInt32(AppID).ToString());
                sqlCommand.Parameters.AddWithValue("@BComplete", Convert.ToInt32(BComplete).ToString());
                sqlCommand.Parameters.AddWithValue("@CreatedByUserID", Convert.ToInt32(CreatedByUserID).ToString());

                Int32 userCount = Convert.ToInt32(sqlCommand.ExecuteScalar());
                SqlDataReader reader = sqlCommand.ExecuteReader();


                dr = dwt.GetAppBatchData(AppID, BatchNum); //get all app data

                today = DateTime.Today;

                startDate = new DateTime(DateTime.Today.Year, 7, 1);//startdate of fiscal year
                endDate = new DateTime(DateTime.Today.Year + 1, 7, 1).AddDays(-1);//enddate of fiscal year

                if (today < startDate && today < endDate)//check if your in the current fiscal year
                {
                    startDate = startDate.AddYears(-1);
                    endDate = endDate.AddYears(-1);
                }

                if (dr != null)
                {
                    DateTime ModifiedDate = Convert.ToDateTime(dr["CreatedDate"]); //today date
                    if (ModifiedDate > startDate && ModifiedDate < endDate)//if it is in the fiscal year?
                    {
                        if (reader.HasRows)//Does BatchNum, AppID already exsists ?
                        {
                            checkbox2 = true;
                        }
                    }
                }
                reader.Close();
                sqlConnection.Close();


            }

            return checkbox2;
        }

        protected void batch_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBox txtBatchNum = (TextBox)FormView1.FindControl("BatchNumTextBox");
            DropDownList batch = (DropDownList)FormView1.FindControl("batch");

            txtBatchNum.Text = "";
            txtBatchNum.Text = batch.SelectedItem.ToString();
            lblmsg.Text = "";
        }

        protected void ShowPopUpOkCancel(string header, string message, string routine, string subroutine)
        {
            // The ShowPopUpOkCancel method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called OkCancel. It takes 2 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function OkCancel(head, mess, newpage) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-danger',");
            sb.Append("label: '&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;',");
            sb.Append("action: function (dialogItself) {__doPostBack('" + routine + "', '" + subroutine + "');}},");
            sb.Append("{cssClass: 'btn-success',");
            sb.Append("label: 'Cancel',");
            sb.Append("action: function (dialogItself) {dialogItself.close();}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.            
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation. 
            string funcCall = "<script language='javascript'>OkCancel('" + header + "','<b>" + message + "</b>');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }



    }
}