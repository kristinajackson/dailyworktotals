﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Web.Security;

namespace DailyWT.User
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
        //    Response.Redirect(Page.ResolveUrl("http://twraintranet.nash.tenn/DailyWorkTotalsTest"), false);
            
        }

        protected void Redirect(object sender, EventArgs e)
        {
            string num = "123";
            Session["SessionID"] = num.ToString();
            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", "123"));

            Response.Redirect(Page.ResolveUrl("Login.aspx"), false);
            
        }
    }
}