﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="DailyWT.User.Print" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<style>
    /*This is a wide form, so set column 1 to 0%, and the rest of the columns to 100%*/
    .col-xs-1 {
        width:0% !important;    
    }
    .col-xs-11 {
        width: 100% !important;
    }
</style>
<script>
    // used to show the nicely formatted tooltips
      $(document).ready(function () {
          $('[data-toggle="tooltip"]').tooltip();
      });
</script>
<script>
    $(document).ready(function () {
        var FormsUsed = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Qty must be a decimal number'
                }
            }
        },
        NewEndSeq = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'Last Seq# Used must be alphabetic chars'
                }
            }
        },
        FormsReq = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Qty must be a decimal number'
                }
            }
        },
        Requester = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'The Requester must be alphabetic chars'
                }
            }
        },

        fIndex = 0;

        // get number of lines so the correct number of validators are created
        var formCount = document.getElementById("<%=hfForms.ClientID%>").value;

        while (fIndex < formCount) {
            $('#mpForm')
            .bootstrapValidator('addField', 'tbFormsUsed' + fIndex, FormsUsed)
            .bootstrapValidator('addField', 'tbNewEndSeq' + fIndex, NewEndSeq)
            .bootstrapValidator('addField', 'tbFormsReq' + fIndex, FormsReq)
            .bootstrapValidator('addField', 'tbRequester' + fIndex++, Requester)
        }
    });
</script>

<div class="container-fluid toggle">
            <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Print Module</span><span class="col-xs-1">&nbsp;</span>
<%--                <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static"></asp:TextBox>--%>
                <!--hidden fields used to pass number of lines to javascript -->
                <input type="hidden" runat="server" id="hfForms" />
            </div>
</div>
<!-- Section 1 -->
<section class="container-fluid toggle" id="section1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Form Printing</span></div>
<div class="container-fluid">
  <table class="table table-condensed">
    <thead>
      <tr class="h4">
        <th>Job Name</th>
        <th>Forms Per Box</th>
        <th>Total Boxes</th>
        <th>Open Box Amt</th>
        <th>Beg Amt</th>
        <th>Beg Seq#</th>
        <th>Last Seq# Used</th>
        <th>Qty Used</th>
        <th>Qty Req</th>
        <th>Requester</th>
      </tr>
    </thead>
    <tbody id="tblBodyPri" runat="server">
    </tbody>
  </table>

</div>
<div class="panel-footer">
<%--    <button type="submit" name="btnSave" onclick="Save_Click" class="btn btn-success btn-lg" >--%>
    <button type="submit" name="btnSave" class="btn btn-success btn-lg" >
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="reset" id="resetregform" class="btn btn-default btn-lg">
        Clear Form</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="button" id="btnJobConfig" class="btn btn-warning btn-lg pull-right" onclick="NewPage('/User/JobConfig.aspx')">
        &nbsp;&nbsp;Configure Jobs&nbsp;&nbsp;</button>&nbsp;
</div>

</div>


</section>


</asp:Content>
