﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using DailyWT.Classes;
using System.Net.Mail;
using System.Windows;

namespace DailyWT.User
{
    public partial class Printing : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDJob();
                }
                else
                {
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: DoAdmin
                    if (eventTarget == "SaveData")
                    {
                        SaveData();
                    }
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }
        protected void LoadDDJob()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";

            ddlJob.DataBind();
            ddlJob.Items.Insert(0, "-- Select Formtype --");

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.Items.Insert(0, "-- Select Job --");
            ddlForm.Enabled = false;
        }
        protected void LoadDDForm(int JobID)
        {
            DWUtility dwt = new DWUtility();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.DataSource = dwt.GetFormsByJobID(JobID);
            ddlForm.DataTextField = "FormName";
            ddlForm.DataValueField = "FormID";

            ddlForm.DataBind();
            ddlForm.Items.Insert(0, "-- Select Job --");


        }



        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            tblForm.Visible = false;    // something selected, so hide data

            if (ddlJob.SelectedIndex > 0)  // did they select one?
            {
                int JobID = Convert.ToInt32(ddlJob.SelectedValue);
                if (JobID > 0)
                {
                    ddlForm.Enabled = true;
                    LoadDDForm(JobID);
                }
                else
                {
                    ddlForm.Enabled = false;
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            else
            {
                ddlForm.Items.Clear();       // clear in case it's already populated
                ddlForm.Items.Insert(0, "-- Select Job --");
                ddlForm.Enabled = false;
            }

        }
        protected void ddlForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlForm.SelectedIndex > 0)  // did they select one?
            {
                int FormID = Convert.ToInt32(ddlForm.SelectedValue);
                if (FormID > 0)
                {
                    tblForm.Visible = true;
                    LoadForm(FormID);
                    btnSave.CssClass = btnSave.CssClass.ToString().Replace("disabled", ""); // remove disabled class
                }
                else
                {
                    // if the Save button doesn't contain a 'disabled' class, add one
                    if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    tblForm.Visible = false;    // invalid form selected, so hide data
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            else
            {
                // if the Save button doesn't contain a 'disabled' class, add one
                if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                tblForm.Visible = false;    // no form selected, so hide data
            }

        }
        protected void LoadForm(int FormID)
        {
            DWUtility dwt = new DWUtility();

            DataSet ds2 = new DataSet();

            bool IsUsed = true;

            string FormName = ddlForm.SelectedItem.ToString();

            FormConfigInfo fci = new FormConfigInfo();
            fci = dwt.GetFormByID(FormID);

            ds2 = dwt.GetFormData(IsUsed, FormID);  // get all form data

            // is there data in table?
            if (ds2.Tables[0].Rows.Count > 0)
            {
                bool IsReasonUsed = true;

                // hide no data message and show printing form
                divNoData.Visible = false;
                tblForm.Visible = true;

                ddlReason.Items.Clear();       // clear in case it's already populated
                ddlReason.DataSource = dwt.GetReasons(IsReasonUsed);
                ddlReason.DataTextField = "Reason";
                ddlReason.DataValueField = "ReasonID";

                ddlReason.DataBind();
                ddlReason.Items.Insert(0, "-- Select Reason --");
                Style myStyle = new Style();
                myStyle.ForeColor = System.Drawing.Color.Black;
                ddlReason.ApplyStyle(myStyle);


                DataRow dr = null;

                dr = dwt.GetFormDataByID(FormID);

                if (dr != null)
                {

                    lbldate.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();

                    lblEndAmount.InnerText = dr["EndAmt"].ToString();

                    // do we have sequernce numbers?
                    if (fci.Sequence)
                    {
                        trSeq1.Visible = true;
                        trSeq2.Visible = true;
                        lblBegSeq.InnerText = dr["BegSeq"].ToString();
                        lblEndSeq.InnerText = dr["EndSeq"].ToString();
                    }

                    else
                    {
                        trSeq1.Visible = false;
                        trSeq2.Visible = false;
                    }
                }
                else
                {
                    // hide printing form and show no data message 
                    tblForm.Visible = false;
                    divNoData.Visible = true;
                }
            }
            else
            {
                // hide printing form and show no data message 
                tblForm.Visible = false;
                divNoData.Visible = true;
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
            int FormsReq=0, Voids=0;
            int.TryParse(Request.Form["tbFormsReq"], out FormsReq);
            int.TryParse(Request.Form["tbvoids"], out Voids);
         
            string FormName = ddlForm.SelectedItem.ToString();

            ShowPopUpOkCancel("Save Data?", "Are you sure you want to print " + FormName + "?<br/><br/>" + FormsReq + " Forms<br/><br/>" + Voids + " Voids <br /><br />", "SaveData", "");
          
        }


        protected void SaveData()
        {
            if (ddlForm.SelectedIndex > 0)  // did they select one?
            {
                DateTime ShipDate = System.DateTime.Today;
                DateTime Shippingdate = System.DateTime.Today;
                DateTime DateReceived = System.DateTime.Today;
                string  Reason = ""; ;
                int Voids = 0, FormsReq = 0, PrinterErrorJam = 0, Sample = 0, ShippingDamage = 0, DamagedFolding = 0, TextPrintAlign = 0, PagesStuckTogether = 0;


                string Requester = Regex.Replace(Request.Form["tbRequester"], @"[^a-zA-Z\ \-]", "").ToUpper();

                string Comment = Regex.Replace(Request.Form["tbComment"], @"[^A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]", "").ToUpper();  // get rid of any junk characters
                Comment = Regex.Replace(Comment, @"\r\n", " "); // ok, now change the cr/lf to spaces

                int.TryParse(Request.Form["tbFormsReq"], out FormsReq);
                int.TryParse(Request.Form["tbvoids"], out Voids);
                DateReceived = Convert.ToDateTime(lbldate.Text);
                FormsReq = FormsReq + Voids;

                if (FormsReq < 1 && ddlReason.SelectedIndex <= 0)
                {
                    ShowPopUp("Error!", "Quantity Printed cannot be blank or zero.");
                }
                if (Requester.Length < 1)
                {
                    ShowPopUp("Error!", "Requester cannot be blank.");
                }
                else if (Voids > 0 && ddlReason.SelectedIndex <= 0)
                {
                    ShowPopUp("Error!", "Please select a Reason for Voids.");
                }
                else if (Voids <= 0 && ddlReason.SelectedIndex > 0)
                {
                    ShowPopUp("Error!", "Enter number of Voids.");
                }
                else
                {

                    DWUtility dwt = new DWUtility();
                    DataSet ds1 = new DataSet();
                    DataSet ds2 = new DataSet();


                    bool IsUsed = true;
                    int ReasonID = 0;
                    if (ddlReason.SelectedIndex > 0)
                    {
                        ReasonID = Convert.ToInt32(ddlReason.SelectedValue);
                        Reason = ddlReason.SelectedItem.ToString();
                        switch (Reason)
                        {
                            case "PRINTER ERROR- JAMMED":
                                PrinterErrorJam = Voids;
                                break;
                            case "SAMPLE":
                                Sample = Voids;
                                break;
                            case "SHIPPING DAMAGE":
                                ShippingDamage = Voids;
                                break;
                            case "DAMAGED BY PRINT SHOP - FOLDING":
                                DamagedFolding = Voids;
                                break;
                            case "TEXT PRINTED OUT OF ALIGNMENT":
                                TextPrintAlign = Voids;
                                break;
                            case "PAGES STUCK TOGETHER":
                                PagesStuckTogether = Voids;
                                break;
                            default:
                                {
                                    break;
                                }
                        }
                    }
                    int FormID = Convert.ToInt32(ddlForm.SelectedValue);

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    FormConfigInfo fci = new FormConfigInfo();
                    fci = dwt.GetFormByID(FormID);

                    ds2 = dwt.GetFormData(IsUsed, FormID);  // get all form data

                    DataRow dr = null;

                    dr = dwt.GetFormDataByID(FormID);

                    if (dr != null)
                    {

                        int BegAmt = Convert.ToInt32(dr["EndAmt"]);


                        int EndAmt = Convert.ToInt32(dr["EndAmt"]);

                        int TotalForms = 0;

                        TotalForms = EndAmt;


                        // calculate new totals
                        TotalForms = TotalForms - FormsReq;
                        if (TotalForms < 0)
                        {
                            // Notify the user that an error occurred.
                            ShowPopUp("Error!", "The number of forms used is more than the quantity on hand. Please inform the administrator.");
                            return;
                        }


                        EndAmt = TotalForms;


                        string BegSeq = "";
                        string EndSeq = "";

                        // do we have sequernce numbers?

                        if (fci.Sequence)
                        {

                            string begg = dr["BegSeq"].ToString();

                            string endd = dr["EndSeq"].ToString();

                            // get the length to format it in case there are leading 0's
                            int SeqLen = ((string)ds2.Tables[0].Rows[0].ItemArray[5]).Length;

                            BegSeq = (Convert.ToInt32(begg) + FormsReq).ToString("D" + SeqLen.ToString());
                            EndSeq = (Convert.ToInt32(endd)).ToString("D" + SeqLen.ToString());
                        }
                        else
                        {
                            BegSeq = "";
                            EndSeq = "";
                        }

                        FormsReq = 0;
                        int.TryParse(Request.Form["tbFormsReq"], out FormsReq);
                        ShipDate = DateReceived;
                        DateTime CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);

                        int status = dwt.UpsertPrintData(FormID, IsUsed, BegAmt, EndAmt, BegSeq, EndSeq,  FormsReq, Requester, ReasonID, Comment, MyUserID, Voids, DateReceived, CreatedDate, Reason);
                        if (status < 0)
                        {
                            // Notify the user that an error occurred.
                            ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                            return;
                        }
                        else
                        {

                            IsUsed = true;
                            FormID = Convert.ToInt32(ddlForm.SelectedValue);
                            ds2 = dwt.GetFormData(IsUsed, FormID);  // get all form data
                            EndAmt = Convert.ToInt32(dr["EndAmt"]);
                            ds1 = dwt.GetJobByJobID(FormID);

                            string JobName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();


                            string FormName = ddlForm.SelectedItem.ToString();

                            IsUsed = true;
                            DataSet ds = new DataSet();
                            ds = dwt.GetEmailPrint(IsUsed);

                            int dsCount = ds.Tables[0].Rows.Count;

                            for (int i = 0; i < dsCount; i++)
                            {

                                string from = "Kristina Jackson <Kristina.Jackson@tn.gov>";
                                string to = ds.Tables[0].Rows[i]["Email"].ToString();

                                string subject = "";
                                string body = "";

                                if (EndAmt < 15000 && EndAmt > 0 && JobName.Contains("BOAT") == true)
                                {//BOAT DECAL, BOAT LICENSE
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System. With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt < 500 && EndAmt > 0 && FormName.Contains("safety") == true && FormName.Contains("instructor") == true)
                                {//HUNTER SAFETY INSTRUCTOR
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System.With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 4000 && EndAmt > 0 && FormName.Contains("safety") == true && FormName.Contains("student") == true)
                                {//HUNTER SAFETY STUDENTS
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System. With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 7500 && EndAmt > 0 && FormName.Contains("boating") == true && FormName.Contains("education") == true)
                                {//BOATING EDUCATION
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System. With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 36 && EndAmt > 0 && FormName.Contains("ribbons") == true && FormName.Contains("fargo") == true)
                                {//FARGO COLOR RIBBONS
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System.  With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 3000 && EndAmt > 0 && FormName.Contains("data") == true && FormName.Contains("card") == true)
                                {//DATA CARDS
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System.  With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 3 && EndAmt > 0 && FormName.Contains("TONER") == true)
                                {//FIERY TONERS
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System.  With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 15000 && EndAmt > 0 && FormName.Contains("envelope") == true)
                                {//ENVELOPES
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System.  With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt <= 2 && EndAmt > 0)
                                {//ENVELOPES
                                    subject = FormName + " Inventory has a low supply -- Daily Work Totals";
                                    body = " Quanitiy on hand for " + FormName + " is running low in the Daily Work Totals System.  With a balance of " + EndAmt + " forms. Please restock soon.";
                                    SendEmail(from, to, subject, body);
                                }
                                else if (EndAmt == 0)
                                {
                                    subject = FormName + "  Inventory in demand -- Daily Work Totals";
                                    body = FormName + " currently out-of-stock in the Daily Work Totals System.  With a balance of " + EndAmt + " forms. Please Restock now.";
                                    SendEmail(from, to, subject, body);
                                }
                            }

                            LoadDDJob();

                            // if the Save button doesn't contain a 'disabled' class, add one
                            if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                            tblForm.Visible = false;    // no form selected, so hide data
                            ShowPopUp("Success!", "The data was successfully saved!");
                        }
                    }
                    else
                    {
                        ShowPopUp("Error!", "No inventory in the system!");

                    }

                }
            }


            else
            {
                ShowPopUp("Error!", "Please select data to save!");
            }

        }

        public void SendEmail(string from, string to, string subject, string body)
        {
           
                MailMessage msg = new MailMessage(from, to, subject, body);
                SmtpClient cl = new SmtpClient();
                try
                {
                    cl.Send(msg);
                }
                catch
                {
                }

        }

        protected void ShowPopUpOkCancel(string header, string message, string routine, string subroutine)
        {
            // The ShowPopUpOkCancel method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called OkCancel. It takes 2 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function OkCancel(head, mess, newpage) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-danger',");
            sb.Append("label: '&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;',");
            sb.Append("action: function (dialogItself) {__doPostBack('" + routine + "', '" + subroutine + "');}},");
            sb.Append("{cssClass: 'btn-success',");
            sb.Append("label: 'Cancel',");
            sb.Append("action: function (dialogItself) {dialogItself.close();}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.            
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation. 
            string funcCall = "<script language='javascript'>OkCancel('" + header + "','<b>" + message + "</b>');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }


        }
    }