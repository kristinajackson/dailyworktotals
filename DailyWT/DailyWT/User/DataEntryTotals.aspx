﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntryTotals.aspx.cs" Inherits="DailyWT.DataEntryTotals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
     <script>
         $(document).ready(function () {
             $("#tbDEDate").daterangepicker({
                 singleDatePicker: true,
                 showDropdowns: true
             },
             function (start, end) { // when calendar closes, this function is called, so do a postback to read new data
                 //__doPostBack('tbDEDate', start.format('MM/DD/YYYY'));
             });
         });

         </script>

    <div class="container-fluid">
        <br />
        <br />
        <!--Row with one column -->
        <div class="row">
            <div class="h3 text-center">
                <span class="col-xs-3 col-xs-offset-2">Daily Data Entry Totals</span><span class="col-xs-1">&nbsp;</span>
                <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static" ></asp:TextBox>
                <%-- <input type="hidden" runat="server" id="hfApps" />
                <input type="hidden" runat="server" id="hfWorked" />--%>
            </div>
        </div>
    </div> 
    <section class="container-fluid" id="section1">
        <div class="col-xs-7 col-xs-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Daily Data Entry Totals Module</span>
                </div>
                <div class="panel-body">
                    <div class="form-group h4 MyInfo">

                        <label class="control-label col-xs-6">Application:</label>
                        <div class="col-xs-6">
                            <asp:DropDownList ID="ddlApplication" OnSelectedIndexChanged="ddlApplication_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div id="tblForm1" runat="server" visible="false">
                        <div class="form-group h4 MyInfoNC">
                            <label class="control-label col-xs-6">Quantity </label>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" id="tbUncomApp" name="tbUncomApp" value="<%= this.tbUncomApp %>" placeholder="Quantity Incomplete" />
                            </div>
                        </div>
                        <div id="top" runat="server" visible="false" class="form-group h4 MyInfoNC">
                            <label class="control-label col-xs-6">Total Uncompleted Applications</label>
                            <div class="col-xs-6">
                                <label runat="server" id="lbluncomplete" class='info'></label>
                            </div>
                        </div>
                        <div id="Div1" runat="server" class="form-group h4 MyInfo">
                            <label class="control-label col-xs-6">Total Completed</label>
                            <div class="col-xs-6">
                                <label runat="server" id="lblTotalCompleted" class='info'></label>
                            </div>
                        </div>
                        <div class="form-group h4 MyInfoNC">
                            <label class="control-label col-xs-6">Remaining Balance </label>
                            <div class="col-xs-6">
                                <label runat="server" id="lblremaining" class='info'></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                        <div style="text-align:center;">
                    <asp:Button ID="btnSave" CssClass="btn btn-success btn-lg submit-button disabled" runat="server" Text="Save Changes" OnClick="SaveChanges_Click"  />
                    &nbsp;&nbsp;
                    <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/DataEntry.aspx')">
                        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;
                            <button type="button" id="btnAddForms" class="" runat="server" onserverclick="btnAddForms_ServerClick">&nbsp;&nbsp;Add New Forms&nbsp;&nbsp;</button>
                            </div>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
