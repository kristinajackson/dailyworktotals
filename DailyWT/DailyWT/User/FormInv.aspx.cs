﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.SqlClient;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class FormInv : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    //if (Session["Admin"] != null)
                    //{
                       CreateForm();
                        LoadDDJob();
               //     }
                   
                }
                else
                {
                    string eventTarget = this.Request["__EVENTTARGET"];     // event name: tbDEDate
                    if (eventTarget == "Approve")
                    {
                        ApproveBox();
                    }
                    
                }
            }

                else
                {
                    ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
                }

        }
        protected void CreateForm()
        {
            mastertable.Visible = false;
            msgLabel.Text = "";
            if (ddlJob.SelectedIndex > 0)
            {

                DWUtility dwt = new DWUtility();

                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet ds3 = new DataSet();

                bool IsFormUsed = true;
                bool IsUsed = true;

                int MyUserID = Convert.ToInt32(Session["MyUserID"]);
                int JobID;

                JobID = Convert.ToInt32(JobnmeTextBox.Text);

                ds1 = dwt.GetFormConfig(IsFormUsed);
                ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data
                ds3 = dwt.GetJob(IsUsed, JobID);



                int ds1Count = ds1.Tables[0].Rows.Count;
                int ds2Count = ds2.Tables[0].Rows.Count;
                int ds3Count = ds3.Tables[0].Rows.Count;

                // copy number of rows to hidden field for javascript
                hfForms.Value = ds3Count.ToString();

                string iStr = "";

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < ds3Count; i++)
                {
                    iStr = i.ToString();
                    int EndAmt = 0;
                    int.TryParse(ds3.Tables[0].Rows[i][3].ToString(), out EndAmt);
                    DateTime date = DateTime.Parse(ds3.Tables[0].Rows[i][7].ToString());
                    // search the dataset for a row (by FormID) since we can't sort it
                    DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds3.Tables[0].Rows[i][0] + "'");

                    // assign 'info' styling to every other line

                    sb.Append("<tr " + ((i % 2 == 0) ? "class='info'" : "") + ">");
                    sb.Append("<td target='tdName' >" + ds3.Tables[0].Rows[i][12].ToString() + "</td>");
                    sb.Append("<td target='tdName' class='form-group' ><input type='text'  class='form-control' readonly name='tbShipDate'" + iStr +
                            "' value='" + date.ToShortDateString() +
                            "' placeholder='  MM/DD/YYYY  ' /></td>");
                    sb.Append("<td target='tdNumb' class='form-group'><input type='text'  class='form-control' readonly name='tbBegAmt" + iStr +
                        "' value='" + ds3.Tables[0].Rows[i][2].ToString() +
                            "' placeholder='BeginAmt' /></td>");
                    sb.Append("<td target='tdNumb' class='form-group'><input type='text'  class='form-control' readonly  name='tbEndAmt" + iStr +
                            "' value='" + ds3.Tables[0].Rows[i][3].ToString() +
                            "' placeholder='EndAmt' /></td>");
                    sb.Append("<td target='tdText' class='form-group'><input type='text'  class='form-control'  readonly name='tbBegSeq" + iStr +
                          "' value='" + ds3.Tables[0].Rows[i][4].ToString() + "' placeholder='BegSeq#' /></td>");
                    sb.Append("<td target='tdText' class='form-group'><input type='text' class='form-control'  readonly name='tbEndSeq" + iStr +
                             "' value='" + ds3.Tables[0].Rows[i][5].ToString() +
                             "' placeholder='EndSeq#' /></td>");
                    sb.Append("</tr>");

                }
                if (sb.Length > 0)
                {
                    mastertable.Visible = true;
                    // display it on the screen
                    tblBodyInv.InnerHtml = sb.ToString();

                }
                else
                {
                    mastertable.Visible = false;
                    msgLabel.Text = "No Data Available";
                }


            }
        }



        protected void LoadDDJob()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";

            ddlJob.DataBind();
            ddlJob.Items.Insert(0, "-- Select FormType --");

        }
        //protected void SaveData()
        //{
        //    string BegSeq, EndSeq, JobName, FormName;
        //    int status, BegAmt, EndAmt, MyUserID, JobID;

        //    MyUserID = Convert.ToInt32(Session["MyUserID"]);
        //    JobID = Convert.ToInt32(JobnmeTextBox.Text);

        //    DWUtility dwt = new DWUtility();

        //    DataSet ds1 = new DataSet();
        //    DataSet ds2 = new DataSet();
        //    DataSet ds3 = new DataSet();

        //    bool IsFormUsed = true;
        //    bool IsUsed = true;
        //    bool FormInvUpdated = false;

        //     MyUserID = Convert.ToInt32(Session["MyUserID"]);

        //    JobID = Convert.ToInt32(JobnmeTextBox.Text);
        //    JobName = JobnmeTextBox.Text;
        //    FormName = "";

        //    ds1 = dwt.GetFormConfig(IsFormUsed);
        //    ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data
        //    ds3 = dwt.GetJob(IsUsed, JobID);



        //    int ds1Count = ds1.Tables[0].Rows.Count;
        //    int ds2Count = ds2.Tables[0].Rows.Count;
        //    int ds3Count = ds3.Tables[0].Rows.Count;

        //    // copy number of rows to hidden field for javascript
        //    hfForms.Value = ds3Count.ToString();

        //    string iStr = "";

        //    //StringBuilder sb = new StringBuilder();
        //    //for (int i = 0; i < ds3Count; i++)
        //    //{
        //    //    iStr = i.ToString();
        //    //    // search the dataset for a row (by FormID) since we can't sort it
        //    //    DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds3.Tables[0].Rows[i][0] + "'");



        //    for (int i = 0; i < ds3Count; i++)
        //    {
        //        iStr = i.ToString();
        //        FormInvUpdated = false;

        //        // search the dataset for a row (by FormID) since we can't sort it
        //     //   DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");
        //        DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

        //        if (DRow.Length > 0)
        //        {
        //            FormName = DRow[0].ItemArray[26].ToString();
        //            JobName = DRow[0].ItemArray[2].ToString();
        //        }

        //        int.TryParse(Request.Form["tbBegAmt" + iStr], out BegAmt);

        //        if (DRow.Length > 0)
        //        {
                   

        //            if (BegAmt != (int)DRow[0].ItemArray[10])
        //                FormInvUpdated = true;
        //        }


        //        int.TryParse(Request.Form["tbEndAmt" + iStr], out EndAmt);
        //        if (DRow.Length > 0)
        //        {

        //            if (EndAmt != (int)DRow[0].ItemArray[11])
        //                FormInvUpdated = true;
        //        }

        //        BegSeq = Request.Form["tbBegSeq" + iStr] ?? "";
        //        if (DRow.Length > 0)
        //        {
        //            if (BegSeq != DRow[0].ItemArray[12].ToString())
        //                FormInvUpdated = true;
        //        }


        //        EndSeq = Request.Form["tbEndSeq" + iStr] ?? "";
        //        if (DRow.Length > 0)
        //        {
        //            if (EndSeq != DRow[0].ItemArray[13].ToString())
        //                FormInvUpdated = true;
        //        }


              


        //        if (FormInvUpdated)
        //        {


        //            // set IsUsed to true for now
        //            status = dwt.UpsertForm((int)ds1.Tables[0].Rows[i][0], BegAmt, EndAmt, BegSeq, EndSeq, MyUserID, JobID, JobName, FormName);

        //            if (status < 0)
        //            {
        //                // Notify the user that an error occurred.
        //                ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
        //                return;
        //            }
        //            else
        //            {
        //                CreateForm();
        //                buttonsave.Visible = false;
        //            }

        //          //  status = dwt.UpsertForm(FormID, ShipDate, BegAmt, EndAmt, BegSeq, EndSeq, MyUserID, JobID);
        //        }
        //    }
        //}
        protected void ReadData(string action)
        { //Grabs all data from the form. If vaules on form does not match values in the database 
            //then the system will update to new values
            bool AdminApprove = false;
            bool approve = false;
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();           

            bool IsFormUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data
            

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;


            DateTime ShipDate;
            string ShipNum, BegSeq, EndSeq, FormLocation, UserEndAmt;
            int status, FormsPerBox, TotalBoxes, PartialBoxAmt, BegAmt, EndAmt, UserBegAmt, Total;
            bool FormInvUpdated = false;

            string iStr = "";

            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();
                FormInvUpdated = false;

                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

               // myNewValue = myValue ?? new MyValue();
                ShipNum = Request.Form["tbShipNum" + iStr] ?? "";
                ShipNum = ShipNum.ToUpper();
                if (DRow.Length > 0)
                {
                    if (ShipNum != DRow[0].ItemArray[3].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (ShipNum.Length > 0)
                        FormInvUpdated = true;
                }

                DateTime.TryParse(Request.Form["tbShipDate" + iStr], out ShipDate);
                if (DRow.Length > 0)
                {
                    if (ShipDate == Convert.ToDateTime("1/1/0001"))
                    {
                        DateTime ship = Convert.ToDateTime("01/01/1800");
                        ShipDate = ship;  // load a dummy, but legit date
                    }

                    if (ShipDate != (DateTime)DRow[0].ItemArray[4])
                        FormInvUpdated = true;

                    

                }
                else
                {
                    if (ShipDate != new DateTime())
                    {
                        FormInvUpdated = true;
                    }
                    else
                    {
                        DateTime ship = Convert.ToDateTime("01/01/1800");
                        ShipDate = ship;  // load a dummy, but legit date
                    }
                }

                int.TryParse(Request.Form["tbFormsPerBox" + iStr], out FormsPerBox);
                if (DRow.Length > 0)
                {
                    if (FormsPerBox != (int)DRow[0].ItemArray[5])
                        FormInvUpdated = true;
                }
                else
                {
                    if (FormsPerBox > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbTotalBoxes" + iStr], out TotalBoxes);
                if (DRow.Length > 0)
                { 
                    if (action == "A")
                    {
                        TotalBoxes = (int)DRow[0].ItemArray[17];
                        AdminApprove = true;
                    }

                    if (TotalBoxes != (int)DRow[0].ItemArray[6])
                        FormInvUpdated = true;
                }
                else
                {
                    if (TotalBoxes > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbPartialBoxAmt" + iStr], out PartialBoxAmt);
                if (DRow.Length > 0)
                {
                    if (PartialBoxAmt != (int)DRow[0].ItemArray[7])
                        FormInvUpdated = true;
                }
                else
                {
                    if (PartialBoxAmt > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbUserBegAmt" + iStr], out UserBegAmt);
                if (DRow.Length > 0)
                {
                    if (UserBegAmt != (int)DRow[0].ItemArray[17])
                        FormInvUpdated = true;
                }
                else
                {
                    if (UserBegAmt > 0)
                        FormInvUpdated = true;
                }

                UserEndAmt = Request.Form["tbUserEndAmt" + iStr] ?? "";
                UserEndAmt = UserEndAmt.ToUpper();
                if (DRow.Length > 0)
                {
                    if (UserEndAmt != DRow[0].ItemArray[18].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (UserEndAmt.Length > 0)
                        FormInvUpdated = true;
                }

                int.TryParse(Request.Form["tbBegAmt" + iStr], out BegAmt);

                if (DRow.Length > 0)
                {
                    if (action == "A")
                    {
                        BegAmt = PartialBoxAmt + ( FormsPerBox * UserBegAmt); //sum open box with unopened boxes
                        AdminApprove = true;
                    }

                    if (BegAmt != (int)DRow[0].ItemArray[8])
                        FormInvUpdated = true;
                }
                else
                {
                    if (BegAmt > 0)
                        FormInvUpdated = true;
                }


                int.TryParse(Request.Form["tbEndAmt" + iStr], out EndAmt);
                if (DRow.Length > 0)
                {
                    if (action == "A")
                    {
                        EndAmt = BegAmt; //start from the beginning, EndAmt subtracts as you print
                        AdminApprove = true;
                    }

                    if (EndAmt != (int)DRow[0].ItemArray[9])
                        FormInvUpdated = true;
                }
                else
                {
                    if (EndAmt > 0)
                        FormInvUpdated = true;
                }

                BegSeq = Request.Form["tbBegSeq" + iStr] ?? "";
                if (DRow.Length > 0)
                {
                    if (BegSeq != DRow[0].ItemArray[10].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (BegSeq.Length > 0)
                        FormInvUpdated = true;
                }

                EndSeq = Request.Form["tbEndSeq" + iStr] ?? "";
                if (DRow.Length > 0)
                {
                    if (EndSeq != DRow[0].ItemArray[11].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (EndSeq.Length > 0)
                        FormInvUpdated = true;
                }

                FormLocation = Request.Form["tbFormLocation" + iStr] ?? "";
                FormLocation = FormLocation.ToUpper();
                if (DRow.Length > 0)
                {
                    if (FormLocation != DRow[0].ItemArray[12].ToString())
                        FormInvUpdated = true;
                }
                else
                {
                    if (FormLocation.Length > 0)
                        FormInvUpdated = true;
                }

                string Comment = "";
                if (Request.Form["tbComment"] != null)
                {
                    Regex.Replace(Request.Form["tbComment" + iStr], @"[^A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]", "").ToUpper();
                    Comment = Regex.Replace(Comment, @"\r\n", " "); // ok, now change the cr/lf to spaces
                }
                if (UserBegAmt > 0 && UserBegAmt != TotalBoxes) //boxes in system match boxes counted??
                {
                    approve = true;
                }



                if (FormInvUpdated)
                {
                    // total all up so we don't insert a bunch of zeros data
                    Total = FormsPerBox + TotalBoxes +  BegAmt + EndAmt;

                    // set IsUsed to true for now
                    status = dwt.UpsertFormData((int)ds1.Tables[0].Rows[i][0], true, ShipDate, FormsPerBox, TotalBoxes, 0, BegAmt, EndAmt, UserBegAmt, UserEndAmt, BegSeq, EndSeq, FormLocation, Comment, MyUserID, Total, AdminApprove);

                    if (status < 0)
                    {
                        // Notify the user that an error occurred.
                        ShowPopUp("Error!", "An error occurred saving your data. Please tell the administrator this error number: " + status.ToString());
                        return;
                    }

                }
            }

            if (approve == true && Session["Admin"] == null)
            {
                ShowPopUp("Error!", "Your User Total Boxes does not match with the Total of New, Unopened Boxes. Please contact Administration for approval");
                tblBodyInv.Visible = false;
            }
            else
                ShowPopUp("Success!", "The data was successfully saved!");


        if (approve == true && Session["Admin"] != null)
            {
                ShowPopUp("Warning!", "Please click the Approve button to update the table!");
            }
            else
                ShowPopUp("Thanks", "Data has been Saved! ");

        }

        protected string CreateFormatSpec(string Example)
        {
            // this is presently not used

            char[] StrArray = Example.ToCharArray();
            string NewRegex = "";
            int ExLen = Example.Length;
            for (int i = 0; i < ExLen; i++)
            {

                if (StrArray[i] == ' ')
                    NewRegex += @"[\ ]";
                else
                    if (StrArray[i] == '-')
                        NewRegex += @"[\-]";
                    else
                        if (StrArray[i] == '.')
                            NewRegex += @"[\.]";
                        else
                            if (Regex.IsMatch(StrArray[i].ToString(), @"[0-9]"))
                                NewRegex += @"[0-9]";
                            else
                                if (Regex.IsMatch(StrArray[i].ToString(), @"[A-Za-z]"))
                                    NewRegex += @"[A-Za-z]";
                                else
                                    return "";
            }
            return NewRegex;
        }



        protected void CheckUser()
        {//when the page load check to see if boxes match
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;
            bool allMatch = true;

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data

            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;


            string iStr = "";

            for (int i = 0; i < ds1Count; i++)
            {
                iStr = i.ToString();
                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds1.Tables[0].Rows[i][0] + "'");

                if (DRow.Length > 0)
                {
                    int systemTotalBoxes = (int)DRow[0].ItemArray[6];
                    int userTotalBoxes = (int)DRow[0].ItemArray[17];

                    if (systemTotalBoxes != userTotalBoxes)
                    {
                        allMatch = false;
                        break;
                    }
                }
            }
            if (!allMatch)
            {
                ShowPopUp("Error!", "User Total Boxes doesnt match Total of New, Unopened Boxes. Please contact your Administrator for Approval!");
                tblBodyInv.Visible = false;
            }
            if (!allMatch && Session["Admin"] != null)
            {
                ShowPopUp("Error!", "User Total Boxes doesnt match Total of New, Unopened Boxes. Please contact Administrator for Approval!");
                tblBodyInv.Visible = false;
            }
            else
                CreateForm();

        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            msgLabel.Text = "";
            int JobID;
            JobID = Convert.ToInt32(ddlJob.SelectedValue);
            JobnmeTextBox.Text = JobID.ToString();
            mastertable.Visible = false;
            CreateForm();
        }

        protected void ApproveBox()
        { 
       //     -		base	{InnerText = "<tr class='info'><td target='tdName' >TEST</td><td target='tdnAME' class='form-group' ><input type='text'  class='form-control'  readonly name='tbShipDate'0' value='7/26/2017' placeholder='  MM/DD/YYYY  ' /></td><td target='tdNumb' class='form-group'><input type='text'  class='form-control' readonly name='tbBegAmt0' value='100' placeholder='BeginAmt' /></td><td target='tdNumb' class='form-group'><input type='text'  class='form-control' readonly  name='tbEndAmt0' value='38' placeholder='EndAmt' /></td><td target='tdText' class='form-group'><input type='text'  class='form-control'  readonly name='tbBegSeq0' value='62' placeholder='BegSeq#' /></td><td target='tdText' class='form-group'><input type='text' class='form-control'  readonly name='tbEndSeq0' value='100' placeholder='EndSeq#' /></td></tr>"}	System.Web.UI.HtmlControls.HtmlContainerControl {System.Web.UI.HtmlControls.HtmlGenericControl}
            buttonsave.Visible = true;
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            DataSet ds3 = new DataSet();

            bool IsFormUsed = true;
            bool IsUsed = true;

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            int JobID;

            JobID = Convert.ToInt32(JobnmeTextBox.Text);

            ds1 = dwt.GetFormConfig(IsFormUsed);
            ds2 = dwt.GetFormData(IsUsed, (int)0);  // get all form data
            ds3 = dwt.GetJob(IsUsed, JobID);



            int ds1Count = ds1.Tables[0].Rows.Count;
            int ds2Count = ds2.Tables[0].Rows.Count;
            int ds3Count = ds3.Tables[0].Rows.Count;

            // copy number of rows to hidden field for javascript
            hfForms.Value = ds3Count.ToString();

            string iStr = "";

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < ds3Count; i++)
            {
                iStr = i.ToString();
                int EndAmt = 0;
                int.TryParse(ds3.Tables[0].Rows[i][11].ToString(), out EndAmt);
                DateTime date = DateTime.Parse(ds3.Tables[0].Rows[i][18].ToString());
                // search the dataset for a row (by FormID) since we can't sort it
                DataRow[] DRow = ds2.Tables[0].Select("FormID = '" + (int)ds3.Tables[0].Rows[i][0] + "'");

                // assign 'info' styling to every other line

                sb.Append("<tr " + ((i % 2 == 0) ? "class='info'" : "") + ">");
                sb.Append("<td target='tdName' >" + ds3.Tables[0].Rows[i][26].ToString() + "</td>");
                sb.Append("<td target='tdName' class='form-group' ><input type='text'  class='form-control' readonly  name='tbShipDate'" + iStr +
                        "' value='" + date.ToShortDateString() +
                        "' placeholder='  MM/DD/YYYY  ' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text'  class='form-control'  name='tbBegAmt" + iStr +
                    "' value='" + ds3.Tables[0].Rows[i][10].ToString() +
                        "' placeholder='BeginAmt' /></td>");
                sb.Append("<td target='tdNumb' class='form-group'><input type='text'  class='form-control' name='tbEndAmt" + iStr +
                        "' value='" + ds3.Tables[0].Rows[i][11].ToString() +
                        "' placeholder='EndAmt' /></td>");
                sb.Append("<td target='tdText' class='form-group'><input type='text'  class='form-control'   name='tbBegSeq" + iStr +
                      "' value='" + ds3.Tables[0].Rows[i][12].ToString() + "' placeholder='BegSeq#' /></td>");
                sb.Append("<td target='tdText' class='form-group'><input type='text' class='form-control'   name='tbEndSeq" + iStr +
                         "' value='" + ds3.Tables[0].Rows[i][13].ToString() +
                         "' placeholder='EndSeq#' /></td>");
                sb.Append("</tr>");

            }
            if (sb.Length > 0)
            {
                mastertable.Visible = true;
                // display it on the screen
                tblBodyInv.InnerHtml = sb.ToString();

            }
            else
            {
                mastertable.Visible = false;
                msgLabel.Text = "No Data Available";
            }
        }
    }
}