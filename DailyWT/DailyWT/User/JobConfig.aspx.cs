﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.User
{
    public partial class JobConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddJob
                        string eventArgument = this.Request["__EVENTARGUMENT"]; // event arg: Job name

                        if (eventTarget == "AddJob")
                        {
                            if (Session["Admin"] != null)
                            {
                                AddJob();
                            }
                           
                            else if (eventTarget == "Cancel")
                            {
                                Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"), false);
                            }
                        }
                        else if (eventTarget == "SaveFormT")
                        {
                            ibAddJForm_Click();
                        }
                        else if (eventTarget == "ibRemoveJob_ServerClick")
                        {
                            ibRemoveJob_ServerClick();
                        }
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
        }

        protected void CreateForm()
        {
            LoadJobDDL();
            LoadJFormDDL(); 
            LoadAFormDDL();
            ReportViewerjobformtype.LocalReport.Refresh();
        }

        protected void LoadJobDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";
            ddlJob.DataBind();
            ddlJob.Items.Insert(0, " -- Select FormType -- ");


        }
        protected void LoadJFormDDL()
        {

            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;

            ddlJjob.Items.Clear();
            ddlJjob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJjob.DataTextField = "JobName";
            ddlJjob.DataValueField = "JobID";
            ddlJjob.DataBind();
            ddlJjob.Items.Insert(0, " -- Select FormType -- ");


        }
        protected void LoadAFormDDL()
        {//Add form
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlAForms.Items.Clear();       // clear in case it's already populated
            ddlAForms.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlAForms.DataTextField = "FormName";
            ddlAForms.DataValueField = "FormID";
            ddlAForms.DataBind();
            ddlAForms.Items.Insert(0, " -- Select Job -- ");

        }
        protected void ddlJob_Selected(object sender, EventArgs e)
        {

            trAForms.Visible = true;
            if (ddlJob.SelectedIndex > 0)
            {
                int JobID = 0;

                JobID = Convert.ToInt32(ddlJob.SelectedValue);
                if (JobID > 0)
                {//populate dropdown

                    LoadJFormDDL();

                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            if (ddlJob.SelectedIndex <= 0)
            {
                CreateForm();
            }
        }

        protected void AddJob()
        {
            if (Request.Form["tbAddJob"].Length > 0)
            {

                //  string JobName
                DWUtility dwt = new DWUtility();

                DataSet ds1 = new DataSet();

                int MyUserID = 0, status = 0;
                string name = "";
                string JobName = "";

                MyUserID = Convert.ToInt32(Session["MyUserID"]);

                name = Request.Form["tbAddJob"].ToString();
                JobName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

                if (JobName.Length > 2 && JobName.Length < 51)
                {
                    JobName = JobName.ToUpper();
                    // check if this job name already exists
                    ds1 = dwt.GetJobByName(JobName);
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        ShowPopUp("Error!", "You entered an existing formtype. Please try again.");
                    }
                    else
                    {

                        // new job, so JobID=0, IsJobUsed=true
                        status = dwt.UpsertJobConfig(0, true, JobName, MyUserID);
                        if (status < 1)
                        {
                            ShowPopUp("Error!", "FormType could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                        }
                        else
                        {
                            // recreate the form
                            CreateForm();

                            ShowPopUp("Success!", "This FormType " + JobName + " was successfully Added!");

                        }
                    }
                }
                else
                {
                    ShowPopUp("Error!", "You entered an invalid name for the new FormType. Please try again.");
                }
                return;
            }
            else
            {
                ShowPopUp("Error!", "Please enter a FormType, and click Add FormType button!");
            }
            return;
        }

        protected void ibAddJForm_Click()
        {
            if (ddlAForms.SelectedIndex > 0)
            {
                DWUtility dwt = new DWUtility();

                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet ds3 = new DataSet();

                int MyUserID = 0, JobID = 0, FormID = 0, status = 0;
                string FormName = ""; string JobName = "";

                MyUserID = Convert.ToInt32(Session["MyUserID"]);
                JobID = Convert.ToInt32(ddlJob.SelectedValue);
                FormID = Convert.ToInt32(ddlAForms.SelectedValue);

                //Check if Job and Form exist
                ds2 = dwt.GetJobData(FormID, JobID);

                if (ds2.Tables[0].Rows.Count <= 0)
                {

                    FormConfigInfo fci = new FormConfigInfo();
                    fci = dwt.GetFormByID(FormID);
                    JobConfigInfo jci = new JobConfigInfo();
                    jci = dwt.GetJobByID(JobID);

                    FormName = fci.FormName;
                    JobName = jci.JobName;

                    //new form  added for jobid
                    status = dwt.UpsertJobData(JobID, JobName, FormID, FormName, MyUserID);
                    if (status <= 0)
                    {
                        ShowPopUp("Error!", "Job could not be updated. Error = " + status.ToString() + " Please show the administrator this error message.");
                      

                    }
                    else
                    {
                        // recreate the form
                        CreateForm();
                        
                        
                        ShowPopUp("Success!", "The formType " + JobName + " was added to job " + FormName + " successfully.");
                    }
                }
                else
                {
                    ShowPopUp("Error!", "You entered an existing Formtype and job name. Please try again.");
                }
            }

            else
            {
                ShowPopUp("Error!", "Please select a formtype.");
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {
             

                string FormName = ""; string JobName = "";



                FormName = ddlAForms.SelectedItem.Text.ToString();
                JobName = ddlJob.SelectedItem.Text.ToString();

                    ShowPopUpOkCancel("Save Data?", "Are you sure you want add " + JobName + " to " + FormName + " ?<br/><br/>", "SaveFormT", "");
                }
             
        


        protected void ibRemoveJForm_Click(object sender, EventArgs e)
        {


        }

        protected void ibRemoveJob_ServerClick()
        {
            if (ddlJjob.SelectedIndex > 0)
            {
                int JobID = 0, status = 0;
                //declare variables
                JobID = Convert.ToInt32(ddlJjob.SelectedValue);
                string JobName = ddlJjob.SelectedItem.ToString();
                //Run stored prodcedure
                DWUtility dwt = new DWUtility();
                status = dwt.DeleteJobConfigJob(JobID);
                if (status > 0)
                {
                    // recreate the form
                    CreateForm();

                    // Notify the user that the file was deleted successfully.
                    ShowPopUpAndRD("Successful!", "FormType " + JobName + " was successfully Removed.", "/User/JobConfig.aspx");
                }
                else
                {
                    ShowPopUpAndRD("Error!", "Your FormType specification could not be deleted. Please tell the administrator about this error message.", "/User/JobConfig.aspx");
                }
            }
        }

        protected void ShowPopUpOkCancel(string header, string message, string routine, string subroutine)
        {
            // The ShowPopUpOkCancel method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called OkCancel. It takes 2 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function OkCancel(head, mess, newpage) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-danger',");
            sb.Append("label: '&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;',");
            sb.Append("action: function (dialogItself) {__doPostBack('" + routine + "', '" + subroutine + "');}},");
            sb.Append("{cssClass: 'btn-success',");
            sb.Append("label: 'Cancel',");
            sb.Append("action: function (dialogItself) {dialogItself.close();}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.            
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation. 
            string funcCall = "<script language='javascript'>OkCancel('" + header + "','<b>" + message + "</b>');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }

        protected void SaveRemove_Click(object sender, EventArgs e)
        {
            string FormName = ddlJjob.SelectedItem.ToString().Length < 1 ? "0" : ddlJjob.SelectedItem.ToString();
            DWUtility dwt = new DWUtility();

            ShowPopUpOkCancel("Delete?", "Are you sure want to delete this formtype? <br/><br/>Form Type: " + FormName + "<br/><br/>", "ibRemoveJob_ServerClick", "");

        }

    }
}