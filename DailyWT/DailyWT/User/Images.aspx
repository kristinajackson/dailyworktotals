﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Images.aspx.cs" Inherits="DailyWT.User.Images" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
         <style>
        #section1 {
        /*margin-left:-30%;
        padding-left:0px;*/
        }

    </style>
<%--    <div class="container">
     <!--Row with one column -->
    <div class="row">
        <br />
        <br />
        <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Images Module</span><span class="col-xs-1">&nbsp;</span>
        </div>
    </div>
</div>--%>
     <br />
        <br />
     <br />
        <br />
<section class="container" id="section1">
<div class="col-xs-9">
<div class="panel panel-default">

    <div class="panel-heading">
        
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Images Module</span></div>
<div class="panel-body">
    <div class="container">
<div id="pnlUpload" runat="server" visible="false">
   <%-- <br />
        <br />--%>
    <asp:FileUpload ID="FileUpload1" CssClass="bg-primary fUpload" runat="server"  /><br />
    <asp:Button ID="btnUpload" runat="server" Text="Upload Pic" CssClass="btn btn-info btn-sm" OnClick="btnUpload_Click" />
</div>
<br />
<!-- Gridview Form -->
<asp:GridView ID="gvImages" DataKeyNames="ID" OnRowCommand="gv_RowCommand" OnRowDeleting="gvImages_RowDeleting" runat="server" AutoGenerateColumns = "false" Font-Names = "Arial" CssClass="gvStyle">
<Columns>
    <asp:ButtonField Text="Set Background Image" CommandName="SetBkg" ControlStyle-CssClass="btn btn-info btn-xs gvPadding" />
    <asp:ButtonField Text="Delete" CommandName="Delete" ControlStyle-CssClass="btn btn-danger btn-xs gvPadding" />
    <asp:TemplateField>  
        <ItemTemplate>  
            <asp:Image CssClass="gvImageStyle gvPadding" ID="Image1" runat="server" ImageUrl='<%# "../ImgHandler.ashx?ImageID="+ Eval("ID") %>' />  
        </ItemTemplate>  
    </asp:TemplateField>
    <asp:TemplateField>
        <ItemTemplate>
            <div class="gvImageNameStyle gvPadding bg-info">
                <asp:Label runat="server" Text='<%# Eval("ImageName") %>' ></asp:Label>
            </div>
        </ItemTemplate>
    </asp:TemplateField> 
</Columns>
</asp:GridView>
    </div>
    </div>
    
      <div class="panel-footer">
            <br />

</div>
    </div>
    </section>

</asp:Content>
