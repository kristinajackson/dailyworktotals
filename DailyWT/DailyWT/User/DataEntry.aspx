﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntry.aspx.cs" Inherits="DailyWT.User.DataEntry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <script>
        $(document).ready(function () {
            $("#tbDEDate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            },
            function (start, end) { // when calendar closes, this function is called, so do a postback to read new data
                //__doPostBack('tbDEDate', start.format('MM/DD/YYYY'));
            });
        });

        //function check() {//To allow users who created total forms to modify for that specific batchnum
        //    var a = document.getElementById("tbCreatedUser").value;
        //    var b = document.getElementById("tbModifiedUser").value;
        //    var c = document.getElementById("tbUser").value;
        //    if (a ==  c || a=="" || a == 0)
        //    {
        //        document.getElementById("tbFormCount").readOnly = false;
        //    }
        //    else {
        //        document.getElementById("tbFormCount").readOnly = true;
        //        return;
            
        //    }


        //}
        function validNumb() {
            var x = "", text = "", y="";

            // Get the value of the input field with id="numb"
            x = document.getElementById('<%=tbFormCount.ClientID%>').value;

            // If x is Not a Number or less than one or greater than 10
            if (isNaN(x) || x < 1 || x > 100000000) {
                text = "The number entered must be a decimal number";
            } else {
                text = "";
            }
            document.getElementById("demo").innerHTML = text;

            y = document.getElementById('tbaddedforms').value;

            // If x is Not a Number or less than one or greater than 10
            if (isNaN(x) || y < 1 || y > 100000000) {
                text = "The number entered must be a decimal number";
            } else {
                text = "";
            }
            document.getElementById("P1").innerHTML = text;
        };
        
        function myFunction() {
            var person = prompt("How many news forms recieved today?", "");

            if (person != null) {
                document.getElementById("demo").innerHTML =
                person;
            }
        };

        //$(document).ready(function () {
        function closefunction() {//used for when pressing enter
            $("#btnaddnf").click(function () {
                $("#myModal").modal({ keyboard: true });
            });
        };
     
    </script>
<script>
    $(document).ready(function () {
        var Region = {
            validators: {
                regexp: {
                    regexp: /^[1-5]+$/,
                    message: 'The Region# must be a decimal number'
                }
            }
        },
        Qty = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The number entered must be a decimal number'
                }
            }
        };

        $('#mpForm')
        .bootstrapValidator('addField', 'tbRegion', Region)
        .bootstrapValidator('addField', 'tbBatchNum', Qty)
        .bootstrapValidator('addField', 'tbInterviews', Qty)
        .bootstrapValidator('addField', 'tbQuantity', Qty)
        .bootstrapValidator('addField', 'tbQtyVerified', Qty)
        .bootstrapValidator('addField', 'tbPackets', Qty)
        .bootstrapValidator('addField', 'tbCreatedUser', Qty)
        .bootstrapValidator('addField', 'tbModifiedUser', Qty)
        .bootstrapValidator('addField', 'tbaddedforms', Qty)
        .bootstrapValidator('addField', 'lblMyName', Qty);
    });
</script>


<div class="container-fluid">
   
    <!--Row with one column -->
    <div class="row">
        <br />
        <br />
        <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Data Entry Module</span><span class="col-xs-1">&nbsp;</span>

            <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static"></asp:TextBox>
            <!--hidden fields used to pass number of lines to javascript -->
            <input type="hidden" runat="server" id="hfApps" />
            <input type="hidden" runat="server" id="hfWorked" />
        </div>
    </div>
</div>


    <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Number of New forms</h4>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" id="tbaddedforms" name="tbaddedforms" value="<%= this.tbaddedforms %>" placeholder="New Forms" onchange="validNumb()"  />
     <p id="P1"></p>
           </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
           <asp:Button ID="Button1" runat="server" CssClass="btn btn-primary" OnClientClick="closefunction()" OnClick="btnaddnf_ServerClick"  Text="Add New Forms" />
      </div>
    </div>
  </div>
</div>

<!-- Section 1 -->
<section class="container-fluid" id="section2">
<div class="col-xs-7 col-xs-offset-1">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Data Entry Module

                                                                   </span></div>
<div class="panel-body">
    <div class="form-group h4 MyInfo">        
        <input type="hidden" class="form-control" id="tbUser" name="tbUser" value="<%= this.tbUser %>"  />
         <input type="hidden" class="form-control" id="tbCreatedUser" name="tbCreatedUser" value="<%= this.tbCreatedUser %>"  />
        <input type="hidden" class="form-control" id="tbModifiedUser" name="tbModifiedUser" value="<%= this.tbModifiedUser %>" />
        <label class="control-label col-xs-6">Application:</label>
        <div class="col-xs-6">
            <asp:DropDownList ID="ddlApplication" OnSelectedIndexChanged="ddlApplication_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true" CssClass="form-control"></asp:DropDownList>
        </div>
    </div>
    
    <table class="table table-condensed" id="tblForm" runat="server" visible="false">
        <tr > <td></td></tr>
        <tr class="info">
            <td><b>Total Incomplete Applications</b></td>
            <td><label id="lblincomplete" runat="server"></label></td>
        </tr>
        <tr>
            <td><b>Total Completed</b></td>
            <td><label id="lblTotalCompleted" runat="server"></label></td>
        </tr>
        <tr class="info">
            <td><b>Outstanding Balance </b></td>
            <td><label id="lblremaining" runat="server"></label></td>
        </tr>
        </table>  
    <div id="tblForm1" runat="server" visible="false">
        <div class="form-group h4 MyInfoNC">        
            <label class="control-label col-xs-6">Batch Number</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbBatchNum" value="<%= this.tbBatchNum %>" placeholder="Batch#"  />
            </div>
        </div>
        </div>
      
            
    <div id="tblForm2" runat="server" >
  <table class="table table-striped" >
    <tbody>
      <tr class="MyInfo">
        <td>            
            <div  visible="false" id="trTotalForms" runat="server">        
            <label class="control-label col-xs-6">Total Forms</label>
            <div class="col-xs-6">
            <input type="text" class="form-control"  id="tbFormCount" onchange="validNumb()"  name="tbFormCount" value="<%= this.tbFormCount %>" placeholder="Total Forms" runat="server" disabled="disabled" title="Enter number of new forms"  />
           <p id="demo"></p>
            </div>
            <div class="col-xs-2">
            <button type="button"  class="btn btn-primary btn-sm" data-toggle="modal"  data-target="#myModal" runat="server" id="btnaddnf" onchange="">Add New Forms</button>
            </div>
        </div>
        </td>
          
      </tr>
        <tr>
        <td>
             <div runat ="server" visible="false" id="trQuantity">        
            <label class="control-label col-xs-6">Quantity</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbQuantity" value="<%= this.tbQuantity %>" placeholder="Quantity"  />
            </div>
        </div>
        </td>
            </tr>
        <tr class=" MyInfo">
        <td> <div  runat ="server" visible="false" id="trInterviews" >        
            <label class="control-label col-xs-6"> Interviews</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbInterviews" value="<%= this.tbInterviews %>" placeholder="Number of Interviews"/>
            </div>
        </div>
        </td>
            </tr>
        <tr>
          <td><div  id="trPackets" runat="server" visible="false">        
            <label class="control-label col-xs-6">Packets</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbPackets" value="<%= this.tbPackets %>" placeholder="Packets" />
            </div>
        </div></td>
            </tr>
        <tr class=" MyInfo">
          <td> <div  id="trRegion" runat="server" visible="false" >        
            <label class="control-label col-xs-6">Region</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbRegion" value="<%= this.tbRegion %>" placeholder="Region" />
            </div>
        </div>

          </td>
      </tr>

    </tbody>
  </table>
          </div>
    </div>

    


<div class="panel-footer">
            <span class="MyAboveBelow h6" id="spBatch" runat="server" visible="false" >
            <label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="cbBatch" type="checkbox" runat="server" style="zoom:1.5;"  />
                <br />&nbsp;&nbsp;&nbsp;Batch
                <br />Complete
            </label>
            </span>
      <span class="MyAboveBelow h6" id="trVerifyQuantity" runat="server" visible="false" >
            <label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="tbQtyVerified" type="checkbox" runat="server" style="zoom:1.5;"  />
                <br />&nbsp;&nbsp;&nbsp;Verify
                <br />Quantity
            </label>
            </span>
    <%--<td> <div  runat ="server" visible="false" id="trVerifyQuantity" >        
            <label class="control-label col-xs-6">Verify Quantity</label>
            <div class="col-xs-6">
                <input type="text" class="form-control" name="tbQtyVerified" value="<%= this.tbQtyVerified %>" placeholder="Verify Quantity" />
            </div>
        </div>
        </td>--%>
        <asp:Button ID="btnSave" CausesValidation="false" CssClass="btn btn-success btn-lg submit-button disabled" ValidationGroup="dtat" runat="server" Text="Save Changes" OnClick="SaveChanges_Click"   />
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>

</div>
</div>
</section>
    <%--ValidationGroup="numberonly"--%>
</asp:Content>

