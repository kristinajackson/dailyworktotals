﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Text;
using System.Net.Mail;


namespace DailyWT.User
{
    public partial class PhysicalInventory : BSDialogs 
    {
         SqlConnection myConnection;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                       myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
                    alert.Visible = false;
                    danger.Visible = false;
                    delete.Visible = false;

                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }
        protected void ddlForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            TextBox FormNameTextBox = (TextBox)ListView1.InsertItem.FindControl("FormNameTextBox");
            DropDownList ddlForm = (DropDownList)ListView1.InsertItem.FindControl("ddlForm");
            TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");
            TextBox TotalFormsTextBox = (TextBox)ListView1.InsertItem.FindControl("TotalFormsTextBox");

            FormNameTextBox.Text = ddlForm.SelectedItem.ToString();

            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            string FormName = string.IsNullOrEmpty(FormNameTextBox.Text) ? "" : Regex.Replace(FormNameTextBox.Text, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            ds1 = dwt.GetFormByName(FormName);

            int FormID = (int)ds1.Tables[0].Rows[0].ItemArray[0];
            FormIDTextBox.Text = Convert.ToInt32(FormID).ToString();

            SetFocus(ddlForm);
        }

        protected void ListView1_DataBound(object sender, EventArgs e)
        {

            TextBox ActionTextBox = (TextBox)ListView1.InsertItem.FindControl("ActionTextBox");

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            TextBox CreatedByUserIDTextBox = (TextBox)ListView1.InsertItem.FindControl("CreatedByUserIDTextBox");


            DropDownList ddlForm = (DropDownList)ListView1.InsertItem.FindControl("ddlForm");

            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlForm.DataTextField = "FormName";
            ddlForm.DataValueField = "FormID";
            ddlForm.DataBind();
            ddlForm.Items.Insert(0, "- Select Job -");


            ActionTextBox.Text = "INSERT-PHYINV";

            CreatedByUserIDTextBox.Text = Convert.ToInt32(MyUserID).ToString();
        }

        protected void ListView1_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    alert.Visible = true;
                    danger.Visible = false;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true); 

                }
                else {
                    alert.Visible = false;
                    danger.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true); 
                }
            }
            else
            { 
                    //lbltop.Text = e.Exception.Message;
                    danger.Visible = true;
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                    e.ExceptionHandled = true;
                    e.KeepInInsertMode = true;
                }
        
            }

        protected void ListView1_PagePropertiesChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
        }

        protected void ListView1_ItemDeleted(object sender, ListViewDeletedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    delete.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + delete.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    delete.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  Record was not deleted!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;
               
            }
        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {

            myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            TextBox DateTextBox = (TextBox)ListView1.InsertItem.FindControl("DateTextBox");
            TextBox ActionTextBox = (TextBox)ListView1.InsertItem.FindControl("ActionTextBox");
            TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");
            TextBox FormNameTextBox = (TextBox)ListView1.InsertItem.FindControl("FormNameTextBox");
            TextBox TotalBoxesTextBox = (TextBox)ListView1.InsertItem.FindControl("TotalBoxesTextBox");
            TextBox TotalFormsTextBox = (TextBox)ListView1.InsertItem.FindControl("TotalFormsTextBox");
            TextBox CommentTextBox = (TextBox)ListView1.InsertItem.FindControl("CommentTextBox");
            TextBox CreatedByUserIDTextBox = (TextBox)ListView1.InsertItem.FindControl("CreatedByUserIDTextBox");

                DWUtility dwt = new DWUtility();
            DataSet ds2 = new DataSet();
            DataRow dr = null;
            int FormID = Convert.ToInt32(FormIDTextBox.Text);
            dr = dwt.GetFormDataByID(FormID);
                string FormName = FormNameTextBox.Text;
                
            bool IsUsed = true;
            //TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");
            //TextBox TotalFormsTextBox = (TextBox)ListView1.InsertItem.FindControl("TotalFormsTextBox");
            
            ds2 = dwt.GetFormData(IsUsed, FormID);  //  form data
            int EndAmtSystem = (int)ds2.Tables[0].Rows[0].ItemArray[4];
            int EndAmtPhysical = Convert.ToInt32(TotalFormsTextBox.Text);

            if (EndAmtPhysical != EndAmtSystem)
            {

                IsUsed = true;
                DataSet ds = new DataSet();
                ds = dwt.GetEmailPrint(IsUsed);

                int dsCount = ds.Tables[0].Rows.Count;

                for (int i = 0; i < dsCount; i++)
                {

                    string from = "Kristina Jackson <Kristina.Jackson@tn.gov>";
                    string to = ds.Tables[0].Rows[i]["Email"].ToString();


                string subject =  "Inaccurate cycle count for " + FormName + " -- Daily Work Totals";
                string body = " Cycle count entered for " + FormName + " does not match the inventory mangement system in the Daily Work Totals System. Please recount forms.";
                SendEmail(from, to, subject, body, FormName);
            }
        }
        }

        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {

        }

        public void SendEmail(string from, string to, string subject, string body, string FormName)
        {
            
                MailMessage msg = new MailMessage(from, to, subject, body);
                SmtpClient cl = new SmtpClient();
                try
                {
                    cl.Send(msg);
                }
                catch
                {
                }
            }
        }
    }