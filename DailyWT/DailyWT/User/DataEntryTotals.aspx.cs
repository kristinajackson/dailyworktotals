﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Text.RegularExpressions;
using DailyWT.Classes;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

namespace DailyWT
{
    public partial class DataEntryTotals : BSDialogs
    {
        protected string tbUncomApp { get; set; }
        protected string tbBatchNum { get; set; }
      //  protected string lbluncomplete { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    LoadDDApp();
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/Error.aspx");
            }
        }
        protected void LoadDDApp()
        {//load the page to view application dropdownlist
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");

            tblForm1.Visible = false;
        }

     protected void LoadQuanApp()
        {//this is used to display the total uncompleted applications
            DWUtility dwt = new DWUtility();

            int AppID = Convert.ToInt32(ddlApplication.SelectedValue);

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            DataSet ds3 = new DataSet();

            DataRow dr = null;
          //  ds1 = dwt.GetSumAppByAppData(AppID, MyUserID, WorkDate); //total quantity
            ds2 = dwt.GetSumAppByID(AppID);  // get sum quantity
            ds3 = dwt.GetSumAppByIncomAppData(AppID);

            int ds1Count = ds1.Tables[0].Rows.Count;

            dr = dwt.GetAppBatchData(AppID);


            if (ds2.Tables[0].Rows.Count > 0)//check if anything is already in the system
            {
                


                string iStr = "";

                for (int i = 0; i < ds1Count; i++)
                {
                    iStr = i.ToString();

                    top.Visible = true;
                    //only way i can convert from string to int

                    string stringlbluncomplete = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                    lbluncomplete.InnerHtml = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                    //lbluncomplete.InnerHtml = lbluncomplete1.ToString();
                  

                    string stringuncomplete = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                     int uncomplete = Convert.ToInt32(stringuncomplete);
                     lbluncomplete.InnerHtml = ds2.Tables[0].Rows[0].ItemArray[0].ToString();

                    if (uncomplete < 0)
                    {
                        uncomplete = 0;
                        lbluncomplete.InnerHtml = uncomplete.ToString();
                    }

                    lblTotalCompleted.InnerHtml = ds1.Tables[0].Rows[0].ItemArray[0].ToString();
                    string stringtotalcomplete = ds1.Tables[0].Rows[0].ItemArray[0].ToString();
                     int totalcomplete = Convert.ToInt32(stringtotalcomplete);

                    if (totalcomplete < 0)
                    {
                        totalcomplete = 0;
                        lblTotalCompleted.InnerHtml = totalcomplete.ToString();
                    }

                    int reaming = (uncomplete - totalcomplete);
                    lblremaining.InnerHtml = reaming.ToString();

                    if (reaming < 0)
                    {
                        reaming = 0;
                        //totalcomplete = 0;
                        //uncomplete = 0;
                        lbluncomplete.InnerHtml = uncomplete.ToString();
                        lblTotalCompleted.InnerHtml = totalcomplete.ToString();
                        lblremaining.InnerHtml = reaming.ToString();
                    }

                    else
                    {
                        top.Visible = true;
                    }

                    
                }

                return;

            }
            else
            {
                lbluncomplete.InnerHtml = "";
                lblTotalCompleted.InnerHtml = "";
                lblremaining.InnerHtml = "";
            }
            
        }


        protected void ddlApplication_SelectedIndexChanged(object sender, EventArgs e)
        {//displays the tablform1 and shows the total uncompleted applications

            if (ddlApplication.SelectedIndex > 0)  // did they select one?
            {
                int AppID = Convert.ToInt32(ddlApplication.SelectedValue);
                if (AppID > 0)
                {
                    tblForm1.Visible = true;
                    btnSave.CssClass = btnSave.CssClass.ToString().Replace("disabled", ""); // remove disabled class

                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    LoadQuanApp();

                }
                else
                {
                    // if the Save button doesn't contain a 'disabled' class, add one
                    if (!(btnSave.CssClass.ToString().Contains("disabled"))) btnSave.CssClass = btnSave.CssClass.ToString() + " disabled";

                    tblForm1.Visible = false;    // invalid form selected, so hide data

                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }

        protected void SaveChanges_Click(object sender, EventArgs e)
        {//save information to database
            DWUtility dwt = new DWUtility();

            int AppID = Convert.ToInt32(ddlApplication.SelectedValue);

            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            DataSet ds3 = new DataSet();

            DataRow dr = null;
       //     ds1 = dwt.GetSumAppByAppData(AppID); //total quantity
            ds2 = dwt.GetSumAppByID(AppID);  // get sum quantity
            ds3 = dwt.GetSumAppByIncomAppData(AppID);

            int ds1Count = ds1.Tables[0].Rows.Count;

            dr = dwt.GetAppBatchData(AppID);

            int addedforms, uncompletetotal, totalcomplete, reaming;

            if (ds2.Tables[0].Rows.Count > 0)//check if anything is already in the system
            {

                string iStr = "";

                for (int i = 0; i < ds1Count; i++)
                {
                    iStr = i.ToString();


                    //only way i can convert from string to int


                    int.TryParse(Request.Form["tbUncomApp"], out addedforms);
                    //string stringaddedforms = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                    //naforms = Convert.ToInt32(stringaddedforms);

                    string stringuncomplete = ds2.Tables[0].Rows[0].ItemArray[0].ToString();
                    uncompletetotal = Convert.ToInt32(stringuncomplete);

                    //if (uncompletetotal < 0)
                    //{
                    //    uncompletetotal = 0;
                    //}

                    string stringtotalcomplete = ds1.Tables[0].Rows[0].ItemArray[0].ToString();
                    totalcomplete = Convert.ToInt32(stringtotalcomplete);

                    //if (totalcomplete < 0)
                    //{
                    //    totalcomplete = 0;
                    //}

                    reaming = (uncompletetotal - totalcomplete);

                    if (reaming == 0)
                    {
                        reaming = 0;
                    }



                    int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                    int TQuantity;



                    TQuantity = uncompletetotal + addedforms;

                    //  int.TryParse(Request.Form["lbluncomplete"], out TQuantity);

                    DateTime WorkDate = Convert.ToDateTime(tbDEDate.Text);

                    // new form, so   Region=0, BatchNum =0, TQuantity=0, QtyVerified=0,  Quantity=0, Packets=0, BComplete = false
                    int status = dwt.UpsertAppDataTotals(AppID, 0, 0, TQuantity, 0, 0, 0, MyUserID, WorkDate, false,0,0,0,0);
                    if (status < 0)
                    {
                        ShowPopUp("Error!", "New Forms could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                    }
                    else
                    {
                        // recreate the form
                        LoadDDApp();

                        ShowPopUp("Success!", "You added new forms to be completed! ");
                    }
                }
            }
            else
            {
                LoadDDApp();
                ShowPopUp("Error!", "There is no forms to be completed for this application. Please try again.");
            }
        }

        protected void btnAddForms_ServerClick(object sender, EventArgs e)
        {
            DWUtility dwt = new DWUtility();

            int AppID = Convert.ToInt32(ddlApplication.SelectedValue);

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            int TQuantity;

            int.TryParse(Request.Form["lbluncomplete"], out TQuantity);

            DateTime WorkDate = Convert.ToDateTime(tbDEDate.Text);

            // new form, so   Region=0, BatchNum =0, TQuantity=0, QtyVerified=0,  Quantity=0, Packets=0, BComplete = false
            int status = dwt.UpsertAppData(AppID, 0, 0, TQuantity, 0, 0, 0, MyUserID, WorkDate, false,0,0,0,0);
            if (status < 0)
            {
                ShowPopUp("Error!", "Form could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
            }
            else
            {
                // recreate the form
                LoadQuanApp();

                ShowPopUp("Success!", "Form was added successfully!<br /><br />Do not forget to configure the Shipping Info and the Sequence Numbers options!");
            }
        }
    }

}

