﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntry.aspx.cs" Inherits="DailyWT.User.DataEntry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <script>
        $(document).ready(function () {
            $("#tbDEDate").daterangepicker({
                singleDatePicker: true,
                showDropdowns: true
            },
            function (start, end) { // when calendar closes, this function is called, so do a postback to read new data
                __doPostBack('tbDEDate', start.format('MM/DD/YYYY'));
            });
        });
    </script>
<script>
//    function CloneBatchRow(trTag) {
//        $(trTag).clone().appendTo(trTag);
    function CloneBatchRow(lnum) {
        //        $('#tr0').clone().attr('id', 'tra').appendTo('#tr0');
//        var $cloneTr = $("#tr" + lnum).clone(true, true);
        var $cloneTr = $("#tr" + lnum).clone(true);
        $cloneTr.find("tr").removeAttr("id");
//        $cloneTr.html().replace("0\" val", "00\" val");
//        $cloneTr.replace("0' val", "9' val");
        $cloneTr.find(":text").val('');
        $cloneTr.insertAfter($("#tr" + lnum));
    }
    //function CloneBatchRow() {
    //    var $tr = $(this).closest('tr');
    //    var $clone = $tr.clone();
    //    $clone.find(':text').val('');
    //    $tr.after($clone);
    //};
</script>
<script>
    $(document).ready(function () {
        var Region = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The Region# must be a decimal number'
                }
            }
        },
        BatchNum = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The Batch# must be a decimal number'
                }
            }
        },
        Quantity = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The Quantity must be a decimal number'
                }
            }
        },
        QtyVerified = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'The Verify Qty must be a decimal number'
                }
            }
        },
        Packets = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Packets must be a decimal number'
                }
            }
        },
        Hours = {
            validators: {
                regexp: {
                    regexp: /^\d*\.?\d*$/,
                    message: 'Hours must be a decimal number, ex: 3.4 or 3'
                }
            }
        },

        fIndex = 0;

        $('#mpForm')
            .bootstrapValidator({
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                //},
                //fields: {
                //    'tbRegion': Region,
                //    'tbBatchNum': BatchNum,
                //    'tbQuantity': Quantity,
                //    'tbQtyVerified': QtyVerified,
                //    'tbPackets': Packets,
                //    'tbHours': Hours
                }
            })  // end data form

        // get number of lines so the correct number of validators are created
        var appCount = document.getElementById("<%=hfApps.ClientID%>").value;
        var workCount = document.getElementById("<%=hfWorked.ClientID%>").value;

        while (fIndex < appCount)
        {
            $('#mpForm')
            .bootstrapValidator('addField', 'tbRegion' + fIndex, Region)
            .bootstrapValidator('addField', 'tbBatchNum' + fIndex, BatchNum)
            .bootstrapValidator('addField', 'tbQuantity' + fIndex, Quantity)
            .bootstrapValidator('addField', 'tbQtyVerified' + fIndex, QtyVerified)
            .bootstrapValidator('addField', 'tbPackets' + fIndex++, Packets);
        }

        fIndex = 0; // reset index
        while (fIndex < workCount)
        {
            $('#mpForm')
            .bootstrapValidator('addField', 'tbHours' + fIndex++, Hours);
        }
    });
</script>


    <div class="container-fluid">

        <!--Row with one column -->
        <div class="row">
            <div class="h3 text-center"><span class="col-xs-3 col-xs-offset-2">Data Entry Module</span><span class="col-xs-1">&nbsp;</span>
<%--                <asp:Calendar CssClass="col-xs-2"  ID="tbDEDate" OnSelectionChanged="tbDEDate_SelectionChanged" runat="server"></asp:Calendar>--%>
<%--                <input class="col-xs-2" type="text" name="tbDEDate" value="<%=Request.Form["tbDEDate"] %>" required />--%>
                <asp:TextBox ID="tbDEDate" CssClass="col-xs-2 text-info" runat="server"  ClientIDMode="Static"></asp:TextBox>
                <!--hidden fields used to pass number of lines to javascript -->
                <input type="hidden" runat="server" id="hfApps" />
                <input type="hidden" runat="server" id="hfWorked" />
            </div>
        </div>
    </div>

<!-- Section 1 -->
<section class="container-fluid toggle" id="section1">
<div class="col-xs-11">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Data Entry</span></div>
<div class="container">
  <table class="table table-condensed">
    <thead>
      <tr class="h4">
        <th>Application</th>
        <th>Region</th>
        <th>Quantity</th>
        <th>Verify Qty</th>
        <th>Packets</th>
        <th>Batch#</th>
          <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody id="tblBodyDE" runat="server"></tbody>
  </table>

</div>

</div>
</div>

<div class="col-xs-11">
<div class="panel panel-default">
    <div class="panel-heading">
            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Timed Work</span>
    </div>
<div class="container">
  <table class="table table-condensed">
    <thead>
      <tr class="h4">
        <th class='thWorkHours'>Work Activity</th>
        <th class='tdWorkHours'>Total Hours Worked</th>
        <th class='tdWorkHours'>&nbsp;</th>
      </tr>
    </thead>
    <tbody id="tblBodyTW" runat="server">
    </tbody>
  </table>

</div>
<div class="panel-footer">
<%--    <button type="submit" name="btnSave" onclick="Save_Click" class="btn btn-success btn-lg" >--%>
    <button type="submit" name="btnSave" class="btn btn-success btn-lg">
        &nbsp;&nbsp;Save&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="reset" id="resetregform" class="btn btn-default btn-lg">
        Clear Forms</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-lg" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
</div>
</div>
</div>

</section>
<!-- End Section 1 -->



</asp:Content>
