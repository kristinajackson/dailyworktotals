﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ReportsMain.aspx.cs" Inherits="DailyWT.ReportsMain" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
     <%--<br />
     <br />
     <br />
     <br />--%>
    <!-- Section 1 -->
    <section class="container" id="section1" style="color:#000000;" >
           <div class="col-xs-9">
<div class="panel panel-default" >
    <div class="panel-heading">
                    <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Reports Module

                    </span>
                </div>
                <div class="panel-body"  >   
    <div class="container">
        <div class="col-xs-4 ">
        <h3><asp:Label runat="server" Font-Underline="true">Printing Report</asp:Label></h3>
       <h5><span><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Reports/DailyPrintReport.aspx" ForeColor="#0000CC">Printing Report</asp:HyperLink></span></h5>
             <h5><span><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/Reports/PrintTotals.aspx" ForeColor="#0000CC">Printing Totals Report</asp:HyperLink></span></h5>
            <h5><span><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Reports/PrintAverage.aspx" ForeColor="#0000CC">Average Printing Report</asp:HyperLink></span></h5>  
        <h5><span><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Reports/PrintInventory.aspx" ForeColor="#0000CC">Printing Inventory</asp:HyperLink></span></h5>       
                <h5><span><asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/Reports/ReprintReport.aspx" ForeColor="#0000CC">Reprint Report</asp:HyperLink></span></h5>       
              <h5><span><asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/Reports/ReprintTotals.aspx" ForeColor="#0000CC">Reprint Totals Report</asp:HyperLink></span></h5>       
        </div>
        <div class="col-xs-4">
        <h3><asp:Label runat="server" Font-Underline="true">DataEntry Report</asp:Label></h3>
            <h5> <span> <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~\Reports\HistorialDataEntry.aspx" ForeColor="#0000CC">Daily Totals Report</asp:HyperLink></span></h5>
            <h5> <span> <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/Reports/Dataentrytotals.aspx" ForeColor="#0000CC">DataEntry Totals Report</asp:HyperLink></span></h5> 
            <h5> <span> <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/Reports/DataEntryIncomplete.aspx" ForeColor="#0000CC">Incomplete Applications Report</asp:HyperLink></span></h5>
        </div>      
        </div>   
                    </div>
    <br />
    <div class="panel-footer">
              <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;

</div>

                </div>
            </div>
        </section>
</asp:Content>
