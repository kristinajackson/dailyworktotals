﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntryIncomplete.aspx.cs" Inherits="DailyWT.Reports.DataEntryIncomplete" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <!-- Section 1 -->
      <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <section class="container" id="section1" style="color: #000000; margin-left: -5%; ">
                <div class="col-xs-10 col-xs-offset-1">
                    <div class="panel panel-default" style="height: 5000px;">
                        <br />
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Incomplete Applications Report Module

                            </span>
                        </div>
                        <div class="panel-body">
                            <div style="background-color: #f5f5f0;" runat="server" id="bodby">
                                <!---------  BUTTONS ---------->
                                <br />
                                <div class="container" style="margin-left: 2%">
                    <button id="Backbutton" runat="server" class="btn-primary" onserverclick="Backbutton_Click"><span class="glyphicon glyphicon-chevron-left"></span>Back to Reports Menu</button>
   </div>
                                <br />
                            </div>
                                  
                    
                 &nbsp;&nbsp; &nbsp;&nbsp;    <div class="row" style="margin-left: 2%">
                                <div style="margin-bottom: 10px; margin-top: 10px;" class="col-xs-8  form-group">
                                    <br />
                                  <asp:Label ID="lblPrintInstr" runat="server" Text="*Please Note: To Print the report, click on the Export drop down menu (the blue disk icon, third button from right ) --> Select Excel --> Open the file or save it to your computer --> From MS Excel go to File --> Print --> Select Landscape Orientation --> And change the No Scaling option (the last option) to Fit All Columns on One Page  --> Click on the Print button." ForeColor="Black" BackColor="#FFFF66" Font-Bold="True" Font-Names="Times New Roman" />
                                </div>
                            </div>
                   </div>
                    <!---------  REPORT ---------->
                         <div class="container" style="margin-left:1%">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="100%" SizeToReportContent="True">
            <LocalReport ReportEmbeddedResource="DailyWT.Reports.IncompleteApplications.rdlc" ReportPath="Reports/IncompleteApplications.rdlc">
                <DataSources>
                    <rsweb:ReportDataSource DataSourceId="ObjectDataSource2" Name="DataSet1" />
                </DataSources>
            </LocalReport>
        </rsweb:ReportViewer>
                             <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetData" TypeName="DataEntryIncompleteTableAdapters.GetDataEntryIncompleteTableAdapter"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DailyWT.App_Code.DataEntryIncompleteTableAdapters.GetDataEntryIncompleteTableAdapter"></asp:ObjectDataSource>


             </div>

                    </div>
                </div>
            
            </section>

            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
