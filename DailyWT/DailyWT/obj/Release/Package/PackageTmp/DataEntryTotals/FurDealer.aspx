﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FurDealer.aspx.cs" Inherits="DailyWT.DataEntryTotals.FurDealer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ScriptManager>



    <section class="container" id="section1">
        <div class="col-sm-12">

            <div class=" panel panel-default">
                <br />
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;FurDealer Module</span>
                </div>
                <div>
                    <div class="container">
                        <div class="panel-body">
                            <div class="alert alert-danger col-sm-10" runat="server" id="danger" visible="false" role="alert">
                                <strong>ERROR!</strong> Record was not saved. Please check your work
                            </div>

                            <div class="alert alert-success col-sm-10" runat="server" id="alert" visible="false" role="alert">
                                <strong>Success!</strong> Record entered successfully.
                            </div>
                            <div class="alert alert-warning col-sm-10" runat="server" id="delete" visible="false" role="alert">
                                <strong>Success!</strong> Record deleted successfully.
                            </div>
                            <!-- Listview -->
                            <asp:ListView ID="ListView5" runat="server" DataSourceID="SqlDataSource5" InsertItemPosition="LastItem" DataKeyNames="ID" OnDataBound="ListView5_DataBound" OnItemInserted="ListView5_ItemInserted" OnPagePropertiesChanged="ListView5_PagePropertiesChanged"  OnItemDeleted="ListView5_ItemDeleted">
                                <LayoutTemplate>
                                    <table id="Table2" runat="server">
                                        <tr id="Tr1" runat="server">
                                            <td id="Td1" runat="server">
                                                <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px; font-family: Verdana, Arial, Helvetica, sans-serif; margin-left: 0px; padding-left: 0px;" class="table table-bordered">
                                                    <tr id="Tr2" runat="server">
                                                        <th id="Th1" runat="server" style="width: 200px; text-align: center;">Action</th>
                                                        <th id="Th2" runat="server" style="width: 200px; text-align: center;">Date</th>
                                                        <th id="Th3" runat="server" style="width: 200px; text-align: center;">Source</th>
                                                        <th id="Th4" runat="server" style="width: 200px; text-align: center;">Sequence</th>
                                                        <th id="Th5" runat="server" style="width: 200px; text-align: center;">Count</th>
                                                    </tr>
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered" style="width: 1000px;">
                                        <tr id="Tr3" runat="server" style="background-color: #DCDCDC; color: #000000;">
                                            <td id="Td2" runat="server" style="text-align: center; font-size: 12px; background-color: #DCDCDC; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; width: 1000px;" class="form-group">
                                                <asp:DataPager ID="DataPager1" runat="server" PageSize="4">
                                                    <Fields>
                                                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" ButtonCssClass="btn" />
                                                    </Fields>
                                                </asp:DataPager>
                                            </td>
                                        </tr>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td class="form-group" style="width: 200px; text-align: center;">
                                                <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" CssClass="btn btn-danger btn-md btn-rounded" OnClientClick="return confirm('Are You Certain You Want to Delete?');" Width="150px" />
                                            </td>
                                            <td class="form-group" style="width: 200px; text-align: center;">
                                                <asp:Label ID="DateLabel" runat="server" Text='<%# Eval("Date") %>' Width="200px" />
                                            </td>
                                            <td class="form-group" style="width: 200px; text-align: center;">
                                                <asp:Label ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' Width="200px" />
                                            </td>
                                            <td class="form-group" style="width: 200px; text-align: center;">
                                                <asp:Label ID="SequenceLabel" runat="server" Text='<%# Eval("Sequence") %>' Width="200px" />
                                            </td>
                                            <td class="form-group" style="width: 200px; text-align: center;">
                                                <asp:Label ID="CountLabel" runat="server" Text='<%# Eval("Count") %>' Width="200px" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <EmptyDataTemplate>
                                    <table id="Table1" runat="server" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px;">
                                        <tr>
                                            <td>No data was returned.</td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <InsertItemTemplate>
                                    <asp:Panel ID="pnlInsert" runat="server" DefaultButton="InsertButton">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                    <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text=" Enter " CssClass="btn btn-success btn-md" Width="80px" ToolTip="Save Record" />
<%--                                                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text=" Clear " CssClass="btn btn-default btn-sm" Width="60px" ToolTip="Clear Textboxes" />--%>
                                                    <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-md btn-warning" Text=" Cancel " OnClick="btnCancel_Click" ToolTip="Exit out this module" Width="80px" />

                                                </td>
                                                <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                  <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" TargetControlID="DateTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="99/99/9999" MaskType="Date" />
                                                      <asp:TextBox ID="DateTextBox" runat="server" Text='<%# Bind("Date") %>' CssClass="form-control" />
                                                    <asp:CalendarExtender ID="RTCE" runat="server" TargetControlID="DateTextBox" Format="M/dd/yyyy" PopupButtonID="ImageButton1" />
                                                </td>
                                                <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Text='<%# Bind("Name") %>' CssClass="form-control" OnTextChanged="DropDownList1_TextChanged">
                                                        <asp:ListItem>FUR DEALER</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                    <asp:TextBox ID="SequenceTextBox" runat="server" Text='<%# Bind("Sequence") %>' CssClass="form-control" />
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="SequenceTextBox" AutoComplete="False" ClearMaskOnLostFocus="false" Mask="99999-99999" MaskType="Number" />
                                                </td>
                                                <td class="form-group" style="width: 200px; border-top: solid #C8C8C8; border-top-width: thick; text-align: center;">
                                                    <asp:TextBox ID="CountTextBox" runat="server" Text='<%# Bind("Count") %>' CssClass="form-control" />
                                                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="CountTextBox" AutoComplete="False" ClearMaskOnLostFocus="False" Mask="999-999" MaskType="Number" />
                                                    <asp:TextBox ID="Labeluserid" runat="server" Text='<%# Bind("CreatedByUserID") %>' Visible="false"></asp:TextBox>
                                                    <asp:TextBox ID="lblappid" runat="server" Visible="false" Text='<%# Bind("AppID") %>'></asp:TextBox>
                                                    <asp:TextBox ID="lblappname" runat="server" Visible="false" Text='<%# Bind("AppName") %>'></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td style="text-align: center;">
                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.png" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </InsertItemTemplate>


                            </asp:ListView>
                            <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" DeleteCommand="DELETE FROM DWT.FurDLR WHERE (ID = @ID)" InsertCommand="INSERT INTO DWT.FurDLR(Date, Name, Sequence, Count, CreatedByUserID) VALUES (@Date, @Name, @Sequence, @Count, @CreatedByUserID)" SelectCommand="SELECT ID, Date, Name, Sequence, Count FROM DWT.FurDLR ORDER BY CreatedDate desc">
                                <DeleteParameters>
                                    <asp:Parameter Name="ID" />
                                </DeleteParameters>
                                <InsertParameters>
                                    <asp:Parameter Name="Date" />
                                    <asp:Parameter Name="Name" />
                                    <asp:Parameter Name="Sequence" />
                                    <asp:Parameter Name="Count" />
                                    <asp:Parameter Name="CreatedByUserID" />
                                </InsertParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</asp:Content>
