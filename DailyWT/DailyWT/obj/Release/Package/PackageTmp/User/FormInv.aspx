﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="FormInv.aspx.cs" Inherits="DailyWT.User.FormInv" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
  <style>
    /*This is a wide form, so set column 1 to 0%, and the rest of the columns to 100%*/
    /*.col-xs-1 {
        width:0% !important;    
    }
    .col-xs-11 {
        width: 150% !important;
    }*/
        #section1 {
        /*margin-left:5%;
        padding-left:0px;*/
        }

    </style>
<script>
    // extend the Number function
    Number.prototype.pad = function (size) {
        var s = String(this);
        while (s.length < (size || 2)) { s = "0" + s; }
        return s;
    };
</script>
<script>
    $('#mpForm').on('dblclick', 'td', function (e) {
        var str = this.firstChild.name; // get the name of the input tag that was double-clicked
        var num = str.replace(/^\D+/g, ''); // get the numeric portion of the tag name

        var pos1 = str.indexOf("Partial");   // was this tbPartialBoxAmtxx?
        if (pos1 > 0) {
            var fpb = document.querySelector('[name=tbFormsPerBox' + num + ']');
            var tb = document.querySelector('[name=tbTotalBoxes' + num + ']');

            // is either one blank?
            if (fpb.value.length < 1 || tb.value.length < 1)
                return;

            var pba = document.querySelector('[name=tbPartialBoxAmt' + num + ']');
            var ba = document.querySelector('[name=tbBegAmt' + num + ']');
            var ea = document.querySelector('[name=tbEndAmt' + num + ']');

            // calculate and populate the boxes
            var total = fpb.value * tb.value;
            pba.value = 0;
            ba.value = total;
            ea.value = total;
            return;
        }

        var pos2 = str.indexOf("EndAmt");   // was this tbEndAmtxx?
        if (pos2 > 0) {
            var to = document.querySelector('[name=tbBegAmt' + num + ']');
            var from = document.querySelector('[name=' + str + ']');
            to.value = from.value;
            from.value = "";
            return;
        };

        var pos3 = str.indexOf("EndSeq");   // was this tbEndSeqxx?
        if (pos3 > 0) {
            var to = document.querySelector('[name=tbBegSeq' + num + ']');
            var from = document.querySelector('[name=' + str + ']');
            if (from.value.length > 0) {
                var newval = (Number(from.value) + 1).pad(to.value.length);
                to.value = newval;
                from.value = "";
            };
        };
        return;
    });
    // function to copy the value from one input tag to another, then clears the input tag that was double clicked
    function CopyAndClear(totag, fromtag) {
        var to = document.querySelector('[name=' + totag + ']');
        var from = document.querySelector('[name=' + fromtag + ']');
        //to.value = from.value;
        //from.value = "";
    };
</script>
<script>
    $(document).ready(function () {
        var ShipNum = {
            validators: {
                regexp: {
                    regexp: /^\d{2}\/\d{2}\/\d{4}$/,
                    message: 'Recieving date must be MM/DD/YYYY'
                }
            }
        }, 
        ShipDate = {
            validators: {
                regexp: {
                    regexp: /^\d{2}\/\d{2}\/\d{4}$/,
                    message: 'Shipping date must be MM/DD/YYYY'
                }
            }
        },
        FormsPerBox = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Forms Per Box must be a decimal number'
                }
            }
        },
                 UserBegAmt = {
                     validators: {
                         regexp: {
                             regexp: /^[0-9]+$/,
                             message: 'Total Boxes must be a decimal number'
                         }
                     }
                 },
         UserEndAmt = {
             validators: {
                 regexp: {
                     regexp: /^[A-Za-z0-9\ \-\.]+$/,
                     message: 'Form Location must be alphabetic chars'
                 }
             }
         },
          BegSeq = {
              validators: {
                  regexp: {
                      regexp: /^[0-9]+$/,
                      message: 'Beginning Seq# must be a decimal number'
                  }
              }
          },
        EndSeq = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Ending Seq# must be a decimal number'
                }
            }
        },
        FormLocation = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\ \-\.]+$/,
                    message: 'Form Location must be alphabetic chars'
                }
            }
        },
        TotalBoxes = {
            validators: {
                regexp: {
                    regexp: /^\d*\.?\d*$/,
                    message: 'Total Boxes must be a decimal number'
                }
            }
        },
        PartialBoxAmt = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Partial Box Amt must be a decimal number'
                }
            }
        },
        BegAmt = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Beginning Amt must be a decimal number'
                }
            }
        },
        EndAmt = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Ending Amt must be a decimal number'
                }
            }
        },
        Comment = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]+$/,
                    message: 'The Comments must be alphabetic chars'
                }
            }
        },

        fIndex = 0;


        // get number of lines so the correct number of validators are created
        var formCount = document.getElementById("<%=hfForms.ClientID%>").value;

        while (fIndex < formCount) {
            $('#mpForm')
            .bootstrapValidator('addField', 'tbShipNum' + fIndex, ShipNum)
            .bootstrapValidator('addField', 'tbShipDate' + fIndex, ShipDate)
            .bootstrapValidator('addField', 'tbFormsPerBox' + fIndex, FormsPerBox)
            .bootstrapValidator('addField', 'tbUserBegAmt' + fIndex, UserBegAmt)
            .bootstrapValidator('addField', 'tbUserEndAmt' + fIndex, UserEndAmt)
            .bootstrapValidator('addField', 'tbBegSeq' + fIndex, BegSeq)
            .bootstrapValidator('addField', 'tbEndSeq' + fIndex, EndSeq)
            .bootstrapValidator('addField', 'tbFormLocation' + fIndex, FormLocation)
            .bootstrapValidator('addField', 'tbTotalBoxes' + fIndex, TotalBoxes)
            .bootstrapValidator('addField', 'tbPartialBoxAmt' + fIndex, PartialBoxAmt)
            .bootstrapValidator('addField', 'tbBegAmt' + fIndex, BegAmt)
            .bootstrapValidator('addField', 'tbEndAmt' + fIndex, EndAmt)
            .bootstrapValidator('addField', 'tbComment' + fIndex++, Comment);
        }
    });

</script>
                <input type="hidden" runat="server" id="hfForms" />
  <%--  <br />
    <br />
    <br />
    <br />--%>
<!-- Section 1 -->
<section class="container" id="section1"  >
    <div class="col-xs-16">
<div class="panel panel-default" >
    <br />
    <div class="panel-heading" >
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Inventory Management Module</span></div>

<div class="container">
     <div class="panel-body">
    <div class="h4 form-group">         
        <label class="v-align-tc">&nbsp;&nbsp;FormType :&nbsp;&nbsp<asp:DropDownList ID="ddlJob" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <asp:TextBox ID="JobnmeTextBox" runat="server" Visible="false"></asp:TextBox>
    </div>
         <div id="mastertable" runat="server" visible="false" style="margin-left:-2%">
  <table class="table table-condensed" >

    <thead>
      <tr >
        <th>Inventory</th>
        <th>Last Print Date</th>
        <th>Initial Inventory</th>
        <th>Total Remaining Inventory</th>
          <th>Beg Seq#</th>
        <th>End Seq#</th>
        
          </tr>
    </thead>
    <tbody id="tblBodyInv" runat="server">
    </tbody>
  </table>
             </div>
         <asp:Label ID="msgLabel" runat="server" Font-Size="X-Large" Font-Bold="true"></asp:Label>


<div class="col-xs-12" style="margin-left:-2%">
<div class="panel-footer">

    <button id="buttonsave" type="submit" name="btnSave" class="btn btn-success btn-md" onclick="__doPostBack('Save', '');" runat="server" visible="false" >
        &nbsp;&nbsp;Save Approve Changes&nbsp;&nbsp;</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>
    <%--<button type="button" id="ButtonApprove" class="btn btn-default btn-md" onclick="__doPostBack('Approve', '');" runat="server" visible="false" >
        &nbsp;&nbsp;Approve&nbsp;&nbsp;</button>--%>
    </div>
</div>
</div>
    </div>
</div>
        </div>
</section>
  <
</asp:Content>