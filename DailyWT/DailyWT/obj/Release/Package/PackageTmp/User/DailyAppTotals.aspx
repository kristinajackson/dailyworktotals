﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DailyAppTotals.aspx.cs" Inherits="DailyWT.User.DailyAppTotals" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
        <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ScriptManager>
    <script type="text/javascript">
        $(document).keypress(function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    </script>
    <style>
        .txtboxde {
        text-align: right;
        color: #000000;
        }

        .line {
        border-top:solid;
        border-left:none;
        border-right: none;
        border-bottom:double;
        background-color:#f7f8f9;
        }
        .outstanding {
       border-top:solid;
        border-left:none;
        border-right: none;
        border-bottom:none;
        background-color:#f7f8f9;
        }
        .labeldwt {
        /*width:300px;*/
        text-align: right;
        border-left:none;
        border-right: none;
        border-bottom:none;
        border-top:none;
        }
    </style>

    <section class="container " id="section1" >
        <div class="col-lg-11" style="margin-left:-5%" >
        
        <div class=" panel panel-default">
            <br />
            <div class="panel-heading" >
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Daily Data Entry Module</span>
            </div>
            <div >
                <div class="container" style="margin-left:5%">
                <div class="panel-body">
                
 
     <label class="control-label col-xs-3">Application:</label>
        <div class="col-xs-4">
            <asp:DropDownList ID="ddlApplication" runat="server" OnSelectedIndexChanged="ddlApplication_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control"></asp:DropDownList>
        </div>
 <br />
   <div class=" col-sm-9" >
        <asp:Label ID="lblmsg" runat="server"  ForeColor="#FF3300" Font-Size="Large" Font-Bold="True" ></asp:Label>
   </div>
                    <br />
                    <br />
                    <br />
                    <div class="col-lg-10" >
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Italic="True" Font-Size="Medium" ></asp:Label>
      <asp:FormView ID="FormView1" runat="server" DataKeyNames="ID" DataSourceID="SqlDataSource1" DefaultMode="Insert">
            <InsertItemTemplate>
              
                <asp:TextBox ID="AppIDTextBox" runat="server" Text='<%# Bind("AppID") %>' Visible ="false" />
            
            
                <asp:TextBox ID="AppNameTextBox" runat="server" Text='<%# Bind("AppName") %>' Visible ="false" />
                        <table id="datetable" class="table table-striped " runat="server" visible="false"  >
            <tbody >
          <tr  >
            <td class="form-group col-sm-12">
                <div  >
               <div class="col-sm-6" style="font-weight:bold;"> Date:</div>
            <div class=" col-sm-2" >  <asp:TextBox ID="txtDateRecieved" runat="server" Text='<%# Bind("DateRecieved") %>'  CssClass="txtboxde form-control" Width="300px"   TabIndex="1" />
                 <asp:MaskedEditExtender ID="txtDateRecieved_MaskedEditExtender" runat="server"  Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="txtDateRecieved"></asp:MaskedEditExtender>
            </div> </div>
    &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
              </tr>
         </table> 
              
                <div id="RegiontblForm" runat="server" visible="false">
           <table id="tblfrm4" class="table table-striped" runat="server" >
               <tbody> 
           <tr style="background-color:#cccc00;">
            <td   class="form-group" id="trRegion">
                <div >
                <div class="col-sm-6" style="font-weight:bold;"> Region:</div>
                <div class=" col-sm-2" > <asp:DropDownList ID="txtRegion" runat="server" Width="300px" CssClass="btn dropdown-toggle txtboxde" style="color:#000000; border:1px; border-color:#f7f8f9;" TabIndex="2" SelectedValue='<%# Bind("Region") %>'>
                                                            <asp:ListItem Value="1">Region 1</asp:ListItem>
                                                            <asp:ListItem Value="2">Region 2</asp:ListItem>
                                                            <asp:ListItem Value="3">Region 3</asp:ListItem>
                                                            <asp:ListItem Value="4">Region 4</asp:ListItem>
                                                        </asp:DropDownList> </div> </div> </td>
            </tr> 
      </tbody>
  </table>
        </div>
                
                <!------- Batch Number ------>
                     <div id="BatchnumbertblForm" runat="server" visible="false"  >
  <table id="tblfrm3" class="table table-striped" runat="server" >
      <tbody>
          <tr style="background-color:#cccc00;">
            <td  class="form-group"  id="trBatchNum">
                <div >
               <div class="col-sm-6" style="font-weight:bold;"> BatchNum:</div>
                <div class=" col-sm-2" >
                    <asp:TextBox ID="BatchNumTextBox" Visible="true" runat="server"  CssClass="txtboxde form-control" Text='<%# Bind("BatchNum") %>'  ToolTip="What is the batch number?" Width="300px" TabIndex="1" />
                <asp:DropDownList ID="batch" Visible="false" runat="server" OnSelectedIndexChanged="batch_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true"   CssClass="btn dropdown-toggle txtboxde"></asp:DropDownList></div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="BatchNumTextBox" SetFocusOnError="True"></asp:RegularExpressionValidator> 
            </div> </td>
            </tr> 
          </tbody>
          </table></div>

                             <!------- Packets ------>
      <div id="PacketstblForm" runat="server" visible="false"  >
  <table id="Table2" class="table table-striped" runat="server" >
      <tbody> 
    <tr >
            <td   id="trPackets" class="form-group" style="background-color:#cccc00;">
                <div >
               <div class="col-sm-6" style="font-weight:bold;">Number of Packets Recieved:</div>
                <div class=" col-sm-2" ><asp:TextBox ID="PacketsTextBox" runat="server" Text='<%# Bind("Packets") %>'  CssClass="txtboxde form-control" ToolTip="How many Packets have you recieved?" Width="300px" TabIndex="3"  /></div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="PacketsTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                
             </div>  </td>
            </tr> 
          </tbody>
       </table>
          </div>

                 <!------ Angular ------>
          <div id="AngulartblForm" runat="server" visible="false"  >
  <table id="Table3" class="table table-striped" runat="server" >
      <tbody>
        <tr style="background-color:#cccc00;">
            <td id="trAngular" class="form-group" >
                <div >
               <div class="col-sm-6" style="font-weight:bold;">Number of Angular Recieved:</div>
                <div class=" col-sm-2" ><asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>' CssClass="txtboxde form-control"  ToolTip="How many Angular did you recieved?"  AutoPostBack="true" Width="300px" TabIndex="4" /></div>
                  <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="QuantityTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
              </div>  </td>
            </tr> 
          </tbody>
      </table>
              </div>

                  <!------- Interviews ------->
                <div id="InterviewtblForm" runat="server" visible="false"  >
  <table id="Table4" class="table table-striped" runat="server" >
      <tbody>
        <tr style="background-color:#cccc00;">
            <td  id="trInterviews"  class="form-group" >
                <div >
                <div class="col-sm-6" style="font-weight:bold;">Number of Interviews Recieved:</div>
               <div class=" col-sm-2" > <asp:TextBox ID="InterviewsTextBox" runat="server" Text='<%# Bind("Interviews") %>' CssClass="txtboxde form-control"  ToolTip="How many Interviews did you recieved?"  AutoPostBack="true" Width="300px" TabIndex="5"/></div>
                              <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="InterviewsTextBox" SetFocusOnError="True"></asp:RegularExpressionValidator>
              </div>  </td>
            </tr> 
          </tbody>
      </table>
                    </div>                

 

                  <!------ Table Form ------>
    <div id="tblForm1" runat="server" visible="false" >
        <table id="Table1" class="table table-striped " runat="server"  >
            <tbody >
          <tr  style="background-color:#cccc00;">
            <td class="form-group col-sm-12">
                <div  >
               <div class="col-sm-6" style="font-weight:bold;"> Total Forms Recieved:</div>
            <div class=" col-sm-2" >  <asp:TextBox ID="NAddFormsTextBox" runat="server" Text='<%# Bind("NAddForms") %>'  CssClass="txtboxde form-control" Width="300px"   TabIndex="6" /></div> </div>
    &nbsp;&nbsp;&nbsp;&nbsp;
            </td>
              </tr>
         </table> 
        <table id="Table11" class="table table-striped " runat="server">     
            <tbody>
   <tr>
            <td >
                <div class="col-xs-7">
                <label id="Label4" for="outtxt"></label></div>
               <label>=</label>
                
                  </td>
            </tr>
                  </tbody>
         </table>  




        <table id="Table5" class="table table-striped " runat="server"  visible="false">
            <tbody>
               <tr style="color:#A0A0A0;">
            <td class="form-group col-sm-10" >
                 <div>                  
                <div class="col-sm-6" style="font-weight:bold;">Total Incomplete:</div>
             <div class=" col-sm-2" > <asp:TextBox ID="TIncompleteTextBox" runat="server" Text='<%# Bind("TIncomplete") %>' CssClass="labeldwt form-control"  style="background-color:#f7f8f9;" Width="300px" Enabled="False" ReadOnly="true" />
             </div> </div>  </td>
        </tr> 
                </tbody>
            </table>

        <table id="Table6" class="table table-striped " runat="server" visible="false">
            <tbody>
                <tr style="background-color: #00cc66;">
            <td class="form-group" >
                <div >
                <div class="col-sm-6" style="font-weight:bold;">Total to be Keyed:</div>
                <div class=" col-sm-2" > <asp:TextBox ID="TQuantityTextBox" runat="server" Text='<%# Bind("TQuantity") %>'  style="background-color:#f7f8f9;" CssClass="labeldwt form-control"   Width="300px" Enabled="False" ReadOnly="true" TabIndex="7" /></div> 
                </div> </td>
                   
                </tr> 
                  
                </tbody>
            </table>
                           <!------ Angular Keyed------>
        <table id="Table7" class="table table-striped " runat="server" >
            <tbody>
        <tr  runat="server" id="tblangukey" style="background-color:#cccc00;" >
            <td   class="form-group">
                <div >
                
                <div class="col-sm-6" style="font-weight:bold;">Number of Angular Keyed:</div>
               <div class=" col-sm-2" > <asp:TextBox ID="AngularCompletedTextBox" runat="server" Text='<%# Bind("AngularCompleted") %>' CssClass="txtboxde form-control"  ToolTip="How many Angular did you complete?" AutoPostBack="true" Width="300px" TabIndex="4"  /></div>
                 <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="AngularCompletedTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
              </div></td>
            </tr> 
                </tbody>
            </table>
              
                <!------- Interviews Keyed ------->
        <table id="tblinterkey" class="table table-striped " runat="server" >
            <tbody>
        <tr  runat="server" style="background-color:#cccc00;"  >
            <td  class="form-group" >
                <div >
                <div class="col-sm-6" style="font-weight:bold;">Number of Interviews Keyed:</div>
              <div class=" col-sm-2" >  <asp:TextBox ID="InterviewsCompletedTextBox" runat="server" Text='<%# Bind("InterviewsCompleted") %>' CssClass="txtboxde form-control"  ToolTip="How many Interviews did you complete?" AutoPostBack="true" Width="300px" TabIndex="5" /></div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="dewt" Font-Bold="True" ControlToValidate="InterviewsCompletedTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
              </div> </td>           
            </tr> 
               </tbody>
            </table>
                <table id="Table8" class="table table-striped " runat="server" >
            <tbody> 
               <tr style="background-color:#cccc00;">
            <td  class="form-group">
                
                <div >
                <div class="col-sm-6" style="font-weight:bold;">Total Keyed:</label> </div> 
                 <div class=" col-sm-2" > <asp:TextBox ID="TCompleteTextBox" runat="server" Text='<%# Bind("TComplete") %>'  CssClass="txtboxde form-control" Width="300px" TabIndex="8" /></div>
                  <label style="font-size: small" runat="server" visible="false" id="lblsum">Sum of angular and Interviews</label>
                </div> </td>
            </tr>
              </table>

                        <!------ Verify Quantity ------>
    <div id="VerifyQuantitytblForm" runat="server" visible="false"  >
  <table id="tblfrm" class="table table-striped" runat="server" >
    <tbody>
        <tr style="background-color:#cccc00;">
            <td class="form-group"><div id="trVerifyQuantity" runat="server"  >
                <div class="col-xs-6">*
                <label id="Label2" for="QtyVerifiedCheckBox">          
                Verify Forms </label></div>
                <div class="col-xs-offset-7">     <asp:CheckBox ID="QtyVerifiedCheckBox" runat="server" Checked='<%# Bind("QtyVerified") %>' CssClass="checkbox"  Font-Size="XX-Large" OnCheckedChanged="tbQtyVerified_CheckedChanged" ToolTip="Check If All forms are Verified" TabIndex="9" /></div>
                 </div>  </td>
            </tr> 
           
       
        </tbody>
        </table>
        </div>
                 <!------ Batch Complete ------>
    <div id="BatchnumtblForm" runat="server" visible="false"  >
  <table id="batchtblfrm" class="table table-striped" runat="server" >
    <tbody>
        <tr style="background-color:#cccc00;">
            <td class="form-group"><div id="trBatchNumCheck" runat="server" >
                <div class="col-xs-6">*
                <label id="Label6" for="tbBathNumCheck">Batch Completed </label></div>
           <div class="col-xs-offset-7"> 
                <asp:CheckBox ID="BCompleteCheckBox" runat="server" Checked='<%# Bind("BComplete") %>' Font-Size="XX-Large" ToolTip="Batch number completed checkbox" CssClass="checkbox" TabIndex="11" /></div>
                  </div>  </td>
            </tr> 
           
       
        </tbody>
        </table>
        </div>

        <table id="Table10" class="table table-striped " runat="server" >
                 <tr>
            <td >
                <div class="col-xs-7">
                <label id="Label5" for="outtxt"></label></div>
               <label>=</label>
                
                  </td>
            </tr>
         </table>       
                <table id="Table9" class="table table-striped " runat="server">
                    <tbody>
                <tr >
            <td class="form-group">
                 <div >
                 <div class="col-sm-6" style="font-weight:bold;">Need To Be Keyed:</div>
               <div class=" col-sm-2" > <asp:TextBox ID="RBalanceTextBox" runat="server" Text='<%# Bind("RBalance") %>' CssClass="line txtboxde form-control" ReadOnly="true" TextMode="Number" ToolTip="Can't type here" Enabled="false" Width="300px" /></div>
                 </td>
            </tr>
             
    </tbody>
  </table>

                <asp:TextBox ID="CreatedByUserIDTextBox" runat="server" Text='<%# Bind("CreatedByUserID") %>' Visible ="false"/>

                <asp:TextBox ID="CreatedDateTextBox" runat="server" Text='<%# Bind("CreatedDate") %>' Visible ="false"/>

                <asp:TextBox ID="ModifiedByUserIDTextBox" runat="server" Text='<%# Bind("ModifiedByUserID") %>' Visible ="false"/>

                <asp:TextBox ID="ModifiedDateTextBox" runat="server" Text='<%# Bind("ModifiedDate") %>'  Visible ="false"/>


    <br />
        </div>  
          </InsertItemTemplate>
            <ItemTemplate>
                <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' Visible="false" />
                <br />
                AppID:
                <asp:Label ID="AppIDLabel" runat="server" Text='<%# Bind("AppID") %>' Visible="false"/>
                <br />
                AppName:
                <asp:Label ID="AppNameLabel" runat="server" Text='<%# Bind("AppName") %>' />
                <br />
                Region:
                <asp:Label ID="RegionLabel" runat="server" Text='<%# Bind("Region") %>' />
                <br />
                BatchNum:
                <asp:Label ID="BatchNumLabel" runat="server" Text='<%# Bind("BatchNum") %>' />
                <br />
                QtyVerified:
                <asp:CheckBox ID="QtyVerifiedCheckBox" runat="server" Checked='<%# Bind("QtyVerified") %>' Enabled="false" />
                <br />
                Quantity:
                <asp:Label ID="QuantityLabel" runat="server" Text='<%# Bind("Quantity") %>' />
                <br />
                Interviews:
                <asp:Label ID="InterviewsLabel" runat="server" Text='<%# Bind("Interviews") %>' />
                <br />
                Packets:
                <asp:Label ID="PacketsLabel" runat="server" Text='<%# Bind("Packets") %>' />
                <br />
                BComplete:
                <asp:CheckBox ID="BCompleteCheckBox" runat="server" Checked='<%# Bind("BComplete") %>' Enabled="false" />
                <br />
                TQuantity:
                <asp:Label ID="TQuantityLabel" runat="server" Text='<%# Bind("TQuantity") %>' />
                <br />
                TIncomplete:
                <asp:Label ID="TIncompleteLabel" runat="server" Text='<%# Bind("TIncomplete") %>' />
                <br />
                TComplete:
                <asp:Label ID="TCompleteLabel" runat="server" Text='<%# Bind("TComplete") %>' />
                <br />
                AngularCompleted:
                <asp:Label ID="AngularCompletedLabel" runat="server" Text='<%# Bind("AngularCompleted") %>' />
                <br />
                InterviewsCompleted:
                <asp:Label ID="InterviewsCompletedLabel" runat="server" Text='<%# Bind("InterviewsCompleted") %>' />
                <br />
                RBalance:
                <asp:Label ID="RBalanceLabel" runat="server" Text='<%# Bind("RBalance") %>' />
                <br />
                NAddForms:
                <asp:Label ID="NAddFormsLabel" runat="server" Text='<%# Bind("NAddForms") %>' />
                <br />
                CreatedByUserID:
                <asp:Label ID="CreatedByUserIDLabel" runat="server" Text='<%# Bind("CreatedByUserID") %>' />
                <br />
                CreatedDate:
                <asp:Label ID="CreatedDateLabel" runat="server" Text='<%# Bind("CreatedDate") %>' />
                <br />
                ModifiedByUserID:
                <asp:Label ID="ModifiedByUserIDLabel" runat="server" Text='<%# Bind("ModifiedByUserID") %>' />
                <br />
                ModifiedDate:
                <asp:Label ID="ModifiedDateLabel" runat="server" Text='<%# Bind("ModifiedDate") %>' />
                <br />
                <asp:LinkButton ID="NewButton" runat="server" CausesValidation="False" CommandName="New" Text="New" />
            </ItemTemplate>
        </asp:FormView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" InsertCommand="INSERT INTO DWT.AppData(AppID, AppName, Region, BatchNum, QtyVerified, Quantity, Interviews, Packets, BComplete, TQuantity, TIncomplete, TComplete, AngularCompleted, InterviewsCompleted, RBalance, NAddForms, CreatedByUserID, CreatedDate, ModifiedByUserID, ModifiedDate, DateRecieved) VALUES (@AppID, @AppName, @Region, @BatchNum, @QtyVerified, @Quantity, @Interviews, @Packets, @BComplete, @TQuantity, @TIncomplete, @TComplete, @AngularCompleted, @InterviewsCompleted, @RBalance, @NAddForms, @CreatedByUserID, GETDATE(), @ModifiedByUserID, GETDATE())" SelectCommand="SELECT DWT.AppData.* FROM DWT.AppData" UpdateCommand="UPDATE DWT.AppData SET Region = @Region, BatchNum = @BatchNum, QtyVerified = @QtyVerified, Quantity = @Quantity, Interviews = @Interviews, Packets = @Packets, BComplete = @BComplete, TQuantity = @TQuantity, TIncomplete = @TIncomplete, TComplete = @TComplete, AngularCompleted = @AngularCompleted, InterviewsCompleted = @InterviewsCompleted, RBalance = @RBalance, NAddForms = @NAddForms, ModifiedByUserID = @ModifiedByUserID, ModifiedDate = @ModifiedDate,DateRecieved=@date">
            <InsertParameters>
                <asp:Parameter Name="AppID" />
                <asp:Parameter Name="AppName" />
                <asp:Parameter Name="Region" />
                <asp:Parameter Name="BatchNum" />
                <asp:Parameter Name="QtyVerified" />
                <asp:Parameter Name="Quantity" />
                <asp:Parameter Name="Interviews" />
                <asp:Parameter Name="Packets" />
                <asp:Parameter Name="BComplete" />
                <asp:Parameter Name="TQuantity" />
                <asp:Parameter Name="TIncomplete" />
                <asp:Parameter Name="TComplete" />
                <asp:Parameter Name="AngularCompleted" />
                <asp:Parameter Name="InterviewsCompleted" />
                <asp:Parameter Name="RBalance" />
                <asp:Parameter Name="NAddForms" />
                <asp:Parameter Name="CreatedByUserID" />
                <asp:Parameter Name="ModifiedByUserID" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Region" />
                <asp:Parameter Name="BatchNum" />
                <asp:Parameter Name="QtyVerified" />
                <asp:Parameter Name="Quantity" />
                <asp:Parameter Name="Interviews" />
                <asp:Parameter Name="Packets" />
                <asp:Parameter Name="BComplete" />
                <asp:Parameter Name="TQuantity" />
                <asp:Parameter Name="TIncomplete" />
                <asp:Parameter Name="TComplete" />
                <asp:Parameter Name="AngularCompleted" />
                <asp:Parameter Name="InterviewsCompleted" />
                <asp:Parameter Name="RBalance" />
                <asp:Parameter Name="NAddForms" />
                <asp:Parameter Name="ModifiedByUserID" />
                <asp:Parameter Name="ModifiedDate" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
         </div>

    </div>
       <div class="panel-footer">
           <asp:Button ID="btnSave" runat="server" Text="Save"   OnClick="SaveData" CssClass="btn btn-success btn-md " UseSubmitBehavior="false" />
                <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
                    &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>        
    </div>
    </div>
           </div>
            </div>
        </section>
</asp:Content>
