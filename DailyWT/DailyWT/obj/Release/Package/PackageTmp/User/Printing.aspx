﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Printing.aspx.cs" Inherits="DailyWT.User.Printing" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
     <style>
        /*#section1 {
        margin-left:-30%;
        padding-left:0px;
        }*/

    </style>
<script>
    $(document).ready(function () {
        var Qty = {
            validators: {
                regexp: {
                    regexp: /^[0-9]+$/,
                    message: 'Numbers only'
                }
            }
        },
        Requester = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z\ \-\.]+$/,
                    message: 'The Requester must be alphabetic chars'
                }
            }
        },
         Approver = {
             validators: {
                 regexp: {
                     regexp: /^[A-Za-z\ \-\.]+$/,
                     message: 'The Requester must be alphabetic chars'
                 }
             }
         },
        Comment = {
            validators: {
                regexp: {
                    regexp: /^[A-Za-z0-9\r\n\ \-\+,.!@#$%()=;:?\/]+$/,
                    message: 'The Comments must be alphabetic chars'
                }
            }
        };

        $('#mpForm')
        .bootstrapValidator('addField', 'tbFormsUsed', Qty)
        .bootstrapValidator('addField', 'tbFormsReq', Qty)
        .bootstrapValidator('addField', 'tbRequester', Requester)
        .bootstrapValidator('addField', 'tbApprover', Approver)
        .bootstrapValidator('addField', 'tbComment', Comment);
    });
</script>
 <asp:ScriptManager ID="ToolkitScriptManager1" runat="server">
</asp:ScriptManager>

<!-- Section 1 -->
   <%-- <br />
    <br />
    <br />--%>
<section class="container " id="section1" >
    <div class="col-lg-13">
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Printing Module</span></div>
<div class="container toggle ">
    <div class="h4 form-group">         
        <label class="v-align-tc">&nbsp;&nbsp;FormType:&nbsp;&nbsp<asp:DropDownList ID="ddlJob" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>
    <div class="h4 form-group">        
        <label class="v-align-tc">&nbsp;&nbsp;Job Used:&nbsp;&nbsp<asp:DropDownList ID="ddlForm" Enabled="false" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <br />
    </div>
    <div class="col-lg-12" style="margin-left:-1%;">
    <table class="table table-condensed" id="tblForm" runat="server" visible="false">      
            <tr runat="server" class="info">
            <td><b>Inventory on Hand</b></td>
            <td><label id="lblEndAmount" runat="server"></label></td>
        </tr>
          <tr id="Tr1"  >
            <td class="col-xs-6"><b>Print Date</b></td>
            <td class="col-xs-6"><asp:Textbox id="lbldate"  runat="server" ForeColor="Black" CssClass="form-control"></asp:Textbox>
                 <asp:MaskedEditExtender ID="lbldate_MaskedEditExtender" runat="server"  Enabled="True" Mask="99/99/9999" MaskType="Date" TargetControlID="lbldate"></asp:MaskedEditExtender>
            </td>
        </tr>
    
        <tr class="info"  id="trSeq1" runat="server" visible="false">
            <td><b>Beginning Sequence#</b></td>
            <td><label id="lblBegSeq" runat="server"></label></td>
        </tr>
        <tr id="trSeq2" runat="server" visible="false">
            <td><b>Ending Sequence#</b></td>
            <td><label id="lblEndSeq" runat="server"></label></td>
        </tr>
        <tr class="info">
            <td><b>Number Forms Printed</b></td>
            <td class="form-group"><input type='text' class='form-control' name='tbFormsReq' placeholder='Forms Printed' value="<%=Request.Form["tbFormsReq"] %>" /></td>
        </tr>
         <tr >
            <td><b>Number of Voids Printed</b></td>
            <td class="form-group"><input type='text' class='form-control' name='tbvoids' placeholder='Voids Printed' value="<%=Request.Form["tbvoids"] %>" /></td>
        </tr>
        <tr class="info">
            <td><b>Reason for Voids</b></td>
            <td class="form-group"><asp:DropDownList ID="ddlReason" runat="server" AppendDataBoundItems="true"></asp:DropDownList></td>
        </tr>
        
        <tr >
            <td><b>Requester</b></td>
            <td class="form-group"><input type='text' class='form-control' name='tbRequester' placeholder='Requester' value="<%=Request.Form["tbRequester"] %>" /></td>
        </tr>
        <tr class="info">
            <td><b>Comment</b></td>
            <td class="form-group"><textarea class='form-control' name='tbComment' rows="3" placeholder='Comment'><%=Request.Form["tbComment"] %></textarea></td>
        </tr>
     </table>
            </div>
    <div id="divNoData" runat="server" visible="false">
        <br />
        <div class="h3 text-warning">&nbsp;&nbsp;Initial form inventory not entered.
        </div>
    </div>
</div>
    
<div class="panel-footer">
        <asp:Button ID="btnSave" CssClass="btn btn-success btn-md submit-button disabled" runat="server" Text="Save Changes" OnClick="SaveChanges_Click"   />
    &nbsp;&nbsp;
            <button type="reset" id="resetregform" class="btn btn-default btn-md">
        Clear Form</button>&nbsp;&nbsp;
            <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;

</div>
</div>
</div>
</section>


</asp:Content>
