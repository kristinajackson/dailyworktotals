﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="DailyWT.Error" %>

    <link href="/CSS/flatly/bootstrap.min.css" rel="stylesheet" title="main" />

    <div class="container-fluid">
        <div class="jumbotron">
            <div style="text-align: center;" >
                <br />
                <br />
                <label class="h1 alert alert-danger">An Error has occured. Please contact your friendly System Administrator!</label>
                <br />
                <br />
                <br />
                <br />
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://twraintranet.nash.tenn/DailyWorkTotals" Target="_blank">Return back to Daily Work Totals System</asp:HyperLink>
                
                <br />
                <br />
            </div>
        </div>
    </div>
