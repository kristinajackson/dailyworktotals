﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PhysicalInventory.aspx.cs" Inherits="DailyWT.User.PhysicalInventory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true">
    </asp:ScriptManager>

    <style>
        #section1 {
        /*margin-left:-30%;
        padding-left:0px;*/
        }

    </style>
     <script type="text/javascript">
         function myFunction() {

             var r = confirm("Are You Certain You Want to Delete?");
             if (r == true) {
                 return true;

             } else {
                 return false;
             }
         }
</script>
   
      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
    <!-- Section 1 -->
    <div class="container" id="section1" >
        <div class="col-md-12" >
        
        <div class=" panel panel-default">
            <br />
            <div class="panel-heading" >
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Physical Inventory Module</span>
            </div>
            <div >
                <div class="container">
                <div class="panel-body">
                <div class="alert alert-danger col-md-10" runat="server" id="danger" visible="false" role="alert">
                    <strong>ERROR!</strong> Record was not saved. Please check your work
                </div>

                <div class="alert alert-success col-md-10" runat="server" id="alert" visible="false" role="alert">
                    <strong>Success!</strong> Record entered successfully.
                </div>
                      <div class="alert alert-warning col-md-10" runat="server" id="delete" visible="false" role="alert">
                    <strong>Success!</strong> Record deleted successfully.
                </div>
                <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" DataKeyNames="ID" InsertItemPosition="LastItem" OnDataBound="ListView1_DataBound" OnItemInserted="ListView1_ItemInserted" OnPagePropertiesChanged="ListView1_PagePropertiesChanged" OnItemDeleted="ListView1_ItemDeleted" OnItemDeleting="ListView1_ItemDeleting">
                    <EmptyDataTemplate>
                        <table id="Table2" runat="server" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px;">
                            <tr>
                                <td>No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <LayoutTemplate>
                        <table id="Table1" runat="server">
                            <tr id="Tr1" runat="server">
                                <td id="Td1" runat="server">
                                    <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF; border-collapse: collapse; border-color: #999999; border-style: none; border-width: 1px; font-family: Verdana, Arial, Helvetica, sans-serif;  margin-left:0px; padding-left:0px;" class="table table-bordered">
                                        <tr id="Tr2" runat="server" style="background-color: #DCDCDC; color: #000000;">
                                            <th id="Th1" runat="server" style="width: 152px; text-align:center; font-size:15px;">Action</th>
                                            <th id="Th2" runat="server" style="width: 152px; text-align:center; font-size:15px;">Date</th>
                                            <th id="Th3" runat="server" style="width: 152px; word-break:normal; font-size:15px; text-align:center;">Job Name</th>
                                            <th id="Th4" runat="server" style="width: 152px; word-break:normal; font-size:15px; text-align:center;">Total Number of Forms</th>
                                            <th id="Th5" runat="server" style="width: 152px; word-break:normal; font-size:15px; text-align:center;">Total Number of Boxes</th>
                                            <th id="Th6" runat="server" style="width: 152px; word-break:normal; font-size:15px; text-align:center;">Comment</th>
                                        </tr>
                                        <tr id="itemPlaceholder" runat="server">
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table class="table table-bordered" style="width:1000px; top:0;">
                            <tr id="Tr3" runat="server" style="background-color: #DCDCDC; color: #000000; ">
                                <td id="Td2" runat="server" style="text-align: center; font-size:12px; background-color: #DCDCDC; font-family: Verdana, Arial, Helvetica, sans-serif; color: #000000; width:1000px;" class="form-group">
                                    <asp:DataPager ID="DataPager1" runat="server" PageSize="4">
                                        <Fields>
                                            <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" ButtonCssClass=" btn" />
                                        </Fields>
                                    </asp:DataPager>
                                </td>
                            </tr>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <table class="table table-bordered">
                            <tr style="color: #000000;">
                                <td class="form-group" style="width: 152px;">
                                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" CssClass="btn" Width="152px" OnClientClick="return confirm('Are You Certain You Want to Delete?');" />
                                    <%--<asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" CssClass="btn btn-danger btn-xs btn-rounded" OnClientClick="return confirm('Are You Certain You Want to Delete?');" />--%>
                                </td>
                                <td class="form-group" style="width: 152px;">
                                    <asp:Label ID="DateLabel" runat="server" Text='<%# Eval("Date","{0:MM/dd/yyyy}") %>' Width="152px" Font-Size="12px" />
                                </td>
                                <td class="form-group" style="width: 152px;">
                                    <asp:Label ID="FormNameLabel" runat="server" Text='<%# Eval("FormName") %>' Width="152px" Font-Size="12px"/>
                                </td>
                                <td class="form-group" style="width: 152px;">
                                    <asp:Label ID="TotalFormsLabel" runat="server" Text='<%# Eval("TotalForms") %>' Width="152px" Font-Size="12px" />
                                </td>
                                <td class="form-group" style="width: 152px;">
                                    <asp:Label ID="TotalBoxesLabel" runat="server" Text='<%# Eval("TotalBoxes") %>' Width="152px" Font-Size="12px"/>
                                </td>
                                <td class="form-group" style="width: 152px;">
                            <asp:Label ID="CommentLabel" runat="server" Text='<%# Eval("Comment") %>'  Width="152px" Font-Size="12px"/>
                        </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <InsertItemTemplate>
                        <asp:Panel ID="pnlInsert" runat="server" DefaultButton="InsertButton">
                            <table class="table table-bordered">
                                <tr >
                                    <td class="form-group" style="width: 152px; border-top:solid #C8C8C8; border-top-width:thick; text-align:center;">
                                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Enter" CssClass="btn btn-md btn-success" ValidationGroup="PINV"  OnClick="InsertButton_Click" OnClientClick="return confirm('Are You Certain You Want to Save?');"/>
                                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" CssClass="btn btn-md" />
                                   <button type="button" id="btnCancel" class="btn btn-md btn-danger" onclick="NewPage('/User/LandingPage.aspx')">Cancel</button>
                                          </td>
                                    <td class="form-group" style="width: 152px; border-top:solid #C8C8C8; border-top-width:thick; text-align:center;">
                                        <asp:TextBox ID="DateTextBox" runat="server" Text='<%# Bind("Date") %>' CssClass="form-control" Width="152px" placeholder="Date" />
                                        <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" TargetControlID="DateTextBox" AutoComplete="False" ClearMaskOnLostFocus="true" Mask="99/99/9999" MaskType="Date" />
                                        <asp:CalendarExtender ID="CalendarExtender7" runat="server" TargetControlID="DateTextBox" PopupButtonID="ImageButton1"></asp:CalendarExtender>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidatorDateTextBox" runat="server" ErrorMessage="Required Field" ControlToValidate="DateTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="PINV" SetFocusOnError="True" Font-Size="Large"></asp:RequiredFieldValidator>
                                         <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Format must be MM/DD/YYYY" ValidationExpression="^([0-9]|0[1-9]|1[012])\/([0-9]|0[1-9]|[12][0-9]|3[01])\/(19|20)\d\d$" ValidationGroup="PINV" Font-Bold="True" Font-Size="Large" ControlToValidate="DateTextBox" SetFocusOnError="True" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </td>

                                    <td class="form-group" style="width: 152px;  border-top:solid #C8C8C8; border-top-width:thick; text-align:center;">
                                        <asp:TextBox ID="FormNameTextBox" runat="server" Text='<%# Bind("FormName") %>' CssClass="form-control" Visible="false"  />
                                        <asp:DropDownList ID="ddlForm" placeholder="InventoryName" OnSelectedIndexChanged="ddlForm_SelectedIndexChanged" runat="server" AutoPostBack="true" AppendDataBoundItems="true" CssClass="form-control dropdown" Width="152px" Font-Size="Small"></asp:DropDownList>
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Field" ControlToValidate="FormNameTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="PINV" SetFocusOnError="True" Font-Size="Large"></asp:RequiredFieldValidator>
                                    </td>
                                    <td class="form-group" style="width: 152px;  border-top:solid #C8C8C8; border-top-width:thick; text-align:center;">
                                        <asp:TextBox ID="TotalFormsTextBox" runat="server" Text='<%# Bind("TotalForms") %>' CssClass="form-control" Width="152px" placeholder="Total Forms"  />
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidatorTotalFormsTextBox" runat="server" ErrorMessage="Required Field" ControlToValidate="TotalFormsTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="PINV" SetFocusOnError="True" Font-Size="Large"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="PINV" Font-Bold="True" ControlToValidate="TotalFormsTextBox" SetFocusOnError="True" Font-Size="Medium" Display="Dynamic"></asp:RegularExpressionValidator> 
                                    </td>
                                    <td class="form-group" style="width: 152px;  border-top:solid #C8C8C8; border-top-width:thick; text-align:center;">
                                        <asp:TextBox ID="TotalBoxesTextBox" runat="server" Text='<%# Bind("TotalBoxes") %>' CssClass="form-control" Width="152px" placeholder="Total Boxes" />
                                         <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorTotalBoxesTextBox" runat="server" ErrorMessage="Required Field" ControlToValidate="TotalBoxesTextBox" Display="Dynamic" Font-Bold="True" ValidationGroup="PINV" SetFocusOnError="True" Font-Size="Large"></asp:RequiredFieldValidator>--%>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Number Only" ValidationExpression="^\d+$" ValidationGroup="PINV" Font-Bold="True" ControlToValidate="TotalBoxesTextBox" SetFocusOnError="True" Font-Size="Medium" Display="Dynamic"></asp:RegularExpressionValidator> 
                                    </td>               
                                    <td class="form-group" style="width: 152px;  border-top:solid #C8C8C8; border-top-width:thick; text-align:center;" >
                                <asp:TextBox ID="CommentTextBox" runat="server" Text='<%# Bind("Comment") %>' CssClass="form-control" placeholder="Comment"  Width="152px" Font-Size="Small"/>
                            </td>
                                     <td id="Td4" runat="server" visible="false">
                                        <asp:TextBox ID="CreatedByUserIDTextBox" runat="server" Text='<%# Bind("CreatedByUserID") %>' />
                                    </td>
                                    <td id="Td5" runat="server" visible="false">
                                        <asp:TextBox ID="ActionTextBox" runat="server" Text='<%# Bind("Action") %>' CssClass="form-control" Width="100px" />
                                    </td>
                                    <td id="Td3" runat="server" visible="false">
                                        <asp:TextBox ID="FormIDTextBox" runat="server" Text='<%# Bind("FormID") %>' CssClass="form-control" Width="100px" />
                                    </td>
                                </tr>
                                <tr style="top: 0px; text-align:center; box-sizing:inherit;">
                                    <td style="top: 0px;"></td>
                                    <td style="top: 0px;">
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.png" /></td>
                                    
                                </tr>
                            </table>
                        </asp:Panel>
                    </InsertItemTemplate>



                </asp:ListView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" DeleteCommand="DELETE FROM DWT.PhysicalInventory WHERE (ID = @ID)" InsertCommand="INSERT INTO DWT.PhysicalInventory(Date, FormID, FormName, TotalForms, TotalBoxes, CreatedByUserID, Action, Comment) VALUES (@Date, @FormID, @FormName, @TotalForms, @TotalBoxes, @CreatedByUserID, @Action, @Comment)" SelectCommand="SELECT DWT.PhysicalInventory.* FROM DWT.PhysicalInventory ORDER BY Date DESC">
                    <DeleteParameters>
                        <asp:Parameter Name="ID" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Date" />
                        <asp:Parameter Name="FormID" />
                        <asp:Parameter Name="FormName" />
                        <asp:Parameter Name="TotalForms" />
                        <asp:Parameter Name="TotalBoxes" />
                        <asp:Parameter Name="CreatedByUserID" />
                        <asp:Parameter Name="Action" />
                        <asp:Parameter Name="Comment" />
                    </InsertParameters>
                </asp:SqlDataSource>

              <%-- <div class="panel-footer">
        

    </div>--%>

            </div>
</div>
        </div>
                </div>
        </div>
    </div>
                                  
    <br />
    <br />
    <br />

    <br />
                                </ContentTemplate>
          </asp:UpdatePanel>
</asp:Content>
