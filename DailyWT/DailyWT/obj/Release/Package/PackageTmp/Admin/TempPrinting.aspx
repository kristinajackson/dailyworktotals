﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="TempPrinting.aspx.cs" Inherits="DailyWT.Admin.TempPrinting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <style>
        .headert {
        width: 50%;
        text-align:center;
        font-size:medium;
        }
        .tablefont {
        font-size:medium;
        text-align:center;
        }
        .btn-sample { 
  color: #ffffff; 
  background-color: #611BBD; 
  border-color: #130269; 
} 
 
.btn-sample:hover, 
.btn-sample:focus, 
.btn-sample:active, 
.btn-sample.active, 
.open .dropdown-toggle.btn-sample { 
  color: #ffffff; 
  background-color: #49247A; 
  border-color: #130269; 
} 
 
.btn-sample:active, 
.btn-sample.active, 
.open .dropdown-toggle.btn-sample { 
  background-image: none; 
} 
 
.btn-sample.disabled, 
.btn-sample[disabled], 
fieldset[disabled] .btn-sample, 
.btn-sample.disabled:hover, 
.btn-sample[disabled]:hover, 
fieldset[disabled] .btn-sample:hover, 
.btn-sample.disabled:focus, 
.btn-sample[disabled]:focus, 
fieldset[disabled] .btn-sample:focus, 
.btn-sample.disabled:active, 
.btn-sample[disabled]:active, 
fieldset[disabled] .btn-sample:active, 
.btn-sample.disabled.active, 
.btn-sample[disabled].active, 
fieldset[disabled] .btn-sample.active { 
  background-color: #611BBD; 
  border-color: #130269; 
} 
 
.btn-sample .badge { 
  color: #611BBD; 
  background-color: #ffffff; 
}
        </style>
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true">
    </asp:ScriptManager>
      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
    <!-- Section 1 -->
    <div class="container" id="section1" >
        <div class="col-sm-25" >
        
        <div class=" panel panel-default">
            <br />
            <div class="panel-heading" >
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Print Job Config Module</span>
            </div>
            <div >
                <%--<div class="container">--%>
                <div class="panel-body">
                <div class="alert alert-danger col-md-10" runat="server" id="danger" visible="false" role="alert">
                    <strong>ERROR!</strong> Record was not saved. Please check your work
                </div>

                <div class="alert alert-success col-md-10" runat="server" id="alert" visible="false" role="alert">
                    <strong>Success!</strong> Record entered successfully.
                </div>
                      <div class="alert alert-warning col-md-10" runat="server" id="delete" visible="false" role="alert">
                    <strong>Success!</strong> Record deleted successfully.
                </div>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" DeleteCommand="DELETE FROM DWT.TempPrint WHERE (ID = @ID)" SelectCommand="SELECT FormName, JobName, EndAmt, FormsReq, Voids, BegSeq, EndSeq, CreatedDate, FormID FROM DWT.TempPrint" UpdateCommand="UPDATE DWT.TempPrint SET  EndAmt = @EndAmt, BegSeq = @BegSeq, EndSeq = @EndSeq, FormsReq = @FormsReq, Voids = @Voids WHERE ID = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="EndAmt" />
            <asp:Parameter Name="BegSeq" />
            <asp:Parameter Name="EndSeq" />
            <asp:Parameter Name="FormsReq" />
            <asp:Parameter Name="Voids" />
            <asp:Parameter Name="ID" />
            <asp:Parameter Name="FormID" />
        </UpdateParameters>
    </asp:SqlDataSource>

    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" OnItemInserting="ListView1_ItemInserting">    
        <EditItemTemplate>
            <tr style="" class="tablefont">
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" CssClass="btn btn-sample" Width="100%"/>
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" CssClass="btn btn-default"  Width="100%"/>
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:Label ID="FormNameTextBox" runat="server" Text='<%# Bind("FormName") %>' Width="100%"  />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:Label ID="JobNameTextBox" runat="server" Text='<%# Bind("JobName") %>' Width="100%" />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:TextBox ID="EndAmtTextBox" runat="server" Text='<%# Bind("EndAmt") %>' Width="100%" CssClass="form-control" />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:TextBox ID="FormsReqTextBox" runat="server" Text='<%# Bind("FormsReq") %>' Width="100%" CssClass="form-control" />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:TextBox ID="VoidsTextBox" runat="server" Text='<%# Bind("Voids") %>' Width="100%" CssClass="form-control" />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:TextBox ID="BegSeqTextBox" runat="server" Text='<%# Bind("BegSeq") %>' Width="100%" CssClass="form-control" />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:TextBox ID="EndSeqTextBox" runat="server" Text='<%# Bind("EndSeq") %>' Width="100%" CssClass="form-control" />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:Label ID="CreatedDateTextBox" runat="server" Text='<%# Bind("CreatedDate") %>' Width="100%"  />
                </td>
                <td style="font-size:medium; text-align:center; width: 12%;">
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("FormID") %>' Width="100%"  />
                </td>
            </tr>
        </EditItemTemplate>
        <EmptyDataTemplate>
            <table runat="server" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
       
     
        <ItemTemplate>
            <tr style="width:100%;" class="tablefont">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" Width="100%" />
                    <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" CssClass="btn btn-info" Width="100%" />
                </td>
                <td>
                    <asp:Label ID="FormNameLabel" runat="server" Text='<%# Eval("FormName") %>' />
                </td>
                <td>
                    <asp:Label ID="JobNameLabel" runat="server" Text='<%# Eval("JobName") %>' />
                </td>
                <td>
                    <asp:Label ID="EndAmtLabel" runat="server" Text='<%# Eval("EndAmt") %>' />
                </td>
                <td>
                    <asp:Label ID="FormsReqLabel" runat="server" Text='<%# Eval("FormsReq") %>' />
                </td>
                <td>
                    <asp:Label ID="VoidsLabel" runat="server" Text='<%# Eval("Voids") %>' />
                </td>
                <td>
                    <asp:Label ID="BegSeqLabel" runat="server" Text='<%# Eval("BegSeq") %>' />
                </td>
                <td>
                    <asp:Label ID="EndSeqLabel" runat="server" Text='<%# Eval("EndSeq") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedDateLabel" runat="server" Text='<%# Eval("CreatedDate") %>' />
                </td>
                     <td>
                    <asp:Label ID="FormIDLbl" runat="server" Text='<%# Eval("FormID") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table runat="server" style="width:100%;" class="table table-bordered">
                <tr runat="server">
                    <td runat="server">
                        <table id="itemPlaceholderContainer" runat="server" border="1" style="width:100%; background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif; " class="table table-bordered">
                            <tr runat="server" style="background-color:#DCDCDC;color: #000000; width:100%;">
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;" ></th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">FormName</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">JobName</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">EndAmt</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">FormsReq</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">Voids</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">BegSeq</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">EndSeq</th>
                                <th runat="server" style="width: 12%; text-align:center; font-size:large;">CreatedDate</th>
                                <th id="Th1" runat="server" style="width: 12%; text-align:center; font-size:large;">FormID</th>
                            </tr>
                            <tr runat="server" id="itemPlaceholder">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" style="text-align: center;background-color: #CCCCCC;font-family: Verdana, Arial, Helvetica, sans-serif;color: #000000;">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" ButtonCssClass=" btn" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
       
    </asp:ListView>
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
  </div>
                <div class="panel-footer">
                     <asp:Button ID="btnSave" CssClass="btn btn-success btn-md submit-button " CommandName="Insert"  runat="server" Text="Approve Jobs" OnClick="btnSave_Click"/>
    <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="btnSave_Click"/>
                </div>
<%--</div>--%>
        </div>
                </div>
        </div>
    </div>
                                  
                                </ContentTemplate>
          </asp:UpdatePanel>
</asp:Content>
