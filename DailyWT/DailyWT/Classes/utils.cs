﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace DailyWT.Classes
{
    public class utils
    {
        public static void Message(Page p, String msg)
        {
            p.ClientScript.RegisterStartupScript(typeof(Page), "alert", "alert('" + msg + "');", true);
        }

        public static bool checkEmptyTextBoxes(TextBox[] textboxes)
        {
            //new TextBox[] { txtAddress, txtCity, txtEmail, txtName, txtOrganization, txtPhone, txtZip}
            var textBoxes = textboxes;
            if (textBoxes.Any(tb => tb.Text == String.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool checkEmptydropdown(DropDownList[] dropdownlist)
        {
            //new TextBox[] { txtAddress, txtCity, txtEmail, txtName, txtOrganization, txtPhone, txtZip}
            var dropdownList = dropdownlist;
            if (dropdownList.Any(tb => tb.Text == String.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}