﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;

namespace DailyWT.Classes
{
    public class BSDialogs : System.Web.UI.Page
    {
        protected void ShowPopUpAndRD(string header, string message, string newpage)
        {
            // The ShowPopUpAndRD method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called AccessError. It takes 3 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  newpage: when the user clicks the OK button, this is the new page the browser is redirected to - newpage  is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function AccessError(head, mess, newpage) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-warning',");
            sb.Append("label: 'Close',");
            //            sb.Append("action: function (dialogItself) {NewPage('/User/Intro.aspx');}");
            sb.Append("action: function (dialogItself) {NewPage(newpage);}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.            
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation. 
            string funcCall = "<script language='javascript'>AccessError('" + header + "','<b>" + message + "</b>','" + newpage + "');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }


        protected void ShowPopUp(string header, string message)
        {
            // The ShowPopUp method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called PopUpError. It takes 2 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function PopUpError(head, mess) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-warning',");
            sb.Append("label: 'Close',");
            sb.Append("action: function (dialogItself) {dialogItself.close();}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation.
            string funcCall = "<script language='javascript'>PopUpError('" + header + "','<b>" + message + "</b>');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }
        protected void ShowPopUpOkCancel(string header, string message, string routine, string subroutine)
        {
            // The ShowPopUpOkCancel method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called OkCancel. It takes 2 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function OkCancel(head, mess, newpage) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-danger',");
            sb.Append("label: '&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;',");
            sb.Append("action: function (dialogItself) {__doPostBack('" + routine + "', '" + subroutine + "');}},");
            sb.Append("{cssClass: 'btn-success',");
            sb.Append("label: 'Cancel',");
            sb.Append("action: function (dialogItself) {dialogItself.close();}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.            
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation. 
            string funcCall = "<script language='javascript'>OkCancel('" + header + "','<b>" + message + "</b>');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }
    }
}