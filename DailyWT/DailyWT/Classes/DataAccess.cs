﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DailyWT.Classes
{
    public class DataAccess
    {
        //comments
        /* 
         line 1
         line 2
         */
        #region creates/updates

        //creates new requests on print request form PrintRequestForm.aspx
        public static int CreateRequest(string formName, string requestor, DateTime date, DateTime requestTime, string identifyDocument, int noOfDocuments, DateTime completionDate, string printerUsed, string printOperator, string miscComments)
        {

            int status = 0;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.CreateRequest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FORM_NAME", formName);
                    cmd.Parameters.AddWithValue("@REQUESTOR", requestor);
                    cmd.Parameters.AddWithValue("@DATE", date);
                    cmd.Parameters.AddWithValue("@REQUEST_TIME", requestTime);
                    cmd.Parameters.AddWithValue("@IDENTIFY_DOCUMENT", identifyDocument);
                    cmd.Parameters.AddWithValue("@NO_OF_DOCUMENTS", noOfDocuments);
                    cmd.Parameters.AddWithValue("@COMPLETION_DATE", completionDate);
                    cmd.Parameters.AddWithValue("@PRINTER_USED", printerUsed);
                    cmd.Parameters.AddWithValue("@PRINT_OPERATOR", printOperator);
                    cmd.Parameters.AddWithValue("@MISC_DATA_COMMENTS", miscComments);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();

                }

                return status;
            }
        }


        public static int UpdateRequest(string formName, string requestor, DateTime date, DateTime requestTime, string identifyDocument, int noOfDocuments, DateTime completionDate, string printerUsed, string printOperator, string miscComments, int id)
        {

            int status = 0;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dwt.UpdateRequest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FORM_NAME", formName);
                    cmd.Parameters.AddWithValue("@REQUESTOR", requestor);
                    cmd.Parameters.AddWithValue("@DATE", date);
                    cmd.Parameters.AddWithValue("@REQUEST_TIME", requestTime);
                    cmd.Parameters.AddWithValue("@IDENTIFY_DOCUMENT", identifyDocument);
                    cmd.Parameters.AddWithValue("@NO_OF_DOCUMENTS", noOfDocuments);
                    cmd.Parameters.AddWithValue("@COMPLETION_DATE", completionDate);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@PRINTER_USED", printerUsed);
                    cmd.Parameters.AddWithValue("@PRINT_OPERATOR", printOperator);
                    cmd.Parameters.AddWithValue("@MISC_DATA_COMMENTS", miscComments);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();

                }

                return status;
            }
        }
        #endregion



      

        public DataSet GetFormByName(string FormName)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.GetFormByName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FormName", FormName);

                    using (SqlDataAdapter da = new SqlDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                    }
                }
                return ds;
            }
        }

        public int DeleteNewForm(int ID)
        {
            int status = -1;

            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("dbo.DeleteNewForm", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.Connection = con;
                    con.Open();
                    status = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
            }
            return status;
        }


    }
}