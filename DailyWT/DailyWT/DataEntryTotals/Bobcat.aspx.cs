﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text.RegularExpressions;

namespace DailyWT.DataEntryTotals
{
    public partial class Bobcat : BSDialogs
    {
        SqlConnection myConnection;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
                    alert.Visible = false;
                    danger.Visible = false;
                    delete.Visible = false;

                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }

        protected void ListView2_DataBound(object sender, EventArgs e)
        {
            TextBox DateTextBox = (TextBox)ListView2.InsertItem.FindControl("DateTextBox");
            DateTextBox.Text = System.DateTime.Now.ToShortDateString();
            TextBox Labeluserid = (TextBox)ListView2.InsertItem.FindControl("Labeluserid");
            TextBox lblappid = (TextBox)ListView2.InsertItem.FindControl("lblappid");
            TextBox lblappname = (TextBox)ListView2.InsertItem.FindControl("lblappname");
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            Labeluserid.Text = MyUserID.ToString();


            //  string JobName
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int AppId = 0;
            string name = "";
            string AppName = "";

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DropDownList ddlbob = (DropDownList)ListView2.InsertItem.FindControl("ddlBobcatName");
            //DropDownList ddlbob = (DropDownList)ListView2.FindControl("ddlBobcatName");
            name = ddlbob.SelectedValue.ToString();
            AppName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (AppName.Length > 2 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this job name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    AppId = (int)ds1.Tables[0].Rows[0].ItemArray[0];
                    AppName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
            lblappid.Text = AppId.ToString();
            lblappname.Text = AppName.ToString();


        }

        protected void ListView2_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    alert.Visible = true;
                    danger.Visible = false;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    alert.Visible = false;
                    danger.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void ListView2_PagePropertiesChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
        }

        protected void ListView2_ItemDeleted(object sender, ListViewDeletedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    delete.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + delete.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    delete.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  Record was not deleted!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;

            }
        }

        protected void ListView2_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {


        }

        protected void ListView2_ItemInserting(object sender, ListViewInsertEventArgs e)
        {

        }

        protected void ddlBobcatName_TextChanged(object sender, EventArgs e)
        {
       //     TextBox DateTextBox = (TextBox)ListView2.InsertItem.FindControl("DateTextBox");
         //  DateTextBox.Text = ;
            TextBox Labeluserid = (TextBox)ListView2.InsertItem.FindControl("Labeluserid");
            TextBox lblappid = (TextBox)ListView2.InsertItem.FindControl("lblappid");
            TextBox lblappname = (TextBox)ListView2.InsertItem.FindControl("lblappname");
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            Labeluserid.Text = MyUserID.ToString();


            //  string JobName
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int AppId = 0;
            string name = "";
            string AppName = "";

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DropDownList ddlbob = (DropDownList)ListView2.InsertItem.FindControl("ddlBobcatName");
            //DropDownList ddlbob = (DropDownList)ListView2.FindControl("ddlBobcatName");
            name = ddlbob.SelectedValue.ToString();
            AppName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (AppName.Length > 2 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this job name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    AppId = (int)ds1.Tables[0].Rows[0].ItemArray[0];
                    AppName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
            lblappid.Text = AppId.ToString();
            lblappname.Text = AppName.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"));
        }

        protected void ListView2_Load(object sender, EventArgs e)
        {

        }

    }
}