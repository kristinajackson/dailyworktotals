﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text.RegularExpressions;

namespace DailyWT.DataEntryTotals
{
    public partial class Creel : BSDialogs
    {
        SqlConnection myConnection;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
                    alert.Visible = false;
                    danger.Visible = false;
                    delete.Visible = false;

                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }

        protected void Listview1_DataBound(object sender, EventArgs e)
        {
            TextBox DateTextBox = (TextBox)ListView1.InsertItem.FindControl("DateTextBox");
            DateTextBox.Text = System.DateTime.Now.ToShortDateString();
            TextBox Labeluserid = (TextBox)ListView1.InsertItem.FindControl("CreatedByUserIDTextBox");
            TextBox lblappid = (TextBox)ListView1.InsertItem.FindControl("AppIDTextBox");
            TextBox lblappname = (TextBox)ListView1.InsertItem.FindControl("AppNameTextBox");
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            Labeluserid.Text = MyUserID.ToString();


            //  string JobName
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int AppId = 0;
            string name = "";
            string AppName = "";

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DropDownList ddlbob = (DropDownList)ListView1.InsertItem.FindControl("ddlBobcatName");

            name = ddlbob.SelectedValue.ToString();
            AppName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (AppName.Length > 2 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this job name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    AppId = (int)ds1.Tables[0].Rows[0].ItemArray[0];
                    AppName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
            lblappid.Text = AppId.ToString();
            lblappname.Text = AppName.ToString();


        }

        protected void Listview1_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    alert.Visible = true;
                    danger.Visible = false;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    alert.Visible = false;
                    danger.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void Listview1_PagePropertiesChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
        }

        protected void Listview1_ItemDeleted(object sender, ListViewDeletedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    delete.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + delete.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    delete.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  Record was not deleted!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;

            }
        }

        protected void Listview1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            //TextBox lblappid = (TextBox)ListView1.InsertItem.FindControl("AppIDTextBox");
            //TextBox lblappname = (TextBox)ListView1.InsertItem.FindControl("AppNameTextBox");

            //DWUtility dwt = new DWUtility();

            //DataRow dr = null;
            //int ID = Convert.ToInt32(e.Keys["ID"]);
            //dr = dwt.GetBobcatOtter(ID);


            //int AppID = 0;
            //string AppName = "";
            //string BobcatDate = "";
            //string BobcatName = "";
            //string BobcatQuantity = "";
            //int BobcatCreatedByUserID = 0;

            //AppID = Convert.ToInt32(lblappid.Text);
            //AppName = lblappname.Text.ToString();
            //BobcatDate = dr["Date"].ToString();
            //BobcatName = dr["Name"].ToString();
            //BobcatQuantity = dr["Quantity"].ToString();
            //BobcatCreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"].ToString());



            //string action = "DELETE-BOBCATOTTER";
            //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            //// Build a SQL Insert statement string for all field values.      
            //String insertCmd = "INSERT INTO DWT.[AppDataLog] ([Action] ,[AppID] ,[AppName] ,[Region] ,[BatchNum] ,[QtyVerified] ,[Quantity] ,[Interviews] ,[Packets] ,[BComplete] ,[TQuantity] ,[TIncomplete] ,[TComplete] ,AngularCompleted,InterviewsCompleted, [RBalance] ,[NAddForms] ,[OldTQuantity] ,[OldTIncomplete] ,[OldTComplete] ,[OldRBalance] ,[OldNAddForms] ,[CreatedByUserID]  ,[FrmsRecDate] ,[TotalKeyedDate] ,[BobcatDate] ,[BobcatName] ,[BobcatQuantity] ,[BobcatCreatedByUserID] ,[CitationDate] ,[CitationRegion] ,[CitationDisposition] ,[CitationCreatedByUserID] ,[ExoticFalDate] ,[ExoticFalName] ,[ExoticFalBatchNo] ,[ExoticFalCount] ,[ExoticFalCreatedByUserID] ,[ExpungementsDate] ,[ExpungementsName] ,[ExpungementsCount] ,[ExpungementsCreatedByUserID] ,[FurDLRDate] ,[FurDLRName] ,[FurDLRSequence] ,[FurDLRCount] ,[FurDLRCreatedByUserID] ,[MusselDate] ,[MusselName] ,[MusselStartSeq] ,[MusselEndSeq] ,[MusselCreatedByUserID] ,[TibrsDate] ,[TibrsName] ,[TibrsCount] ,[TibrsCreatedByUserID]) VALUES (@Action ,@AppID ,@AppName ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0,0,0 ,@BobcatCreatedByUserID ,@BobcatCreatedByUserID ,0 ,@BobcatDate ,@BobcatName ,@BobcatQuantity ,@BobcatCreatedByUserID ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,0)";

            //// Initialize the SqlCommand with the new SQL string and the connection information.
            //SqlCommand myCommand = new SqlCommand(insertCmd, myConnection);
            //// Create new parameters for the SqlCommand object 

            //myCommand.Parameters.Add("AppID", SqlDbType.Int).Value = Convert.ToInt32(AppID);
            //myCommand.Parameters.Add("Action", SqlDbType.NVarChar).Value = action.ToString();
            //myCommand.Parameters.Add("AppName", SqlDbType.NVarChar).Value = AppName.ToString();
            //myCommand.Parameters.Add("BobcatDate", SqlDbType.NVarChar).Value = BobcatDate.ToString();
            //myCommand.Parameters.Add("BobcatName", SqlDbType.NVarChar).Value = BobcatName.ToString();
            //myCommand.Parameters.Add("BobcatQuantity", SqlDbType.NVarChar).Value = BobcatQuantity.ToString();
            //myCommand.Parameters.Add("BobcatCreatedByUserID", SqlDbType.Int).Value = Convert.ToInt32(BobcatCreatedByUserID);


            //myCommand.Connection.Open();
            //myCommand.ExecuteNonQuery();
            //myCommand.Connection.Close();

        }

        protected void Listview1_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            //TextBox lblappid = (TextBox)Listview1.InsertItem.FindControl("lblappid");
            //TextBox lblappname = (TextBox)Listview1.InsertItem.FindControl("lblappname");
            //TextBox Labeluserid = (TextBox)Listview1.InsertItem.FindControl("Labeluserid");
            //TextBox QuantityTextBox = (TextBox)Listview1.InsertItem.FindControl("QuantityTextBox");
            //DropDownList ddlBobcatName = (DropDownList)Listview1.InsertItem.FindControl("ddlBobcatName");
            //TextBox DateTextBox = (TextBox)Listview1.InsertItem.FindControl("DateTextBox");

            //int AppID = 0;
            //string AppName = "";
            //string BobcatDate = "";
            //string BobcatName = "";
            //string BobcatQuantity = "";
            //int BobcatCreatedByUserID = 0;

            //AppID = Convert.ToInt32(lblappid.Text);
            //AppName = lblappname.Text.ToString();
            //BobcatDate = DateTextBox.Text.ToString();
            //BobcatName = ddlBobcatName.SelectedItem.ToString();
            //BobcatQuantity = QuantityTextBox.Text.ToString();
            //BobcatCreatedByUserID = Convert.ToInt32(Labeluserid.Text);



            //string action = "INSERT-BOBCATOTTER";
            //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            //// Build a SQL Insert statement string for all field values.      
            //String insertCmd = "INSERT INTO DWT.[AppDataLog] ([Action] ,[AppID] ,[AppName] ,[Region] ,[BatchNum] ,[QtyVerified] ,[Quantity] ,[Interviews] ,[Packets] ,[BComplete] ,[TQuantity] ,[TIncomplete] ,[TComplete] ,AngularCompleted,InterviewsCompleted, [RBalance] ,[NAddForms] ,[OldTQuantity] ,[OldTIncomplete] ,[OldTComplete] ,[OldRBalance] ,[OldNAddForms] ,[CreatedByUserID]  ,[FrmsRecDate] ,[TotalKeyedDate] ,[BobcatDate] ,[BobcatName] ,[BobcatQuantity] ,[BobcatCreatedByUserID] ,[CitationDate] ,[CitationRegion] ,[CitationDisposition] ,[CitationCreatedByUserID] ,[ExoticFalDate] ,[ExoticFalName] ,[ExoticFalBatchNo] ,[ExoticFalCount] ,[ExoticFalCreatedByUserID] ,[ExpungementsDate] ,[ExpungementsName] ,[ExpungementsCount] ,[ExpungementsCreatedByUserID] ,[FurDLRDate] ,[FurDLRName] ,[FurDLRSequence] ,[FurDLRCount] ,[FurDLRCreatedByUserID] ,[MusselDate] ,[MusselName] ,[MusselStartSeq] ,[MusselEndSeq] ,[MusselCreatedByUserID] ,[TibrsDate] ,[TibrsName] ,[TibrsCount] ,[TibrsCreatedByUserID]) VALUES (@Action ,@AppID ,@AppName ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,@BobcatQuantity ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0,0,0 ,@BobcatCreatedByUserID ,@BobcatCreatedByUserID ,0 ,@BobcatDate ,@BobcatName ,@BobcatQuantity ,@BobcatCreatedByUserID ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,'0' ,0 ,'0' ,'0' ,'0' ,0)";

            //// Initialize the SqlCommand with the new SQL string and the connection information.
            //SqlCommand myCommand = new SqlCommand(insertCmd, myConnection);
            //// Create new parameters for the SqlCommand object 

            //myCommand.Parameters.Add("AppID", SqlDbType.Int).Value = Convert.ToInt32(AppID);
            //myCommand.Parameters.Add("Action", SqlDbType.NVarChar).Value = action.ToString();
            //myCommand.Parameters.Add("AppName", SqlDbType.NVarChar).Value = AppName.ToString();
            //myCommand.Parameters.Add("BobcatDate", SqlDbType.NVarChar).Value = BobcatDate.ToString();
            //myCommand.Parameters.Add("BobcatName", SqlDbType.NVarChar).Value = BobcatName.ToString();
            //myCommand.Parameters.Add("BobcatQuantity", SqlDbType.NVarChar).Value = BobcatQuantity.ToString();
            //myCommand.Parameters.Add("BobcatCreatedByUserID", SqlDbType.Int).Value = Convert.ToInt32(BobcatCreatedByUserID);


            //myCommand.Connection.Open();
            //myCommand.ExecuteNonQuery();
            //myCommand.Connection.Close();
        }

        protected void ddlBobcatName_TextChanged(object sender, EventArgs e)
        {
            
          //  TextBox DateTextBox = (TextBox)ListView1.InsertItem.FindControl("DateTextBox");
           // DateTextBox.Text = System.DateTime.Now.ToShortDateString();
            TextBox Labeluserid = (TextBox)ListView1.InsertItem.FindControl("CreatedByUserIDTextBox");
            TextBox lblappid = (TextBox)ListView1.InsertItem.FindControl("AppIDTextBox");
            TextBox lblappname = (TextBox)ListView1.InsertItem.FindControl("AppNameTextBox");
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            Labeluserid.Text = MyUserID.ToString();

            //  string JobName
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int AppId = 0;
            string name = "";
            string AppName = "";

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DropDownList ddlbob = (DropDownList)ListView1.InsertItem.FindControl("ddlBobcatName");

            name = ddlbob.SelectedValue.ToString();
            AppName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (AppName.Length > 2 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this job name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    AppId = (int)ds1.Tables[0].Rows[0].ItemArray[0];
                    AppName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
            lblappid.Text = AppId.ToString();
            lblappname.Text = AppName.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"));
        }
    }
}