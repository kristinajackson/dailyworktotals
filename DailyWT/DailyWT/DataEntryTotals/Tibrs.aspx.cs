﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text.RegularExpressions;

namespace DailyWT.DataEntryTotals
{
    public partial class Tibrs : BSDialogs
    {
        SqlConnection myConnection;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
                    alert.Visible = false;
                    danger.Visible = false;
                    delete.Visible = false;

                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }


        protected void ListView6_DataBound(object sender, EventArgs e)
        {
            TextBox DateTextBox = (TextBox)ListView6.InsertItem.FindControl("DateTextBox");
            DateTextBox.Text = System.DateTime.Now.ToShortDateString();
            TextBox Labeluserid = (TextBox)ListView6.InsertItem.FindControl("Labeluserid");
            TextBox lblappid = (TextBox)ListView6.InsertItem.FindControl("lblappid");
            TextBox lblappname = (TextBox)ListView6.InsertItem.FindControl("lblappname");
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            Labeluserid.Text = MyUserID.ToString();


            //  string JobName
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int AppId = 0;
            string name = "";
            string AppName = "";

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DropDownList ddlbob = (DropDownList)ListView6.InsertItem.FindControl("DropDownList1");
            //DropDownList ddlbob = (DropDownList)ListView2.FindControl("ddlBobcatName");
            name = ddlbob.SelectedValue.ToString();
            AppName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (AppName.Length > 2 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this job name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    AppId = (int)ds1.Tables[0].Rows[0].ItemArray[0];
                    AppName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
            lblappid.Text = AppId.ToString();
            lblappname.Text = AppName.ToString();

        }

        protected void ListView6_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    alert.Visible = true;
                    danger.Visible = false;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    alert.Visible = false;
                    danger.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void ListView6_PagePropertiesChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
        }

        protected void ListView6_ItemDeleted(object sender, ListViewDeletedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    delete.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + delete.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    delete.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  Record was not deleted!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;

            }
        }

        protected void ListView6_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            //TextBox lblappid = (TextBox)ListView6.InsertItem.FindControl("lblappid");
            //TextBox lblappname = (TextBox)ListView6.InsertItem.FindControl("lblappname");
            //TextBox Labeluserid = (TextBox)ListView6.InsertItem.FindControl("Labeluserid");
            //TextBox CountTextBox = (TextBox)ListView6.InsertItem.FindControl("CountTextBox");
            //DropDownList DropDownList1 = (DropDownList)ListView6.InsertItem.FindControl("DropDownList1");
            //TextBox DateTextBox = (TextBox)ListView6.InsertItem.FindControl("DateTextBox");

            //DWUtility dwt = new DWUtility();

            //DataRow dr = null;
            //int ID = Convert.ToInt32(e.Keys["ID"]);
            //dr = dwt.GetTibrs(ID);

            
            //int AppID = 0;
            //string AppName = "";
            //string TibrsDate = "";
            //string TibrsName = "";
            //string TibrsCount = "";
            //int TibrsCreatedByUserID = 0;

            //AppID = Convert.ToInt32(lblappid.Text);
            //AppName = lblappname.Text.ToString();
            //TibrsDate = dr["Date"].ToString();
            //TibrsName = dr["Name"].ToString();
            //TibrsCount = dr["Count"].ToString();
            //TibrsCreatedByUserID = Convert.ToInt32(Labeluserid.Text);




            //string action = "DELETE-TIBRS";
            //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            //// Build a SQL Insert statement string for all field values.      
            //String insertCmd = "INSERT INTO DWT.AppDataLog(Action, AppID, AppName, Region, BatchNum, QtyVerified, Quantity, Interviews, Packets, BComplete, TQuantity, TIncomplete, TComplete, RBalance, NAddForms, OldTQuantity, OldTIncomplete, OldTComplete, OldRBalance, OldNAddForms, CreatedByUserID, FrmsRecDate, TotalKeyedDate, BobcatDate, BobcatName, BobcatQuantity, BobcatCreatedByUserID, CitationDate, CitationRegion, CitationCitations, CitationDisposition, CitationCreatedByUserID, ExoticFalDate, ExoticFalName, ExoticFalBatchNo, ExoticFalCount, ExoticFalCreatedByUserID, ExpungementsDate, ExpungementsName, ExpungementsCount, ExpungementsCreatedByUserID, FurDLRDate, FurDLRName, FurDLRSequence, FurDLRCount, FurDLRCreatedByUserID, MusselDate, MusselName, MusselStartSeq, MusselEndSeq, MusselCreatedByUserID, TibrsDate, TibrsName, TibrsCount, TibrsCreatedByUserID) VALUES (@Action, @AppID, @AppName, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @TibrsCreatedByUserID, @TibrsCreatedByUserID, getdate(),  '0', '0', '0', '0','0', '0', '0', '0', '0','0', '0', '0', '0', 0, '0', '0', '0', 0, '0', '0', '0', '0', 0,'0','0', '0', '0', 0, @TibrsDate, @TibrsName, @TibrsCount, @TibrsCreatedByUserID)";

            //// Initialize the SqlCommand with the new SQL string and the connection information.
            //SqlCommand myCommand = new SqlCommand(insertCmd, myConnection);
            //// Create new parameters for the SqlCommand object 

            //myCommand.Parameters.Add("AppID", SqlDbType.Int).Value = Convert.ToInt32(AppID);
            //myCommand.Parameters.Add("Action", SqlDbType.NVarChar).Value = action.ToString();
            //myCommand.Parameters.Add("AppName", SqlDbType.NVarChar).Value = AppName.ToString();
            //myCommand.Parameters.Add("TibrsDate", SqlDbType.NVarChar).Value = TibrsDate.ToString();
            //myCommand.Parameters.Add("TibrsName", SqlDbType.NVarChar).Value = TibrsName.ToString();
            //myCommand.Parameters.Add("TibrsCount", SqlDbType.NVarChar).Value = TibrsCount.ToString();
            //myCommand.Parameters.Add("TibrsCreatedByUserID", SqlDbType.Int).Value = Convert.ToInt32(TibrsCreatedByUserID);


            //myCommand.Connection.Open();
            //myCommand.ExecuteNonQuery();
            //myCommand.Connection.Close();
        }

        protected void ListView6_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            //TextBox lblappid = (TextBox)ListView6.InsertItem.FindControl("lblappid");
            //TextBox lblappname = (TextBox)ListView6.InsertItem.FindControl("lblappname");
            //TextBox Labeluserid = (TextBox)ListView6.InsertItem.FindControl("Labeluserid");
            //TextBox CountTextBox = (TextBox)ListView6.InsertItem.FindControl("CountTextBox");
            //DropDownList DropDownList1 = (DropDownList)ListView6.InsertItem.FindControl("DropDownList1");
            //TextBox DateTextBox = (TextBox)ListView6.InsertItem.FindControl("DateTextBox");


            //int AppID = 0;
            //string AppName = "";
            //string TibrsDate = "";
            //string TibrsName = "";
            //string TibrsCount = "";
            //int TibrsCreatedByUserID = 0;

            //AppID = Convert.ToInt32(lblappid.Text);
            //AppName = lblappname.Text.ToString();
            //TibrsDate = DateTextBox.Text.ToString();
            //TibrsName = DropDownList1.SelectedItem.ToString();
            //TibrsCount = CountTextBox.Text.ToString();
            //TibrsCreatedByUserID = Convert.ToInt32(Labeluserid.Text);



            //string action = "INSERT-TIBRS";
            //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            //// Build a SQL Insert statement string for all field values.      
            //String insertCmd = "INSERT INTO DWT.AppDataLog(Action, AppID, AppName, Region, BatchNum, QtyVerified, Quantity, Interviews, Packets, BComplete, TQuantity, TIncomplete, TComplete, AngularCompleted, InterviewsCompleted, RBalance, NAddForms, OldTQuantity, OldTIncomplete, OldTComplete, OldRBalance, OldNAddForms, CreatedByUserID, FrmsRecDate, TotalKeyedDate, BobcatDate, BobcatName, BobcatQuantity, BobcatCreatedByUserID, CitationDate, CitationRegion, CitationCitations, CitationDisposition, CitationCreatedByUserID, ExoticFalDate, ExoticFalName, ExoticFalBatchNo, ExoticFalCount, ExoticFalCreatedByUserID, ExpungementsDate, ExpungementsName, ExpungementsCount, ExpungementsCreatedByUserID, FurDLRDate, FurDLRName, FurDLRSequence, FurDLRCount, FurDLRCreatedByUserID, MusselDate, MusselName, MusselStartSeq, MusselEndSeq, MusselCreatedByUserID, TibrsDate, TibrsName, TibrsCount, TibrsCreatedByUserID) VALUES (@Action, @AppID, @AppName, 0, 0, 0, 0, 0, 0, 0, @TibrsCount, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @TibrsCreatedByUserID, @TibrsCreatedByUserID, getdate(),  '0', '0', '0', '0','0', '0', '0', '0', '0','0', '0', '0', '0', 0, '0', '0', '0', 0, '0', '0', '0', '0', 0,'0','0', '0', '0', 0, @TibrsDate, @TibrsName, @TibrsCount, @TibrsCreatedByUserID)";

            //// Initialize the SqlCommand with the new SQL string and the connection information.
            //SqlCommand myCommand = new SqlCommand(insertCmd, myConnection);
            //// Create new parameters for the SqlCommand object 

            //myCommand.Parameters.Add("AppID", SqlDbType.Int).Value = Convert.ToInt32(AppID);
            //myCommand.Parameters.Add("Action", SqlDbType.NVarChar).Value = action.ToString();
            //myCommand.Parameters.Add("AppName", SqlDbType.NVarChar).Value = AppName.ToString();
            //myCommand.Parameters.Add("TibrsDate", SqlDbType.NVarChar).Value = TibrsDate.ToString();
            //myCommand.Parameters.Add("TibrsName", SqlDbType.NVarChar).Value = TibrsName.ToString();
            //myCommand.Parameters.Add("TibrsCount", SqlDbType.NVarChar).Value = TibrsCount.ToString();
            //myCommand.Parameters.Add("TibrsCreatedByUserID", SqlDbType.Int).Value = Convert.ToInt32(TibrsCreatedByUserID);


            //myCommand.Connection.Open();
            //myCommand.ExecuteNonQuery();
            //myCommand.Connection.Close();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"));
        }
      
    }
}