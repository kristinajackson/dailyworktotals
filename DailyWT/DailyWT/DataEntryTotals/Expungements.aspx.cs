﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Windows;

namespace DailyWT.DataEntryTotals
{
    public partial class Expungements : BSDialogs
    {
        SqlConnection myConnection;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (!Page.IsPostBack)
                {
                    myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
                    alert.Visible = false;
                    danger.Visible = false;
                    delete.Visible = false;

                }
            }

            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }


        protected void ListView7_DataBound(object sender, EventArgs e)
        {
            TextBox DateTextBox = (TextBox)ListView7.InsertItem.FindControl("DateTextBox");
            TextBox Labeluserid = (TextBox)ListView7.InsertItem.FindControl("Labeluserid");
            TextBox lblappid = (TextBox)ListView7.InsertItem.FindControl("lblappid");
            TextBox lblappname = (TextBox)ListView7.InsertItem.FindControl("lblappname");
            DateTextBox.Text = System.DateTime.Now.ToShortDateString();
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            Labeluserid.Text = MyUserID.ToString();

            //  string AppName
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int AppId = 0;
            string name = "";
            string AppName = "";

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            DropDownList ddlbob = (DropDownList)ListView7.InsertItem.FindControl("DropDownList1");
            name = ddlbob.SelectedValue.ToString();
            AppName = string.IsNullOrEmpty(name) ? "" : Regex.Replace(name, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            if (AppName.Length > 2 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this job name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    AppId = (int)ds1.Tables[0].Rows[0].ItemArray[0];
                    AppName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();
                }
            }
            lblappid.Text = AppId.ToString();
            lblappname.Text = AppName.ToString();
        }

        protected void ListView7_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    alert.Visible = true;
                    danger.Visible = false;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    alert.Visible = false;
                    danger.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;
                e.KeepInInsertMode = true;
            }
        }

        protected void ListView7_PagePropertiesChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
        }

        protected void ListView7_ItemDeleted(object sender, ListViewDeletedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    delete.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + delete.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    delete.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  Record was not deleted!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;

            }
        }

        protected void ListView7_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {
            //TextBox lblappid = (TextBox)ListView7.InsertItem.FindControl("lblappid");
            //TextBox lblappname = (TextBox)ListView7.InsertItem.FindControl("lblappname");
            //TextBox DateTextBox = (TextBox)ListView7.InsertItem.FindControl("DateTextBox");
            //TextBox CountTextBox = (TextBox)ListView7.InsertItem.FindControl("CountTextBox");
            ////TextBox lblappname = (TextBox)ListView7.InsertItem.FindControl("lblappname");


            //DWUtility dwt = new DWUtility();

            //DataRow dr = null;
            //int ID = Convert.ToInt32(e.Keys["ID"]);
            //dr = dwt.GetExpungements(ID);


            //int AppID = 0;
            //string AppName = "";
            //string ExpungementsDate = "";
            //string ExpungementsName = "";
            //string ExpungementsCount = "";
            //int ExpungementsCreatedByUserID = 0;

            ////lblappid.Text = AppId.ToString();
            //// lblappname.Text = AppName.ToString();
            //AppID = Convert.ToInt32(lblappid.Text);
            //AppName = lblappname.Text.ToString();
            //ExpungementsDate = dr["Date"].ToString();
            //ExpungementsName = dr["Name"].ToString();
            //ExpungementsCount = dr["Count"].ToString();
            //ExpungementsCreatedByUserID = Convert.ToInt32(dr["CreatedByUserID"].ToString());



            //string action = "DELETE-EXPUNGEMENTS";
            //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            //// Build a SQL Insert statement string for all field values.      
            //String insertCmd = "INSERT INTO DWT.AppDataLog(Action, AppID, AppName, Region, BatchNum, QtyVerified, Quantity, Interviews, Packets, BComplete, TQuantity, TIncomplete, TComplete, RBalance, NAddForms, OldTQuantity, OldTIncomplete, OldTComplete, OldRBalance, OldNAddForms, CreatedByUserID, FrmsRecDate, TotalKeyedDate, BobcatDate, BobcatName, BobcatQuantity, BobcatCreatedByUserID, CitationDate, CitationRegion, CitationCitations, CitationDisposition, CitationCreatedByUserID, ExoticFalDate, ExoticFalName, ExoticFalBatchNo, ExoticFalCount, ExoticFalCreatedByUserID, ExpungementsDate, ExpungementsName, ExpungementsCount, ExpungementsCreatedByUserID, FurDLRDate, FurDLRName, FurDLRSequence, FurDLRCount, FurDLRCreatedByUserID, MusselDate, MusselName, MusselStartSeq, MusselEndSeq, MusselCreatedByUserID, TibrsDate, TibrsName, TibrsCount, TibrsCreatedByUserID) VALUES ('INSERT-APPDATA', @AppID, @AppName, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @ExpungementsCreatedByUserID, @ExpungementsCreatedByUserID, getdate(),  '0', '0', '0', '0','0', '0', '0', '0', '0','0', '0', '0', '0', 0, @ExpungementsDate, @ExpungementsName, @ExpungementsCount, @ExpungementsCreatedByUserID, '0', '0', '0', '0', 0, '0', '0', '0', '0', 0, '0', '0', '0', 0)";

            //// Initialize the SqlCommand with the new SQL string and the connection information.
            //SqlCommand myCommand = new SqlCommand(insertCmd, myConnection);
            //// Create new parameters for the SqlCommand object 

            //myCommand.Parameters.Add("AppID", SqlDbType.Int).Value = Convert.ToInt32(AppID);
            //myCommand.Parameters.Add("Action", SqlDbType.NVarChar).Value = action.ToString();
            //myCommand.Parameters.Add("AppName", SqlDbType.NVarChar).Value = AppName.ToString();
            //myCommand.Parameters.Add("ExpungementsDate", SqlDbType.NVarChar).Value = ExpungementsDate.ToString();
            //myCommand.Parameters.Add("ExpungementsName", SqlDbType.NVarChar).Value = ExpungementsName.ToString();
            //myCommand.Parameters.Add("ExpungementsCount", SqlDbType.NVarChar).Value = ExpungementsCount.ToString();
            //myCommand.Parameters.Add("ExpungementsCreatedByUserID", SqlDbType.Int).Value = Convert.ToInt32(ExpungementsCreatedByUserID);


            //myCommand.Connection.Open();
            //myCommand.ExecuteNonQuery();
            //myCommand.Connection.Close();

        }

        protected void ListView7_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            //TextBox lblappid = (TextBox)ListView7.InsertItem.FindControl("lblappid");
            //TextBox lblappname = (TextBox)ListView7.InsertItem.FindControl("lblappname");
            //TextBox DateTextBox = (TextBox)ListView7.InsertItem.FindControl("DateTextBox");
            //DropDownList Name = (DropDownList)ListView7.InsertItem.FindControl("DropDownList1");
            //TextBox CountTextBox = (TextBox)ListView7.InsertItem.FindControl("CountTextBox");
            //int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            ////DWUtility dwt = new DWUtility();

            ////DataRow dr = null;
            ////int ID =  Convert.ToInt32(e.Keys["ID"]);
            ////dr = dwt.GetExpungements(ID);


            //int AppID = 0;
            //string AppName = "";
            //string ExpungementsDate = "";
            //string ExpungementsName = "";
            //string ExpungementsCount = "";
            //int ExpungementsCreatedByUserID = 0;

            ////lblappid.Text = AppId.ToString();
            //// lblappname.Text = AppName.ToString();
            //AppID = Convert.ToInt32(lblappid.Text);
            //AppName = lblappname.Text.ToString();
            //ExpungementsDate = DateTextBox.Text.ToString();
            //ExpungementsName = Name.SelectedItem.ToString();
            //ExpungementsCount = CountTextBox.Text.ToString();
            //ExpungementsCreatedByUserID = Convert.ToInt32(MyUserID);



            //string action = "INSERT-EXPUNGEMENTS";
            //myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
            //// Build a SQL Insert statement string for all field values.      
            //String insertCmd = "INSERT INTO DWT.AppDataLog(Action, AppID, AppName, Region, BatchNum, QtyVerified, Quantity, Interviews, Packets, BComplete, TQuantity, TIncomplete, TComplete, AngularCompleted, InterviewsCompleted, RBalance, NAddForms, OldTQuantity, OldTIncomplete, OldTComplete, OldRBalance, OldNAddForms, CreatedByUserID, FrmsRecDate, TotalKeyedDate, BobcatDate, BobcatName, BobcatQuantity, BobcatCreatedByUserID, CitationDate, CitationRegion, CitationCitations, CitationDisposition, CitationCreatedByUserID, ExoticFalDate, ExoticFalName, ExoticFalBatchNo, ExoticFalCount, ExoticFalCreatedByUserID, ExpungementsDate, ExpungementsName, ExpungementsCount, ExpungementsCreatedByUserID, FurDLRDate, FurDLRName, FurDLRSequence, FurDLRCount, FurDLRCreatedByUserID, MusselDate, MusselName, MusselStartSeq, MusselEndSeq, MusselCreatedByUserID, TibrsDate, TibrsName, TibrsCount, TibrsCreatedByUserID) VALUES (@Action, @AppID, @AppName, 0, 0, 0, 0, 0, 0, 0, @ExpungementsCount, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, @ExpungementsCreatedByUserID, @ExpungementsCreatedByUserID, getdate(),  '0', '0', '0', '0','0', '0', '0', '0', '0','0', '0', '0', '0', 0, @ExpungementsDate, @ExpungementsName, @ExpungementsCount, @ExpungementsCreatedByUserID, '0', '0', '0', '0', 0, '0', '0', '0', '0', 0, '0', '0', '0', 0)";

            //// Initialize the SqlCommand with the new SQL string and the connection information.
            //SqlCommand myCommand = new SqlCommand(insertCmd, myConnection);
            //// Create new parameters for the SqlCommand object 

            //myCommand.Parameters.Add("AppID", SqlDbType.Int).Value = Convert.ToInt32(AppID);
            //myCommand.Parameters.Add("Action", SqlDbType.NVarChar).Value = action.ToString();
            //myCommand.Parameters.Add("AppName", SqlDbType.NVarChar).Value = AppName.ToString();
            //myCommand.Parameters.Add("ExpungementsDate", SqlDbType.NVarChar).Value = ExpungementsDate.ToString();
            //myCommand.Parameters.Add("ExpungementsName", SqlDbType.NVarChar).Value = ExpungementsName.ToString();
            //myCommand.Parameters.Add("ExpungementsCount", SqlDbType.NVarChar).Value = ExpungementsCount.ToString();
            //myCommand.Parameters.Add("ExpungementsCreatedByUserID", SqlDbType.Int).Value = Convert.ToInt32(ExpungementsCreatedByUserID);


            //myCommand.Connection.Open();
            //myCommand.ExecuteNonQuery();
            //myCommand.Connection.Close();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(Page.ResolveUrl(@"~/User/LandingPage.aspx"));
        }
      
    }
}