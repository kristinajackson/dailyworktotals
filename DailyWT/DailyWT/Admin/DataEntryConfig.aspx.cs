﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Admin
{
    public partial class DataEntryConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddForm
                        if (eventTarget == "AddApp")
                        {
                            AddApp();
                        }
                        else if (eventTarget == "Save")
                        {
                            if (ddlApp.SelectedIndex > 0)  // did they select one?
                            {
                                SaveChanges();
                            }
                            else
                            {
                                ShowPopUp("Error!", "Please selct an Application from the dropdown list");
                            }
                        }
                        else if (eventTarget == "ibRemoveJForm_Click")
                        {
                            ibRemoveJForm_Click();
                        }
                        else if (eventTarget == "SaveNew_Click")
                        {
                            SaveNew_Click();
                        }
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }
        protected void ddlApp_Selected(object sender, EventArgs e)
        {
            if (ddlApp.SelectedIndex > 0)  // did they select one?
            {
            //    LoadAppRemDDL();
                int AppID = Convert.ToInt32(ddlApp.SelectedValue);
                if (AppID > 0)
                {
                    LoadDDApp(AppID);
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }

            if (ddlApp.SelectedIndex <= 0)
            {
                CreateForm();
 
            }
        }
        protected void SaveChanges()
        {
            if (ddlApp.SelectedIndex > 0)  // is an app selected?
            {
                
                int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                int AppID = Convert.ToInt32(ddlApp.SelectedValue);
                if (AppID > 0)
                {
                    DWUtility dwt = new DWUtility();
                    AppConfigInfo aci = new AppConfigInfo();

                    // get the saved App configuration
                    aci = dwt.GetAppByID(AppID);

                    if (aci != null)
                    {
                        // get the checked values from the screen
                        bool Region = cbRegion.Checked;
                        bool BatchNum = cbBatchNum.Checked;
                        bool Packets = cbPackets.Checked;
                        bool VerifiedQuantity = cbVerQuantity.Checked;
                        bool TotalForms = cbTotalForms.Checked;
                        bool Interviews = cbNumberInterview.Checked;
                        bool Quantity = cbQuantity.Checked;


                        // now save the changes
                        int status = dwt.UpsertAppConfig(AppID, aci.IsAppUsed, aci.AppName, Region, BatchNum, Packets, Quantity, VerifiedQuantity, TotalForms, Interviews, MyUserID);
                        if (status > 0)
                        {
                            ShowPopUp("Success!", "The application was updated successfully.");
                        }
                        else
                        {
                            ShowPopUp("Error!", "Application could not be updated. Error = " + status.ToString() + " Please show the administrator this error message.");
                        }
                    }
                    else
                    {
                        ShowPopUp("Error!", "The application could not be retrieved. Please try again.");
                    }
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            else
            {
                ShowPopUp("Error!", "Please select an application from dropdown menu.");
            }
        }
        protected void LoadDDApp(int AppID)
        {
            DWUtility dwt = new DWUtility();

            AppConfigInfo aci = new AppConfigInfo();
            aci = dwt.GetAppByID(AppID);

            if (aci != null)
            {
                cbRegion.Checked = aci.Region;
                cbBatchNum.Checked = aci.BatchNum;
                cbPackets.Checked = aci.Packets;
                cbVerQuantity.Checked = aci.VerifiedQuantity;
                cbTotalForms.Checked = aci.TQuantity;
                cbNumberInterview.Checked = aci.Interviews;
                cbQuantity.Checked = aci.Quantity;

            }
            else
            {
                ShowPopUp("Error!", "The application you selected could not be retrieved. Please try again.");
            }

        }
        protected void LoadAppRemDDL()
        {
            //DWUtility dwt = new DWUtility();

            
            //int AppID = 0;
            //AppID = Convert.ToInt32(ddlApp.SelectedValue);

            //ddlapprem.Items.Clear();       // clear in case it's already populated
            //ddlapprem.DataSource = dwt.GetAppConfigByApp(AppID);
            //ddlapprem.DataTextField = "AppName";
            //ddlapprem.DataValueField = "AppID";
            //ddlapprem.DataBind();
            //ddlapprem.Items.Insert(0, " -- Select Application -- ");



        }
        protected void CreateForm()
        {
            LoadAppDDL();
            LoadAppRemDDL();
            //ddlapprem.Items.Clear();  
            // default to unchecked
            cbRegion.Checked = false;
            cbBatchNum.Checked = false;
            cbPackets.Checked = false;
            cbVerQuantity.Checked = false;
            cbTotalForms.Checked = false;
            cbNumberInterview.Checked = false;
            cbQuantity.Checked = false;

        }
        protected void LoadAppDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;

            ddlApp.Items.Clear();       // clear in case it's already populated
            ddlApp.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApp.DataTextField = "AppName";
            ddlApp.DataValueField = "AppID";
            ddlApp.DataBind();
            ddlApp.Items.Insert(0, " -- Select Application -- ");


            ddlapprem.Items.Clear();       // clear in case it's already populated
            ddlapprem.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlapprem.DataTextField = "AppName";
            ddlapprem.DataValueField = "AppID";
            ddlapprem.DataBind();
            ddlapprem.Items.Insert(0, " -- Select Application -- ");
        }

        protected void AddApp()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            string tbAddApp1 = tbAddApp.Text.ToString();
            string AppName = string.IsNullOrEmpty(tbAddApp1) ? "" : Regex.Replace(tbAddApp1, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();
            if (AppName.Length > 1 && AppName.Length < 51)
            {
                AppName = AppName.ToUpper();
                // check if this App name already exists
                ds1 = dwt.GetAppByName(AppName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    ShowPopUp("Error!", "You entered an existing application name. Please try again.");
                }
                else
                {
                    // new App, so AppID=0, AppIsUsed=true, Region=false, BatchNum=false, Packets=false
                    int status = dwt.UpsertAppConfig(0, true, AppName, false, false, false, false, false,false, false, MyUserID);
                    if (status < 1)
                    {
                        ShowPopUp("Error!", "Application could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                    }
                    else
                    {
                        // recreate the form
                        CreateForm();
                        tbAddApp.Text = "";
                        ShowPopUp("Success!", "Application " + AppName +" was added successfully!<br /><br />Do not forget to configure the Region, Batch#, and Packets options!");
                    }
                }
            }
            else
            {
                ShowPopUp("Error!", "You entered an invalid name for the new Application. Please try again.");
            }
            return;
        }

        protected void ibRemoveJForm_Click()
        {

            if (ddlapprem.SelectedIndex > 0)
            {
                int AppID = 0, status = 0;
                //declare variables
                AppID = Convert.ToInt32(ddlapprem.SelectedValue);
                if (AppID > 0)
                {
                    //Run stored prodcedure
                    DWUtility dwt = new DWUtility();
                    status = dwt.DeleteAppConfig(AppID);
                    switch (status)
                    {
                        case 0:

                            ShowPopUp("Error!", "Application could not be removed. Error = " + status.ToString() + " Please show the administrator this error message.");
                            break;
                        case 1:

                            CreateForm();

                            ShowPopUp("Success!", "This Application  was successfully Removed.");
                            break;
                        default:
                            Console.WriteLine("Invalid Please try again");
                            break;
                    }
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }

        protected void SaveRemove_Click(object sender, EventArgs e)
        {
            string FormName = ddlapprem.SelectedItem.ToString().Length < 1 ? "0" : ddlapprem.SelectedItem.ToString();
            DWUtility dwt = new DWUtility();

            ShowPopUpOkCancel("Delete?", "Are you sure want to delete this application? <br/><br/>Application: " + FormName + "<br/><br/>", "ibRemoveJForm_Click", "");

        }

        protected void SaveNew_Click()
        {
            string tbAddApp1 = tbAddApp.Text.ToString();
            string AppName = string.IsNullOrEmpty(tbAddApp1) ? "" : Regex.Replace(tbAddApp1, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

                AppName = AppName.ToUpper();
            
                ShowPopUpOkCancel("New Application?", "Are you sure want to add this new application? <br/><br/>Application: " + AppName + "<br/><br/>", "AddApp", "");

        }

        protected void ShowPopUpOkCancel(string header, string message, string routine, string subroutine)
        {
            // The ShowPopUpOkCancel method creates a javascript function so the generic bootstrap dialog boxes can be used
            // The script created is called OkCancel. It takes 2 parameters:
            //  head: this is the text shown in the dialog box header - header is passed to this parameter
            //  mess: this is the text shown in the dialog box body - message is passed to this parameter
            //  Note: if the script has already been created, it is not recreated, it is reused.
            //
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("function OkCancel(head, mess, newpage) {BootstrapDialog.show({");
            sb.Append("size: BootstrapDialog.SIZE_SMALL,");
            sb.Append("type: BootstrapDialog.TYPE_DANGER,");
            sb.Append("title: head,");
            sb.Append("message: mess,");
            sb.Append("draggable: true,");
            sb.Append("buttons: [{");
            sb.Append("cssClass: 'btn-danger',");
            sb.Append("label: '&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;',");
            sb.Append("action: function (dialogItself) {__doPostBack('" + routine + "', '" + subroutine + "');}},");
            sb.Append("{cssClass: 'btn-success',");
            sb.Append("label: 'Cancel',");
            sb.Append("action: function (dialogItself) {dialogItself.close();}");
            sb.Append("}]");
            sb.Append("});");
            sb.Append("};");
            sb.Append("</script>");

            //Render the function definition.            
            if (!ClientScript.IsClientScriptBlockRegistered("JSScriptBlock"))
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptBlock", sb.ToString());
            }

            //Render the function invocation. 
            string funcCall = "<script language='javascript'>OkCancel('" + header + "','<b>" + message + "</b>');</script>";

            if (!ClientScript.IsStartupScriptRegistered("JSScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "JSScript", funcCall);
            }
        }
    
    }
}