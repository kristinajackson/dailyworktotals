﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Admin
{
    public partial class TimedWorkConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddForm
                        if (eventTarget == "AddTWork")
                        {
                            AddTWork();
                        }
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }
        protected void AddTWork()
        {
            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            string tbAddTWork = Request.Form["tbAddTWork"].ToString();
            string TWorkName = string.IsNullOrEmpty(tbAddTWork) ? "" : Regex.Replace(tbAddTWork, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();
            TWorkName = TWorkName.ToUpper();

            if (TWorkName.Length > 5 && TWorkName.Length < 51)
            {
                // check if this form name already exists
                ds1 = dwt.GetTWorkByName(TWorkName);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    ShowPopUp("Error!", "You entered an existing Timed Work Activity. Please try again.");
                }
                else
                {
                    // new actvitity, so TWorkID=0, IsTWorkUsed=true
                    int status = dwt.UpsertTWorkConfig(0, true, TWorkName, MyUserID);
                    if (status < 1)
                    {
                        ShowPopUp("Error!", "Timed Work Activity could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                    }
                    else
                    {
                        // recreate the form
                        CreateForm();

                        ShowPopUp("Success!", "Timed Work Activity was added successfully!");
                    }
                }
            }
            else
            {
                ShowPopUp("Error!", "You entered an invalid name for the new Timed Work Activity. Please try again.");
            }
            return;
        }
        protected void CreateForm()
        {
            DWUtility dwt = new DWUtility();

            bool IsTWorkUsed = true;

            ddlTWork.Items.Clear();       // clear in case it's already populated
            ddlTWork.DataSource = dwt.GetTWorkConfig(IsTWorkUsed);
            ddlTWork.DataTextField = "TWorkName";
            ddlTWork.DataValueField = "TWorkID";
            ddlTWork.DataBind();
            ddlTWork.Items.Insert(0, " -- Select Work Activity -- ");
        }
    }
}