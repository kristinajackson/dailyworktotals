﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Text;
using System.Web.Services;


namespace DailyWT.Admin
{
    public partial class Shipping : BSDialogs
    {
        SqlConnection myConnection;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {                    
                        myConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["twraConnString"].ConnectionString);
                        alert.Visible = false;
                        danger.Visible = false;
                        delete.Visible = false;
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }



        protected void ListView1_DataBound(object sender, EventArgs e)
        {
            TextBox ActionTextBox = (TextBox)ListView1.InsertItem.FindControl("ActionTextBox");

            int MyUserID = Convert.ToInt32(Session["MyUserID"]);
            TextBox CreatedByUserIDTextBox = (TextBox)ListView1.InsertItem.FindControl("CreatedByUserIDTextBox");


            DropDownList ddlForm = (DropDownList)ListView1.InsertItem.FindControl("ddlForm");

            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlForm.DataTextField = "FormName";
            ddlForm.DataValueField = "FormID";
            ddlForm.DataBind();
            ddlForm.Items.Insert(0, "- Select Job  -");


            ActionTextBox.Text = "INSERT-SHIP";

            CreatedByUserIDTextBox.Text = Convert.ToInt32(MyUserID).ToString();


        }


        protected void ddlForm_SelectedIndexChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            int FormID = 0;
            string FormName = "";
            TextBox FormNameTextBox = (TextBox)ListView1.InsertItem.FindControl("FormNameTextBox");
            DropDownList ddlForm = (DropDownList)ListView1.InsertItem.FindControl("ddlForm");
            TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");

            FormNameTextBox.Text = ddlForm.SelectedItem.ToString();

            DWUtility dwt = new DWUtility();

            DataSet ds1 = new DataSet();

             FormName = string.IsNullOrEmpty(FormNameTextBox.Text) ? "" : Regex.Replace(FormNameTextBox.Text, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();

            ds1 = dwt.GetFormByName(FormName);

            FormID = (int)ds1.Tables[0].Rows[0].ItemArray[0];
            FormIDTextBox.Text = Convert.ToInt32(FormID).ToString();

            SetFocus(ddlForm);

        }





        protected void InsertButton_Click(object sender, EventArgs e)
        {
            DWUtility dwt = new DWUtility();
            DataSet ds1 = new DataSet();
            DataSet ds2 = new DataSet();
            
            TextBox ShippingdateTextBox = (TextBox)ListView1.InsertItem.FindControl("ShippingdateTextBox");
            TextBox DateReceivedTextBox = (TextBox)ListView1.InsertItem.FindControl("DateReceivedTextBox");
            TextBox ShippingNumTextBox = (TextBox)ListView1.InsertItem.FindControl("ShippingNumTextBox");
            TextBox ActionTextBox = (TextBox)ListView1.InsertItem.FindControl("ActionTextBox");
            TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");
            TextBox FormNameTextBox = (TextBox)ListView1.InsertItem.FindControl("FormNameTextBox");
            TextBox PurchaseOrderTextBox = (TextBox)ListView1.InsertItem.FindControl("PurchaseOrderTextBox");
            TextBox TotalBoxesTextBox = (TextBox)ListView1.InsertItem.FindControl("TotalBoxesTextBox");
            TextBox TotalFormsTextBox = (TextBox)ListView1.InsertItem.FindControl("TotalFormsTextBox");
            TextBox BegSeqTextBox = (TextBox)ListView1.InsertItem.FindControl("BegSeqTextBox");
            TextBox EndSeqTextBox = (TextBox)ListView1.InsertItem.FindControl("EndSeqTextBox");
            DropDownList FormLocationTextBox = (DropDownList)ListView1.InsertItem.FindControl("FormLocationTextBox");
            TextBox CommentTextBox = (TextBox)ListView1.InsertItem.FindControl("CommentTextBox");
            TextBox CreatedByUserIDTextBox = (TextBox)ListView1.InsertItem.FindControl("CreatedByUserIDTextBox");

            // Check that if they entered anything. If any of them
            if (ShippingdateTextBox.Text.Length > 0 && DateReceivedTextBox.Text.Length > 0 && ShippingNumTextBox.Text.Length > 0 && PurchaseOrderTextBox.Text.Length > 0)
            {

                string BegSeq, EndSeq,  Comment, JobName;
                int status, BegAmt, EndAmt,  FormID,  MyUserID, CreatedByUserID, JobID;
                bool IsUsed;
                BegSeq = BegSeqTextBox.Text.ToString();
                EndSeq = EndSeqTextBox.Text.ToString();
                Comment = CommentTextBox.Text.ToString();
                BegAmt = Convert.ToInt32(TotalFormsTextBox.Text);
                EndAmt = Convert.ToInt32(TotalFormsTextBox.Text);
                FormID = Convert.ToInt32(FormIDTextBox.Text);
                MyUserID = Convert.ToInt32(Session["MyUserID"]);
                CreatedByUserID = MyUserID;
                IsUsed = true;

                ds1 = dwt.GetJobByJobID(FormID);

                if (ds1.Tables[0].Rows.Count > 0)
                {
                    JobID = (int)ds1.Tables[0].Rows[0].ItemArray[1];
                    

                    JobName = ds1.Tables[0].Rows[0].ItemArray[2].ToString();

                    DataRow dr = null;
                    FormConfigInfo fci = new FormConfigInfo();
                    fci = dwt.GetFormByID(FormID);
                    dr = dwt.GetFormDataByID(FormID);

                    if (dr != null)
                    {
                        int OldEndAmt = 0;
                        int OldBegAmt = 0;
                        int.TryParse(dr["EndAmt"].ToString(), out OldEndAmt);
                        int.TryParse(dr["BegAmt"].ToString(), out OldBegAmt);
                        
                        EndAmt = OldEndAmt + EndAmt;
                        BegAmt = OldBegAmt + BegAmt;

                        if (fci.Sequence)
                        {
                            int OldEndSeq = 0; int.TryParse(dr["EndSeq"].ToString(), out OldEndSeq);
                            int nbs = OldEndSeq + Convert.ToInt32(EndSeq);
                            EndSeq = nbs.ToString();
                        }
                        
                    }

              
                    status = dwt.UpsertShipData(FormID, IsUsed, BegAmt, EndAmt, BegSeq, EndSeq, MyUserID);

                }
                else
                {
                    delete.Visible = false;
                    alert.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  This jobname does not have a formtype. Please add a formtype to this jobname!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                    ShowPopUp("Error!", "This jobname does not have a formtype.<br/><br/> Please add a formtype to this jobname");
                    //return;
                }
            }
            else
            {
                delete.Visible = false;
                alert.Visible = false;
                danger.Visible = true;
                danger.InnerText = "Error!  No data to enter!";
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                //return;
            }
        }


        protected void ListView1_ItemDeleting(object sender, ListViewDeleteEventArgs e)
        {

        }            
    
        protected void ListView1_ItemInserted(object sender, ListViewInsertedEventArgs e)
        {
            DWUtility dwt = new DWUtility();
            DataSet ds1 = new DataSet();
            TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");
            int FormID = Convert.ToInt32(FormIDTextBox.Text);
             ds1 = dwt.GetJobByJobID(FormID);

             if (ds1.Tables[0].Rows.Count > 0)
             {
                 alert.Visible = false;
                 danger.Visible = false;
                 delete.Visible = false;
                 if (e.Exception == null)
                 {
                     if (e.AffectedRows == 1)
                     {   // Show success popup when applicant is saved and removes popup after 3 seconds
                         alert.Visible = true;
                         danger.Visible = false;

                         ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + alert.ClientID + "').style.display = 'none' },5000);", true);

                     }
                     else
                     {
                         alert.Visible = false;
                         danger.Visible = true;

                         ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                     }
                 }
                 else
                 {
                     //lbltop.Text = e.Exception.Message;
                     danger.Visible = true;
                     ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                     e.ExceptionHandled = true;
                     e.KeepInInsertMode = true;
                 }
             }
             else
             {
                 delete.Visible = false;
                 alert.Visible = false;
                 danger.Visible = true;
                 danger.InnerText = "Error!  This jobname does not have a formtype. Please add a formtype to this jobname!";
                 ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
             }
        }

        protected void ListView1_PagePropertiesChanged(object sender, EventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
        }

        protected void ListView1_ItemDeleted(object sender, ListViewDeletedEventArgs e)
        {
            alert.Visible = false;
            danger.Visible = false;
            delete.Visible = false;
            if (e.Exception == null)
            {
                if (e.AffectedRows == 1)
                {   // Show success popup when applicant is saved and removes popup after 3 seconds
                    delete.Visible = true;

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + delete.ClientID + "').style.display = 'none' },5000);", true);

                }
                else
                {
                    delete.Visible = false;
                    danger.Visible = true;
                    danger.InnerText = "Error!  Record was not deleted!";
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                }
            }
            else
            {
                //lbltop.Text = e.Exception.Message;
                danger.Visible = true;
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "window.setTimeout(function() { document.getElementById('" + danger.ClientID + "').style.display = 'none' },5000);", true);
                e.ExceptionHandled = true;

            }
        }

        protected void ListView1_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
            DWUtility dwt = new DWUtility();
            DataSet ds1 = new DataSet();
            TextBox FormIDTextBox = (TextBox)ListView1.InsertItem.FindControl("FormIDTextBox");
            int FormID = Convert.ToInt32(FormIDTextBox.Text);
            ds1 = dwt.GetJobByJobID(FormID);

            if (ds1.Tables[0].Rows.Count > 0)
            {
            }
            else
            {
                e.Cancel = true;
            }
        }

    }
}

