﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Admin
{
  
    public partial class FormConfig : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {
                    if (!Page.IsPostBack)
                    {
                        CreateForm();
                    }
                    else
                    {
                        string eventTarget = this.Request["__EVENTTARGET"];     // event name: AddForm
                        if (eventTarget == "AddForm")
                        {
                            AddForm();
                        }
                        else if (eventTarget == "Save")
                        {
                            if (ddlForm.SelectedIndex > 0)  // did they select one?
                            {
                                SaveChanges();
                            }
                        }
                        else if (eventTarget == "RemoveForm")
                        {
                            if (ddlFormsRemove.SelectedIndex > 0)  // did they select one?
                            {
                                btnRemoveFormClick();
                            }
                        }
                        else if (eventTarget == "SaveRemove_Click")
                        {
                            if (ddlFormsRemove.SelectedIndex > 0)  // did they select one?
                            {
                                SaveRemove_Click();
                            }
                        }
                       
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }
        protected void ddlForm_Selected(object sender, EventArgs e)
        {
            if (ddlForm.SelectedIndex > 0)  // did they select one?
            {
                
                int FormID = Convert.ToInt32(ddlForm.SelectedValue);
                if (FormID > 0)
                {
                    
                    LoadDDForm(FormID);
                    LoadAFormDDL();//remove form
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
            if (ddlForm.SelectedIndex <= 0)
            {
                CreateForm();
            }
        }
        protected void SaveChanges()
        {
            if (ddlForm.SelectedIndex > 0)  // is a form selected?
            {
                
                int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                int FormID = Convert.ToInt32(ddlForm.SelectedValue);
                if (FormID > 0)
                {
                    DWUtility dwt = new DWUtility();
                    FormConfigInfo fci = new FormConfigInfo();

                    // get the saved form configuration
                    fci = dwt.GetFormByID(FormID);

                    if (fci != null)
                    {
                        // get the checked values from the screen
                        bool ShipInfo = cbShipInfo.Checked;
                        bool Sequence = cbSequence.Checked;

                        // now save the changes
                        int status = dwt.UpsertFormConfig(FormID, fci.IsFormUsed, fci.FormName, ShipInfo, Sequence, MyUserID);
                        if (status > 0)
                        {
                            CreateForm();
                            ShowPopUp("Success!", "The job specifications was updated successfully.");
                        }
                        else
                        {
                            ShowPopUp("Error!", "Job could not be updated. Error = " + status.ToString() + " Please show the administrator this error message.");
                        }
                    }
                    else
                    {
                        ShowPopUp("Error!", "The job could not be retrieved. Please try again.");
                    }
                }
                else
                {
                    ShowPopUp("Error!", "The dropdown was not valid. Please try reloading this application and try again.");
                }
            }
        }
        protected void LoadDDForm(int FormID)
        {
            DWUtility dwt = new DWUtility();

            FormConfigInfo fci = new FormConfigInfo();
            fci = dwt.GetFormByID(FormID);

            if (fci != null)
            {
                cbShipInfo.Checked = fci.ShipInfo;
                cbSequence.Checked = fci.Sequence;
            }
            else
            {
                ShowPopUp("Error!", "The job you selected could not be retrieved. Please try again.");
            }

        }
        protected void CreateForm()
        {
            LoadFormDDL();
            LoadAFormDDL();

            // default to unchecked
            cbShipInfo.Checked = false;
            cbSequence.Checked = false;
        }
        protected void LoadFormDDL()
        {
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlForm.Items.Clear();       // clear in case it's already populated
            ddlForm.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlForm.DataTextField = "FormName";
            ddlForm.DataValueField = "FormID";
            ddlForm.DataBind();
            ddlForm.Items.Insert(0, " -- Select Job -- ");
        }

        protected void LoadAFormDDL()
        {//for remove dropdown
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;
            ddlFormsRemove.Items.Clear();  // clear in case it's already populated
            ddlFormsRemove.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlFormsRemove.DataTextField = "FormName";
            ddlFormsRemove.DataValueField = "FormID";
            ddlFormsRemove.DataBind();
            ddlFormsRemove.Items.Insert(0, " -- Select Job -- ");

        }

        protected void AddForm()
        {
           if(Request.Form["tbAddForm"].Length >  0)
           {
                DWUtility dwt = new DWUtility();

                DataSet ds1 = new DataSet();

                int MyUserID = Convert.ToInt32(Session["MyUserID"]);

                string tbAddForm = Request.Form["tbAddForm"].ToString();
                string FormName = string.IsNullOrEmpty(tbAddForm) ? "" : Regex.Replace(tbAddForm, @"[^0-9a-zA-Z\ \-]{6,50}$", "").ToUpper();
                if (FormName.Length > 2 && FormName.Length < 51)
                {
                    FormName = FormName.ToUpper();
                    // check if this form name already exists
                    ds1 = dwt.GetFormByName(FormName);
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        ShowPopUp("Error!", "You entered an existing job name. Please try again.");
                    }
                    else
                    {
                    //    ShowPopUpOkCancel("Add Jobname?", "Are you sure you want to add new Jobname?<br /><br />", "", "Returnvalues");
                        // new form, so FormID=0, FormIsUsed=true, ShipInfo=false, Sequence=false
                        int status = dwt.UpsertFormConfig(0, true, FormName, false, false, MyUserID);
                        if (status < 1)
                        {
                            ShowPopUp("Error!", "Job could not be added. Error = " + status.ToString() + " Please show the administrator this error message.");
                        }
                        else
                        {
                            
                            // recreate the form
                            CreateForm();

                            ShowPopUp("Success!", "Job was added successfully!");
                        }
                    }
                }
                else
                {
                    ShowPopUp("Error!", "You entered an invalid name for the new job. Please try again.");
                }
                return;
            }
           else
           {
               ShowPopUp("Error!", "Please enter a Job Name, and click Add Job button!");
           }
           return;
        }

        protected void Returnvalues()
        {
            ShowPopUp("Ok!", "Job not saved!");
            return;
        }

        protected void btnRemoveFormClick()
        {
            if (ddlFormsRemove.SelectedIndex > 0)
            {
                int FormID = 0, status = 0;
                //declare variables
                FormID = Convert.ToInt32(ddlFormsRemove.SelectedValue);

                //Run stored prodcedure
                DWUtility dwt = new DWUtility();
                status = dwt.DeleteFormConfig(FormID);
                if (status > 0)
                {
                    // recreate the form
                    CreateForm();

                    // Notify the user that the file was deleted successfully.
                    ShowPopUpAndRD("Successful!", "This job  was successfully Removed.", "/Admin/FormConfig.aspx");
                }
                else
                {
                    ShowPopUpAndRD("Error!", "Your job specification could not be deleted. Please tell the administrator about this error message.", "/Admin/FormConfig.aspx");
                }
            }
        }

        protected void SaveRemove_Click()
        {
            string JobName = ddlFormsRemove.SelectedItem.ToString().Length < 1 ? "0" : ddlFormsRemove.SelectedItem.ToString();
            DWUtility dwt = new DWUtility();

            ShowPopUpOkCancel("Delete?", "Are you sure want to delete this Job ? <br/><br/>Job Name: " + JobName + "<br/><br/>", "RemoveForm", "");

        }

    }
}