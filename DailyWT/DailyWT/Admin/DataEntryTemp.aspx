﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataEntryTemp.aspx.cs" Inherits="DailyWT.Admin.DataEntryTemp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

      <asp:ScriptManager ID="ToolkitScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true">
    </asp:ScriptManager>
      <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
    <!-- Section 1 -->
    <div class="container" id="section1" >
        <div class="col-sm-25" >
        
        <div class=" panel panel-default">
            <br />
            <div class="panel-heading" >
                <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Print Job Config Module</span>
            </div>
            <div >
                <%--<div class="container">--%>
                <div class="panel-body">
                <div class="alert alert-danger col-md-10" runat="server" id="danger" visible="false" role="alert">
                    <strong>ERROR!</strong> Record was not saved. Please check your work
                </div>

                <div class="alert alert-success col-md-10" runat="server" id="alert" visible="false" role="alert">
                    <strong>Success!</strong> Record entered successfully.
                </div>
                      <div class="alert alert-warning col-md-10" runat="server" id="delete" visible="false" role="alert">
                    <strong>Success!</strong> Record deleted successfully.
                </div>
    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" DataKeyNames="ID">
        <AlternatingItemTemplate>
            <tr style="background-color: #FFFFFF;color: #284775;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                </td>
                <td>
                    <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                </td>
                <td>
                    <asp:Label ID="AppIDLabel" runat="server" Text='<%# Eval("AppID") %>' />
                </td>
                <td>
                    <asp:Label ID="AppNameLabel" runat="server" Text='<%# Eval("AppName") %>' />
                </td>
                <td>
                    <asp:Label ID="RegionLabel" runat="server" Text='<%# Eval("Region") %>' />
                </td>
                <td>
                    <asp:Label ID="BatchNumLabel" runat="server" Text='<%# Eval("BatchNum") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="QtyVerifiedCheckBox" runat="server" Checked='<%# Eval("QtyVerified") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>' />
                </td>
                <td>
                    <asp:Label ID="InterviewsLabel" runat="server" Text='<%# Eval("Interviews") %>' />
                </td>
                <td>
                    <asp:Label ID="PacketsLabel" runat="server" Text='<%# Eval("Packets") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="BCompleteCheckBox" runat="server" Checked='<%# Eval("BComplete") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Label ID="TQuantityLabel" runat="server" Text='<%# Eval("TQuantity") %>' />
                </td>
                <td>
                    <asp:Label ID="TIncompleteLabel" runat="server" Text='<%# Eval("TIncomplete") %>' />
                </td>
                <td>
                    <asp:Label ID="TCompleteLabel" runat="server" Text='<%# Eval("TComplete") %>' />
                </td>
                <td>
                    <asp:Label ID="AngularCompletedLabel" runat="server" Text='<%# Eval("AngularCompleted") %>' />
                </td>
                <td>
                    <asp:Label ID="InterviewsCompletedLabel" runat="server" Text='<%# Eval("InterviewsCompleted") %>' />
                </td>
                <td>
                    <asp:Label ID="RBalanceLabel" runat="server" Text='<%# Eval("RBalance") %>' />
                </td>
                <td>
                    <asp:Label ID="NAddFormsLabel" runat="server" Text='<%# Eval("NAddForms") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedByUserIDLabel" runat="server" Text='<%# Eval("CreatedByUserID") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedDateLabel" runat="server" Text='<%# Eval("CreatedDate") %>' />
                </td>
            </tr>
        </AlternatingItemTemplate>
       
        <EditItemTemplate>
            <tr style="background-color: #999999;">
                <td>
                    <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                    <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                </td>
                <td>
                    <asp:Label ID="IDLabel1" runat="server" Text='<%# Eval("ID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="AppIDTextBox" runat="server" Text='<%# Bind("AppID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="AppNameTextBox" runat="server" Text='<%# Bind("AppName") %>' />
                </td>
                <td>
                    <asp:TextBox ID="RegionTextBox" runat="server" Text='<%# Bind("Region") %>' />
                </td>
                <td>
                    <asp:TextBox ID="BatchNumTextBox" runat="server" Text='<%# Bind("BatchNum") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="QtyVerifiedCheckBox" runat="server" Checked='<%# Bind("QtyVerified") %>' />
                </td>
                <td>
                    <asp:TextBox ID="QuantityTextBox" runat="server" Text='<%# Bind("Quantity") %>' />
                </td>
                <td>
                    <asp:TextBox ID="InterviewsTextBox" runat="server" Text='<%# Bind("Interviews") %>' />
                </td>
                <td>
                    <asp:TextBox ID="PacketsTextBox" runat="server" Text='<%# Bind("Packets") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="BCompleteCheckBox" runat="server" Checked='<%# Bind("BComplete") %>' />
                </td>
                <td>
                    <asp:TextBox ID="TQuantityTextBox" runat="server" Text='<%# Bind("TQuantity") %>' />
                </td>
                <td>
                    <asp:TextBox ID="TIncompleteTextBox" runat="server" Text='<%# Bind("TIncomplete") %>' />
                </td>
                <td>
                    <asp:TextBox ID="TCompleteTextBox" runat="server" Text='<%# Bind("TComplete") %>' />
                </td>
                <td>
                    <asp:TextBox ID="AngularCompletedTextBox" runat="server" Text='<%# Bind("AngularCompleted") %>' />
                </td>
                <td>
                    <asp:TextBox ID="InterviewsCompletedTextBox" runat="server" Text='<%# Bind("InterviewsCompleted") %>' />
                </td>
                <td>
                    <asp:TextBox ID="RBalanceTextBox" runat="server" Text='<%# Bind("RBalance") %>' />
                </td>
                <td>
                    <asp:TextBox ID="NAddFormsTextBox" runat="server" Text='<%# Bind("NAddForms") %>' />
                </td>
                <td>
                    <asp:TextBox ID="CreatedByUserIDTextBox" runat="server" Text='<%# Bind("CreatedByUserID") %>' />
                </td>
                <td>
                    <asp:TextBox ID="CreatedDateTextBox" runat="server" Text='<%# Bind("CreatedDate") %>' />
                </td>
            </tr>
        </EditItemTemplate>
       
        <EmptyDataTemplate>
            <table runat="server" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                <tr>
                    <td>No data was returned.</td>
                </tr>
            </table>
        </EmptyDataTemplate>
      
        <ItemTemplate>
            <tr style="background-color: #E0FFFF;color: #333333;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                </td>
                <td>
                    <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                </td>
                <td>
                    <asp:Label ID="AppIDLabel" runat="server" Text='<%# Eval("AppID") %>' />
                </td>
                <td>
                    <asp:Label ID="AppNameLabel" runat="server" Text='<%# Eval("AppName") %>' />
                </td>
                <td>
                    <asp:Label ID="RegionLabel" runat="server" Text='<%# Eval("Region") %>' />
                </td>
                <td>
                    <asp:Label ID="BatchNumLabel" runat="server" Text='<%# Eval("BatchNum") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="QtyVerifiedCheckBox" runat="server" Checked='<%# Eval("QtyVerified") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>' />
                </td>
                <td>
                    <asp:Label ID="InterviewsLabel" runat="server" Text='<%# Eval("Interviews") %>' />
                </td>
                <td>
                    <asp:Label ID="PacketsLabel" runat="server" Text='<%# Eval("Packets") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="BCompleteCheckBox" runat="server" Checked='<%# Eval("BComplete") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Label ID="TQuantityLabel" runat="server" Text='<%# Eval("TQuantity") %>' />
                </td>
                <td>
                    <asp:Label ID="TIncompleteLabel" runat="server" Text='<%# Eval("TIncomplete") %>' />
                </td>
                <td>
                    <asp:Label ID="TCompleteLabel" runat="server" Text='<%# Eval("TComplete") %>' />
                </td>
                <td>
                    <asp:Label ID="AngularCompletedLabel" runat="server" Text='<%# Eval("AngularCompleted") %>' />
                </td>
                <td>
                    <asp:Label ID="InterviewsCompletedLabel" runat="server" Text='<%# Eval("InterviewsCompleted") %>' />
                </td>
                <td>
                    <asp:Label ID="RBalanceLabel" runat="server" Text='<%# Eval("RBalance") %>' />
                </td>
                <td>
                    <asp:Label ID="NAddFormsLabel" runat="server" Text='<%# Eval("NAddForms") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedByUserIDLabel" runat="server" Text='<%# Eval("CreatedByUserID") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedDateLabel" runat="server" Text='<%# Eval("CreatedDate") %>' />
                </td>
            </tr>
        </ItemTemplate>
        <LayoutTemplate>
            <table runat="server">
                <tr runat="server">
                    <td runat="server">
                        <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                            <tr runat="server" style="background-color: #E0FFFF;color: #333333;">
                                <th runat="server"></th>
                                <th runat="server">ID</th>
                                <th runat="server">AppID</th>
                                <th runat="server">AppName</th>
                                <th runat="server">Region</th>
                                <th runat="server">BatchNum</th>
                                <th runat="server">QtyVerified</th>
                                <th runat="server">Quantity</th>
                                <th runat="server">Interviews</th>
                                <th runat="server">Packets</th>
                                <th runat="server">BComplete</th>
                                <th runat="server">TQuantity</th>
                                <th runat="server">TIncomplete</th>
                                <th runat="server">TComplete</th>
                                <th runat="server">AngularCompleted</th>
                                <th runat="server">InterviewsCompleted</th>
                                <th runat="server">RBalance</th>
                                <th runat="server">NAddForms</th>
                                <th runat="server">CreatedByUserID</th>
                                <th runat="server">CreatedDate</th>
                            </tr>
                            <tr runat="server" id="itemPlaceholder">
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr runat="server">
                    <td runat="server" style="text-align: center;background-color: #5D7B9D;font-family: Verdana, Arial, Helvetica, sans-serif;color: #FFFFFF">
                        <asp:DataPager ID="DataPager1" runat="server">
                            <Fields>
                                <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                            </Fields>
                        </asp:DataPager>
                    </td>
                </tr>
            </table>
        </LayoutTemplate>
        <SelectedItemTemplate>
            <tr style="background-color: #E2DED6;font-weight: bold;color: #333333;">
                <td>
                    <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                </td>
                <td>
                    <asp:Label ID="IDLabel" runat="server" Text='<%# Eval("ID") %>' />
                </td>
                <td>
                    <asp:Label ID="AppIDLabel" runat="server" Text='<%# Eval("AppID") %>' />
                </td>
                <td>
                    <asp:Label ID="AppNameLabel" runat="server" Text='<%# Eval("AppName") %>' />
                </td>
                <td>
                    <asp:Label ID="RegionLabel" runat="server" Text='<%# Eval("Region") %>' />
                </td>
                <td>
                    <asp:Label ID="BatchNumLabel" runat="server" Text='<%# Eval("BatchNum") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="QtyVerifiedCheckBox" runat="server" Checked='<%# Eval("QtyVerified") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Label ID="QuantityLabel" runat="server" Text='<%# Eval("Quantity") %>' />
                </td>
                <td>
                    <asp:Label ID="InterviewsLabel" runat="server" Text='<%# Eval("Interviews") %>' />
                </td>
                <td>
                    <asp:Label ID="PacketsLabel" runat="server" Text='<%# Eval("Packets") %>' />
                </td>
                <td>
                    <asp:CheckBox ID="BCompleteCheckBox" runat="server" Checked='<%# Eval("BComplete") %>' Enabled="false" />
                </td>
                <td>
                    <asp:Label ID="TQuantityLabel" runat="server" Text='<%# Eval("TQuantity") %>' />
                </td>
                <td>
                    <asp:Label ID="TIncompleteLabel" runat="server" Text='<%# Eval("TIncomplete") %>' />
                </td>
                <td>
                    <asp:Label ID="TCompleteLabel" runat="server" Text='<%# Eval("TComplete") %>' />
                </td>
                <td>
                    <asp:Label ID="AngularCompletedLabel" runat="server" Text='<%# Eval("AngularCompleted") %>' />
                </td>
                <td>
                    <asp:Label ID="InterviewsCompletedLabel" runat="server" Text='<%# Eval("InterviewsCompleted") %>' />
                </td>
                <td>
                    <asp:Label ID="RBalanceLabel" runat="server" Text='<%# Eval("RBalance") %>' />
                </td>
                <td>
                    <asp:Label ID="NAddFormsLabel" runat="server" Text='<%# Eval("NAddForms") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedByUserIDLabel" runat="server" Text='<%# Eval("CreatedByUserID") %>' />
                </td>
                <td>
                    <asp:Label ID="CreatedDateLabel" runat="server" Text='<%# Eval("CreatedDate") %>' />
                </td>
            </tr>
        </SelectedItemTemplate>
    </asp:ListView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:twraConnString %>" DeleteCommand="DELETE FROM DWT.AppTempData WHERE (ID = @ID)" SelectCommand="SELECT DWT.AppTempData.* FROM DWT.AppTempData">
        <DeleteParameters>
            <asp:Parameter Name="ID" />
        </DeleteParameters>
    </asp:SqlDataSource>

                      <div class="panel-footer">
                     <asp:Button ID="btnSave" CssClass="btn btn-success btn-md submit-button " CommandName="Insert"  runat="server" Text="Approve" OnClick="btnSave_Click"/>
    <button type="button" id="btnCancel" class="btn btn-danger btn-md" onclick="NewPage('/User/LandingPage.aspx')">
        &nbsp;&nbsp;Cancel&nbsp;&nbsp;</button>&nbsp;&nbsp;
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="btnSave_Click"/>
 </div>
        </div>
                </div>
        </div>
    </div>
                                  
                                </ContentTemplate>
          </asp:UpdatePanel>
</asp:Content>
