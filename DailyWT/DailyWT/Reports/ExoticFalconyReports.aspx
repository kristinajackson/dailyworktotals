﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="ExoticFalconyReports.aspx.cs" Inherits="DailyWT.Reports.ExoticFalconyReports" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


    <!-- Section 1 -->
    <section class="container" id="section1" style="color: #000000; margin-left:-5%; width:1450px;" >
        <div class="col-lg-500">
            <div class="panel panel-default">
                <br />
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;User Standing Daily Totals Report Module

                    </span>
                </div>
                <div class="panel-body">
                    <div style="background-color: #f5f5f0;" runat="server" id="bodby">
                        <div runat="server" style="width:1000px; left:0px;" >
                                    <!---------  START DATE ---------->
                                    <div class="container" style="margin-left: 2%">
                            <div class="row">
                                <div class="form-group">
                                    <label class="v-align-tc">
                                        &nbsp;&nbsp;
                                        <asp:Label ID="Label1" runat="server" Text="Begin Date: "></asp:Label>
                                        <asp:TextBox ID="srtdatetxt" runat="server" Width="150px" ReadOnly="true" Enabled="False"></asp:TextBox>
                                        <asp:MaskedEditExtender ID="TFDMEE" runat="server" TargetControlID="srtdatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                                       <%-- <asp:CalendarExtender ID="TFDCE" runat="server" TargetControlID="srtdatetxt" Format="MM/dd/yyyy" PopupButtonID="TFDCI" />--%>
                                   <%--     <asp:Image ID="TFDCI" runat="server" ImageUrl="~/images/Calendar.png" />--%>
                                        <asp:RegularExpressionValidator ID="TFDRegExpV" runat="server" ControlToValidate="srtdatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                                        &nbsp;&nbsp;
                                   </label>

                                    <!---------  END DATE ---------->

                                    <asp:Label CssClass="v-align-tc" runat="server" Visible="false" >
                                        &nbsp;&nbsp;
                                        <asp:Label ID="Label2" runat="server" Text="End Date: "></asp:Label>
                                        <asp:TextBox ID="enddatetxt" runat="server" Width="150px"></asp:TextBox>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="enddatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="enddatetxt" Format="MM/dd/yyyy" PopupButtonID="Imageend" />
                                        <asp:Image ID="Imageend" runat="server" ImageUrl="~/images/Calendar.png" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="enddatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                                    </asp:Label></div></div></div><br />
                                    <!---------  REGION ---------->
                                       <div class="container" style="margin-left: 2%">
                            <div class="row">
                                <div class="form-group">
                                    <label class="v-align-tc">
                                        &nbsp;&nbsp; <asp:Label ID="Label3" runat="server" Text="Region: " ></asp:Label><asp:DropDownList ID="ddlRegion" runat="server">
                                            <asp:ListItem Value="%">-- Select Regions --</asp:ListItem><asp:ListItem Value="1">Region 1</asp:ListItem><asp:ListItem Value="2">Region 2</asp:ListItem><asp:ListItem Value="3">Region 3</asp:ListItem><asp:ListItem Value="4">Region 4</asp:ListItem></asp:DropDownList>&nbsp;&nbsp; </label><!---------  APPLICATION ----------><label class="v-align-tc">&nbsp;&nbsp; <asp:Label ID="Label4" runat="server" Text="Application: "></asp:Label
                                        ><asp:DropDownList ID="ddlApplication" runat="server" AppendDataBoundItems="true" ></asp:DropDownList>
                                        &nbsp;&nbsp; </label><!---------  BCompeted ----------><label class="v-align-tc">&nbsp;&nbsp; <asp:Label ID="Label6" runat="server" Text="Batch Completed: "></asp:Label><asp:DropDownList ID="ddlBComplete" runat="server">
                                            <asp:ListItem Value="%">All</asp:ListItem><asp:ListItem Value="1">True</asp:ListItem><asp:ListItem Value="0">False</asp:ListItem></asp:DropDownList>&nbsp;&nbsp; </label></div></div></div><br />

                                    <!---------  BATCH NUMBER ---------->
                                    <div class="container" style="margin-left: 2%">
                            <div class="row">
                                <div class="form-group">
                                  <label class="v-align-tc">&nbsp;&nbsp;<asp:Label ID="lblbatchnum" runat="server" Text="Batch Number: "></asp:Label><asp:TextBox ID="txtbatchnum" runat="server"></asp:TextBox>&nbsp;&nbsp; </label><!---------  Users ----------><label class="v-align-tc">&nbsp;&nbsp; <asp:Label ID="lbluser" runat="server" Text="User: "></asp:Label><asp:Dropdownlist ID="txtuser" runat="server" AppendDataBoundItems="true" >
                                     <asp:ListItem Text="Jackson, Kristina"></asp:ListItem></asp:Dropdownlist>&nbsp;&nbsp; </label><!---------  VERIFIED ----------><label class="v-align-tc">&nbsp;&nbsp; <asp:Label ID="Label5" runat="server" Text="Verified: "></asp:Label><asp:DropDownList ID="ddlverified" runat="server">
                                            <asp:ListItem Value="%">All</asp:ListItem><asp:ListItem Value="1">True</asp:ListItem><asp:ListItem Value="0">False</asp:ListItem></asp:DropDownList>&nbsp;&nbsp; </label
                                        ></div></div></div></div><br />
                             <div class="container" style="margin-left: 2%;" >
                            <div class="row">
                                <div class="form-group">
                                        <button id="Runbutton" runat="server" class="btn-success" onserverclick="Runbutton_Click"><span class="glyphicon glyphicon-repeat"></span> Run Report</button>&nbsp; <asp:Button ID="Clearbutton" runat="server" Text="Clear" CssClass="btn-danger" OnClick="Clearbutton_Click" />
                                        &nbsp;<button id="Backbutton" runat="server" class="btn-primary" onserverclick="Backbutton_Click"><span class="glyphicon glyphicon-chevron-left"></span>Back to Reports Menu</button></div></div></div></div><asp:TextBox ID="txtprivate" runat="server" Visible="false"></asp:TextBox><asp:TextBox ID="txtprivateuser" runat="server" Visible="false"></asp:TextBox><!---------  Print Label ----------><div class="row" style="margin-left: 2%">
                                <div style="margin-bottom: 10px; margin-top: 10px;" class="col-xs-10  form-group">
                                    <br />
                                    <asp:Label ID="lblPrintInstr" runat="server" Text="*Please Note: To Print the report, click on the Export drop down menu (the blue disk icon, third button from right ) --> Select Excel --> Open the file or save it to your computer --> From MS Excel go to File --> Print --> Select Landscape Orientation --> And change the No Scaling option (the last option) to Fit All Columns on One Page  --> Click on the Print button." ForeColor="Black" BackColor="#FFFF66" Font-Bold="True" Font-Names="Times New Roman" />
                                </div>
                            </div>
                   
                    <!---------  REPORT ---------->
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="100%" SizeToReportContent="True">
                        <LocalReport ReportEmbeddedResource="DailyWT.Reports.DataEntryAppReport.rdlc" ReportPath="Reports/DataEntryAppReport.rdlc"><DataSources>
                                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" /></DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DailyWT.App_Code.DataEntryAppTableAdapters.AppDataLogTableAdapter" >

                          <SelectParameters>
                            <asp:ControlParameter ControlID="enddatetxt" Name="enddate" Type="String" ConvertEmptyStringToNull="true" DefaultValue="12/31/3000" />
                            <asp:ControlParameter ControlID="srtdatetxt" Name="startdate" Type="String" ConvertEmptyStringToNull="true" DefaultValue="01/01/1000" />
                            <asp:ControlParameter ControlID="txtprivate" Name="appid" Type="String" ConvertEmptyStringToNull="true" DefaultValue="%"  />
                            <asp:ControlParameter ControlID="txtbatchnum" Name="batchnum" Type="String" ConvertEmptyStringToNull="true" DefaultValue="%"  />
                            <asp:ControlParameter ControlID="ddlRegion" Name="region" Type="String" ConvertEmptyStringToNull="true" DefaultValue="%" />
                            <asp:ControlParameter ControlID="txtprivateuser" Name="userid" Type="String" ConvertEmptyStringToNull="true" DefaultValue="%"/>
                            <asp:ControlParameter ControlID="ddlverified" Name="qtyverified" Type="String" ConvertEmptyStringToNull="true"  DefaultValue="%" />
                            <asp:ControlParameter ControlID="ddlBComplete" Name="btchnumber" Type="String" ConvertEmptyStringToNull="true" DefaultValue="%" />
                        </SelectParameters>
                    </asp:ObjectDataSource>


                </div>
            </div>
                
            </div>
    </section>

    <br />
</asp:Content>
