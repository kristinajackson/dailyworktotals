﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PrintTotals.aspx.cs" Inherits="DailyWT.Reports.PrintTotals" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <section class="container" id="section1" style="color: #000000; margin-left: -5%; height: 500px;">
                <div class="col-xs-13 col-xs-offset-1">
                    <div class="panel panel-default" style="height: 5000px;">
                        <br />
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Printing Totals Report Module
                            </span>
                        </div>
                        <div class="panel-body">

                            <div style="background-color: #f5f5f0;" runat="server" id="bodby">

                                <!---------  START DATE ---------->
                                <div class="container" style="margin-left: 2%">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="v-align-tc">
                                                &nbsp;&nbsp;Start Date: 
                    <asp:TextBox ID="srtdatetxt" runat="server"></asp:TextBox>
                                                <asp:MaskedEditExtender ID="TFDMEE" runat="server" TargetControlID="srtdatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                                                <asp:CalendarExtender ID="TFDCE" runat="server" TargetControlID="srtdatetxt" Format="MM/dd/yyyy" PopupButtonID="TFDCI" />
                                                <asp:Image ID="TFDCI" runat="server" ImageUrl="~/images/Calendar.png" />
                                                <asp:RegularExpressionValidator ID="TFDRegExpV" runat="server" ControlToValidate="srtdatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                                                &nbsp;&nbsp;</label>
                                        </div>
                                    </div>
                                </div>

                                <!---------  END DATE ---------->
                                <div class="container" style="margin-left: 2%">
                                    <div class="row ">
                                        <div class="form-group">
                                            <label class="v-align-tc">
                                                &nbsp;&nbsp;End Date:
                    <asp:TextBox ID="enddatetxt" runat="server"></asp:TextBox>
                                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="enddatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="enddatetxt" Format="MM/dd/yyyy" PopupButtonID="Imageend" />
                                                <asp:Image ID="Imageend" runat="server" ImageUrl="~/images/Calendar.png" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="enddatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!---------  Form Type ---------->
                                <div id="Div1"  class="container" style="margin-left: 2%" runat="server">
                                    <div class="row ">
                                        <div class="form-group">
                                            <label class="v-align-tc">&nbsp;&nbsp;FormType:&nbsp;&nbsp<asp:DropDownList ID="ddlform" OnSelectedIndexChanged="ddlform_SelectedIndexChanged" AutoPostBack="true" runat="server" AppendDataBoundItems="true"></asp:DropDownList></label>
                                        </div>
                                    </div>
                                    </div>
                                    <!---------  Job Name ---------->
                                    <div id="Div2" class="container" style="margin-left: 2%" runat="server">
                                        <div class="row ">
                                            <div class="form-group">
                                                <label class="v-align-tc">&nbsp;&nbsp;Job Name:&nbsp;&nbsp<asp:DropDownList ID="ddlJob" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlJob_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></label>
                                            </div>
                                        </div>
                                    </div>


                                    <!---------  BUTTONS ---------->
                                    <div class="container" style="margin-left: 2%">
                                        <div class="row">
                                            <div class="form-group">
                                                <button id="Runbutton" runat="server" class="btn-success" onserverclick="Runbutton_Click"><span class="glyphicon glyphicon-repeat"></span>Run Report</button>
                                                <asp:Button ID="Clearbutton" runat="server" Text="Reset" CssClass="btn-danger" OnClick="Clearbutton_Click" />
                                                <button id="Backbutton" runat="server" class="btn-primary" onserverclick="Backbutton_Click"><span class="glyphicon glyphicon-chevron-left"></span>Back to Reports Menu</button>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <asp:TextBox ID="txtprivate" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtformtype" runat="server" Visible="false"></asp:TextBox>
                            <div class="row" style="margin-left: 2%">
                                <div style="margin-bottom: 10px; margin-top: 10px;" class="col-xs-10  form-group">
                                    <br />
                                    <asp:Label ID="lblPrintInstr" runat="server" Text="*Please Note: To Print the report, click on the Export drop down menu (the blue disk icon, third button from right ) --> Select Excel --> Open the file or save it to your computer --> From MS Excel go to File --> Print --> Select Landscape Orientation --> And change the No Scaling option (the last option) to Fit All Columns on One Page  --> Click on the Print button." ForeColor="Black" BackColor="#FFFF66" Font-Bold="True" Font-Names="Times New Roman" />
                                </div>
                            </div>
                            <div class="row" style="margin-left: 2%">

                                <rsweb:ReportViewer ID="ReportViewerPrintTotals" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"  SizeToReportContent="True" Width="100%" Height="100%">
                                    <LocalReport ReportEmbeddedResource="DailyWT.Reports.PrintTotals.rdlc" ReportPath="Reports/PrintTotals.rdlc">
                                        <DataSources>
                                            <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                                        </DataSources>
                                    </LocalReport>
                                </rsweb:ReportViewer>
                               


                                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DailyWT.App_Code.PrintingReportTableAdapters.GetPrintReportTableAdapter">
                                     <SelectParameters>
                                        <asp:ControlParameter ControlID="srtdatetxt" Name="startdate" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="enddatetxt" Name="enddate" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="txtformtype" Name="jobid" PropertyName="Text" Type="String" />
                                        <asp:ControlParameter ControlID="txtprivate" Name="formid" PropertyName="Text" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                               


                            </div>
                        </div>
                    </div>
            </section>

            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
