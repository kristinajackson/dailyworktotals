﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Reports
{
    public partial class DailyPrintReport : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {

                    if (!Page.IsPostBack)
                    {
                        LoadDForm();
                        LoadDDJob();
                        LoadDate();
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }

        protected void LoadDForm()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlform.Items.Clear();       // clear in case it's already populated
            ddlform.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlform.DataTextField = "JobName";
            ddlform.DataValueField = "JobID";

            ddlform.DataBind();
            ddlform.Items.Insert(0, "-- Select Formtype --");

            LoadTextForm();
        }

        protected void LoadDDJob()
        {
            DWUtility dwt = new DWUtility();

            bool IsFormUsed = true;

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetFormConfig(IsFormUsed);
            ddlJob.DataTextField = "FormName";
            ddlJob.DataValueField = "FormID";
            ddlJob.DataBind();
            ddlJob.Items.Insert(0, " -- Select Job -- ");

            LoadText();

        }


        protected void Runbutton_Click(object sender, EventArgs e)
        {
            ReportViewerDailyPrint.LocalReport.Refresh();
        }

        protected void Backbutton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Reports/ReportsMain.aspx");
        }

        protected void Clearbutton_Click(object sender, EventArgs e)
        {
            LoadDForm();
            LoadDate();
            txtprivate.Text = "";
            txtformtype.Text = "";
            LoadDDJob();
            ReportViewerDailyPrint.LocalReport.Refresh();

        }

        protected void LoadText()
        {
            txtprivate.Text = "";
            txtprivate.Text = ddlJob.SelectedValue.ToString();

            if (ddlJob.SelectedIndex == 0)
            {
                txtprivate.Text = null;            
            }
            

        }
        protected void LoadTextForm()
        {
            txtformtype.Text = "";
            txtformtype.Text = ddlform.SelectedValue.ToString();

            if (ddlform.SelectedIndex == 0)
            {
                txtformtype.Text = null;
            }


        }
        protected void LoadDate()
        {
            srtdatetxt.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();
            enddatetxt.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();
        }

        protected void ddlJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadText();
        }

        protected void ddlform_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadTextForm();
        }


   
    }
}