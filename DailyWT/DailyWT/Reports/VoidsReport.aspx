﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="VoidsReport.aspx.cs" Inherits="DailyWT.Reports.VoidsReport" MaintainScrollPositionOnPostback="true" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <%--<br />
    <br />--%>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <section class="container" id="section1" style="color:#000000; height:500px; margin-left:-5%;" >
        <div class="col-xs-12 col-xs-offset-1"  >
            <div class="panel panel-default" style="height:5000px; ">
                <br />
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Voids Report Module

                    </span>
                </div>
                <div id=""></div>
                <div class="panel-body" style="height:500px;" >

                   <div style="background-color:#f5f5f0;" runat="server" id="bodby" >

                    <!---------  START DATE ---------->
                       <div class="container" style="margin-left:2%">
                       <div class="row">
                               <div class="form-group">
                    <label class="v-align-tc">&nbsp;&nbsp;Start Date: 
                    <asp:TextBox ID="srtdatetxt" runat="server"></asp:TextBox>
                    <asp:MaskedEditExtender ID="TFDMEE" runat="server" TargetControlID="srtdatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                    <asp:CalendarExtender ID="TFDCE" runat="server" TargetControlID="srtdatetxt" Format="MM/dd/yyyy" PopupButtonID="TFDCI" />
                    <asp:Image ID="TFDCI" runat="server" ImageUrl="~/images/Calendar.png" />
                    <asp:RegularExpressionValidator ID="TFDRegExpV" runat="server" ControlToValidate="srtdatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                               &nbsp;&nbsp;</label>
                               </div>
                           </div>
                           </div>
                               
                     <!---------  END DATE ---------->
                       <div class="container" style="margin-left:2%">
                       <div class="row ">
                               <div class="form-group">
                   <label class="v-align-tc">&nbsp;&nbsp;End Date:
                    <asp:TextBox ID="enddatetxt" runat="server" ></asp:TextBox>
                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="enddatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="enddatetxt" Format="MM/dd/yyyy" PopupButtonID="Imageend" />
                    <asp:Image ID="Imageend" runat="server" ImageUrl="~/images/Calendar.png" />
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="enddatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                            </label>       </div>
                                   </div>
                           </div>
                       <div class="container" style="margin-left:2%">
                              <div class="row ">
                               <div class="form-group">      
        <label class="v-align-tc">&nbsp;&nbsp;Form Type:&nbsp;&nbsp<asp:DropDownList ID="ddlJob" runat="server" AppendDataBoundItems="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                               </div>
                               </div>
                           </div>
                       <div class="container" style="margin-left:2%">
                                      <div class="row">
                               <div class="form-group">                    
                    <asp:Button ID="Runbutton" runat="server" Text="Run Report" CssClass="btn-success" OnClick="Runbutton_Click"/>
                    <asp:Button ID="Clearbutton" runat="server" Text="Clear" CssClass="btn-danger" OnClick="Clearbutton_Click" />
                    <asp:Button ID="Backbutton" runat="server" Text="Back" CssClass="btn-primary" OnClick="Backbutton_Click"/>
                                  
                                   </div> 
                               </div>
                           </div>
   </div>
               <asp:TextBox ID="txtprivate" runat="server" Visible="false"></asp:TextBox>  
                                                 <div class="row" style="margin-left:2%">
          <div style="margin-bottom: 10px; margin-top: 10px;" class="col-xs-10  form-group">
              <br />
        <asp:Label ID="lblPrintInstr" runat="server" Text="*Please Note: To Print the report, click on the Export drop down menu (the blue disk icon, third button from right ) --> Select Excel --> Open the file or save it to your computer --> From MS Excel go to File --> Print --> Select Landscape Orientation --> And change the No Scaling option (the last option) to Fit All Columns on One Page  --> Click on the Print button." ForeColor="Black" BackColor="#FFFF66" Font-Bold="True" Font-Names="Times New Roman" />
              </div>
    </div>   
                    <div class="container" style="margin-left:1%">
                    <rsweb:ReportViewer ID="ReportViewerVoids" runat="server" Font-Names="Verdana" Font-Size="8pt" SizeToReportContent="True" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="80%" Height="100%" PromptAreaCollapsed="True">
                        <LocalReport ReportEmbeddedResource="DailyWT.Reports.VoidsReport.rdlc" ReportPath="Reports/VoidsReport.rdlc">
                            <DataSources>
                                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                            </DataSources>
                        </LocalReport>
                    </rsweb:ReportViewer>
                    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="DailyWT.App_Code.VoidsTableAdapters.FormDataLogTableAdapter" OldValuesParameterFormatString="original_{0}" >
                        <SelectParameters>
                            <asp:ControlParameter ControlID="srtdatetxt" Name="startdate" PropertyName="Text" Type="String" ConvertEmptyStringToNull="true" DefaultValue="01/01/1000"/>
                            <asp:ControlParameter ControlID="enddatetxt" Name="enddate" PropertyName="Text" Type="String" ConvertEmptyStringToNull="true" DefaultValue="12/31/3000"/>
                            <asp:ControlParameter ControlID="txtprivate" Name="JobID" Type="String" ConvertEmptyStringToNull="true" />
                        </SelectParameters>                        
                    </asp:ObjectDataSource>
</div>
                    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
