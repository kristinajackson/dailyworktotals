﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;

namespace DailyWT.Reports
{
    public partial class ExoticFalconyReports : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // needed to handle postback generated from javascript
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {

                    if (!Page.IsPostBack)
                    {
                        LoadDDApp();
                        LoadDate();
                        LoadUsers();
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }
        }

        protected void LoadDDApp()
        {

            DWUtility dwt = new DWUtility();

            bool IsAppUsed = true;

            int MyUserID = 0;

            MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlApplication.Items.Clear();       // clear in case it's already populated
            ddlApplication.DataSource = dwt.GetAppConfig(IsAppUsed);
            ddlApplication.DataTextField = "AppName";
            ddlApplication.DataValueField = "AppID";

            ddlApplication.DataBind();
            ddlApplication.Items.Insert(0, "-- Select Application --");

            LoadText();

        }

        protected void LoadUsers()
        {
            DWUtility dwt = new DWUtility();

            bool IsActive = true;
            txtuser.Items.Clear();
            txtuser.DataSource = dwt.GetUserInfoByActiveID(IsActive);
            txtuser.DataTextField = "Name";
            txtuser.DataValueField = "UserID";

            txtuser.DataBind();
            txtuser.Items.Insert(0, "-- Select User --");
            LoadText();
        }


        protected void LoadText()
        {
            txtprivate.Text = "";
            txtprivate.Text = ddlApplication.SelectedValue.ToString();
            txtprivateuser.Text = "";
            txtprivateuser.Text = txtuser.SelectedValue.ToString();

            if (ddlApplication.SelectedIndex == 0)
            {
                char app = '%';
                txtprivate.Text = app.ToString();
            }

            if (txtuser.SelectedIndex == 0)
            {
                char app = '%';
                txtprivateuser.Text = app.ToString();
            }
        }

        protected void Runbutton_Click(object sender, EventArgs e)
        {
            LoadText();
            ReportViewer1.LocalReport.Refresh();
        }

        protected void Backbutton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Reports/ReportsMain.aspx");
        }

        protected void Clearbutton_Click(object sender, EventArgs e)
        {
         //   srtdatetxt.Text = null;
            //enddatetxt.Text = null;
            txtbatchnum.Text = null;
            txtprivate.Text = null;
            txtprivateuser.Text = null;
            ddlApplication.ClearSelection();
            ddlBComplete.ClearSelection();
            ddlverified.ClearSelection();
            ddlRegion.ClearSelection();
            txtuser.ClearSelection();
            LoadText();
            ReportViewer1.LocalReport.Refresh();

        }

        protected void LoadDate()
        {
            srtdatetxt.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();
            //enddatetxt.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();
        }
    }
}