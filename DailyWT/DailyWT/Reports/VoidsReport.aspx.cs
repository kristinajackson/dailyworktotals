﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DailyWT.Classes;


namespace DailyWT.Reports
{
    public partial class VoidsReport : BSDialogs
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["MyUserID"] != null)
            {
                if (Session["Admin"] != null)
                {

                    if (!Page.IsPostBack)
                    {
                        LoadDDJob();
                        LoadDate();
                    }
                }
                else
                {
                    ShowPopUpAndRD("Error!", "You must have Admin rights to access this page. Please check with the administrator if you need to use this page.", "/User/LandingPage.aspx");
                }
            }
            else
            {
                ShowPopUpAndRD("Error!", "Your session may have expired. Try this website again or tell the administrator about this error message.", "/User/Error.aspx");
            }

        }

        protected void LoadDDJob()
        {
            DWUtility dwt = new DWUtility();

            bool IsJobUsed = true;
            int MyUserID = Convert.ToInt32(Session["MyUserID"]);

            ddlJob.Items.Clear();       // clear in case it's already populated
            ddlJob.DataSource = dwt.GetJobConfig(IsJobUsed);
            ddlJob.DataTextField = "JobName";
            ddlJob.DataValueField = "JobID";

            ddlJob.DataBind();
            ddlJob.Items.Insert(0, "-- Select FormType --");

            LoadText();

        }

        protected void Runbutton_Click(object sender, EventArgs e)
        {
            LoadText();
            ReportViewerVoids.LocalReport.Refresh();
        }

        protected void Backbutton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Reports/ReportsMain.aspx");
        }

        protected void Clearbutton_Click(object sender, EventArgs e)
        {
            srtdatetxt.Text = null;
            enddatetxt.Text = null;
            txtprivate.Text = null;
            LoadDDJob();
            MaintainScrollPositionOnPostBack = true;
            ReportViewerVoids.LocalReport.Refresh();
            MaintainScrollPositionOnPostBack = true;

        }

        protected void LoadText()
        {
            txtprivate.Text = "";
            txtprivate.Text = ddlJob.SelectedValue.ToString();

            if (ddlJob.SelectedIndex == 0)
            {
                char app = '%';
                txtprivate.Text = app.ToString();
            }


        }
        protected void LoadDate()
        {
            srtdatetxt.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();
            enddatetxt.Text = String.Format("{0:M/dd/yyyy}", DateTime.Now).ToString();
        }

    }
}