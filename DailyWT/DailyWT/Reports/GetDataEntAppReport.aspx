﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="GetDataEntAppReport.aspx.cs" Inherits="DailyWT.Reports.GetDataEntAppReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphMain" runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <!-- Section 1 -->
      <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <section class="container" id="section1" style="color: #000000; margin-left: -5%; height: 500px;">
                <div class="col-xs-12 col-xs-offset-1">
                    <div class="panel panel-default" style="height: 5000px;">
                        <br />
                        <div class="panel-heading">
                            <span class="glyphicon glyphicon-registration-mark"></span><span class="panel-title h4">&nbsp;&nbsp;&nbsp;Daily Application Report Module

                            </span>
                        </div>
                        <div class="panel-body">
                            <div style="background-color: #f5f5f0;" runat="server" id="bodby">
                                 <!---------  START DATE ---------->
                                    <div class="container" style="margin-left: 2%">
                            <div class="row">
                                <div class="form-group">
                                    <label class="v-align-tc">
                                        &nbsp;&nbsp;
                                        <asp:Label ID="Label1" runat="server" Text="Forms Recieved Date: "></asp:Label>
                                        <asp:TextBox ID="srtdatetxt" runat="server" Width="150px" ></asp:TextBox>
                                        <asp:MaskedEditExtender ID="TFDMEE" runat="server" TargetControlID="srtdatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                                        <asp:CalendarExtender ID="TFDCE" runat="server" TargetControlID="srtdatetxt" Format="MM/dd/yyyy" PopupButtonID="TFDCI" />
                                        <asp:Image ID="TFDCI" runat="server" ImageUrl="~/images/Calendar.png" />
                                        <asp:RegularExpressionValidator ID="TFDRegExpV" runat="server" ControlToValidate="srtdatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                                        &nbsp;&nbsp;
                                   </label>

                                    <!---------  END DATE ---------->

                                    <label class="v-align-tc">
                                        &nbsp;&nbsp;
                                        <asp:Label ID="Label2" runat="server" Text="Forms Completed Date: "></asp:Label>
                                        <asp:TextBox ID="enddatetxt" runat="server" Width="150px"></asp:TextBox>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" TargetControlID="enddatetxt" Mask="99/99/9999" MaskType="Date" ErrorTooltipEnabled="true" UserDateFormat="MonthDayYear" AutoComplete="false" />
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="enddatetxt" Format="MM/dd/yyyy" PopupButtonID="Imageend" />
                                        <asp:Image ID="Imageend" runat="server" ImageUrl="~/images/Calendar.png" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="enddatetxt" ErrorMessage="*Invalid Date!" ValidationExpression="^[0-9]{2}/[0-9]{2}/[0-9]{4}|^$|^__/__/____" Style="color: #CC0000" Display="Dynamic" SetFocusOnError="true" />
                                    </label>
                                </div>
                            </div>
                        </div>
                        <br />
                                <!---------  BUTTONS ---------->
                                <br />
                                <div class="container" style="margin-left: 2%">

                    <button id="Backbutton" runat="server" class="btn-primary" onserverclick="Backbutton_Click"><span class="glyphicon glyphicon-chevron-left"></span>Back to Reports Menu</button>
   </div>
                                <br />
                            </div>
                                  
                    
                 &nbsp;&nbsp; &nbsp;&nbsp;    <div class="row" style="margin-left: 2%">
                                <div style="margin-bottom: 10px; margin-top: 10px;" class="col-xs-10  form-group">
                                    <br />
                                  <asp:Label ID="lblPrintInstr" runat="server" Text="*Please Note: To Print the report, click on the Export drop down menu (the blue disk icon, third button from right ) --> Select Excel --> Open the file or save it to your computer --> From MS Excel go to File --> Print --> Select Landscape Orientation --> And change the No Scaling option (the last option) to Fit All Columns on One Page  --> Click on the Print button." ForeColor="Black" BackColor="#FFFF66" Font-Bold="True" Font-Names="Times New Roman" />
                                </div>
                            </div>
                   </div>
                    <!---------  REPORT ---------->
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server"></rsweb:ReportViewer>


             </div>

                    </div>
                </div>
            </section>

            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
